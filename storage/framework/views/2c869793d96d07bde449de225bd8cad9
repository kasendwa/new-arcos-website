<?php $__env->startSection( 'content' ); ?>

	<h1>
		<?php echo e(utf8_encode($page_title)); ?>

	</h1>

	<?php echo Form::open( [ 'method' => 'post', 'files' => true ] ); ?>


	<div class="row">

		<div class="col-lg-9 col-md-8">

			<ul class="nav nav-tabs" role="tablist">

				<?php foreach( $languages as $lang ): ?>

					<li class="nav-item">
						<a class="nav-link <?php echo e(utf8_encode(( $lang->code == $language ? 'active' : '' ))); ?>" data-toggle="tab" href="#<?php echo e(utf8_encode($lang->code)); ?>_panel" role="tab">
							<?php echo e(utf8_encode($lang->name)); ?>

						</a>
					</li>

				<?php endforeach; ?>

			</ul>

			<div class="tab-content">

				<?php foreach( $languages as $lang ): ?>

					<div class="tab-pane <?php echo e(utf8_encode(( $lang->code == $language ? 'active' : '' ))); ?>" id="<?php echo e(utf8_encode($lang->code)); ?>_panel" role="tabpanel">

						<div class="form-group">
							<?php echo Form::label( 'title_' . $lang->code, $translations->form_translations->title->{ $lang->code } ); ?>

							<?php echo Form::text( 'title_' . $lang->code, $item_data->title->{ $lang->code }, [ 'class' => 'form-control', 'placeholder' => 'Title', 'required' => '' ] ); ?>

						</div>

						<div class="form-group">
							<?php echo Form::label( 'content_' . $lang->code, $translations->form_translations->content->{ $lang->code } ); ?>

							<?php echo Form::textarea( 'content_' . $lang->code, $item_data->content->{ $lang->code }, [ 'class' => 'form-control wysiwyg-large', 'placeholder' => 'Content' ] ); ?>

						</div>

						<div class="form-group">
							<?php echo Form::label( 'summary_' . $lang->code, $translations->form_translations->summary->{ $lang->code } ); ?>

							<?php echo Form::textarea( 'summary_' . $lang->code, $item_data->summary->{ $lang->code }, [ 'class' => 'form-control wysiwyg-minimal', 'placeholder' => 'Summary' ] ); ?>

						</div>

						<h4><?php echo e(utf8_encode($translations->form_translations->menu->{ $lang->code })); ?></h4>

						<div class="form-group">
							<?php echo Form::label( 'menu_title_' . $lang->code, $translations->form_translations->menu_title->{ $lang->code } ); ?>

							<?php echo Form::text( 'menu_title_' . $lang->code, $item_data->menu_title->{ $lang->code }, [ 'class' => 'form-control', 'placeholder' => 'Menu title' ] ); ?>

						</div>

					</div>

				<?php endforeach; ?>

			</div>

			<div class="row">

				<div class="col-lg-4 col-md-4">
					<div class="form-group">
						<?php echo Form::label( 'show_in_menu', $translations->show_in_menu ); ?>

						<br />
						<?php echo Form::checkbox( 'show_in_menu', 1, $item_data->show_in_menu ); ?> <?php echo e(utf8_encode($translations->show_this_item)); ?>

					</div>
				</div>

				<div class="col-lg-4 col-md-4">
					<div class="form-group">
						<?php echo Form::label( 'menu_order', $translations->menu_order ); ?>

						<?php echo Form::text( 'menu_order', $item_data->menu_order, [ 'class' => 'form-control' ] ); ?>

					</div>
				</div>

			</div>

			<div>
				<div class="form-group">
					<?php echo Form::label( 'name', $translations->slug ); ?>

					<?php echo Form::text( 'name', $item_data->name, [ 'class' => 'form-control', 'placeholder' => 'Menu title' ] ); ?>

				</div>
			</div>

			<div class="form-group">
				<?php echo Form::hidden( 'page_id', $item_data->id ); ?>

				<?php echo Form::submit( $translations->update_page, [ 'class' => 'btn btn-primary btn-sm' ] ); ?>

			</div>

		</div>

		<div class="col-lg-3 col-md-4">

			<h4>Featured Image</h4>

			<?php if( isset( $item_data->thumbnail->thumbnail ) ): ?>
				<img src="<?php echo e(utf8_encode($item_data->thumbnail->featured_articles_image)); ?>" class="img-fluid" />
			<?php endif; ?>

			<div class="form-group">
				<?php echo Form::label( 'featured_image', 'Choose an image' ); ?>

				<?php echo Form::file( 'featured_image' ); ?>

			</div>

			<div class="form-group">

				<?php echo Form::label( 'parent', 'Page parent' ); ?>


				<select name="parent" class="form-control">

					<?php foreach( $parents as $id => $name ): ?>
						<option value="<?php echo e(utf8_encode($id)); ?>" <?php if( $id == $item_data->parent ): ?> selected <?php endif; ?>><?php echo e(utf8_encode($name)); ?></option>
					<?php endforeach; ?>

				</select>

			</div>

			<div class="form-group">

				<?php echo Form::label( 'template', 'Page template' ); ?>


				<select name="template" class="form-control">

					<?php foreach( $templates as $key => $value ): ?>
						<option value="<?php echo e(utf8_encode($key)); ?>" <?php if( $key == $item_data->template ): ?> selected <?php endif; ?> data-has-related-articles="<?php echo e(utf8_encode($value->has_related_articles)); ?>" data-has-related-projects="<?php echo e(utf8_encode($value->has_related_projects)); ?>"><?php echo e(utf8_encode($value->name)); ?></option>
					<?php endforeach; ?>

				</select>

			</div>

			<div id="related_articles_container">

				<div class="form-group">

					<?php echo Form::label( 'related_articles', 'Related articles' ); ?>


					<div class="checklist_container">

						<ul>

							<?php foreach( $articles->list as $id => $title ): ?>

								<li class="form-group">
									<label title="<?php echo e(utf8_encode($title)); ?>">
										<?php echo Form::checkbox( 'related_articles[]', $id, ( in_array( $id, $articles->selected ) ? true : false ) ); ?>

										<?php echo e(utf8_encode($title)); ?>

									</label>
								</li>

							<?php endforeach; ?>

						</ul>

					</div>

				</div>

			</div>

			<div id="related_projects_container">

				<div class="form-group">

					<?php echo Form::label( 'related_projects', 'Related projects' ); ?>


					<div class="checklist_container">

						<ul>

							<?php foreach( $projects->list as $id => $title ): ?>

								<li class="form-group">
									<label title="<?php echo e(utf8_encode($title)); ?>">
										<?php echo Form::checkbox( 'related_projects[]', $id, ( in_array( $id, $projects->selected ) ? true : false ) ); ?>

										<?php echo e(utf8_encode($title)); ?>

									</label>
								</li>

							<?php endforeach; ?>

						</ul>

					</div>

				</div>

			</div>

		</div>

	</div>

	<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make( 'admin.structure' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>