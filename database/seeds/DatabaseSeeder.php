<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\File;

class DatabaseSeeder extends Seeder {

	public static function load_file( $path ) {
		return File::get( 'database/seeds/content/' . $path );
	}

    public function run() {
	    $uploads_path = public_path( 'uploads' );

	    File::exists( $uploads_path ) ? File::deleteDirectory( $uploads_path, true ) : null;

        Model::unguard();

        $this->call( OptionsTableSeeder::class );
        $this->call( LanguagesTableSeeder::class );
	    
	    $this->call( UserRolesTableSeeder::class );
	    $this->call( UsersTableSeeder::class );

	    $this->call( TermsTableSeeder::class );
	    $this->call( PostsTableSeeder::class );

        Model::reguard();
    }

}
