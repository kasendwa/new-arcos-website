<?php

use Illuminate\Database\Seeder;

use App\App;

class UserRolesTableSeeder extends Seeder {

	private function store_meta_data( $user_role_id, $meta_data, $prefix = '' ) {
		foreach( $meta_data as $key => $value ) :

			if ( is_array( $value ) ) :

				App::add_user_role_meta( $user_role_id, !empty( $prefix ) ? $prefix . '_' . $key : $key, json_encode( array_keys( $value ) ) );

				$this->store_meta_data( $user_role_id, $value, ( !empty( $prefix ) ? $prefix . '_' : '' ) . $key );

			else :

				App::add_user_role_meta( $user_role_id, !empty( $prefix ) ? $prefix . '_' . $key : $key, $value );

			endif;

		endforeach;
	}

	public function run() {
		$administrator = App::add_user_role( [
			'name' => 'administrator'
		] );

			$administrator_permissions = App::get_permissions_template();

			$this->store_meta_data( $administrator->id, [
				'label' => [
					'en' => 'Administrator',
					'fr' => 'Administrateur'
				],
				'permissions' => $administrator_permissions
			] );

		$editor = App::add_user_role( [
			'name' => 'editor'
		] );

			$editor_permissions = App::get_permissions_template();

				$editor_permissions[ 'manage_options' ] = false;

			$this->store_meta_data( $editor->id, [
					'label' => [
						'en' => 'Editor',
						'fr' => 'Editeur'
					],
					'permissions' => $editor_permissions
				] );

		$author = App::add_user_role( [
			'name' => 'author'
		] );

			$author_permissions = App::get_permissions_template();

				$author_permissions[ 'manage_options' ] = false;

				$author_permissions[ 'post_types' ][ 'default' ][ 'crud' ][ 'read' ][ 'others' ] = false;
				$author_permissions[ 'post_types' ][ 'default' ][ 'crud' ][ 'update' ][ 'others' ] = false;
				$author_permissions[ 'post_types' ][ 'default' ][ 'crud' ][ 'delete' ][ 'others' ] = false;

				$author_permissions[ 'post_types' ][ 'default' ][ 'status' ][ 'publish' ] = false;
				$author_permissions[ 'post_types' ][ 'default' ][ 'status' ][ 'unpublish' ] = false;

			$this->store_meta_data( $author->id, [
				'label' => [
					'en' => 'Author',
					'fr' => 'Auteur'
				],
				'permissions' => $author_permissions
			] );
	}

}