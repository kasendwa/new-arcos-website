<?php

use Illuminate\Database\Seeder;

use App\App;
use App\Entities\Page;
use App\Entities\Article;
use App\Entities\Project;

class PostsTableSeeder extends Seeder {

	private function seed_articles() {
		$articles = [
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'sustainable-mountain-development' )->id,
					App::get_term( 'great-lakes-freshwater-news' )->id,
				],
				'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'sustainable-mountain-development' )->id,
					App::get_term( 'great-lakes-freshwater-news' )->id,
				],
				'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'sustainable-mountain-development' )->id,
					App::get_term( 'great-lakes-freshwater-news' )->id,
				],
				'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'sustainable-mountain-development' )->id,
					App::get_term( 'great-lakes-freshwater-news' )->id,
				],
				'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'sustainable-mountain-development' )->id,
					App::get_term( 'great-lakes-freshwater-news' )->id,
				],
				'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'sustainable-mountain-development' )->id,
					App::get_term( 'great-lakes-freshwater-news' )->id,
				],
				'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'environment-and-development' )->id,
					App::get_term( 'community-network-in-action' )->id
				],
				'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'environment-and-development' )->id,
					App::get_term( 'community-network-in-action' )->id
				],
				'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'environment-and-development' )->id,
					App::get_term( 'community-network-in-action' )->id
				],
				'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'environment-and-development' )->id,
					App::get_term( 'community-network-in-action' )->id
				],
				'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'environment-and-development' )->id,
					App::get_term( 'community-network-in-action' )->id
				],
				'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'environment-and-development' )->id,
					App::get_term( 'community-network-in-action' )->id
				],
				'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'sustainable-mountain-development' )->id,
					App::get_term( 'great-lakes-freshwater-news' )->id,
				],
				'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'sustainable-mountain-development' )->id,
					App::get_term( 'great-lakes-freshwater-news' )->id,
				],
				'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'sustainable-mountain-development' )->id,
					App::get_term( 'great-lakes-freshwater-news' )->id,
				],
				'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'sustainable-mountain-development' )->id,
					App::get_term( 'great-lakes-freshwater-news' )->id,
				],
				'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'sustainable-mountain-development' )->id,
					App::get_term( 'great-lakes-freshwater-news' )->id,
				],
				'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'sustainable-mountain-development' )->id,
					App::get_term( 'great-lakes-freshwater-news' )->id,
				],
				'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'environment-and-development' )->id,
					App::get_term( 'community-network-in-action' )->id
				],
				'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'environment-and-development' )->id,
					App::get_term( 'community-network-in-action' )->id
				],
				'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'environment-and-development' )->id,
					App::get_term( 'community-network-in-action' )->id
				],
				'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'environment-and-development' )->id,
					App::get_term( 'community-network-in-action' )->id
				],
				'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'environment-and-development' )->id,
					App::get_term( 'community-network-in-action' )->id
				],
				'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'environment-and-development' )->id,
					App::get_term( 'community-network-in-action' )->id
				],
				'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'sustainable-mountain-development' )->id,
					App::get_term( 'great-lakes-freshwater-news' )->id,
				],
				'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'sustainable-mountain-development' )->id,
					App::get_term( 'great-lakes-freshwater-news' )->id,
				],
				'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'sustainable-mountain-development' )->id,
					App::get_term( 'great-lakes-freshwater-news' )->id,
				],
				'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'sustainable-mountain-development' )->id,
					App::get_term( 'great-lakes-freshwater-news' )->id,
				],
				'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'sustainable-mountain-development' )->id,
					App::get_term( 'great-lakes-freshwater-news' )->id,
				],
				'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'sustainable-mountain-development' )->id,
					App::get_term( 'great-lakes-freshwater-news' )->id,
				],
				'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'environment-and-development' )->id,
					App::get_term( 'community-network-in-action' )->id
				],
				'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'environment-and-development' )->id,
					App::get_term( 'community-network-in-action' )->id
				],
				'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'environment-and-development' )->id,
					App::get_term( 'community-network-in-action' )->id
				],
				'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'environment-and-development' )->id,
					App::get_term( 'community-network-in-action' )->id
				],
				'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'environment-and-development' )->id,
					App::get_term( 'community-network-in-action' )->id
				],
				'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
			],
			[
				'title' => [
					'en' => 'Stakeholders meet for 3 days in Kigali for Scenario-guided Review and Regional Policy harmonization workshop',
					'fr' => 'Les parties prenantes se réunissent pendant trois jours à Kigali pour un atelier de révision et d\'harmonisation de la politique régionale'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'categories' => [
					App::get_term( 'environment-and-development' )->id,
					App::get_term( 'community-network-in-action' )->id
				],
				'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
			]
		];

		foreach( $articles as $order => $entry )
			$this->create_article( $entry );
	}

	private function create_article( $entry ) {
		if ( isset( $entry[ 'title' ] ) ) :

			if ( !is_array( $entry[ 'title' ] ) )
				$entry[ 'title' ] = [
					'en' => $entry[ 'title' ],
					'fr' => ''
				];

			if ( isset( $entry[ 'content' ] ) && !is_array( $entry[ 'content' ] ) )
				$entry[ 'content' ] = [
					'en' => $entry[ 'content' ],
					'fr' => ''
				];

			$post_id = Article::add_article([
				'title' => $entry[ 'title' ],
				'author' => App::get_user( 'admin' )->id,
				'status' => 'publish',
				'content' => $entry[ 'content' ]
			]);

			if ( isset( $entry[ 'categories' ] ) && !is_array( $entry[ 'categories' ] ) )
				$entry[ 'categories' ] = [ $entry[ 'categories' ] ];

			if ( count( array_filter( $entry[ 'categories' ] ) ) > 0 )
				App::add_post_meta( $post_id, 'categories', json_encode( $entry[ 'categories' ] ) );

			if ( isset( $entry[ 'thumbnail' ] ) && is_string( $entry[ 'thumbnail' ] ) )
				$entry[ 'thumbnail' ] = App::upload_media_from_path( $entry[ 'thumbnail' ] );

			isset( $entry[ 'thumbnail' ] ) ? App::add_post_meta( $post_id, 'thumbnail', $entry[ 'thumbnail' ] ) : null;

		endif;
	}

	private function seed_projects() {
		$articles = Article::get_all_articles();

		for ( $i = 1; $i < 4; $i++ ) :

			shuffle( $articles );

			${ 'articles_' . $i } = array_slice( $articles, 0, 6 );

		endfor;

		$projects = [
			[
				'title' => [
					'en' => 'Biodiversity surveys in Rugezi wetland',
					'fr' => 'Enquêtes sur la biodiversité dans la zone humide de Rugezi'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'related_articles' => $articles_1,
				'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
			],
			[
				'title' => [
					'en' => 'Biodiversity surveys in Rugezi wetland',
					'fr' => 'Enquêtes sur la biodiversité dans la zone humide de Rugezi'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'related_articles' => $articles_2,
				'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
			],
			[
				'title' => [
					'en' => 'Biodiversity surveys in Rugezi wetland',
					'fr' => 'Enquêtes sur la biodiversité dans la zone humide de Rugezi'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'related_articles' => $articles_3,
				'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
			],
			[
				'title' => [
					'en' => 'Biodiversity surveys in Rugezi wetland',
					'fr' => 'Enquêtes sur la biodiversité dans la zone humide de Rugezi'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'related_articles' => $articles_1,
				'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
			],
			[
				'title' => [
					'en' => 'Biodiversity surveys in Rugezi wetland',
					'fr' => 'Enquêtes sur la biodiversité dans la zone humide de Rugezi'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'related_articles' => $articles_2,
				'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
			],
			[
				'title' => [
					'en' => 'Biodiversity surveys in Rugezi wetland',
					'fr' => 'Enquêtes sur la biodiversité dans la zone humide de Rugezi'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'articles/stakeholders_meet_fr.txt' )
				],
				'related_articles' => $articles_3,
				'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
			],
		];

		foreach( $projects as $order => $entry )
			$this->create_project( $entry );
	}

	private function create_project( $entry ) {
		if ( isset( $entry[ 'title' ] ) ) :

			if ( !is_array( $entry[ 'title' ] ) )
				$entry[ 'title' ] = [
					'en' => $entry[ 'title' ],
					'fr' => ''
				];

			if ( isset( $entry[ 'content' ] ) && !is_array( $entry[ 'content' ] ) )
				$entry[ 'content' ] = [
					'en' => $entry[ 'content' ],
					'fr' => ''
				];

			$post_id = Project::add_project([
				'title' => $entry[ 'title' ],
				'author' => App::get_user( 'admin' )->id,
				'status' => 'publish',
				'content' => $entry[ 'content' ]
			]);

			if ( isset( $entry[ 'related_articles' ] ) && !is_array( $entry[ 'related_articles' ] ) )
				$entry[ 'related_articles' ] = [ $entry[ 'related_articles' ] ];

			if ( count( array_filter( $entry[ 'related_articles' ] ) ) > 0 )
				App::add_post_meta( $post_id, 'related_articles', json_encode( $entry[ 'related_articles' ] ) );

			if ( isset( $entry[ 'thumbnail' ] ) && is_string( $entry[ 'thumbnail' ] ) )
				$entry[ 'thumbnail' ] = App::upload_media_from_path( $entry[ 'thumbnail' ] );

			isset( $entry[ 'thumbnail' ] ) ? App::add_post_meta( $post_id, 'thumbnail', $entry[ 'thumbnail' ] ) : null;

		endif;
	}

	private function seed_pages() {
		$pages = [
			[
				'title' => [
					'en' => 'Who we are',
					'fr' => 'Sur'
				],
				'show_in_menu' => true,
				'content' => [
					'en' => '',
					'fr' => ''
				],
				'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
				'children' => [
					[
						'title' => [
							'en' => 'About us',
							'fr' => 'À propos de nous'
						],
						'show_in_menu' => true,
						'summary' => [
							'en' => '',
							'fr' => ''
						],
						'content' => [
							'en' => DatabaseSeeder::load_file( 'pages/about_us_en.txt' ),
							'fr' => DatabaseSeeder::load_file( 'pages/about_us_fr.txt' )
						],
						'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
						'template' => 'template-7'
					],
					[
						'title' => [
							'en' => 'Meet our team',
							'fr' => 'Rencontrez notre équipe'
						],
						'show_in_menu' => true,
						'summary' => [
							'en' => '',
							'fr' => ''
						],
						'content' => [
							'en' => '',
							'fr' => ''
						],
						'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
						'template' => 'template-6'
					],
					[
						'title' => [
							'en' => 'Our partners',
							'fr' => 'Nos partenaires'
						],
						'show_in_menu' => true,
						'summary' => [
							'en' => '',
							'fr' => ''
						],
						'content' => [
							'en' => '',
							'fr' => ''
						],
						'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
						'template' => 'template-3'
					],
					[
						'title' => [
							'en' => 'Our donors',
							'fr' => 'Nos donateurs'
						],
						'show_in_menu' => true,
						'summary' => [
							'en' => '',
							'fr' => ''
						],
						'content' => [
							'en' => '',
							'fr' => ''
						],
						'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
						'template' => 'template-3'
					],
				]
			],
			[
				'title' => [
					'en' => 'What we do',
					'fr' => 'Notre travail'
				],
				'show_in_menu' => true,
				'content' => [
					'en' => '',
					'fr' => ''
				],
				'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
				'children' => [
					[
						'title' => [
							'en' => 'Biodiversity and ecosystem services',
							'fr' => 'Biodiversité et services écosystémiques'
						],
						'show_in_menu' => true,
						'content' => [
							'en' => DatabaseSeeder::load_file( 'pages/biodiversity_en.txt' ),
							'fr' => DatabaseSeeder::load_file( 'pages/biodiversity_fr.txt' )
						],
						'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
						'template' => 'template-4'
					],
					[
						'title' => [
							'en' => 'Climate change',
							'fr' => 'Changement climatique'
						],
						'show_in_menu' => true,
						'content' => [
							'en' => DatabaseSeeder::load_file( 'pages/climate_en.txt' ),
							'fr' => DatabaseSeeder::load_file( 'pages/climate_fr.txt' )
						],
						'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
						'template' => 'template-4'
					],
					[
						'title' => [
							'en' => 'Water',
							'fr' => 'Eau'
						],
						'show_in_menu' => true,
						'content' => [
							'en' => DatabaseSeeder::load_file( 'pages/biodiversity_en.txt' ),
							'fr' => DatabaseSeeder::load_file( 'pages/biodiversity_fr.txt' )
						],
						'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
						'template' =>'template-4'
					],
					[
						'title' => [
							'en' => 'Energy',
							'fr' => 'Énergie'
						],
						'show_in_menu' => true,
						'content' => [
							'en' => DatabaseSeeder::load_file( 'pages/energy_en.txt' ),
							'fr' => DatabaseSeeder::load_file( 'pages/energy_fr.txt' )
						],
						'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
						'template' =>'template-4'
					],
					[
						'title' => [
							'en' => 'Food security',
							'fr' => 'La sécurité alimentaire'
						],
						'show_in_menu' => true,
						'content' => [
							'en' => DatabaseSeeder::load_file( 'pages/food_security_en.txt' ),
							'fr' => DatabaseSeeder::load_file( 'pages/food_security_fr.txt' )
						],
						'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
						'template' =>'template-4'
					],
				],
			],
			[
				'title' => [
					'en' => 'How we do it',
					'fr' => 'Comment nous le faisons'
				],
				'show_in_menu' => true,
				'content' => [
					'en' => '',
					'fr' => ''
				],
				'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
				'children' => [
					[
						'title' => [
							'en' => 'We empower',
							'fr' => 'Nous donnons pouvoir'
						],
						'show_in_menu' => true,
						'content' => [
							'en' => DatabaseSeeder::load_file( 'pages/empower_en.txt' ),
							'fr' => DatabaseSeeder::load_file( 'pages/empower_fr.txt' )
						],
						'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
						'template' => 'template-1'
					],
					[
						'title' => [
							'en' => 'We network',
							'fr' => 'Nous réseau'
						],
						'show_in_menu' => true,
						'content' => [
							'en' => DatabaseSeeder::load_file( 'pages/network_en.txt' ),
							'fr' => DatabaseSeeder::load_file( 'pages/network_fr.txt' )
						],
						'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
						'template' => 'template-1'
					],
					[
						'title' => [
							'en' => 'We act',
							'fr' => 'Nous agissons'
						],
						'show_in_menu' => true,
						'content' => [
							'en' => DatabaseSeeder::load_file( 'pages/act_en.txt' ),
							'fr' => DatabaseSeeder::load_file( 'pages/act_fr.txt' )
						],
						'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
						'template' => 'template-1'
					],
				],
			],
			[
				'title' => [
					'en' => 'Where we work',
					'fr' => 'Où nous travaillons'
				],
				'show_in_menu' => true,
				'content' => [
					'en' => '',
					'fr' => ''
				],
				'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
				'children' => [
					[
						'title' => [
							'en' => 'Albertine Rift region',
							'fr' => 'Albertine Rift région'
						],
						'show_in_menu' => true,
						'content' => [
							'en' => DatabaseSeeder::load_file( 'pages/albertine_region_en.txt' ),
							'fr' => DatabaseSeeder::load_file( 'pages/albertine_region_fr.txt' )
						],
						'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
						'template' => 'template-1'
					],
					[
						'title' => [
							'en' => 'African Great Lakes',
							'fr' => 'Grands Lacs d\'Afrique'
						],
						'show_in_menu' => true,
						'content' => [
							'en' => DatabaseSeeder::load_file( 'pages/african_great_lakes_en.txt' ),
							'fr' => DatabaseSeeder::load_file( 'pages/african_great_lakes_fr.txt' )
						],
						'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
						'template' => 'template-1'
					],
					[
						'title' => [
							'en' => 'African Mountains',
							'fr' => 'Montagnes d\'Afrique'
						],
						'show_in_menu' => true,
						'content' => [
							'en' => DatabaseSeeder::load_file( 'pages/african_mountains_en.txt' ),
							'fr' => DatabaseSeeder::load_file( 'pages/african_mountains_fr.txt' )
						],
						'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
						'template' => 'template-1'
					]
				]
			],
			[
				'title' => [
					'en' => 'Our services',
					'fr' => 'Nos services'
				],
				'menu_title' => [
					'en' => 'Services',
					'fr' => 'Services'
				],
				'show_in_menu' => true,
				'content' => [
					'en' => '',
					'fr' => ''
				],
				'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
				'children' => [
					[
						'title' => [
							'en' => 'Event management',
							'fr' => 'Gestion des événements'
						],
						'show_in_menu' => true,
						'content' => [
							'en' => DatabaseSeeder::load_file( 'pages/empower_en.txt' ),
							'fr' => DatabaseSeeder::load_file( 'pages/empower_fr.txt' )
						],
						'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
						'template' => 'template-5'
					],
					[
						'title' => [
							'en' => 'Reporting service',
							'fr' => 'Service de reporting'
						],
						'show_in_menu' => true,
						'content' => [
							'en' => DatabaseSeeder::load_file( 'pages/empower_en.txt' ),
							'fr' => DatabaseSeeder::load_file( 'pages/empower_fr.txt' )
						],
						'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
						'template' => 'template-5'
					],
					[
						'title' => [
							'en' => 'Training & information centre',
							'fr' => 'Centre de formation et d\'information'
						],
						'show_in_menu' => true,
						'content' => [
							'en' => DatabaseSeeder::load_file( 'pages/empower_en.txt' ),
							'fr' => DatabaseSeeder::load_file( 'pages/empower_fr.txt' )
						],
						'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
						'template' => 'template-5'
					],
					[
						'title' => [
							'en' => 'Other services',
							'fr' => 'Autres services'
						],
						'show_in_menu' => true,
						'content' => [
							'en' => DatabaseSeeder::load_file( 'pages/empower_en.txt' ),
							'fr' => DatabaseSeeder::load_file( 'pages/empower_fr.txt' )
						],
						'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
						'template' => 'template-5'
					]
				]
			],
			[
				'title' => [
					'en' => 'Resources and publications',
					'fr' => 'Ressources et publications'
				],
				'menu_title' => [
					'en' => 'Resources',
					'fr' => 'Ressources'
				],
				'show_in_menu' => true,
				'content' => [
					'en' => '',
					'fr' => ''
				],
				'thumbnail' => public_path( 'img/seeds/general/antelopes.jpg' ),
				'children' => [
					[
						'title' => [
							'en' => 'Image gallery',
							'fr' => 'Galerie d\'images'
						],
						'show_in_menu' => true,
						'content' => [
							'en' => '',
							'fr' => ''
						],
						'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
						'template' => 'template-8'
					],
					[
						'title' => [
							'en' => 'Video gallery',
							'fr' => 'Galerie vidéo'
						],
						'show_in_menu' => true,
						'content' => [
							'en' => '',
							'fr' => ''
						],
						'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
						'template' => 'template-9'
					],
					[
						'title' => [
							'en' => 'Resources',
							'fr' => 'Ressources'
						],
						'show_in_menu' => true,
						'content' => [
							'en' => '',
							'fr' => ''
						],
						'thumbnail' => public_path( 'img/seeds/general/lion.jpg' ),
						'template' => 'template-10'
					],
					[
						'title' => [
							'en' => 'Our websites',
							'fr' => 'Nos sites web'
						],
						'show_in_menu' => true,
						'summary' => [
							'en' => '',
							'fr' => ''
						],
						'content' => [
							'en' => '',
							'fr' => ''
						],
						'thumbnail' => public_path( 'img/seeds/general/elephants.jpg' ),
						'template' => 'template-2'
					]
				]
			]
		];

		foreach( $pages as $order => $entry )
			$this->create_page( $entry, ( $order + 1 ) );
	}

	private function create_page( $entry, $menu_order = 0, $parent_id = 0 ) {
		if ( isset( $entry[ 'title' ] ) ) :

			if ( !is_array( $entry[ 'title' ] ) )
				$entry[ 'title' ] = [
					'en' => $entry[ 'title' ],
					'fr' => ''
				];

			if ( !isset( $entry[ 'menu_title' ] ) )
				$entry[ 'menu_title' ] = $entry[ 'title' ];

			elseif ( !is_array( $entry[ 'menu_title' ] ) )
				$entry[ 'menu_title' ] = [
					'en' => $entry[ 'menu_title' ],
					'fr' => ''
				];

			if ( isset( $entry[ 'content' ] ) && !is_array( $entry[ 'content' ] ) )
				$entry[ 'content' ] = [
					'en' => $entry[ 'content' ],
					'fr' => ''
				];

			$post_id = Page::add_page( [
				'title' => $entry[ 'title' ],
				'menu_title' => $entry[ 'menu_title' ],
				'author' => App::get_user( 'admin' )->id,
				'status' => 'publish',
				'show_in_menu' => isset( $entry[ 'show_in_menu' ] ) ? ( boolean ) $entry[ 'show_in_menu' ] : false,
				'menu_order' => $menu_order,
				'content' => $entry[ 'content' ]
			] );

			if ( isset( $entry[ 'thumbnail' ] ) && is_string( $entry[ 'thumbnail' ] ) )
				$entry[ 'thumbnail' ] = App::upload_media_from_path( $entry[ 'thumbnail' ] );

			isset( $entry[ 'thumbnail' ] ) ? App::add_post_meta( $post_id, 'thumbnail', $entry[ 'thumbnail' ] ) : null;
			isset( $entry[ 'template' ] ) ? App::add_post_meta( $post_id, 'template', $entry[ 'template' ] ) : null;

			App::add_post_meta( $post_id, 'parent_id', $parent_id );

			if ( isset( $entry[ 'children' ] ) && is_array( $entry[ 'children' ] ) )
				foreach( $entry[ 'children' ] as $order => $sub_entry )
					$this->create_page( $sub_entry, ( $order + 1 ), $post_id );

		endif;
	}

	public function run() {
		$this->seed_articles();
		$this->seed_projects();
		$this->seed_pages();
	}

}
