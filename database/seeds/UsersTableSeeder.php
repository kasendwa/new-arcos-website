<?php

use Illuminate\Database\Seeder;

use App\App;

class UsersTableSeeder extends Seeder {

	private function store_meta_data( $user_id, $meta_data, $prefix = '' ) {
		foreach( $meta_data as $key => $value ) :

			if ( is_array( $value ) ) :

				App::add_user_meta( $user_id, !empty( $prefix ) ? $prefix . '_' . $key : $key, json_encode( array_keys( $value ) ) );

				$this->store_meta_data( $user_id, $value, $prefix . '_' . $key );

			else :

				App::add_user_meta( $user_id, !empty( $prefix ) ? $prefix . '_' . $key : $key, $value );

			endif;

		endforeach;
	}

	public function run() {
		$admin = App::add_user( [
			'username' => 'admin',
			'email' => 'admin@arcosnetwork.com',
			'password' => bcrypt( 'admin' ),
			'user_role' => App::get_user_role_by( 'name', 'administrator' )->id,
			'confirmation_code' => md5( microtime() . env( 'APP_KEY' ) ),
			'confirmed' => 1,
		] );

			$this->store_meta_data( $admin->id, [
				'first_name' => 'Web',
				'middle_name' => '',
				'last_name' => 'Administrator',
				'language' => App::get_language_by( 'code', 'en' )->id
			] );

		$editor = App::add_user( [
			'username' => 'editor',
			'email' => 'editor@arcosnetwork.com',
			'password' => bcrypt( 'editor' ),
			'user_role' => App::get_user_role_by( 'name', 'editor' )->id,
			'confirmation_code' => md5( microtime() . env( 'APP_KEY' ) ),
			'confirmed' => 1,
		] );

			$this->store_meta_data( $editor->id, [
				'first_name' => 'Web',
				'middle_name' => '',
				'last_name' => 'Editor',
				'language' => App::get_language_by( 'code', 'fr' )->id
			] );

		$author = App::add_user( [
			'username' => 'author',
			'email' => 'author@arcosnetwork.com',
			'password' => bcrypt( 'author' ),
			'user_role' => App::get_user_role_by( 'name', 'author' )->id,
			'confirmation_code' => md5( microtime() . env( 'APP_KEY' ) ),
			'confirmed' => 1,
		] );

			$this->store_meta_data( $author->id, [
				'first_name' => 'Web',
				'middle_name' => '',
				'last_name' => 'Author',
				'language' => App::get_language_by( 'code', 'en' )->id
			] );
	}

}
