<?php

use Illuminate\Database\Seeder;

use App\App;

class LanguagesTableSeeder extends Seeder {

    public function run() {
	    App::add_language( [
		    'name' => 'English',
		    'code' => 'en',
	    ] );

	    App::add_language( [
		    'name' => 'Français',
		    'code' => 'fr',
	    ] );
    }

}
