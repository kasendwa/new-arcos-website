<p>Climate Change is one of the principal threats to the afromontane region in general and the Albertine Rift in particular. It is affecting social, cultural and economic life as well as water resources, agriculture, forestry, fisheries, human settlements, ecological systems and even human health. Changes in the snowfall pattern have been observed in mountain and highland systems, notably the Ruwenzori, climate variability with unusual heavy rains causing serious floods and erosions and also longer drought destabilizing agricultural productivity across the Albertine Rift region.</p>

<p>Countries in the Albertine Rift region are approaching the problem differently: national adaptation plans have been elaborated, low carbon strategies are already in place, the EDPRS II, being developed in Rwanda, is including a sub sector of environment and climate change and the climate change policy is to be launched soon in Uganda. Moreover, a number of adaptation and mitigation projects have been or are already being implemented.</p>

<p>However some constraints have been and are still being highlighted by climate change reports (e.g. Climate Change Vulnerability and Adaptation Preparedness in Uganda, 2010). These include the limited awareness about the causes of climate change as well as their impacts, lack of guidelines for mainstreaming climate change into development planning and adequate policies, insufficient conceptualization of the importance of weather and climate information, and the weak coordination mechanisms for synergistic actions.</p>

<p>The cross-cutting impacts of climate change and the imperative for an integrated response call for all institutions and actors to be engaged in the region’s response. Therefore, ARCOS has strategically come in with an aim to facilitate regional mechanisms for assessing and addressing the impacts of climate change in the Albertine Rift for adaption to climate change impacts in the region.</p>

<p>To achieve this, ARCOS embarks in different activities, including:</p>

<ul>
	<li>To promote clear understanding of the impact of climate change on biodiversity, ecosystems (forests, water towers), and people (livelihoods, tourism, disasters, food security/agriculture) in the Albertine Rift;</li>
	<li>To reduce the climate change impacts and vulnerabilities in at least three sites in the key landscapes through sustainable actions such as renewable energy and environmental resilience activities;</li>
	<li>To effectively communicate and build awareness and understanding among various stakeholders (schools, academic institutions, journalists/media, donors, government, communities, public) on the impact of climate change in the Albertine Rift as one of the most vulnerable region to climate change impacts;</li>
	<li>To Support the formulation of National Adaptation Plans in the Albertine Rift;</li>
	<li>To strengthen multi-stakeholder forums to advocate for climate policy recommendations and to share information;</li>
	<li>To engage partnerships with leading organizations in climate change and Reducing Emissions from Deforestation and Degradation (REDD+).</li>
</ul>

<p>In 2012, ARCOS, in partnership with the Purple Field Productions, has produced a film on climate change adaptation practices in the Albertine Rift. The 45-minutes documentary describes best practices by communities that enable them to adapt to the adverse effects of climate change. You may watch the film on our YouTube channel.</p>