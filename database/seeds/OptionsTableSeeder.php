<?php

use Illuminate\Database\Seeder;

use App\App;

class OptionsTableSeeder extends Seeder {

	public function run() {
		App::store_options( [
			'name' => [
				'en' => 'Abertine Rift Conservation Society',
				'fr' => 'Societe de Conservation du Rift Albertin'
			],
			'meta' => [
				'keywords' => [
					'en' => '',
					'fr' => ''
				],
				'description' => [
					'en' => 'We champion collaborative conservation and sustainable management of natural resources in the Albertine Rift region through the promotion of collaborative conservation action for nature and people.',
					'fr' => 'Nous défendons la conservation de collaboration et de gestion durable des ressources naturelles dans la région du Rift Albertin à travers la promotion d\'une action concertée de la conservation de la nature et des gens.'
				]
			],
			'post_types' => [
				'page' => [
					'label' => [
						'singular' => [
							'en' => 'Page',
							'fr' => 'Page'
						],
						'plural' => [
							'en' => 'Pages',
							'fr' => 'Pages'
						]
					],
					'has_archive' => false,
					'rewrite' => [
						'site' => [
							'base' => '',
							'archive' => false
						],
						'admin' => [
							'base' => 'page',
							'archive' => 'pages'
						]
					],
					'controller' => [
						'site' => 'Site\PostTypes\PageController',
						'admin' => 'Admin\PostTypes\PageController'
					]
				],
				'article' => [
					'label' => [
						'singular' => [
							'en' => 'Article',
							'fr' => 'Article'
						],
						'plural' => [
							'en' => 'Articles',
							'fr' => 'Des articles'
						]
					],
					'has_archive' => true,
					'rewrite' => [
						'site' => [
							'base' => 'article',
							'archive' => 'articles'
						],
						'admin' => [
							'base' => 'article',
							'archive' => 'articles'
						],
					],
					'controller' => [
						'site' => 'Site\PostTypes\ArticleController',
						'admin' => 'Admin\PostTypes\ArticleController',
					]
				],
				'project' => [
					'label' => [
						'singular' => [
							'en' => 'Project',
							'fr' => 'Projet'
						],
						'plural' => [
							'en' => 'Projects',
							'fr' => 'Des projets'
						]
					],
					'has_archive' => true,
					'rewrite' => [
						'site' => [
							'base' => 'project',
							'archive' => 'projects'
						],
						'admin' => [
							'base' => 'project',
							'archive' => 'projects'
						],
					],
					'controller' => [
						'site' => 'Site\PostTypes\ProjectController',
						'admin' => 'Admin\PostTypes\ProjectController',
					]
				]
			],
			'taxonomies' => [
				'slider' => [
					'label' => [
						'singular' => [
							'en' => 'Slider',
							'fr' => 'Curseur'
						],
						'plural' => [
							'en' => 'Sliders',
							'fr' => 'Sliders'
						]
					],
					'has_archive' => false,
					'rewrite' => [
						'site' => [
							'base' => false,
							'archive' => false
						],
						'admin' => [
							'base' => 'slider',
							'archive' => 'sliders'
						]
					],
					'controller' => [
						'admin' => 'Admin\Taxonomies\SliderController'
					]
				],
				'about-section' => [
					'label' => [
						'singular' => [
							'en' => 'About sections',
							'fr' => 'À propos de la section'
						],
						'plural' => [
							'en' => 'About sections',
							'fr' => 'À propos des sections'
						]
					],
					'has_archive' => false,
					'rewrite' => [
						'admin' => [
							'base' => 'about-section',
							'archive' => 'about-sections'
						]
					],
					'controller' => [
						'admin' => 'Admin\Taxonomies\AboutSectionController'
					]
				],
				'translation' => [
					'label' => [
						'singular' => [
							'en' => 'Translation',
							'fr' => 'Traduction'
						],
						'plural' => [
							'en' => 'Translations',
							'fr' => 'Traductions'
						]
					],
					'has_archive' => false,
					'rewrite' => [
						'admin' => [
							'base' => 'translation',
							'archive' => 'translations'
						]
					],
					'controller' => [
						'admin' => 'Admin\Taxonomies\TranslationController'
					]
				],
				'category' => [
					'label' => [
						'singular' => [
							'en' => 'Category',
							'fr' => 'Catégorie'
						],
						'plural' => [
							'en' => 'Categories',
							'fr' => 'Catégories'
						]
					],
					'has_archive' => true,
					'rewrite' => [
						'site' => [
							'base' => 'category',
							'archive' => 'categories'
						],
						'admin' => [
							'base' => 'category',
							'archive' => 'categories'
						]
					],
					'controller' => [
						'site' => 'Site\Taxonomies\CategoryController',
						'admin' => 'Admin\Taxonomies\CategoryController'
					]
				],
				'newsletter' => [
					'label' => [
						'singular' => [
							'en' => 'Newsletter',
							'fr' => 'Bulletin'
						],
						'plural' => [
							'en' => 'Newsletters',
							'fr' => 'Lettres d\'information'
						]
					],
					'has_archive' => false,
					'rewrite' => [
					    'site' => [
					        'base' => 'newsletter'
                        ],
						'admin' => [
							'base' => 'newsletter',
							'archive' => 'newsletters'
						]
					],
					'controller' => [
						'admin' => 'Admin\Taxonomies\NewsletterController'
					]
				],
				'edition' => [
					'label' => [
						'singular' => [
							'en' => 'Edition',
							'fr' => 'Édition'
						],
						'plural' => [
							'en' => 'Editions',
							'fr' => 'Éditions'
						]
					],
					'has_archive' => true,
					'rewrite' => [
						'site' => [
							'base' => 'edition',
							'archive' => 'editions'
						],
						'admin' => [
							'base' => 'edition',
							'archive' => 'editions'
						]
					],
					'controller' => [
						'site' => 'Site\Taxonomies\EditionController',
						'admin' => 'Admin\Taxonomies\EditionController'
					]
				],
				'team-category' => [
					'label' => [
						'singular' => [
							'en' => 'Team Category',
							'fr' => 'Catégorie d\'équipe'
						],
						'plural' => [
							'en' => 'Team categories',
							'fr' => 'Catégories d\'équipe'
						]
					],
					'has_archive' => false,
					'rewrite' => [
						'admin' => [
							'base' => 'team-category',
							'archive' => 'team-categories'
						]
					],
					'controller' => [
						'site' => 'Site\Taxonomies\TeamCategoryController',
						'admin' => 'Admin\Taxonomies\TeamCategoryController'
					]
				],
				'team-member' => [
					'label' => [
						'singular' => [
							'en' => 'Team member',
							'fr' => 'Membre de l\'équipe'
						],
						'plural' => [
							'en' => 'Team members',
							'fr' => 'Membres de l\'équipe'
						]
					],
					'has_archive' => true,
					'rewrite' => [
						'site' => [
							'base' => 'team-member',
							'archive' => 'team-members'
						],
						'admin' => [
							'base' => 'team-member',
							'archive' => 'team-members'
						]
					],
					'controller' => [
						'site' => 'Site\Taxonomies\TeamMemberController',
						'admin' => 'Admin\Taxonomies\TeamMemberController'
					]
				],
				'menu' => [
					'label' => [
						'singular' => [
							'en' => 'Menu',
							'fr' => 'Le menu'
						],
						'plural' => [
							'en' => 'Menus',
							'fr' => 'Menus'
						]
					],
					'has_archive' => false,
					'rewrite' => [
						'site' => [
							'base' => false,
							'archive' => false
						],
						'admin' => [
							'base' => 'menu',
							'archive' => 'menus'
						]
					],
					'controller' => [
						'admin' => 'Admin\Taxonomies\MenuController'
					]
				],
				'menu-item' => [
					'label' => [
						'singular' => [
							'en' => 'Menu item',
							'fr' => 'Élément du menu'
						],
						'plural' => [
							'en' => 'Menu items',
							'fr' => 'Éléments du menu'
						]
					],
					'has_archive' => false,
					'rewrite' => [
						'site' => [
							'base' => false,
							'archive' => false
						],
						'admin' => [
							'base' => 'menu-item',
							'archive' => 'menu-items'
						]
					],
					'controller' => [
						'admin' => 'Admin\Taxonomies\MenuItemController'
					]
				],
				'website' => [
					'label' => [
						'singular' => [
							'en' => 'Website',
							'fr' => 'Site Internet'
						],
						'plural' => [
							'en' => 'Websites',
							'fr' => 'Sites Internet'
						]
					],
					'has_archive' => false,
					'rewrite' => [
						'site' => [
							'base' => false,
							'archive' => false
						],
						'admin' => [
							'base' => 'website',
							'archive' => 'websites'
						]
					],
					'controller' => [
						'admin' => 'Admin\Taxonomies\WebsiteController'
					]
				],
			],
			'defaults' => [
				'language' => 'en',
				'meta' => [
					'author' => 'ARCOS Network',
					'keywords' => 'ARCOS, Albertine Rift, Conservation, Endemic, Biodiversity, Community Development, Payment for Ecosystem Services, African Mountains',
					'description' => [
						'en' => 'The views expressed on this website reflect the opinion of the Albertine Rift Conservation Society (ARCOS)',
						'fr' => 'Les opinions exprimées sur ce site reflètent l\'opinion de la Société Albertine Rift Conservation Society (ARCOS)'
					]
				],
				'page_title' => [
					'en' => 'Welcome',
					'fr' => 'Bienvenue'
				],
				'page_template' => 'template-1'
			],
			'image_sizes' => [
				'thumbnail' => [
					'width' => 150,
					'height' => 150,
					'crop' => true,
					'upsize' => false
				],
				'small' => [
					'width' => 250,
					'height' => 250,
					'crop' => true,
					'upsize' => false
				],
				'menu-image' => [
					'width' => 320,
					'height' => 160,
					'crop' => true,
					'upsize' => false
				],
				'slider-image' => [
					'width' => 1400,
					'height' => 600,
					'crop' => true,
					'upsize' => true
				],
				'page-top-image' => [
					'width' => 1400,
					'height' => 400,
					'crop' => true,
					'upsize' => true
				],
				'featured-articles-image' => [
					'width' => 400,
					'height' => 250,
					'crop' => true,
					'upsize' => false
				],
				'section-image' => [
					'width' => 400,
					'height' => 200,
					'crop' => true,
					'upsize' => false
				],
				'small-screenshot' => [
					'width' => 300,
					'height' => 225,
					'crop' => true,
					'upsize' => true
				],
				'medium-screenshot' => [
					'width' => 600,
					'height' => 450,
					'crop' => true,
					'upsize' => true
				],
				'large-screenshot' => [
					'width' => 880,
					'height' => 660,
					'crop' => true,
					'upsize' => true
				],
			],
			'page_templates' => [
				'template-1' => [
					'name' => [
						'en' => 'Template 1',
						'fr' => 'Modèle 1'
					],
					'properties' => [
						'has_related_articles' => true,
						'has_related_projects' => true
					]
				],
				'template-2' => [
					'name' => [
						'en' => 'Template 2',
						'fr' => 'Modèle 2'
					]
				],
				'template-3' => [
					'name' => [
						'en' => 'Template 3',
						'fr' => 'Modèle 3'
					]
				],
				'template-4' => [
					'name' => [
						'en' => 'Template 4',
						'fr' => 'Modèle 4'
					],
					'properties' => [
						'has_related_projects' => true
					]
				],
				'template-5' => [
					'name' => [
						'en' => 'Template 5',
						'fr' => 'Modèle 5'
					],
					'properties' => [
						'has_related_articles' => true,
					]
				],
				'template-6' => [
					'name' => [
						'en' => 'Template 6',
						'fr' => 'Modèle 6'
					]
				],
				'template-7' => [
					'name' => [
						'en' => 'Template 7',
						'fr' => 'Modèle 7'
					]
				],
				'template-8' => [
					'name' => [
						'en' => 'Template 8',
						'fr' => 'Modèle 8'
					]
				],
				'template-9' => [
					'name' => [
						'en' => 'Template 9',
						'fr' => 'Modèle 9'
					]
				],
				'template-10' => [
					'name' => [
						'en' => 'Template 10',
						'fr' => 'Modèle 10'
					]
				],
			]
		], '' );
	}

}