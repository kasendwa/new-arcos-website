<?php

use Illuminate\Database\Seeder;

use App\App;
use App\Entities\Translation;
use App\Entities\Category;
use App\Entities\Slider;
use App\Entities\AboutSection;
use App\Entities\TeamCategory;
use App\Entities\TeamMember;
use App\Entities\Menu;
use App\Entities\MenuItem;
use App\Entities\Website;
use App\Entities\MailmanList;

class TermsTableSeeder extends Seeder {

	private function seed_translations() {
		$translations = [
			[
				'en' => 'Read more',
				'fr' => 'Lire la suite'
			],
			[
				'en' => 'Search',
				'fr' => 'Chercher'
			],
			[
				'en' => 'Username',
				'fr' => 'Nom d\'utilisateur'
			],
			[
				'en' => 'Password',
				'fr' => 'Mot de passe'
			],
			[
				'en' => 'Log In',
				'fr' => 'S\'identifier'
			],
			[
				'en' => 'Read article',
				'fr' => 'Lire l\'article'
			],
			[
				'en' => 'ARCOS at a glance',
				'fr' => 'ARCOS en bref'
			],
			[
				'en' => 'Contact us',
				'fr' => 'Contactez nous'
			],
			[
				'en' => 'Get involved',
				'fr' => 'Être impliqué'
			],
			[
				'en' => 'Connect with us',
				'fr' => 'Connecte-toi avec nous'
			],
			[
				'en' => 'January',
				'fr' => 'Janvier'
			],
			[
				'en' => 'February',
				'fr' => 'Février'
			],
			[
				'en' => 'March',
				'fr' => 'Mars'
			],
			[
				'en' => 'April',
				'fr' => 'Avril'
			],
			[
				'en' => 'May',
				'fr' => 'Mai'
			],
			[
				'en' => 'June',
				'fr' => 'Juin'
			],
			[
				'en' => 'July',
				'fr' => 'Juillet'
			],
			[
				'en' => 'August',
				'fr' => 'Août'
			],
			[
				'en' => 'September',
				'fr' => 'Septembre'
			],
			[
				'en' => 'October',
				'fr' => 'Octobre'
			],
			[
				'en' => 'November',
				'fr' => 'Novembre'
			],
			[
				'en' => 'December',
				'fr' => 'Décembre'
			],
			[
				'en' => 'Posted by',
				'fr' => 'Posté par'
			],
			[
				'en' => 'on',
				'fr' => 'sur'
			],
			[
				'en' => 'Share on',
				'fr' => 'Partager sur'
			],
			[
				'en' => 'under',
				'fr' => 'en dessous de'
			],
			[
				'en' => 'Categories',
				'fr' => 'Catégories'
			],
			[
				'en' => 'Related articles',
				'fr' => 'Articles liés'
			],
			[
				'en' => 'Menu',
				'fr' => 'Menu'
			],
			[
				'en' => 'Add page',
				'fr' => 'Ajouter une page'
			],
			[
				'en' => 'Edit page',
				'fr' => 'Modifier la page'
			],
			[
				'en' => 'Title',
				'fr' => 'Titre'
			],
			[
				'en' => 'Content',
				'fr' => 'Contenu'
			],
			[
				'en' => 'Choose an image',
				'fr' => 'Choisissez une image'
			],
			[
				'en' => 'Page parent',
				'fr' => 'Parent de page'
			],
			[
				'en' => 'Page template',
				'fr' => 'Modèle de page'
			],
			[
				'en' => 'Summary',
				'fr' => 'Résumé'
			],
			[
				'en' => 'Menu title',
				'fr' => 'Titre du menu'
			],
			[
				'en' => 'Show in menu',
				'fr' => 'Afficher dans le menu'
			],
			[
				'en' => 'Show this item',
				'fr' => 'Afficher cet élément'
			],
			[
				'en' => 'Menu order',
				'fr' => 'Commande de menu'
			],
			[
				'en' => 'Publish page',
				'fr' => 'Publier la page'
			],
			[
				'en' => 'Update page',
				'fr' => 'Mise à jour de la page'
			],
			[
				'en' => 'None',
				'fr' => 'Aucun'
			],
			[
				'en' => 'Thumbnail',
				'fr' => 'La vignette'
			],
			[
				'en' => 'Edit',
				'fr' => 'Modifier'
			],
			[
				'en' => 'Trash',
				'fr' => 'Poubelle'
			],
			[
				'en' => 'View',
				'fr' => 'Vue'
			],
			[
				'en' => 'Slug',
				'fr' => 'Limace'
			],
			[
				'en' => 'Read biography',
				'fr' => 'Lire la biographie'
			],
			[
				'en' => 'Country',
				'fr' => 'Pays'
			],
			[
				'en' => 'Position',
				'fr' => 'Position'
			],
			[
				'en' => 'Biography',
				'fr' => 'La biographie'
			],
			[
				'en' => 'Related projects',
				'fr' => 'Projets liés'
			],
			[
				'en' => 'Visit the website',
				'fr' => 'Visitez le site Web'
			],
			[
				'en' => 'Dashboard',
				'fr' => 'Tableau de bord'
			],
			[
				'en' => 'Search results for ?',
				'fr' => 'Résultats de recherche pour ?'
			],
			[
				'en' => 'Add translation',
				'fr' => 'Ajouter une traduction'
			],
			[
				'en' => 'Edit translation',
				'fr' => 'Modifier la traduction'
			],
			[
				'en' => 'Update translation',
				'fr' => 'Mettre à jour la traduction'
			],
			[
				'en' => 'Are you sure you want to delete this record?',
				'fr' => 'Voulez-vous vraiment supprimer cet enregistrement?'
			],
			[
				'en' => 'Page updated successfully',
				'fr' => 'Page mise à jour avec succès'
			],
			[
				'en' => 'Add category',
				'fr' => 'Ajouter une catégorie'
			],
			[
				'en' => 'Edit category',
				'fr' => 'Modifier la catégorie'
			],
			[
				'en' => 'Update category',
				'fr' => 'Mettre à jour la catégorie'
			],
			[
				'en' => 'Add article',
				'fr' => 'Ajouter un article'
			],
			[
				'en' => 'Edit article',
				'fr' => 'Modifier l\'article'
			],
			[
				'en' => 'Update article',
				'fr' => 'Mise à jour de l\'article'
			],
			[
				'en' => 'Add project',
				'fr' => 'Ajouter un projet'
			],
			[
				'en' => 'Edit project',
				'fr' => 'Modifier le projet'
			],
			[
				'en' => 'Update project',
				'fr' => 'Mettre à jour le projet'
			],
			[
				'en' => 'Add team category',
				'fr' => 'Ajouter une catégorie d\'équipe'
			],
			[
				'en' => 'Update team category',
				'fr' => 'Mettre à jour la catégorie d\'équipe'
			],
			[
				'en' => 'Edit team category',
				'fr' => 'Modifier la catégorie d\'équipe'
			],
			[
				'en' => 'Add slider',
				'fr' => 'Ajouter un curseur'
			],
			[
				'en' => 'Update slider',
				'fr' => 'Curseur de mise à jour'
			],
			[
				'en' => 'Edit slider',
				'fr' => 'Modifier le curseur'
			],
			[
				'en' => 'Description',
				'fr' => 'La description'
			],
			[
				'en' => 'Order',
				'fr' => 'Commande'
			],
			[
				'en' => 'Add team member',
				'fr' => 'Ajouter un membre de l\'équipe'
			],
			[
				'en' => 'Edit team member',
				'fr' => 'Éditer membre de l\'équipe'
			],
			[
				'en' => 'Update team member',
				'fr' => 'Mettre à jour les membres de l\'équipe'
			],
			[
				'en' => 'Taxonomy',
				'fr' => 'Taxonomie'
			],
			[
				'en' => 'Post type',
				'fr' => 'Type de poste'
			],
			[
				'en' => 'Entity type',
				'fr' => 'Type d\'entité'
			],
			[
				'en' => 'Entity name',
				'fr' => 'Nom de l\'entité'
			],
			[
				'en' => 'Entity',
				'fr' => 'Entité'
			],
			[
				'en' => 'Custom link',
				'fr' => 'Lien personnalisé'
			],
			[
				'en' => 'Edit menu',
				'fr' => 'Modifier le menu'
			],
			[
				'en' => 'Update menu',
				'fr' => 'Mise à jour du menu'
			],
			[
				'en' => 'Footer',
				'fr' => 'Bas de page'
			],
			[
				'en' => 'Edit about section',
				'fr' => 'Modifier la section'
			],
			[
				'en' => 'Update about section',
				'fr' => 'Mise à jour de la section'
			],
			[
				'en' => 'Anchor',
				'fr' => 'Ancre'
			],
			[
				'en' => 'Button text',
				'fr' => 'Texte du bouton'
			]
		];

		foreach( $translations as $index => $translation )
			Translation::add_translation( [ 'title' => $translation ] );
	}

	private function seed_sliders() {
		$sliders = [
			[
				'image' => public_path( 'img/seeds/general/lion.jpg' ),
				'content' => [
					'title' => [
						'en' => 'ARCOS\' Team Conducting Biodiversity Surveys In Rugezi Island',
						'fr' => 'L\'équipe d\'ARCOS mène des enquêtes sur la biodiversité à l\'île Rugezi'
					],
					'text' => [
						'en' => 'Understanding trends in the status of biodiversity and supply of environmental services is one of the aims of ARCOS ILAM programme.',
						'fr' => 'Comprendre l\'évolution de l\'état de la biodiversité et l\'offre de services environnementaux est l\'un des objectifs du programme ARCOS ILAM.'
					]
				],
				'anchor' => [
					'entity' => [
						'type' => 'post_type',
						'name' => 'page',
						'id' => 5
					],
					'text' => [
						'en' => 'Read more',
						'fr' => 'Lire la suite'
					]
				]
			],
			[
				'image' => public_path( 'img/seeds/general/elephants.jpg' ),
			],
			[
				'image' => public_path( 'img/seeds/general/antelopes.jpg' ),
			]
		];

		foreach( $sliders as $index => $params )
			Slider::add_slider( array_merge( $params, [ 'order' => $index + 1 ] ) );
	}

	private function seed_categories() {
		$categories = [
			[
				'title' => [
					'en' => 'Sustainable Mountain Development',
					'fr' => 'Développement durable des montagnes'
				],
				'description' => [
					'en' => DatabaseSeeder::load_file( 'categories/description_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'categories/description_fr.txt' )
				]
			],
			[
				'title' => [
					'en' => 'Great Lakes Freshwater News',
					'fr' => 'Les Grands Lacs'
				],
				'description' => [
					'en' => DatabaseSeeder::load_file( 'categories/description_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'categories/description_fr.txt' )
				]
			],
			[
				'title' => [
					'en' => 'Environment and Development',
					'fr' => 'Environnement et développement'
				],
				'description' => [
					'en' => DatabaseSeeder::load_file( 'categories/description_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'categories/description_fr.txt' )
				]
			],
			[
				'title' => [
					'en' => 'Community Network in Action',
					'fr' => 'Réseau communautaire en action'
				],
				'description' => [
					'en' => DatabaseSeeder::load_file( 'categories/description_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'categories/description_fr.txt' )
				]
			],
		];

		foreach( $categories as $index => $params )
			$category_ids[] = Category::add_category( $params );

		if ( isset( $category_ids ) && is_array( $category_ids ) )
			App::add_option([
				'name' => 'featured_categories',
				'value' => json_encode( $category_ids )
			]);
	}

	private function seed_our_websites() {
		$websites = [
			[
				'name' => [
					'en' =>  'ARCOS Network',
					'fr' => 'Réseau ARCOS'
				],
				'url' => 'http://www.arcosnetwork.com/',
				'screenshot' => public_path( 'img/seeds/websites/arcos-network.png' ),
				'summary' => [
					'en' => DatabaseSeeder::load_file( 'websites/en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'websites/fr.txt' )
				]
			],
			[
				'name' => [
					'en' => 'BirdLife International',
					'fr' => 'BirdLife International'
				],
				'url' => 'http://www.birdlife.org/',
				'screenshot' => public_path( 'img/seeds/websites/bird-life.png' ),
				'summary' => [
					'en' => DatabaseSeeder::load_file( 'websites/en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'websites/fr.txt' )
				]
			],
		];

		foreach( $websites as $index => $params )
			Website::add_website( array_merge( $params, [ 'order' => ( $index + 1 ) ] ) );
	}

	private function seed_about_sections() {
		$sections = [
			[
				'title' => [
					'en' => 'Our vision',
					'fr' => 'Notre vision'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'about-sections/en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'about-sections/fr.txt' )
				],
				'image' => public_path( 'img/seeds/general/lion.jpg' )
			],
			[
				'title' => [
					'en' => 'Our mission',
					'fr' => 'Notre mission'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'about-sections/en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'about-sections/fr.txt' )
				],
				'image' => public_path( 'img/seeds/general/antelopes.jpg' )
			],
			[
				'title' => [
					'en' => 'Our goal',
					'fr' => 'Notre objectif'
				],
				'content' => [
					'en' => DatabaseSeeder::load_file( 'about-sections/en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'about-sections/fr.txt' )
				],
				'image' => public_path( 'img/seeds/general/elephants.jpg' )
			],
		];

		foreach( $sections as $index => $params )
			AboutSection::add_section( array_merge( $params, [ 'order' => ( $index + 1 ) ] ) );
	}

	private function seed_team_categories() {
		$team_categories = [
			[
				'title' => [
					'en' => 'ARCOS Board',
					'fr' => 'Conseil ARCOS'
				]
			],
			[
				'title' => [
					'en' => 'ARCOS Staff',
					'fr' => 'Personnel d\'ARCOS'
				]
			],
		];

		foreach( $team_categories as $index => $params )
			$team_category_ids[] = TeamCategory::add_team_category( array_merge( $params, [ 'order' => ( $index + 1 ) ] ) );

		return isset( $team_category_ids ) ? $team_category_ids : [];
	}

	private function seed_team_members() {
		$team_categories = $this->seed_team_categories();

		$team_members = [
			[
				'name' => 'Dr Nick King',
				'country' => [
					'en' => 'South Africa',
					'fr' => 'Afrique du Sud'
				],
				'avatar' => public_path( 'img/seeds/team/nick.jpg' ),
				'position' => [
					'en' => 'Chairperson',
					'fr' => 'Président'
				],
				'biography' => [
					'en' => DatabaseSeeder::load_file( 'team/nick_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'team/nick_fr.txt' )
				],
				'team_category' => isset( $team_categories[ 0 ] ) ? $team_categories[ 0 ] : 0,
			],
			[
				'name' => 'Paul Mafabi',
				'country' => [
					'en' => 'Uganda',
					'fr' => 'Ouganda'
				],
				'avatar' => public_path( 'img/seeds/team/mafabi.jpg' ),
				'position' => [
					'en' => 'Vice chairperson',
					'fr' => 'Vice-président'
				],
				'biography' => [
					'en' => DatabaseSeeder::load_file( 'team/nick_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'team/nick_fr.txt' )
				],
				'team_category' => isset( $team_categories[ 0 ] ) ? $team_categories[ 0 ] : 0,
			],
			[
				'name' => 'Prof Lars Kristoferson',
				'country' => [
					'en' => 'Sweden',
					'fr' => 'Suède'
				],
				'avatar' => public_path( 'img/seeds/team/lars.jpg' ),
				'position' => [
					'en' => 'Advisory vice-chairman',
					'fr' => 'Vice-président consultatif'
				],
				'biography' => [
					'en' => DatabaseSeeder::load_file( 'team/nick_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'team/nick_fr.txt' )
				],
				'team_category' => isset( $team_categories[ 0 ] ) ? $team_categories[ 0 ] : 0,
			],
			[
				'name' => 'Prof Beth Kaplin',
				'country' => [
					'en' => 'United States of America',
					'fr' => 'les États-Unis d\'Amérique'
				],
				'avatar' => public_path( 'img/seeds/team/beth.jpg' ),
				'position' => [
					'en' => 'Board member',
					'fr' => 'Membre d\'équipage'
				],
				'biography' => [
					'en' => DatabaseSeeder::load_file( 'team/nick_en.txt' ),
					'fr' => DatabaseSeeder::load_file( 'team/nick_fr.txt' )
				],
				'team_category' => isset( $team_categories[ 0 ] ) ? $team_categories[ 0 ] : 0,
			],
		];

		foreach( $team_members as $index => $params )
			$team_members_ids[] = TeamMember::add_team_member( array_merge( $params, [ 'order' => ( $index + 1 ) ] ) );
	}

	private function seed_menus() {
		$menus = [
			[
				'title' => [
					'en' => 'ARCOS at a glance',
					'fr' => 'ARCOS en bref'
				],
				'position' => 'footer-1'
			],
			[
				'title' => [
					'en' => 'Contact us',
					'fr' => 'Contactez nous'
				],
				'position' => 'footer-2'
			],
			[
				'title' => [
					'en' => 'Get involved',
					'fr' => 'Être impliqué'
				],
				'position' => 'footer-3'
			],
			[
				'title' => [
					'en' => 'Connect with us',
					'fr' => 'Connecte-toi avec nous'
				],
				'position' => 'footer-4'
			],
		];

		foreach( $menus as $index => $params )
			$menu_ids[] = Menu::add_menu( $params );

		return isset( $menu_ids ) ? $menu_ids : [];
	}
	
	private function seed_menu_items() {
		$menus = $this->seed_menus();

		$menu_items = [
			[
				'title' => [
					'en' => 'Annual reports',
					'fr' => 'Rapport annuel'
				],
				'custom_link' => '#',
				'menu' => $menus[ 0 ]
			],
			[
				'title' => [
					'en' => 'ARCOS staff',
					'fr' => 'Personnel d\'ARCOS'
				],
				'custom_link' => '#',
				'menu' => $menus[ 0 ]
			],
			[
				'title' => [
					'en' => 'ARCOS board',
					'fr' => 'Carte ARCOS'
				],
				'custom_link' => '#',
				'menu' => $menus[ 0 ]
			],
			[
				'title' => [
					'en' => 'Strategic plan',
					'fr' => 'Plan stratégique'
				],
				'custom_link' => '#',
				'menu' => $menus[ 0 ]
			],
			[
				'title' => [
					'en' => 'Operational manual',
					'fr' => 'Manuel opérationnel'
				],
				'custom_link' => '#',
				'menu' => $menus[ 0 ]
			],
			[
				'title' => [
					'en' => 'United Kingdom office',
					'fr' => 'Royaume-Uni bureau'
				],
				'custom_link' => '#',
				'menu' => $menus[ 1 ]
			],
			[
				'title' => [
					'en' => 'Kampala office',
					'fr' => 'Bureau de Kampala'
				],
				'custom_link' => '#',
				'menu' => $menus[ 1 ]
			],
			[
				'title' => [
					'en' => 'Kigali office',
					'fr' => 'Bureau de Kigali'
				],
				'custom_link' => '#',
				'menu' => $menus[ 1 ]
			],
			[
				'title' => [
					'en' => 'Burundi office',
					'fr' => 'Bureau du Burundi'
				],
				'custom_link' => '#',
				'menu' => $menus[ 1 ]
			],
			[
				'title' => [
					'en' => 'Subscribe to our newsletter',
					'fr' => 'Abonnez-vous à notre newsletter'
				],
				'custom_link' => '#',
				'menu' => $menus[ 2 ]
			],
			[
				'title' => [
					'en' => 'ARCOS membership programme',
					'fr' => 'Programme d\'adhésion à ARCOS'
				],
				'custom_link' => '#',
				'menu' => $menus[ 2 ]
			],
			[
				'title' => [
					'en' => 'ARCOS CSR programme',
					'fr' => 'Programme CSR d\'ARCOS'
				],
				'custom_link' => '#',
				'menu' => $menus[ 2 ]
			],
			[
				'title' => [
					'en' => 'ARCOS youth programme',
					'fr' => 'Programme jeunesse ARCOS'
				],
				'custom_link' => '#',
				'menu' => $menus[ 2 ]
			],
			[
				'title' => [
					'en' => '<i class="fa fa-twitter"></i> Twitter',
					'fr' => '<i class="fa fa-twitter"></i> Twitter'
				],
				'custom_link' => 'https://twitter.com/ARCOSnetwork',
				'menu' => $menus[ 3 ]
			],
			[
				'title' => [
					'en' => '<i class="fa fa-facebook"></i> Facebook',
					'fr' => '<i class="fa fa-facebook"></i> Facebook'
				],
				'custom_link' => 'https://www.facebook.com/arcosnetwork',
				'menu' => $menus[ 3 ]
			],
			[
				'title' => [
					'en' => '<i class="fa fa-youtube"></i> Youtube',
					'fr' => '<i class="fa fa-youtube"></i> Youtube'
				],
				'custom_link' => 'https://www.youtube.com/user/ARCOSNetwork',
				'menu' => $menus[ 3 ]
			],
		];

		foreach( $menu_items as $index => $params )
			$menu_item_ids[] = MenuItem::add_menu_item( array_merge( $params, [ 'order' => ( $index + 1 ) ] ) );
	}

	private function seed_mailman_lists() {
		foreach( App::fetch_mailman_lists() as $list )
			MailmanList::add_mailman_list( $list );
	}

	public function run() {
		$this->seed_mailman_lists();
		$this->seed_our_websites();
		$this->seed_translations();
		$this->seed_categories();
		$this->seed_sliders();
		$this->seed_about_sections();
		$this->seed_team_members();
		$this->seed_menu_items();
	}

}
