<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRoleMetaTable extends Migration {

    public function up() {
	    Schema::create( 'user_role_meta', function( Blueprint $table ) {
		    $table->engine = 'InnoDB';

		    $table->increments( 'id' )->unsigned();
		    $table->integer( 'user_role_id' )->unsigned();
		    $table->string( 'key' );
		    $table->text( 'value' )->nullable();

		    $table->timestamps();

		    $table->foreign( 'user_role_id' )->references( 'id' )->on( 'user_roles' )->onDelete( 'cascade' );
		    $table->unique( [ 'user_role_id', 'key'  ] );
	    } );
    }

    public function down() {
        Schema::drop( 'user_role_meta' );
    }

}
