<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionsTable extends Migration {

    public function up() {
        Schema::create( 'options', function ( Blueprint $table ) {
            $table->engine = 'InnoDB';

            $table->increments( 'id' )->unsigned();
            $table->string( 'name' )->unique();
            $table->text( 'value' );

            $table->timestamps();
        } );
    }

    public function down() {
        Schema::drop( 'options' );
    }

}
