<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration {

    public function up() {
        Schema::create( 'languages', function( Blueprint $table ) {
            $table->engine = 'InnoDB';

            $table->increments( 'id' )->unsigned();
            $table->string( 'name', 50 )->unique();
            $table->string( 'code', 2 )->unique();

            $table->timestamps();
        } );
    }

    public function down() {
        Schema::drop( 'languages' );
    }

}
