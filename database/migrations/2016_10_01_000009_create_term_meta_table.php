<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermMetaTable extends Migration {

    public function up() {
        Schema::create( 'term_meta', function ( Blueprint $table ) {
	        $table->engine = 'InnoDB';

	        $table->increments( 'id' )->unsigned();
	        $table->integer( 'term_id' )->unsigned();
	        $table->string( 'key' );
	        $table->text( 'value' )->nullable();

	        $table->timestamps();

	        $table->foreign( 'term_id' )->references( 'id' )->on( 'terms' )->onDelete( 'cascade' );
	        $table->unique( [ 'term_id', 'key'  ] );
        });
    }

    public function down() {
        Schema::drop( 'term_meta' );
    }

}
