<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermsTable extends Migration {

    public function up() {
        Schema::create( 'terms', function( Blueprint $table ) {
            $table->engine = 'InnoDB';

            $table->increments( 'id' )->unsigned();
            $table->string( 'slug' )->unique();
            $table->string( 'taxonomy', 50 );

            $table->timestamps();
        } );
    }

    public function down() {
        Schema::drop( 'terms' );
    }
}
