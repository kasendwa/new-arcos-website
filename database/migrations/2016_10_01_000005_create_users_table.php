<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    public function up() {
        Schema::create( 'users', function( Blueprint $table ) {
            $table->engine = 'InnoDB';

            $table->increments( 'id' )->unsigned();
            $table->string( 'username', 60 )->unique();
            $table->string( 'email', 120 )->unique();
            $table->string( 'password', 60 );
            $table->integer( 'user_role' )->unsigned()->nullable();
            $table->string( 'confirmation_code' )->nullable();
            $table->boolean( 'confirmed' )->default( false );

            $table->rememberToken();
            $table->timestamps();

            $table->foreign( 'user_role' )->references( 'id' )->on( 'user_roles' )->onDelete( 'set null' );
        } );
    }

    public function down() {
        Schema::drop( 'users' );
    }

}
