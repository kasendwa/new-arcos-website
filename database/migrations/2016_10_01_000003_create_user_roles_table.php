<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRolesTable extends Migration {

    public function up() {
        Schema::create( 'user_roles', function( Blueprint $table ) {
            $table->engine = 'InnoDB';

            $table->increments( 'id' )->unsigned();
            $table->string( 'name', 15 );

            $table->timestamps();
        } );
    }

    public function down() {
        Schema::drop( 'user_roles' );
    }

}
