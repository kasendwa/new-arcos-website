<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMetaTable extends Migration {

    public function up() {
        Schema::create( 'user_meta', function( Blueprint $table ) {
            $table->engine = 'InnoDB';

            $table->increments( 'id' )->unsigned();
            $table->integer( 'user_id' )->unsigned();
            $table->string( 'key' );
            $table->text( 'value' )->nullable();

            $table->timestamps();

            $table->foreign( 'user_id' )->references( 'id' )->on( 'users' )->onDelete( 'cascade' );
            $table->unique( [ 'user_id', 'key'  ] );
        } );
    }

    public function down() {
        Schema::drop( 'user_meta' );
    }

}
