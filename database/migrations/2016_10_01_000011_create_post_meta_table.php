<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostMetaTable extends Migration {

    public function up() {
        Schema::create( 'post_meta', function( Blueprint $table ) {
            $table->engine = 'InnoDB';

            $table->increments( 'id' )->unsigned();
            $table->integer( 'post_id' )->unsigned();
            $table->string( 'key' );
            $table->text( 'value' )->nullable();

            $table->timestamps();

            $table->foreign( 'post_id' )->references( 'id' )->on( 'posts' )->onDelete( 'cascade' );
            $table->unique( [ 'post_id', 'key' ] );
        } );
    }

    public function down() {
        Schema::drop( 'post_meta' );
    }

}
