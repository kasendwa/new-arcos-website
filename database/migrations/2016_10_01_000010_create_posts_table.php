<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

    public function up() {
        Schema::create( 'posts', function( Blueprint $table ) {
            $table->engine = 'InnoDB';

            $table->increments( 'id' )->unsigned();
            $table->string( 'name' )->unique();
            $table->string( 'type', 50 );
            $table->string( 'status', 15 )->nullable();
            $table->integer( 'user_id' )->unsigned()->nullable();
            $table->integer( 'user_id_edited' )->unsigned()->nullable();

            $table->timestamps();

            $table->foreign( 'user_id' )->references( 'id' )->on( 'users' )->onDelete( 'set null' );
            $table->foreign( 'user_id_edited' )->references( 'id' )->on( 'users' )->onDelete( 'set null' );
        } );
    }

    public function down() {
        Schema::drop( 'posts' );
    }

}
