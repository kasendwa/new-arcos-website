@extends( 'site.structure' )

@section( 'title' )
	{{ $title }}
@endsection

@section( 'content' )

	<div class="container">

		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div id="page_content">

					<h2>
						{{ $title }}
					</h2>

					<ul id="categories_list">

						@foreach( $categories as $category )

							<li>

								<h4>
									<a href="{{ $category->permalink }}">
										{{ $category->title }}
									</a>
								</h4>

								<div>
									{!! $category->description !!}
								</div>

							</li>

						@endforeach

					</ul>

				</div>

			</div>

		</div>

	</div>

@endsection