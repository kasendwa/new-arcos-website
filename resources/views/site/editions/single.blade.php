@extends( 'site.structure' )

@section( 'title' )
	{{ $category->title }}
@endsection

@section( 'content' )

	<div class="container">

		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div id="page_content">

					No editions as yet

				</div>

			</div>

		</div>

	</div>

@endsection