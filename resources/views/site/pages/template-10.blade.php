@extends( 'site.structure' )

@section( 'title' ) {{ $page_data->title }} @endsection

@section( 'content' )

	<div class="container">

		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div id="page_content">

					<h3>
						{{ $page_data->title }}
					</h3>

					<div>
						{!! $page_data->content !!}
					</div>

					<div>

						<ul id="newsletters">

							@foreach( $newsletters as $newsletter )

								<li>

									<div class="row">

										@if ( isset( $newsletter->thumbnail->medium_screenshot ) )

											<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

												<a href="{{ $newsletter->permalink }}" target="_blank">
													<img src="{{ $newsletter->thumbnail->medium_screenshot }}" class="img-fluid" />
												</a>

											</div>

											<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

												<h4>{{ $newsletter->title }}</h4>

												{!! $newsletter->description !!}

												<a href="{{ $newsletter->permalink }}" class="btn btn-primary">
													{{ $translations->see_previous_editions }}
													<i class="fa fa-angle-right"></i>
												</a>

											</div>

										@else

											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

												<h4>{{ $newsletter->title }}</h4>

												{!! $newsletter->description !!}

												<a href="javascript:void(0);" class="btn btn-primary">
													{{ $translations->see_previous_editions }}
													<i class="fa fa-angle-right"></i>
												</a>

											</div>

										@endif

									</div>

								</li>

							@endforeach

						</ul>

					</div>

					<div class="subscription_form">

						<h3>{{ $translations->subscribe_to_our_newsletters }}</h3>

						@if ( isset( $result_type ) )

							<div class="alert {{ $result_type == true ? 'alert-success' : 'alert-warning' }}" role="alert">
								{{ $result_message }}
							</div>

						@endif

						{!! Form::open( [ 'method' => 'post', 'files' => false ] ) !!}

						<div class="row">

							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

								<div class="row">

									<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
										{!! Form::label( 'first_name', $translations->first_name . ' *' ) !!}
										{!! Form::text( 'first_name', '', [ 'class' => 'form-control', 'placeholder' => $translations->first_name, 'required' => '' ] ) !!}
									</div>

									<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
										{!! Form::label( 'last_name', $translations->last_name . ' *' ) !!}
										{!! Form::text( 'last_name', '', [ 'class' => 'form-control', 'placeholder' => $translations->last_name, 'required' => '' ] ) !!}
									</div>

								</div>

								<div class="form-group">
									{!! Form::label( 'email', $translations->email_address . ' *' ) !!}
									{!! Form::email( 'email', '', [ 'class' => 'form-control', 'placeholder' => $translations->email_address, 'required' => '' ] ) !!}
								</div>

								<div class="form-group">
									{!! Form::label( 'phone_number', $translations->phone_number ) !!}
									{!! Form::text( 'phone_number', '', [ 'class' => 'form-control', 'placeholder' => $translations->phone_number ] ) !!}
								</div>

								<div class="row">

									<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
										{!! Form::label( 'institution', $translations->institution ) !!}
										{!! Form::text( 'institution', '', [ 'class' => 'form-control', 'placeholder' => $translations->institution ] ) !!}
									</div>

									<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
										{!! Form::label( 'job_position', $translations->job_position ) !!}
										{!! Form::text( 'job_position', '', [ 'class' => 'form-control', 'placeholder' => $translations->job_position ] ) !!}
									</div>

								</div>

							</div>

							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

								<p>
									{{ $translations->checkboxes_of_newsletters_you_want_to_subscribe_to }}
								</p>

								@foreach( $lists as $name => $label )
									<label class="mailing_list_list">
										<input type="checkbox" name="newsletters[]" value="{{ $name }}" /> {{ $label->{ $language } }}
									</label>
								@endforeach

							</div>

						</div>

						<div class="form-group">
							{!! Form::submit( $translations->subscribe, [ 'class' => 'btn btn-primary btn-sm' ] ) !!}
						</div>

						{!! Form::close() !!}

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection

@section( 'scripts' )

	<script type="text/javascript">
	</script>

@endsection