@extends( 'site.structure' )

@section( 'title' ) {{ $page_data->title }} @endsection

@section( 'content' )

	@if ( isset( $page_data->thumbnail ) && isset( $page_data->thumbnail->page_top_image ) )

		<div id="page_thumbnail" style="background-image: url({{ $page_data->thumbnail->page_top_image }});">

			<div>

				<div class="container">

					<div>

						<h3>
							{{ $page_data->title }}
						</h3>

						@if ( !empty( $page_data->summary ) )

							<p class="hidden-md-down">
								{{ $page_data->summary }}
							</p>

						@endif

					</div>

				</div>

			</div>

		</div>

	@endif

	<div class="container">

		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12">

				<div id="page_content">

					<h3>
						{{ $page_data->title }}
					</h3>

					<div>
						{!! $page_data->content !!}
					</div>

					<div id="our_partners">

						@foreach( $partner_categories as $partner_category )

							<div class="partner_category">

								<div class="title">
									<span>
										{{ $partner_category->title }}
									</span>
								</div>

								<div>
									{!! $partner_category->summary !!}
								</div>

								<div class="partners real-partners">

									<div class="row content-slider">

										@foreach( $partner_category->partners as $partner )

											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">

												<div style>

													<img src="{{ $partner->logo->full }}" class="img-fluid" />

													<h4>
														{{ $partner->name->{ $language } }}
													</h4>

													<div>

														<h3>
															{{ $partner->name->{ $language } }}
														</h3>

														@if ( !empty( $partner->motto->{ $language } ) )

															<span>
																{{ $partner->motto->{ $language } }}
															</span>

														@endif

														<p>
															{{ substr( strip_tags( $partner->summary->{ $language } ), 0, 100 ) }}
														</p>

														<a class="btn btn-primary" target="_blank" href="{{ $partner->url }}">
															{{ $translations->visit_website }} <i class="fa fa-angle-right"></i>
														</a>

													</div>

												</div>

											</div>

										@endforeach

									</div>

								</div>

							</div>

						@endforeach

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection