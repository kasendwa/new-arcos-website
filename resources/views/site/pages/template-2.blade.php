@extends( 'site.structure' )

@section( 'title' ) {{ $page_data->title }} @endsection

@section( 'content' )

	<div class="container">

		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div id="page_content">

					<h3>
						{{ $page_data->title }}
					</h3>

					<div>
						{!! $page_data->content !!}
					</div>

					<div>

						<ul id="our_websites">

							@foreach( $websites as $website )

							<li>

								<div class="row">

									@if ( isset( $website->screenshot->medium_screenshot ) )

										<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

											<a href="{{ $website->url }}" target="_blank">

												<img src="{{ $website->screenshot->medium_screenshot }}" class="img-fluid" />

												<span>
													{{ $website->name }}
												</span>

											</a>

										</div>

										<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

											{!! $website->summary !!}

											<a href="{{ $website->url }}" class="btn btn-primary">
												{{ $translations->visit_the_website }}
												<i class="fa fa-angle-right"></i>
											</a>

										</div>

									@else

										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

											{!! $website->summary !!}

											<a href="{{ $website->url }}" class="btn btn-primary">
												{{ $translations->visit_the_website }}
												<i class="fa fa-angle-right"></i>
											</a>

										</div>

									@endif

								</div>

							</li>

							@endforeach

						</ul>

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection