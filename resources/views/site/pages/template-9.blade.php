@extends( 'site.structure' )

@section( 'title' ) {{ $page_data->title }} @endsection

@section( 'content' )

	<div class="container">

		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div id="page_content">

					<h3>
						{{ $page_data->title }}
					</h3>

					<div>
						{!! $page_data->content !!}
					</div>

					<div id="playlists_container">

						<div class="row">

							@foreach( $playlists as $playlist )

								<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 text-lg-left text-md-left">

									<a href="{{ $playlist->permalink }}" style="background-image: url({{ $playlist->cover->high }});">
									</a>

									<h4>

										<a href="{{ $playlist->permalink }}">
											{{ $playlist->title }}
										</a>

										<span>
											({{ number_format( $playlist->count ) }})
										</span>

									</h4>

									<p>
										{{ strlen( $playlist->description ) > 150 ? substr( $playlist->description, 0, 150 ) . '[...]' : $playlist->description }}
									</p>

								</div>

							@endforeach

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection