@extends( 'site.structure' )

@section( 'title' ) {{ $page_data->title }}
@endsection

@section( 'content' )

	<div class="container">

		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div id="page_content">

					<h3>
						{{ $page_data->title }}
					</h3>

					<div>
						{!! $page_data->content !!}
					</div>

					<ul id="team_categories">

					@foreach( $team_categories as $team_category )

						@if ( count( $team_category->members ) > 0 )

						<li>

							<h4>
								<span>
									{{ $team_category->title }}
								</span>
							</h4>

							<div class="row">

								<?php $counter = 0; ?>

								@foreach( $team_category->members as $index => $member )

									@if ( $counter % 4 == 0 )

										</div><div class="row">

									@endif

									<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 text-center">

										<a href="{{ $member->permalink }}">
											@if ( isset( $member->avatar->small ) )
												<img src="{{ $member->avatar->small }}" class="rounded-circle img-fluid" />
											@else
												<div class="img-placeholder" class="rounded-circle"></div>
											@endif
										</a>

										<p>
											{{ $member->name }}
										</p>

										<p>
											{{ $member->position }}
										</p>

										<a href="{{ $member->permalink }}" class="btn btn-primary">
											{{ $translations->read_biography }}
										</a>

									</div>

									<?php $counter++; ?>

								@endforeach

							</div>

						</li>

						@endif

					@endforeach

					</ul>

				</div>

			</div>

		</div>

	</div>

@endsection