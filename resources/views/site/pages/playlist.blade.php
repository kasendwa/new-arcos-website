@extends( 'site.structure' )

@section( 'title' ) {{ $info->title }} @endsection

@section( 'content' )

	<div class="container">

		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div id="page_content">

					<h3>
						{{ $info->title }}
					</h3>

					<div>
						{!! $info->description !!}
					</div>

					<div id="playlist_container">

						<div class="row playlist_items">

							@foreach( $videos as $video )

								<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 text-lg-left text-md-left">

									<a href="{{ $video->permalink }}">
										<img src="{{ $video->thumbnails->medium }}" class="img-fluid" />
										<span>
											<i class="fa fa-play-circle-o"></i>
										</span>
									</a>

									<h4>
										<a href="{{ $video->permalink }}">
											{{ $video->title }}
										</a>
									</h4>

									<p>
										{{ strlen( $video->description ) > 150 ? substr( $video->description, 0, 150 ) . '[...]' : '' }}
									</p>

								</div>

							@endforeach

						</div>

					</div>
				</div>

			</div>

		</div>

	</div>

@endsection