@extends( 'site.structure' )

@section( 'content' )

	@include( 'site.sections.slider' )

	<div class="container">
		@include( 'site.sections.featured_categories' )
	</div>

@endsection