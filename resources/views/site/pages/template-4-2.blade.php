@extends( 'site.structure' )

@section( 'title' ) {{ $page_data->title }} @endsection

@section( 'content' )

	@if ( isset( $page_data->thumbnail ) && isset( $page_data->thumbnail->page_top_image ) )

		<div id="page_thumbnail" style="background-image: url({{ $page_data->thumbnail->page_top_image }});">

			<div>

				<div class="container">

					<div>

						<h3>
							{{ $page_data->title }}
						</h3>

						@if ( !empty( $page_data->summary ) )

							<p class="hidden-md-down">
								{{ $page_data->summary }}
							</p>

						@endif

					</div>

				</div>

			</div>

		</div>

	@endif

	<div class="container">

		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12">

				<div id="page_content">

					<h3>
						{{ $page_data->title }}
					</h3>

					<div>
						{!! $page_data->content !!}
					</div>

					<div id="our_partners">

						@foreach( $resource_categories as $resource_category )

							<div class="partner_category">

								<div class="title">
									<span>
										{{ $resource_category->title }}
									</span>
								</div>

								<div>
									{!! $resource_category->summary !!}
								</div>

								<div class="partners">

									<div class="row content-slider">

										@foreach( $resource_category->resources as $resource )

											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">

												<div style>

													<img src="{{ $resource->screenshot->featured_articles_image }}" class="img-fluid" />

													<h4>
														{{ $resource->name->{ $language } }}
													</h4>

													<div>

														<h3>
															{{ $resource->name->{ $language } }}
														</h3>

														<p>
															{{ substr( strip_tags( $resource->summary->{ $language } ), 0, 100 ) }}
														</p>

														<a class="btn btn-primary" target="_blank" href="{{ $resource->url }}">
															{{ $translations->download }} <i class="fa fa-angle-right"></i>
														</a>

													</div>

												</div>

											</div>

										@endforeach

									</div>

								</div>

							</div>

						@endforeach

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection