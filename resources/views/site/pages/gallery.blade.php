@extends( 'site.structure' )

@section( 'title' ) {{ $info->title }} @endsection

@section( 'content' )

	<div class="container">

		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div id="page_content">

					<h3>
						{{ $info->title }}
					</h3>

					<div>
						{!! $info->description !!}
					</div>

					<div id="gallery_container">

						<div class="row gallery_items">

							@foreach( $photos as $photo )

								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
									<a href="{{ isset( $photo->thumbnails[ 'large' ] ) ? $photo->thumbnails[ 'large' ] : $photo->thumbnails[ 'medium' ] }}"  data-lightbox="{{ $info->id }}" title="{{ $photo->title }}">
										<img width="180" src="{{ $photo->thumbnails[ 'thumbnail' ] }}" class="img-fluid" />
									</a>
								</div>

							@endforeach

						</div>

					</div>

					<div class="items_pagination">

						<ul class="pagination">

							<li class="page-item {{ ( $pagination->active == 1 ? 'disabled' : '' ) }}">
								<a class="page-link" href="{{ $info->permalink . '/' . ( $pagination->active == 1 ? 1 : ( $pagination->active - 1 ) ) }}">
									<i class="fa fa-angle-double-left"></i>
								</a>
							</li>

							@for( $i = 1; $i <= $pagination->total; $i++ )

								<li class="page-item {{ ( $pagination->active == $i ? 'active' : '' ) }}">
									<a class="page-link" href="{{ $info->permalink . '/' . $i }}">
										{{ $i }}
									</a>
								</li>

							@endfor

							<li class="page-item {{ ( $pagination->active == $pagination->total ? 'disabled' : '' ) }}">
								<a class="page-link" href="{{ $info->permalink . '/' . ( $pagination->active == $pagination->total ? $pagination->total : ( $pagination->active + 1 ) ) }}">
									<i class="fa fa-angle-double-right"></i>
								</a>
							</li>

						</ul>

					</div>

					<div id="article_comments">
						<div id="disqus_thread"></div>
					</div>

				</div>

			</div>

		</div>

	</div>

@endsection