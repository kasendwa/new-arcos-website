@extends( 'site.structure' )

@section( 'title' ) {{ $page_data->title }} @endsection

@section( 'content' )

	@if ( isset( $page_data->thumbnail ) && isset( $page_data->thumbnail->page_top_image ) )

		<div id="page_thumbnail" style="background-image: url({{ $page_data->thumbnail->page_top_image }});">

			<div>

				<div class="container">

					<div>

						<h3>
							{{ $page_data->title }}
						</h3>

						@if ( !empty( $page_data->summary ) )

							<p class="hidden-md-down">
								{{ $page_data->summary }}
							</p>

						@endif

					</div>

				</div>

			</div>

		</div>

	@endif

	<div class="container">

		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div id="page_content">
					{!! $page_data->content !!}
				</div>

			</div>

		</div>

	</div>

@endsection