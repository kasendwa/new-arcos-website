@extends( 'site.structure' )

@section( 'title' ) {{ $page_data->title }} @endsection

@section( 'content' )

	<div class="container">

		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div id="page_content">

					<h3>
						{{ $page_data->title }}
					</h3>

					<div>
						{!! $page_data->content !!}
					</div>

					<div id="galleries_container">

						<div class="row gallery_items">

							@foreach( $galleries as $gallery )

								<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 text-lg-left text-md-left">

									<a href="{{ $gallery->permalink }}">
										<img src="{{ $gallery->cover->medium }}" class="img-fluid" />
									</a>

									<h4>

										<a href="{{ $gallery->permalink }}">
											{{ $gallery->title }}
										</a>

										<span>
											({{ number_format( $gallery->photos ) }})
										</span>

									</h4>

									<p>
										{{ strlen( $gallery->description ) > 150 ? substr( $gallery->description, 0, 150 ) . '[...]' : $gallery->description }}
									</p>

								</div>

							@endforeach

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection