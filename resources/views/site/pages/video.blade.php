@extends( 'site.structure' )

@section( 'title' ) {{ $info->title }} @endsection

@section( 'content' )

	<div class="container">

		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div id="page_content">

					<h3>
						{{ $info->title }}
					</h3>

					<div id="video_container">
						{!! $info->iframe !!}
					</div>

					<div>
						{!! $info->description !!}
					</div>

					<div id="article_comments">
						<div id="disqus_thread"></div>
					</div>

				</div>

			</div>

		</div>

	</div>

@endsection