@extends( 'site.structure' )

@section( 'title' ) {{ $page_data->title }} @endsection

@section( 'content' )

	@if ( isset( $page_data->thumbnail ) && isset( $page_data->thumbnail->page_top_image ) )

		<div id="page_thumbnail" style="background-image: url({{ $page_data->thumbnail->page_top_image }});">

			<div>

				<div class="container">

					<div>

						<h3>
							{{ $page_data->title }}
						</h3>

						@if ( !empty( $page_data->summary ) )

							<p class="hidden-md-down">
								{{ $page_data->summary }}
							</p>

						@endif

					</div>

				</div>

			</div>

		</div>

	@endif

	<div class="container">

		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div id="page_content">
					{!! $page_data->content !!}
				</div>

				<div id="related_articles_list">

					<div class="title">
						<span>
							{{ $translations->related_articles }}
						</span>
					</div>

					<div class="articles">

						<div class="row content-slider">

							@foreach( $related_articles as $article )

								<div class="col-lg-4 col-md-4 col-sm-4">

									<div style="background-image: url({{ $article->thumbnail->featured_articles_image }});">

										<h4>
											{{ $article->title }}
										</h4>

										<div>

											<h3>
												{{ $article->title }}
											</h3>

											<p>
												{{ substr( $article->summary, 0, 100 ) . '[...]' }}
											</p>

											<a class="btn btn-primary" href="{{ $article->permalink }}">
												{{ $translations->read_article }} <i class="fa fa-angle-right"></i>
											</a>

										</div>

									</div>

								</div>

							@endforeach

						</div>

					</div>

				</div>

				<div id="related_projects_list">

					<div class="title">
						<span>
							{{ $translations->related_projects }}
						</span>
					</div>

					<div class="projects">

						<div class="row content-slider">

							@foreach( $related_projects as $project )

								<div class="col-lg-4 col-md-4 col-sm-4">

									<div style="background-image: url({{ $project->thumbnail->featured_articles_image }});">

										<h4>
											{{ $project->title }}
										</h4>

										<div>

											<h3>
												{{ $project->title }}
											</h3>

											<p>
												{{ substr( $project->summary, 0, 190 ) . '[...]' }}
											</p>

											<a class="btn btn-primary" href="{{ $project->permalink }}">
												{{ $translations->read_more }} <i class="fa fa-angle-right"></i>
											</a>

										</div>

									</div>

								</div>

							@endforeach

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection