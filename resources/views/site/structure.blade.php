<!DOCTYPE html>

<html lang="en">

	<head>

		<meta charset="UTF-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>@section( 'title' ){{ $site_info->defaults->page_title }}@show &mdash; {{ $site_info->name }}</title>
		<meta name="keywords" content="@section( 'meta_keywords' ){{ $site_info->defaults->meta->keywords }}@show" />
		<meta name="author" content="@section( 'meta_author' ){{ $site_info->defaults->meta->author }}@show" />
		<meta name="description" content="@section( 'meta_description' ){{ $site_info->defaults->meta->description }}@show" />

		<link href="{{ asset( 'css/bootstrap.css' ) }}" rel="stylesheet">
		<script src="{{ asset( 'js/bootstrap.js' ) }}"></script>

		<link href="{{ asset( 'css/site.css' ) }}" rel="stylesheet">
		@yield( 'styles' )

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<link rel="shortcut icon" href="{!! asset( 'img/favicon.png' ) !!} ">

	</head>

	<body>

		@include( 'site.sections.header' )

		@yield( 'content' )

		@include( 'site.sections.footer' )

		<script src="{{ asset( 'js/site.js' ) }}"></script>
		@yield( 'scripts' )

	</body>

</html>