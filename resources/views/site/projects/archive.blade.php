@extends( 'site.structure' )

@section( 'title' )
	{{ $post_type->label }}
@endsection

@section( 'content' )

	<div class="container">

		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div id="page_content">

					<h2>
						{{ $post_type->label }}
					</h2>

					<div id="category_articles_list">

						<div class="row">

							@foreach( $projects as $project )

								<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">

									<div style="background-image: url({{ $project->thumbnail->featured_articles_image }});">

										<h4>{{ $project->title }}</h4>

										<div>

											<h3>
												{{ $project->title }}
											</h3>

											<p>
												{{ substr( $project->summary, 0, 180 ) . '[...]' }}
											</p>

											<a class="btn btn-primary" href="{{ $project->permalink }}">
												{{ $translations->read_more }} <i class="fa fa-angle-right"></i>
											</a>

										</div>

									</div>

								</div>

							@endforeach

						</div>

					</div>

					<div class="items_pagination">

						<ul class="pagination">

							<li class="page-item {{ ( $pagination->active == 1 ? 'disabled' : '' ) }}">
								<a class="page-link" href="{{ $post_type->permalink . '/' . ( $pagination->active == 1 ? 1 : ( $pagination->active - 1 ) ) }}">
									<i class="fa fa-angle-double-left"></i>
								</a>
							</li>

							@for( $i = 1; $i <= $pagination->total; $i++ )

								<li class="page-item {{ ( $pagination->active == $i ? 'active' : '' ) }}">
									<a class="page-link" href="{{ $post_type->permalink . '/' . $i }}">
										{{ $i }}
									</a>
								</li>

							@endfor

							<li class="page-item {{ ( $pagination->active == $pagination->total ? 'disabled' : '' ) }}">
								<a class="page-link" href="{{ $post_type->permalink . '/' . ( $pagination->active == $pagination->total ? $pagination->total : ( $pagination->active + 1 ) ) }}">
									<i class="fa fa-angle-double-right"></i>
								</a>
							</li>

						</ul>

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection