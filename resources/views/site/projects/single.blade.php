@extends( 'site.structure' )

@section( 'title' ) {{ $project_data->title }} @endsection

@section( 'content' )

	@if ( isset( $project_data->thumbnail ) && isset( $project_data->thumbnail->page_top_image ) )

		<div id="page_thumbnail" style="background-image: url({{ $project_data->thumbnail->page_top_image }});">

			<div>

				<div class="container">

					<div>

						<h3>
							{{ $project_data->title }}
						</h3>

						@if ( !empty( $project_data->summary ) )

							<p class="hidden-md-down">
								{{ $project_data->summary }}
							</p>

						@endif

					</div>

				</div>

			</div>

		</div>

	@endif

	<div class="container">

		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12">

				<div id="page_content">

					{!! $project_data->content !!}

					<div id="related_articles_list">

						<div class="title">
							<span>
								{{ $translations->related_articles }}
							</span>
						</div>

						<div class="articles">

							<div class="row content-slider">

								@foreach( $related_articles as $article )

									<div class="col-lg-4 col-md-4 col-sm-4">

										<div style="background-image: url({{ $article->thumbnail->featured_articles_image }});">

											<h4>
												{{ $article->title }}
											</h4>

											<div>

												<h3>
													{{ $article->title }}
												</h3>

												<p>
													{{ substr( $article->summary, 0, 100 ) . '[...]' }}
												</p>

												<a class="btn btn-primary" href="{{ $article->permalink }}">
													{{ $translations->read_article }} <i class="fa fa-angle-right"></i>
												</a>

											</div>

										</div>

									</div>

								@endforeach

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection