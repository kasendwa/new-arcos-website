@extends( 'site.structure' )

@section( 'title' )
	{{ $translations->search_results_title }}
@endsection

@section( 'content' )

	<div class="container">

		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div id="page_content">

					<h3>
						{{ $translations->search_results_title }}
					</h3>

					<div>

						<ul id="search_results">

							@foreach( $results as $result )

							<li>

								<div class="row">

									@if ( isset( $result->thumbnail->thumbnail ) )

										<div class="col-lg-1 col-md-1 col-sm-2 col-xs-3">
											<a href="{{ $result->permalink }}">
												<img src="{{ $result->thumbnail->thumbnail }}" class="img-fluid" />
											</a>
										</div>

										<div class="col-lg-11 col-md-11 col-sm-10 col-xs-9">

											<h4>
												<a href="{{ $result->permalink }}">
													{{ $result->title }}
												</a>
											</h4>

											<p>
												{{ $result->summary }}
											</p>

										</div>

									@endif

								</div>

							</li>

							@endforeach

						</ul>

					</div>

					<div class="items_pagination">

						<ul class="pagination">

							<li class="page-item {{ ( $pagination->active == 1 ? 'disabled' : '' ) }}">
								<a class="page-link" href="{{ $permalink . '/' . ( $pagination->active == 1 ? 1 : ( $pagination->active - 1 ) ) . '?s=' . $s }}">
									<i class="fa fa-angle-double-left"></i>
								</a>
							</li>

							@for( $i = 1; $i <= $pagination->total; $i++ )

								<li class="page-item {{ ( $pagination->active == $i ? 'active' : '' ) }}">
									<a class="page-link" href="{{ $permalink . '/' . $i . '?s=' . $s }}">
										{{ $i }}
									</a>
								</li>

							@endfor

							<li class="page-item {{ ( $pagination->active == $pagination->total ? 'disabled' : '' ) }}">
								<a class="page-link" href="{{ $permalink . '/' . ( $pagination->active == $pagination->total ? $pagination->total : ( $pagination->active + 1 ) ) . '?s=' . $s }}">
									<i class="fa fa-angle-double-right"></i>
								</a>
							</li>

						</ul>

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection