@extends( 'site.structure' )

@section( 'title' ) {{ $article_data->title }} @endsection

@section( 'content' )

	@if ( isset( $article_data->thumbnail ) && isset( $article_data->thumbnail->page_top_image ) )

		<div id="page_thumbnail" style="background-image: url({{ $article_data->thumbnail->page_top_image }});"></div>

	@else

	@endif

	<div class="container">

		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12">

				<div id="page_content">

					<h2>
						{{ $article_data->title }}
					</h2>

					@if ( !empty( $article_data->summary ) )

						<div class="article_summary">
							{{ $article_data->summary }}
						</div>

					@endif

					<div class="article_meta">

						<div class="row ">

							<div class="col-lg-9 col-md-8 col-sm-12">
								{!! $article_data->meta !!}.
							</div>

							<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 text-lg-right text-md-right text-sm-center text-xs-center">

								{{ $share_on }}:

								<a class="float-lg-right float-md-right float-sm-none" href="javascript:void(0);" data-share-url="{{ $share->twitter[ 'twitter' ] }}" title="Share on Twitter">
									<i class="fa fa-twitter"></i>
								</a>

								<a class="float-lg-right float-md-right float-sm-none" href="javascript:void(0);" data-share-url="{{ $share->facebook[ 'facebook' ] }}" title="Share on Facebook">
									<i class="fa fa-facebook-official"></i>
								</a>

							</div>

						</div>

					</div>

					{!! $article_data->content !!}

					<div id="article_comments">
						<div id="disqus_thread"></div>
					</div>

					<div id="related_articles_list">

						<div class="title">
							<span>
								{{ $translations->related_articles }}
							</span>
						</div>

						<div class="articles">

							<div class="row content-slider">

								@foreach( $related_articles as $article )

									<div class="col-lg-4 col-md-4 col-sm-4">

										<div style="background-image: url({{ $article->thumbnail->featured_articles_image }});">

											<h4>
												{{ $article->title }}
											</h4>

											<div>

												<h3>
													{{ $article->title }}
												</h3>

												<p>
													{{ substr( $article->summary, 0, 100 ) . '[...]' }}
												</p>

												<a class="btn btn-primary" href="{{ $article->permalink }}">
													{{ $translations->read_article }} <i class="fa fa-angle-right"></i>
												</a>

											</div>

										</div>

									</div>

								@endforeach

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection