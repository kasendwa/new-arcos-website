@extends( 'site.structure' )

@section( 'title' )
	{{ $member->name }}
@endsection

@section( 'content' )

	<div class="container">

		<div class="row">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div id="page_content">

					<h3>
						{{ $member->name }}
					</h3>

					<div id="team_member_info">

						<div class="row">

							<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">

								<img src="{{ $member->avatar->small }}" class="img-fluid" />

								<p>

									<h4>
										<span>{{ $translations->position }}</span>
									</h4>

									{{ $member->position }}

								</p>

								<p>

									<h4>
										<span>{{ $translations->country }}</span>
									</h4>

									{{ $member->country }}

								</p>

							</div>

							<div class="col-lg-10 col-md-10 col-sm-9 col-xs-12">

								{!! $member->biography !!}

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

@endsection