<div id="featured_categories">

	@foreach( $categories as $category )

		<div class="featured_category">

			<div class="title">
				<a href="{{ $category->permalink }}">
					{{ $category->title }}
				</a>
			</div>

			<div class="featured-articles">

				<div class="row content-slider">

					@foreach( $category->featured_articles as $article )

						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">

							<div style="background-image: url({{ $article->thumbnail->featured_articles_image }});">

								<h4>{{ $article->title }}</h4>

								<div>

									<h3>
										{{ $article->title }}
									</h3>

									<p>
										{{ substr( $article->summary, 0, 100 ) . '[...]' }}
									</p>

									<a class="btn btn-primary" href="{{ $article->permalink }}">
										{{ $translations->read_article }} <i class="fa fa-angle-right"></i>
									</a>

								</div>

							</div>

						</div>

					@endforeach

				</div>

			</div>

		</div>

	@endforeach

</div>
