<div id="home_slider" class="carousel slide" data-ride="carousel">

	<div class="carousel-inner" role="listbox">

		@foreach( $sliders as $index => $slider )

			@if ( isset( $slider->thumbnail->slider_image ) )

				<{{ !empty( $slider->anchor->link ) && empty( $slider->anchor->text ) ? 'a href="' . $slider->anchor->link . '" ' : 'div' }} class="carousel-item @if ( $index == 0 ) active @endif" @if ( $slider->thumbnail->slider_image != null ) style="background-image: url({{ $slider->thumbnail->slider_image }});" @endif>

					<div class="carousel_container">

						<div class="carousel-caption">

							<div class="container">

								<div class="row">

									@if ( !empty( $slider->anchor->text ) )

										<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 text-lg-left text-md-left hidden-sm-down">

											<h3>{{ $slider->content->title }}</h3>

											@if ( !empty( $slider->content->text ) )
												<p>{{ $slider->content->text }}</p>
											@endif

										</div>

										<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-lg-right text-md-right text-sm-center text-xs-center">
											<a href="{{ $slider->anchor->link }}" class="btn btn-primary">
												{{ $slider->anchor->text }} <i class="fa fa-angle-right"></i>
											</a>
										</div>

									@else

										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-lg-left text-md-left hidden-sm-down">

											<h3>{{ $slider->content->title }}</h3>

											@if ( !empty( $slider->content->text ) )
												<p>{{ $slider->content->text }}</p>
											@endif

										</div>

									@endif

								</div>

							</div>

						</div>

					</div>

				</{{ !empty( $slider->anchor->link ) && empty( $slider->anchor->text ) ? 'a' : 'div' }}>

			@endif

		@endforeach

	</div>

	<a class="left carousel-control" href="#home_slider" role="button" data-slide="prev">
		<span aria-hidden="true">
			<i class="fa fa-angle-left"></i>
		</span>
		<span class="sr-only">Previous</span>
	</a>

	<a class="right carousel-control" href="#home_slider" role="button" data-slide="next">
		<span aria-hidden="true">
			<i class="fa fa-angle-right"></i>
		</span>
		<span class="sr-only">Next</span>
	</a>

</div>