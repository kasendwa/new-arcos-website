<footer>

	<div class="container">

		<div class="row">

			<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">

				@if ( isset( $menus[ 'footer-1' ] ) )

				<div>

					<h3>{{ $menus[ 'footer-1' ]->title }}</h3>

					<ul>

						@foreach( $menus[ 'footer-1' ]->items as $item )

						<li>
							<a href="{{ $item->anchor }}">
								{!! $item->title !!}
							</a>
						</li>

						@endforeach

					</ul>

				</div>

				@endif

			</div>

			<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">

				@if ( isset( $menus[ 'footer-2' ] ) )

					<div>

						<h3>{{ $menus[ 'footer-2' ]->title }}</h3>

						<ul>

							@foreach( $menus[ 'footer-2' ]->items as $item )

								<li>
									<a href="{{ $item->anchor }}">
										{!! $item->title !!}
									</a>
								</li>

							@endforeach

						</ul>

					</div>

				@endif

			</div>

			<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">

				@if ( isset( $menus[ 'footer-3' ] ) )

					<div>

						<h3>{{ $menus[ 'footer-3' ]->title }}</h3>

						<ul>

							@foreach( $menus[ 'footer-3' ]->items as $item )

								<li>
									<a href="{{ $item->anchor }}">
										{!! $item->title !!}
									</a>
								</li>

							@endforeach

						</ul>

					</div>

				@endif

			</div>

			<div class="col-lg-3 col-md-4 hidden-md-down">

				@if ( isset( $menus[ 'footer-4' ] ) )

					<div>

						<h3>{{ $menus[ 'footer-4' ]->title }}</h3>

						<ul>

							@foreach( $menus[ 'footer-4' ]->items as $item )

								<li>
									<a target="_blank" href="{{ $item->anchor }}">
										{!! $item->title !!}
									</a>
								</li>

							@endforeach

						</ul>

					</div>

				@endif

			</div>

		</div>

	</div>

</footer>