<div id="breakpoints">
	<span class="hidden-xl-down"></span>
	<span class="hidden-lg-down"></span>
	<span class="hidden-md-down"></span>
	<span class="hidden-sm-down"></span>
	<span class="hidden-xs-down"></span>
</div>

<header>

	<div class="container">

		<div class="row">

			<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">

				<a href="{{ url( '/' . $site_info->lang ) }}">
					<img src="{{ asset( 'img/logo.png' ) }}" />
				</a>

			</div>

			<div class="col-lg-10 col-md-9 col-sm-8 col-xs-6">

				<div id="language_selector">

					@foreach ( $languages as $language )

						@if ( $language->code != $site_info->lang )

							<a href="{{ url( '/' . $language->code ) }}">
								{{ $language->name }}
							</a>

						@endif

					@endforeach

				</div>

				{!! Form::open( [ 'class' => 'search_form hidden-sm-down', 'url' => url( $site_info->lang . '/search' ), 'method' => 'get' ] ) !!}

					<div class="form-group">
						<input type="text" autocomplete="false" class="form-control" name="s" id="s" placeholder="{{ $translations->search }}">
					</div>

					<a href="javascript:void(0);">
						<i class="fa fa-search"></i>
					</a>

				{!! Form::close() !!}

				<nav class="navbar navbar-light  hidden-md-down" id="desktop_menu">

					<ul class="nav navbar-nav">

						@foreach( $top_menu_items as $menu_item )

							@if ( count( $menu_item->children ) > 0 )

								<li class="nav-item dropdown">

									<a  href="javascript:void(0);" class="nav-link dropdown-toggle" id="{{ $menu_item->name }}" data-hover="dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-delay="50" data-close-others="true">
										{{ $menu_item->menu_title }}
									</a>

									<div class="dropdown-menu" aria-labelledby="{{ $menu_item->name }}">

										<div class="row content-slider">

											@foreach( $menu_item->children as $sub_menu_item )

												<a class="col-lg-3 col-md-3 col-sm-4 col-xs-6" href="{{ $sub_menu_item->permalink }}">

													<div title="{{ $sub_menu_item->menu_title }}">
														<div class="thumbnail-container" style="background-image: url({{ $sub_menu_item->thumbnail->menu_image }});"></div>
														<span>{{ $sub_menu_item->menu_title }}</span>
													</div>

												</a>

											@endforeach

										</div>

									</div>

								</li>

							@else

								<li class="nav-item">

									<a class="nav-link" href="{{ $menu_item->permalink }}">
										{{ $menu_item->menu_title }}
									</a>

								</li>

							@endif

						@endforeach

					</ul>

				</nav>

				<a href="javascript:void(0);" id="toggle_mobile_menu" class="navbar-toggle collapsed float-md-right hidden-lg-up float-sm-right float-xs-right" data-toggle="collapse" data-target="#mobile_menu" aria-expanded="false" aria-controls="navbar">
					<i class="fa fa-bars"></i>
					<i class="fa fa-times"></i> {{ $translations->menu }}
				</a>

				<nav id="mobile_menu" class="navbar navbar-collapse collapse">

					<ul class="nav navbar-nav">

						@foreach( $top_menu_items as $menu_item )

							@if ( count( $menu_item->children ) > 0 )

								<li class="nav-item dropdown">

									<a aria-haspopup="true" href="javascript:void(0);" data-toggle="dropdown" class="nav-link dropdown-toggle" id="{{ $menu_item->name }}">
										{{ $menu_item->menu_title }}
									</a>

									<div class="dropdown-menu" aria-labelledby="{{ $menu_item->name }}">

										@foreach( $menu_item->children as $sub_menu_item )

											<a class="dropdown-item" href="{{ $sub_menu_item->permalink }}">
												{{ $sub_menu_item->menu_title }}
											</a>

										@endforeach

									</div>

								</li>

							@else

								<li class="nav-item">

									<a class="nav-link" href="{{ $menu_item->permalink }}">
										{{ $menu_item->menu_title }}
									</a>

								</li>

							@endif

						@endforeach

					</ul>

				</nav>

			</div>

		</div>

	</div>

</header>
