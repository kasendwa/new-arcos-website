<!DOCTYPE html>

<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>@section( 'title' ){{ $translations->login }}@show &mdash; {{ $site_info->name }}</title>

		<link href="{{ asset( 'css/bootstrap.css' ) }}" rel="stylesheet">
		<script src="{{ asset( 'js/bootstrap.js' ) }}"></script>

		<link href="{{ asset( 'css/login.css' ) }}" rel="stylesheet">
		@yield( 'styles' )

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<link rel="shortcut icon" href="{!! asset( 'img/favicon.png' ) !!} ">

	</head>

	<body>

		<form method="post" id="login_form">

			{!! csrf_field() !!}

			<a href="{{ url( '/' . $site_info->lang ) }}">
				<img src="{{ asset( 'img/logo.png' ) }}" />
			</a>

			<div>
				<input type="text" autocomplete="false" name="username" class="form-control" value="{{ old( 'username' ) }}" placeholder="{{ $translations->username }}">
			</div>

			<div>
				<input type="password" name="password" id="password" class="form-control" placeholder="{{ $translations->password }}">
			</div>

			<div>
				<button type="submit" class="btn btn-block btn-success">
					<i class="fa fa-check"></i> {{ $translations->login }}
				</button>
			</div>

		</form>

		<script src="{{ asset( 'js/login.js' ) }}"></script>
		@yield( 'scripts' )

	</body>

</html>

