@extends( 'admin.structure' )

@section( 'content' )

	<h1>
		{{ $page_title }}
	</h1>

	{!! Form::open( [ 'method' => 'post', 'files' => true ] ) !!}

	<div class="row">

		<div class="col-lg-9 col-md-8">

			<ul class="nav nav-tabs" role="tablist">

				@foreach( $languages as $lang )

					<li class="nav-item">
						<a class="nav-link {{ ( $lang->code == $language ? 'active' : '' ) }}" data-toggle="tab" href="#{{ $lang->code }}_panel" role="tab">
							{{ $lang->name }}
						</a>
					</li>

				@endforeach

			</ul>

			<div class="tab-content">

				@foreach( $languages as $lang )

					<div class="tab-pane {{ ( $lang->code == $language ? 'active' : '' ) }}" id="{{ $lang->code }}_panel" role="tabpanel">

						<div class="form-group">
							{!! Form::label( 'title_' . $lang->code, $translations->form_translations->title->{ $lang->code } ) !!}
							{!! Form::text( 'title_' . $lang->code, '', [ 'class' => 'form-control', 'placeholder' => 'Title', 'required' => '' ] ) !!}
						</div>

						<div class="form-group">
							{!! Form::label( 'description_' . $lang->code, $translations->form_translations->description->{ $lang->code } ) !!}
							{!! Form::textarea( 'description_' . $lang->code, '', [ 'class' => 'form-control wysiwyg-minimal', 'placeholder' => 'Description' ] ) !!}
						</div>

					</div>

				@endforeach

			</div>

			<div class="form-group">
				{!! Form::submit( $translations->add_edition, [ 'class' => 'btn btn-primary btn-sm' ] ) !!}
			</div>

		</div>

		<div class="col-lg-3 col-md-4">

			<h4>File</h4>

			<div class="form-group">
				{!! Form::label( 'file', 'Choose a file' ) !!}
				{!! Form::file( 'file' ) !!}
			</div>

			<h4>Newsletter</h4>

			<div class="form-group">

				{!! Form::label( 'newsletter', $translations->newsletter ) !!}

				<select name="newsletter" class="form-control">
					@foreach( $newsletters as $id => $label )
						<option value="{{ $id }}">{{ $label }}</option>
					@endforeach
				</select>

			</div>

			<div class="form-group">

				{!! Form::label( 'status', $translations->status ) !!}

				<select name="status" class="form-control">
					<option value="draft">{{ $translations->draft }}</option>
					<option value="sent">{{ $translations->sent }}</option>
				</select>

			</div>

		</div>

	</div>

	{!! Form::close() !!}

@endsection