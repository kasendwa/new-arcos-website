@extends( 'admin.structure' )

@section( 'content' )

	<h1>
		{{ $page_title }}
	</h1>

	<table class="table table-striped table-bordered">

		<thead>

			<tr>
				<th>
					#
				</th>
				<th>
					{{ $translations->first_name }}
				</th>
				<th>
					{{ $translations->last_name }}
				</th>
				<th>
					{{ $translations->email_address }}
				</th>
				<th>
					{{ $translations->amount }}
				</th>
				<th>
					{{ $translations->status }}
				</th>
				<th>
				</th>
			</tr>

		</thead>

		<tbody>

			@foreach( $items as $item )

				<tr>

					<td class="column-id">
						{{ $item->id }}
					</td>

					<td class="column-name">
						{{ $item->first_name }}
					</td>

					<td class="column-name">
						{{ $item->last_name }}
					</td>

					<td class="column-email">
						{{ $item->email }}
					</td>

					<td class="column-amount">
						{{ '$' . $item->amount }}
					</td>

					<td class="column-amount">
						{{ $item->status }}
					</td>

					<td class="column-timestamp">
						{{ $item->timestamp }}
					</td>

				</tr>

			@endforeach

		</tbody>

		<thead>

		<tr>
			<th>
				#
			</th>
			<th>
				{{ $translations->first_name }}
			</th>
			<th>
				{{ $translations->last_name }}
			</th>
			<th>
				{{ $translations->email_address }}
			</th>
			<th>
				{{ $translations->amount }}
			</th>
			<th>
				{{ $translations->status }}
			</th>
			<th>
			</th>
		</tr>

		</thead>

	</table>

	<div class="items_pagination">

		<ul class="pagination">

			<li class="page-item {{ ( $pagination->active == 1 ? 'disabled' : '' ) }}">
				<a class="page-link" href="{{ $pagination->base_link . '/' . ( $pagination->active == 1 ? 1 : ( $pagination->active - 1 ) ) }}">
					<i class="fa fa-angle-double-left"></i>
				</a>
			</li>

			@for( $i = 1; $i <= $pagination->total; $i++ )

				<li class="page-item {{ ( $pagination->active == $i ? 'active' : '' ) }}">
					<a class="page-link" href="{{ $pagination->base_link . '/' . $i }}">
						{{ $i }}
					</a>
				</li>

			@endfor

			<li class="page-item {{ ( $pagination->active == $pagination->total ? 'disabled' : '' ) }}">
				<a class="page-link" href="{{ $pagination->base_link . '/' . ( $pagination->active == $pagination->total ? $pagination->total : ( $pagination->active + 1 ) ) }}">
					<i class="fa fa-angle-double-right"></i>
				</a>
			</li>

		</ul>

	</div>

@endsection