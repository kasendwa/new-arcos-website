@extends( 'admin.structure' )

@section( 'content' )

	<h1>
		{{ $page_title }}
	</h1>

	{!! Form::open( [ 'method' => 'post', 'files' => true ] ) !!}

	<div class="row">

		<div class="col-lg-9 col-md-8">

			<ul class="nav nav-tabs" role="tablist">

				@foreach( $languages as $lang )

					<li class="nav-item">
						<a class="nav-link {{ ( $lang->code == $language ? 'active' : '' ) }}" data-toggle="tab" href="#{{ $lang->code }}_panel" role="tab">
							{{ $lang->name }}
						</a>
					</li>

				@endforeach

			</ul>

			<div class="tab-content">

				@foreach( $languages as $lang )

					<div class="tab-pane {{ ( $lang->code == $language ? 'active' : '' ) }}" id="{{ $lang->code }}_panel" role="tabpanel">

						<div class="form-group">
							{!! Form::label( 'title_' . $lang->code, $translations->form_translations->title->{ $lang->code } ) !!}
							{!! Form::text( 'title_' . $lang->code, '', [ 'class' => 'form-control', 'placeholder' => 'Title', 'required' => '' ] ) !!}
						</div>

						<div class="form-group">
							{!! Form::label( 'content_' . $lang->code, $translations->form_translations->content->{ $lang->code } ) !!}
							{!! Form::textarea( 'content_' . $lang->code, '', [ 'class' => 'form-control wysiwyg-large', 'placeholder' => 'Content' ] ) !!}
						</div>

						<div class="form-group">
							{!! Form::label( 'summary_' . $lang->code, $translations->form_translations->summary->{ $lang->code } ) !!}
							{!! Form::textarea( 'summary_' . $lang->code, '', [ 'class' => 'form-control wysiwyg-minimal', 'placeholder' => 'Summary' ] ) !!}
						</div>

						<h4>{{ $translations->form_translations->menu->{ $lang->code } }}</h4>

						<div class="form-group">
							{!! Form::label( 'menu_title_' . $lang->code, $translations->form_translations->menu_title->{ $lang->code } ) !!}
							{!! Form::text( 'menu_title_' . $lang->code, '', [ 'class' => 'form-control', 'placeholder' => 'Menu title' ] ) !!}
						</div>

					</div>

				@endforeach

			</div>

			<div class="row">

				<div class="col-lg-4 col-md-4">
					<div class="form-group">
						{!! Form::label( 'show_in_menu', $translations->show_in_menu ) !!}
						<br />
						{!! Form::checkbox( 'show_in_menu', 1, false ) !!} {{ $translations->show_this_item }}
					</div>
				</div>

				<div class="col-lg-4 col-md-4">
					<div class="form-group">
						{!! Form::label( 'menu_order', $translations->menu_order ) !!}
						{!! Form::text( 'menu_order', 0, [ 'class' => 'form-control' ] ) !!}
					</div>
				</div>

			</div>

			<div class="form-group">
				{!! Form::submit( $translations->add_page, [ 'class' => 'btn btn-primary btn-sm' ] ) !!}
			</div>

		</div>

		<div class="col-lg-3 col-md-4">

			<h4>Featured Image</h4>

			<div class="form-group">
				{!! Form::label( 'featured_image', 'Choose an image' ) !!}
				{!! Form::file( 'featured_image' ) !!}
			</div>

			<div class="form-group">

				{!! Form::label( 'parent', 'Page parent' ) !!}

				<select name="parent" class="form-control">

					@foreach( $parents as $id => $name )
						<option value="{{ $id }}">{{ $name }}</option>
					@endforeach

				</select>

			</div>

			<div class="form-group">

				{!! Form::label( 'template', 'Page template' ) !!}

				<select name="template" class="form-control">

					@foreach( $templates as $key => $value )
						<option value="{{ $key }}" data-has-related-articles="{{ $value->has_related_articles }}" data-has-related-projects="{{ $value->has_related_projects }}">{{ $value->name }}</option>
					@endforeach

				</select>

			</div>

			<div id="related_articles_container">

				<div class="form-group">

					{!! Form::label( 'related_articles', 'Related articles' ) !!}

					<div class="checklist_container">

						<ul>

							@foreach( $articles->list as $id => $title )

								<li class="form-group">
									<label title="{{ $title }}">
										{!! Form::checkbox( 'related_articles[]', $id, ( in_array( $id, $articles->selected ) ? true : false ) ) !!}
										{{ $title }}
									</label>
								</li>

							@endforeach

						</ul>

					</div>

				</div>

			</div>

			<div id="related_projects_container">

				<div class="form-group">

					{!! Form::label( 'related_projects', 'Related projects' ) !!}

					<div class="checklist_container">

						<ul>

							@foreach( $projects->list as $id => $title )

								<li class="form-group">
									<label title="{{ $title }}">
										{!! Form::checkbox( 'related_projects[]', $id, ( in_array( $id, $projects->selected ) ? true : false ) ) !!}
										{{ $title }}
									</label>
								</li>

							@endforeach

						</ul>

					</div>

				</div>

			</div>

		</div>

	</div>

	{!! Form::close() !!}

@endsection