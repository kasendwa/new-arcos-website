@extends( 'admin.structure' )

@section( 'content' )

	<h1>
		{{ $page_title }}

		<a class="btn btn-primary" href="{{ $items_actions_base . '/add' }}">
			{{ $translations->add_website }}
		</a>
	</h1>

	<table class="table table-striped table-bordered">

		<thead>

			<tr>
				<th>
					#
				</th>
				<th>
					{{ $translations->thumbnail }}
				</th>
				<th>
					{{ $translations->title }}
				</th>
				<th>
					{{ $translations->url }}
				</th>
				<th>
					{{ $translations->order }}
				</th>
			</tr>

		</thead>

		<tbody>

			@foreach( $items as $item )

				<tr>

					<td class="column-id">
						{{ $item->id }}
					</td>

					<td class="column-thumbnail">
						@if ( isset( $item->screenshot->thumbnail ) )
							<img src="{{ $item->screenshot->thumbnail }}" />
						@else
							<div></div>
						@endif
					</td>

					<td class="column-title">

						<h4>
							<a href="{{ $items_actions_base . '/edit/' . $item->id }}">
								{{ $item->name }}
							</a>
						</h4>

						<div>

							<a href="{{ $items_actions_base . '/edit/' . $item->id }}">
								{{ $translations->edit }}
							</a>

							<a class="del" href="{{ $items_actions_base . '/delete/' . $item->id }}" onclick="return confirm( '{{ $translations->are_you_sure_you_want_to_delete_this_record }}' )">
								{{ $translations->trash }}
							</a>

						</div>

					</td>

					<td class="column-url">
						{{ $item->url }}
					</td>

					<td class="column-order">
						{{ $item->order }}
					</td>

				</tr>

			@endforeach

		</tbody>

		<thead>

		<tr>
			<th>
				#
			</th>
			<th>
				{{ $translations->thumbnail }}
			</th>
			<th>
				{{ $translations->title }}
			</th>
			<th>
				{{ $translations->url }}
			</th>
			<th>
				{{ $translations->order }}
			</th>
		</tr>

		</thead>

	</table>

	<div class="items_pagination">

		<ul class="pagination">

			<li class="page-item {{ ( $pagination->active == 1 ? 'disabled' : '' ) }}">
				<a class="page-link" href="{{ $pagination->base_link . '/' . ( $pagination->active == 1 ? 1 : ( $pagination->active - 1 ) ) }}">
					<i class="fa fa-angle-double-left"></i>
				</a>
			</li>

			@for( $i = 1; $i <= $pagination->total; $i++ )

				<li class="page-item {{ ( $pagination->active == $i ? 'active' : '' ) }}">
					<a class="page-link" href="{{ $pagination->base_link . '/' . $i }}">
						{{ $i }}
					</a>
				</li>

			@endfor

			<li class="page-item {{ ( $pagination->active == $pagination->total ? 'disabled' : '' ) }}">
				<a class="page-link" href="{{ $pagination->base_link . '/' . ( $pagination->active == $pagination->total ? $pagination->total : ( $pagination->active + 1 ) ) }}">
					<i class="fa fa-angle-double-right"></i>
				</a>
			</li>

		</ul>

	</div>

@endsection