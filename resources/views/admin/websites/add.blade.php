@extends( 'admin.structure' )

@section( 'content' )

	<h1>
		{{ $page_title }}
	</h1>

	{!! Form::open( [ 'method' => 'post', 'files' => true ] ) !!}

	<div class="row">

		<div class="col-lg-9 col-md-8">

			<ul class="nav nav-tabs" role="tablist">

				@foreach( $languages as $lang )

					<li class="nav-item">
						<a class="nav-link {{ ( $lang->code == $language ? 'active' : '' ) }}" data-toggle="tab" href="#{{ $lang->code }}_panel" role="tab">
							{{ $lang->name }}
						</a>
					</li>

				@endforeach

			</ul>

			<div class="tab-content">

				@foreach( $languages as $lang )

					<div class="tab-pane {{ ( $lang->code == $language ? 'active' : '' ) }}" id="{{ $lang->code }}_panel" role="tabpanel">

						<div class="form-group">
							{!! Form::label( 'name_' . $lang->code, $translations->form_translations->title->{ $lang->code } ) !!}
							{!! Form::text( 'name_' . $lang->code, '', [ 'class' => 'form-control', 'placeholder' => $translations->form_translations->title->{ $lang->code }, 'required' => '' ] ) !!}
						</div>

						<div class="form-group">
							{!! Form::label( 'summary_' . $lang->code, $translations->form_translations->summary->{ $lang->code } ) !!}
							{!! Form::textarea( 'summary_' . $lang->code, '', [ 'class' => 'form-control wysiwyg-minimal', 'placeholder' => $translations->form_translations->content->{ $lang->code } ] ) !!}
						</div>

					</div>

				@endforeach

			</div>

			<div class="form-group">
				{!! Form::label( 'url', $translations->url ) !!}
				{!! Form::text( 'url', '', [ 'class' => 'form-control', 'placeholder' => 'URL' ] ) !!}
			</div>

			<div class="form-group">
				{!! Form::label( 'order', $translations->order ) !!}
				{!! Form::number( 'order', '', [ 'class' => 'form-control', 'placeholder' => 'Order' ] ) !!}
			</div>

			<div class="form-group">
				{!! Form::submit( $translations->update_website, [ 'class' => 'btn btn-primary btn-sm' ] ) !!}
			</div>

		</div>

		<div class="col-lg-3 col-md-4">

			<h4>Screenshot</h4>

			<div class="form-group">
				{!! Form::label( 'image', 'Choose an image' ) !!}
				{!! Form::file( 'image' ) !!}
			</div>

		</div>

	</div>

	{!! Form::close() !!}

@endsection