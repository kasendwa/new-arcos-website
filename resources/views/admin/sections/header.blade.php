<header>

	<div class="container">

		<div class="row">

			<div class="col-lg-8 col-md-7">
				<a href="{{ url( $language . '/admin' ) }}">
					{{ $site_info->name }}
				</a>
			</div>

			<div class="col-lg-4 col-md-5">

				<ul class="nav navbar-nav navbar-right pull-right">

					<li class="dropdown">

						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-user"></i>
							{{ $current_user->name->first . ' ' . $current_user->name->last }}
						</a>

						<ul class="dropdown-menu">
							<li>
								<a href="{{ url( $language . '/logout' ) }}">
									Logout
								</a>
							</li>
						</ul>
					</li>
				</ul>

			</div>

		</div>

	</div>

</header>