<div id="sidebar_menu">

	<ul class="nav flex-column">

		<li>
			<span>Post types</span>
		</li>

		@foreach( $post_types as $post_type )

			<li class="nav-item">
				<a title="{{ $post_type->label->plural->{ $language } }}" class="nav-link @if ( isset( $active_menu ) && $active_menu == $post_type->label->plural->{ $language } ) active @endif" href="{{ url( $language . '/admin/' . $post_type->rewrite->admin->archive ) }}">
					{{ $post_type->label->plural->{ $language } }}
				</a>
			</li>

		@endforeach

		<li>
			<span>Taxonomies</span>
		</li>

		@foreach( $taxonomies as $taxonomy )

			<li class="nav-item">
				<a title="{{ $taxonomy->label->plural->{ $language } }}" class="nav-link @if ( isset( $active_menu ) && $active_menu == $taxonomy->label->plural->{ $language } ) active @endif" href="{{ url( $language . '/admin/' . $taxonomy->rewrite->admin->archive ) }}">
					{{ strlen( $taxonomy->label->plural->{ $language } ) > 18 ? substr( $taxonomy->label->plural->{ $language }, 0, 17 ) . '...' : $taxonomy->label->plural->{ $language } }}
				</a>
			</li>

		@endforeach

	</ul>

</div>