@extends( 'admin.structure' )

@section( 'content' )

	<h1>
		{{ $page_title }}
	</h1>

	{!! Form::open( [ 'method' => 'post', 'files' => true ] ) !!}

	<div class="row">

		<div class="col-lg-9 col-md-8">

			<div class="form-group">
				{!! Form::label( 'name', 'Name' ) !!}
				{!! Form::text( 'name', '', [ 'class' => 'form-control', 'placeholder' => 'Name' ] ) !!}
			</div>

			<ul class="nav nav-tabs" role="tablist">

				@foreach( $languages as $lang )

					<li class="nav-item">
						<a class="nav-link {{ ( $lang->code == $language ? 'active' : '' ) }}" data-toggle="tab" href="#{{ $lang->code }}_panel" role="tab">
							{{ $lang->name }}
						</a>
					</li>

				@endforeach

			</ul>

			<div class="tab-content">

				@foreach( $languages as $lang )

					<div class="tab-pane {{ ( $lang->code == $language ? 'active' : '' ) }}" id="{{ $lang->code }}_panel" role="tabpanel">

						<div class="form-group">
							{!! Form::label( 'country_' . $lang->code, $translations->form_translations->country->{ $lang->code } ) !!}
							{!! Form::text( 'country_' . $lang->code, '', [ 'class' => 'form-control', 'placeholder' => 'Country' ] ) !!}
						</div>

						<div class="form-group">
							{!! Form::label( 'position_' . $lang->code, $translations->form_translations->position->{ $lang->code } ) !!}
							{!! Form::text( 'position_' . $lang->code, '', [ 'class' => 'form-control', 'placeholder' => 'Position' ] ) !!}
						</div>

						<div class="form-group">
							{!! Form::label( 'biography_' . $lang->code, $translations->form_translations->biography->{ $lang->code } ) !!}
							{!! Form::textarea( 'biography_' . $lang->code, '', [ 'class' => 'form-control wysiwyg-large', 'placeholder' => 'Biography' ] ) !!}
						</div>

					</div>

				@endforeach

			</div>

			<div class="form-group">
				{!! Form::label( 'order', $translations->order ) !!}
				{!! Form::number( 'order', '', [ 'class' => 'form-control', 'placeholder' => 'Order' ] ) !!}
			</div>

			<div class="form-group">
				{!! Form::submit( $translations->add_team_member, [ 'class' => 'btn btn-primary btn-sm' ] ) !!}
			</div>

		</div>

		<div class="col-lg-3 col-md-4">

			<h4>Avatar</h4>

			<div class="form-group">
				{!! Form::label( 'avatar', 'Choose an image' ) !!}
				{!! Form::file( 'avatar' ) !!}
			</div>

			<div class="form-group">

				{!! Form::label( 'team_category', 'Team category' ) !!}

				<select name="team_category" class="form-control">

					@foreach( $team_categories as $id => $name )
						<option value="{{ $id }}">{{ $name }}</option>
					@endforeach

				</select>

			</div>

		</div>

	</div>

	{!! Form::close() !!}

@endsection