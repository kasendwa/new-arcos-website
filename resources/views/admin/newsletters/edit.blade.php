@extends( 'admin.structure' )

@section( 'content' )

	<h1>
		{{ $page_title }}
	</h1>

	{!! Form::open( [ 'method' => 'post', 'files' => true ] ) !!}

	<div class="row">

		<div class="col-lg-9 col-md-8">

			<ul class="nav nav-tabs" role="tablist">

				@foreach( $languages as $lang )

					<li class="nav-item">
						<a class="nav-link {{ ( $lang->code == $language ? 'active' : '' ) }}" data-toggle="tab" href="#{{ $lang->code }}_panel" role="tab">
							{{ $lang->name }}
						</a>
					</li>

				@endforeach

			</ul>

			<div class="tab-content">

				@foreach( $languages as $lang )

					<div class="tab-pane {{ ( $lang->code == $language ? 'active' : '' ) }}" id="{{ $lang->code }}_panel" role="tabpanel">

						<div class="form-group">
							{!! Form::label( 'title_' . $lang->code, $translations->form_translations->title->{ $lang->code } ) !!}
							{!! Form::text( 'title_' . $lang->code, $item_data->title->{ $lang->code }, [ 'class' => 'form-control', 'placeholder' => 'Title', 'required' => '' ] ) !!}
						</div>

						<div class="form-group">
							{!! Form::label( 'description_' . $lang->code, $translations->form_translations->description->{ $lang->code } ) !!}
							{!! Form::textarea( 'description_' . $lang->code, $item_data->description->{ $lang->code }, [ 'class' => 'form-control wysiwyg-minimal', 'placeholder' => 'Description' ] ) !!}
						</div>

					</div>

				@endforeach

			</div>

			<div class="form-group">
				{!! Form::hidden( 'page_id', $item_data->id ) !!}
				{!! Form::submit( $translations->update_newsletter, [ 'class' => 'btn btn-primary btn-sm' ] ) !!}
			</div>

		</div>

		<div class="col-lg-3 col-md-4">

			<h4>{{ $translations->thumbnail }}</h4>

			@if ( isset( $item_data->thumbnail->thumbnail ) )
				<img src="{{ $item_data->thumbnail->featured_articles_image }}" class="img-fluid" />
			@endif

			<div class="form-group">
				{!! Form::label( 'image', 'Choose an image' ) !!}
				{!! Form::file( 'image' ) !!}
			</div>

			<h4>Mailman</h4>

			<div class="form-group">

				{!! Form::label( 'mailman_list', 'Mailman List' ) !!}

				<select name="mailman_list" class="form-control">
					@foreach( $lists as $name => $label )
						<option value="{{ $name }}" {{ $item_data->mailman_list == $name ? ' selected ' : '' }}>{{ $label->{ $language } }}</option>
					@endforeach
				</select>

			</div>

		</div>

	</div>

	{!! Form::close() !!}

@endsection