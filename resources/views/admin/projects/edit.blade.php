@extends( 'admin.structure' )

@section( 'content' )

	<h1>
		{{ $page_title }}
	</h1>

	{!! Form::open( [ 'method' => 'post', 'files' => true ] ) !!}

	<div class="row">

		<div class="col-lg-9 col-md-8">

			<ul class="nav nav-tabs" role="tablist">

				@foreach( $languages as $lang )

					<li class="nav-item">
						<a class="nav-link {{ ( $lang->code == $language ? 'active' : '' ) }}" data-toggle="tab" href="#{{ $lang->code }}_panel" role="tab">
							{{ $lang->name }}
						</a>
					</li>

				@endforeach

			</ul>

			<div class="tab-content">

				@foreach( $languages as $lang )

					<div class="tab-pane {{ ( $lang->code == $language ? 'active' : '' ) }}" id="{{ $lang->code }}_panel" role="tabpanel">

						<div class="form-group">
							{!! Form::label( 'title_' . $lang->code, $translations->form_translations->title->{ $lang->code } ) !!}
							{!! Form::text( 'title_' . $lang->code, $item_data->title->{ $lang->code }, [ 'class' => 'form-control', 'placeholder' => 'Title', 'required' => '' ] ) !!}
						</div>

						<div class="form-group">
							{!! Form::label( 'content_' . $lang->code, $translations->form_translations->content->{ $lang->code } ) !!}
							{!! Form::textarea( 'content_' . $lang->code, $item_data->content->{ $lang->code }, [ 'class' => 'form-control wysiwyg-large', 'placeholder' => 'Content' ] ) !!}
						</div>

						<div class="form-group">
							{!! Form::label( 'summary_' . $lang->code, $translations->form_translations->summary->{ $lang->code } ) !!}
							{!! Form::textarea( 'summary_' . $lang->code, $item_data->summary->{ $lang->code }, [ 'class' => 'form-control wysiwyg-minimal', 'placeholder' => 'Summary' ] ) !!}
						</div>

					</div>

				@endforeach

			</div>

			<div>
				<div class="form-group">
					{!! Form::label( 'name', $translations->slug ) !!}
					{!! Form::text( 'name', $item_data->name, [ 'class' => 'form-control', 'placeholder' => 'Menu title' ] ) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::hidden( 'page_id', $item_data->id ) !!}
				{!! Form::submit( $translations->update_project, [ 'class' => 'btn btn-primary btn-sm' ] ) !!}
			</div>

		</div>

		<div class="col-lg-3 col-md-4">

			<h4>Featured Image</h4>

			@if ( isset( $item_data->thumbnail->thumbnail ) )
				<img src="{{ $item_data->thumbnail->featured_articles_image }}" class="img-fluid" />
			@endif

			<div class="form-group">
				{!! Form::label( 'featured_image', 'Choose an image' ) !!}
				{!! Form::file( 'featured_image' ) !!}
			</div>

			<div id="related_articles_container">

				<div class="form-group">

					{!! Form::label( 'related_articles', 'Related articles' ) !!}

					<div class="checklist_container">

						<ul>

							@foreach( $related_articles->list as $id => $title )

								<li class="form-group">
									<label title="{{ $title }}">
										{!! Form::checkbox( 'related_articles[]', $id, ( in_array( $id, $related_articles->selected ) ? true : false ) ) !!}
										{{ $title }}
									</label>
								</li>

							@endforeach

						</ul>

					</div>

				</div>

			</div>

		</div>

	</div>

	{!! Form::close() !!}

@endsection