@extends( 'admin.structure' )

@section( 'content' )

	<h1>
		{{ $page_title }}
	</h1>

	{!! Form::open( [ 'method' => 'post', 'files' => true ] ) !!}

	<div class="row">

		<div class="col-lg-9 col-md-8">

			<ul class="nav nav-tabs" role="tablist">

				@foreach( $languages as $lang )

					<li class="nav-item">
						<a class="nav-link {{ ( $lang->code == $language ? 'active' : '' ) }}" data-toggle="tab" href="#{{ $lang->code }}_panel" role="tab">
							{{ $lang->name }}
						</a>
					</li>

				@endforeach

			</ul>

			<div class="tab-content">

				@foreach( $languages as $lang )

					<div class="tab-pane {{ ( $lang->code == $language ? 'active' : '' ) }}" id="{{ $lang->code }}_panel" role="tabpanel">

						<div class="form-group">
							{!! Form::label( 'title_' . $lang->code, $translations->form_translations->title->{ $lang->code } ) !!}
							{!! Form::text( 'title_' . $lang->code, $item_data->content->title->{ $lang->code }, [ 'class' => 'form-control', 'placeholder' => 'Title' ] ) !!}
						</div>

						<div class="form-group">
							{!! Form::label( 'text_' . $lang->code, $translations->form_translations->description->{ $lang->code } ) !!}
							{!! Form::textarea( 'text_' . $lang->code, $item_data->content->text->{ $lang->code }, [ 'class' => 'form-control wysiwyg-minimal', 'placeholder' => 'Description' ] ) !!}
						</div>

						<div class="form-group">
							{!! Form::label( 'button_text_' . $lang->code, $translations->form_translations->button_text->{ $lang->code } ) !!}
							{!! Form::text( 'button_text_' . $lang->code, $item_data->anchor->text->{ $lang->code }, [ 'class' => 'form-control', 'placeholder' => $translations->form_translations->button_text->{ $lang->code } ] ) !!}
						</div>

					</div>

				@endforeach

			</div>

			<div class="form-group">
				{!! Form::label( 'order', $translations->order ) !!}
				{!! Form::number( 'order', $item_data->order, [ 'class' => 'form-control', 'placeholder' => 'Order' ] ) !!}
			</div>

			<div class="form-group">
				{!! Form::submit( $translations->update_slider, [ 'class' => 'btn btn-primary btn-sm' ] ) !!}
			</div>

		</div>

		<div class="col-lg-3 col-md-4">

			<h4>Thumbnail</h4>

			@if ( isset( $item_data->thumbnail->thumbnail ) )
				<img src="{{ $item_data->thumbnail->featured_articles_image }}" class="img-fluid" />
			@endif

			<div class="form-group">
				{!! Form::label( 'image', 'Choose an image' ) !!}
				{!! Form::file( 'image' ) !!}
			</div>

			<h4>{{ $translations->anchor }}</h4>

			<div class="form-group">

				{!! Form::label( 'entity_type', 'Entity type' ) !!}

				<select name="entity_type" class="form-control">

					<option value="" @if ( $item_data->entity->type == '' ) selected @endif>-</option>
					<option value="post_type" @if ( $item_data->entity->type == 'post_type' ) selected @endif>{{ $entity_types->post_type }}</option>
					<option value="taxonomy" @if ( $item_data->entity->type == 'taxonomy' ) selected @endif>{{ $entity_types->taxonomy }}</option>

				</select>

			</div>

			<div class="form-group">

				{!! Form::label( 'entity_name', 'Entity name' ) !!}

				<select name="entity_name" class="form-control">
				</select>

			</div>

			<div class="form-group">

				{!! Form::label( 'entity_id', 'Entity' ) !!}

				<select name="entity_id" class="form-control">
				</select>

			</div>

			<div class="form-group">

				{!! Form::label( 'custom_link', $translations->custom_link ) !!}

				{!! Form::text( 'custom_link', $item_data->anchor->link, [ 'class' => 'form-control', 'placeholder' => 'Custom link' ] ) !!}

			</div>

		</div>

	</div>

	{!! Form::close() !!}

	<script type="text/javascript">
		var init_entity_type = '{{ $item_data->entity->type }}',
			init_entity_name = '{{ $item_data->entity->name }}',
			init_entity_id = '{{ $item_data->entity->id }}';

		var entity_type = $( '[name="entity_type"]' ),
			entity_name = $( '[name="entity_name"]' ),
			entity_id = $( '[name="entity_id"]' ),
			custom_link = $( '[name="custom_link"]' );

		entity_name.on( 'change', function() {
			entity_id.attr( 'disabled', 'disabled' );

			$.get(
					'{{ url( $language . '/admin/api' ) }}',
					{
						method: 'get_entity_entries',
						language: '{{ $language }}',
						entity_type: entity_type.val(),
						entity_name: entity_name.val()
					},
					function( response ) {
						if ( response.result == 'successful' ) {
							entity_id.html( '' );

							$.each( response.entries, function( index, value ) {
								var html = '<option value="' + value.name + '" ' + ( init_entity_id == '' + value.name ? ' selected ' : '' ) + '>' +
										value.label +
									'</option>';

								entity_id.append( html );
							} );
						}

						entity_id.removeAttr( 'disabled' );
					},
					'json'
			);
		} );

		entity_type.on( 'change', function() {
			if ( $( this ).val() == '' ) {
				entity_name.parent( 'div' ).hide();
				entity_id.parent( 'div' ).hide();

				entity_name.val( '' );
				entity_id.val( '' );

				custom_link.parent( 'div' ).show();
			}

			else {
				entity_name.parent( 'div' ).show();
				entity_name.attr( 'disabled', 'disabled' );

				entity_id.parent( 'div' ).show();
				entity_id.attr( 'disabled', 'disabled' );

				custom_link.val( '' );

				var type = $( this ).val();

				var method = type == 'post_type' ? 'get_post_types' : 'get_taxonomies';

				$.get(
					'{{ url( $language . '/admin/api' ) }}',
					{
						method: method,
						language: '{{ $language }}'
					},
					function( response ) {
						if ( response.result == 'successful' ) {
							entity_name.html( '' );

							$.each( response.entries, function( index, value ) {
								var html = '<option value="' + value.name + '" ' + ( init_entity_name == value.name ? ' selected ' : '' ) + '>' +
										value.label +
									'</option>';

								entity_name.append( html );
							} );
						}

						entity_name.removeAttr( 'disabled' ).trigger( 'change' );
					},
					'json'
				);

				custom_link.parent( 'div' ).hide();
			}
		} ).trigger( 'change' );
	</script>

@endsection