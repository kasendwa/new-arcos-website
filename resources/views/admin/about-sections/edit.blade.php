@extends( 'admin.structure' )

@section( 'content' )

	<h1>
		{{ $page_title }}
	</h1>

	{!! Form::open( [ 'method' => 'post', 'files' => true ] ) !!}

	<div class="row">

		<div class="col-lg-9 col-md-8">

			<ul class="nav nav-tabs" role="tablist">

				@foreach( $languages as $lang )

					<li class="nav-item">
						<a class="nav-link {{ ( $lang->code == $language ? 'active' : '' ) }}" data-toggle="tab" href="#{{ $lang->code }}_panel" role="tab">
							{{ $lang->name }}
						</a>
					</li>

				@endforeach

			</ul>

			<div class="tab-content">

				@foreach( $languages as $lang )

					<div class="tab-pane {{ ( $lang->code == $language ? 'active' : '' ) }}" id="{{ $lang->code }}_panel" role="tabpanel">

						<div class="form-group">
							{!! Form::label( 'title_' . $lang->code, $translations->form_translations->title->{ $lang->code } ) !!}
							{!! Form::text( 'title_' . $lang->code, $item_data->title->{ $lang->code }, [ 'class' => 'form-control', 'placeholder' => $translations->form_translations->title->{ $lang->code }, 'required' => '' ] ) !!}
						</div>

						<div class="form-group">
							{!! Form::label( 'content_' . $lang->code, $translations->form_translations->content->{ $lang->code } ) !!}
							{!! Form::textarea( 'content_' . $lang->code, $item_data->content->{ $lang->code }, [ 'class' => 'form-control wysiwyg-minimal', 'placeholder' => $translations->form_translations->content->{ $lang->code } ] ) !!}
						</div>

					</div>

				@endforeach

			</div>

			<div class="form-group">
				{!! Form::label( 'order', $translations->order ) !!}
				{!! Form::number( 'order', $item_data->order, [ 'class' => 'form-control', 'placeholder' => 'Order' ] ) !!}
			</div>

			<div class="form-group">
				{!! Form::submit( $translations->update_about_section, [ 'class' => 'btn btn-primary btn-sm' ] ) !!}
			</div>

		</div>

		<div class="col-lg-3 col-md-4">

			<h4>Thumbnail</h4>

			@if ( isset( $item_data->thumbnail->thumbnail ) )
				<img src="{{ $item_data->thumbnail->featured_articles_image }}" class="img-fluid" />
			@endif

			<div class="form-group">
				{!! Form::label( 'avatar', 'Choose an image' ) !!}
				{!! Form::file( 'image' ) !!}
			</div>

		</div>

	</div>

	{!! Form::close() !!}

@endsection