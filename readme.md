# New ARCOS website

Welcome to the new ARCOS (Abertine Rift Conservation Society) website development repository.

####Requirements

    PHP >= 5.5.9
    OpenSSL PHP Extension
    Mbstring PHP Extension
    Tokenizer PHP Extension
    SQL server(for example MySQL)
    Composer
    Node JS
