$.each( $( '.wysiwyg-large' ), function( $index, $el ) {
	$( $el ).summernote({
		height: 300,
		toolbar: [
			[ 'style', [ 'bold', 'italic', 'underline' ] ],
			[ 'font', [ 'style', 'fontsize', 'color' ] ],
			[ 'paragraph', [ 'strikethrough', 'superscript', 'subscript', 'ul', 'ol', 'paragraph' ] ],
			[ 'insert', [ 'link', 'picture', 'video', 'table' ] ],
			[ 'misc', [ 'codeview', 'fullscreen' ] ]
		]
	});
});

$.each( $( '.wysiwyg-minimal' ), function( $index, $el ) {
	$( $el ).summernote({
		height: 100,
		toolbar: false
	});
});

$( 'select[name="template"]' ).on( 'change', function() {
	var $template = $( this ).val();

	if ( $( this ).find( 'option[value="' + $template + '"]' ).attr( 'data-has-related-articles' ) == '1' )
		$( '#related_articles_container' ).show();

	else
		$( '#related_articles_container' ).hide();

	if ( $( this ).find( 'option[value="' + $template + '"]' ).attr( 'data-has-related-projects' ) == '1' )
		$( '#related_projects_container' ).show();

	else
		$( '#related_projects_container' ).hide();
}).trigger( 'change' );
//# sourceMappingURL=admin.js.map
