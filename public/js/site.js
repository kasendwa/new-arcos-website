$( '#language_selector select' ).on( 'blur', function() {
	window.location = '/' + $( this ).val() + window.location.pathname.substr( 3 );
});

function setup_content_sliders() {
	setup_header_menu_sliders();
	setup_featured_articles_sliders();
}

	function setup_header_menu_sliders() {
		$( '#desktop_menu .dropdown > a' ).on( 'click', function( $event ) {
			window.location = $( this ).attr( 'href' );
		});

		$('#desktop_menu .dropdown-menu').show().css( 'visibility', 'hidden' );

		setTimeout( function () {
			$( '#desktop_menu .dropdown-menu' ).removeAttr( 'style' );
		}, 1000 );

		var $count = 0;

		var $slider_interval = setInterval( function() {
			$.each( $( 'header .content-slider' ), function ($index, $value) {
				$count++;

				if ( $count > 18 )
					clearInterval( $slider_interval );

				var $is_featured_articles = $( $value ).parent( 'div' ).hasClass( '.featured-articles' ) ? true : false;

				$( $value ).slick({
					dots: false,
					infinite: false,
					speed: 300,
					slidesToShow: $is_featured_articles ? 3 : 6,
					slidesToScroll: 1,
					prevArrow: '<a href="javascript:void(0);"><i class="fa fa-chevron-left"></i></a>',
					nextArrow: '<a href="javascript:void(0);"><i class="fa fa-chevron-right"></i></a>',
					responsive: [
						{
							breakpoint: 1400,
							settings: {
								slidesToShow: 4,
								slidesToScroll: 4,
							}
						},
						{
							breakpoint: 1024,
							settings: {
								slidesToShow: 2,
								slidesToScroll: 2,
							}
						},
						{
							breakpoint: 768,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1
							}
						},
						{
							breakpoint: 575,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1
							}
						}
					]
				});
			});
		}, 50 );
	}

	function setup_featured_articles_sliders() {
		$.each( $( '.featured-articles .content-slider, .partner_category .content-slider, #related_articles_list .content-slider, #related_projects_list .content-slider' ), function ($index, $value) {
			$( $value ).slick({
				dots: false,
				infinite: false,
				speed: 300,
				slidesToShow: 3,
				slidesToScroll: 1,
				prevArrow: '<a href="javascript:void(0);"><i class="fa fa-chevron-left"></i></a>',
				nextArrow: '<a href="javascript:void(0);"><i class="fa fa-chevron-right"></i></a>',
				responsive: [
					{
						breakpoint: 1024,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 2,
						}
					},
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 575,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					}
				]
			}).css( 'visibility', 'visible' );
		});
	}

$( window )
	.load( function() {
		$( document ).on( 'click', '#desktop_menu .dropdown-menu', function( $event ) {
			$event.stopPropagation();
		});

		setup_content_sliders();
	})
	.resize( function() {
		setup_content_sliders();
	});

$( '[data-share-url]' ).on( 'click', function() {
	var $width = 500,
		$height = 250;

	var $top_margin = ( screen.height / 2 ) - ( $height / 2 ),
		$left_margin = ( screen.width / 2 ) - ( $width / 2 );

	window.open(
		$( this ).attr( 'data-share-url' ),
		$( this ).attr( 'title' ),
		'top=' + $top_margin + ',left=' + $left_margin + ',toolbar=0,status=0,width=' + $width + ',height=' + $height
	);
} );

$.each( $( '.partners.real-partners img' ), function( index, img ) {
	var width = $( img ).parent( 'div' ).width();

	$( img ).css( {
		'max-height': '200px',
		'max-width': '' + ( width - 30 ) + 'px',
		'display': 'table',
		'margin': '0 auto'
	} );

	setTimeout( function() {
        $( img ).css({
            'padding-top': parseInt( ( $( img ).parent( 'div' ).height() - $( img ).height() ) / 2 )  + 'px'
        } );
    }, 500 );
} );

function is_breakpoint( $breakpoint ) {
    return $( '#breakpoints .hidden-' + $breakpoint + '-down' ).css( 'display' ) == 'none' ? true : false
}

var $home_slider_dimensions = {
    width: $( '#home_slider' ).width(),
    height: $( '#home_slider' ).height()
};

$( '#home_slider' ).css({
    'width': $home_slider_dimensions.width + 'px',
    'height': parseInt( $home_slider_dimensions.width / ( is_breakpoint( 'sm' ) ? 1.2 : 1.7 ) ) + 'px'
});

var $page_thumbnail_dimensions = {
    width: $( '#page_thumbnail' ).width(),
    height: $( '#page_thumbnail' ).height()
};

$( '#page_thumbnail, #page_thumbnail > div > .container' ).css({
    'height': parseInt( $page_thumbnail_dimensions.width * ( is_breakpoint( 'sm' ) ? 0.6 : 0.5 ) ) + 'px'
});

if ( !is_breakpoint( 'sm' ) ) {
    $( '#toggle_mobile_menu' ).addClass( 'padded' );
}

$( '#gallery_container' ).imagesLoaded( function() {
    $( '#gallery_container > .gallery_items' ).wookmark({
        container: $( '#gallery_container' ),
        autoResize: true,
        offset: 0,
        align: 'left',
    });
});

$( '#galleries_container' ).imagesLoaded( function() {
    $( '#galleries_container > .gallery_items' ).wookmark({
        container: $( '#galleries_container' ),
        autoResize: true,
        offset: 0,
        align: 'left',
    });
});

$( '#playlists_container' ).imagesLoaded( function() {
    $( '#playlists_container > .row' ).wookmark({
        container: $( '#playlists_container' ),
        autoResize: true,
        offset: 0,
        align: 'left',
    });
});

$( '#playlist_container' ).imagesLoaded( function() {
    $( '#playlist_container > .row' ).wookmark({
        container: $( '#playlist_container' ),
        autoResize: true,
        offset: 0,
        align: 'left',
    });
});

$( '.gallery_items > div > a' ).simpleLightbox({
    overlay: true,
    spinner: true,
    showCaptions: true,
    captionSelector: 'self',
});

$.each( $( '#playlists_container > .row > div > a' ), function( $index, $value ) {
    var $width = $( $value ).width();

    $( $value ).css( 'height', parseInt( ( $width / 16 ) * 9 ) + 'px' );

    setup_content_sliders();
});

var $video_container_width = $( '#video_container' ).width();

$( '#video_container iframe' ).removeAttr( 'width' ).removeAttr( 'height' ).css({
    width: $video_container_width + 'px',
    height: parseInt( ( $video_container_width / 16 ) * 9 ) + 'px'
});

$( '.search_form a' ).on( 'click', function( $event ) {
    $( this ).parent( 'form' ).submit();
});
//# sourceMappingURL=site.js.map
