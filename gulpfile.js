process.env.DISABLE_NOTIFIER = true;

var $gulp = require( 'gulp' ),
	$watch = require( 'gulp-watch' ),
	$elixir = require( 'laravel-elixir' ),
	$vendors = '../../vendor/',
	$assets = '../../assets/';

$gulp.task( 'default', function() {
	$gulp.start( 'process' );
});

$gulp.task( 'watch', function() {
	$gulp.start( 'default' );

	$watch( 'resources/**', { usePolling: true }, function () {
		$gulp.start( 'default' );
	});
});

$gulp.task( 'process', function() {
	$elixir( function ( $mix ) {
		$mix.copy( 'resources/vendor/font-awesome/fonts/**', 'public/fonts' )
			.copy( 'resources/vendor/summernote/dist/font/**', 'public/fonts' )
			.sass( [
				$vendors + 'bootstrap/scss/bootstrap.scss',
				$vendors + 'font-awesome/scss/font-awesome.scss',
				$vendors + 'bootstrap-select/sass/bootstrap-select.scss',
				$vendors + 'slick-carousel/slick/slick.scss',
				$vendors + 'summernote/dist/summernote.css',
				$vendors + 'simplelightbox/dist/simplelightbox.min.css'
			], 'public/css/bootstrap.css' )
			.scripts( [
				$vendors + 'jquery/dist/jquery.js',
				$vendors + 'tether/dist/js/tether.js',
				$vendors + 'bootstrap/dist/js/bootstrap.js',
				$vendors + 'bootstrap-hover-dropdown/bootstrap-hover-dropdown.js',
				$vendors + 'bootstrap-select/dist/js/bootstrap-select.min.js',
				$vendors + 'slick-carousel/slick/slick.js',
				$vendors + 'summernote/dist/summernote.js',
				$vendors + 'imagesloaded/imagesloaded.pkgd.min.js',
				$vendors + 'wookmark/wookmark.min.js',
				$vendors + 'simplelightbox/dist/simple-lightbox.min.js',
				$assets + 'js/browser_fixes.js'
			], 'public/js/bootstrap.js' )
			.sass( [
				$assets + 'scss/site/styles.scss',
				$assets + 'scss/site/responsive.scss'
			], 'public/css/site.css' )
			.sass( [
				$assets + 'scss/site/styles.scss',
				$assets + 'scss/site/responsive.scss',
				$assets + 'scss/site/gilbert.scss'
			], 'public/css/gilbert.css' )
			.scripts( [
				$assets + 'js/site.js'
			], 'public/js/site.js' )
			.sass( [
				$assets + 'scss/login.scss'
			], 'public/css/login.css' )
			.scripts( [
				$assets + 'js/login.js'
			], 'public/js/login.js' )
			.sass( [
				$assets + 'scss/admin/styles.scss',
				$assets + 'scss/admin/responsive.scss'
			], 'public/css/admin.css' )
			.scripts( [
				$assets + 'js/admin.js'
			], 'public/js/admin.js' );
	} );
});
