<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'paypal' => [
        'sandbox' => [
            'client_id' => 'AaTjKujnGj1fzfSwOY_zl0JdmjEDfj_iwBJOplZ91tEiJXmr3jHODXH9cRLDVa7BQJsMYEXEbAoPaNL2',
            'secret' => 'EAq2nrmunS2sh-h52K03Hla2iUjYwnkxrTXpIU27ITcmK9IonMOdrGm6xNw-9Z9zmCZL4HWYRDqc-mOe',
            'endpoint' => 'https://api.sandbox.paypal.com'
        ],
        'live' => [
            'client_id' => 'AVtye2wOik3MXdOQNDvwqgELZQ6W8zfziRhe4XOejkqfle1SwB1-NT7xSS0CHxTcLTXLQVED3UkIW_uO',
            'secret' => 'EMRcvQo_u-u431DzfUcb-k39vvI5XumoXXLPmmzbkwh92JBim9vYWnbqY9oLjmoe8evnXs3aBre8jKHD',
            'endpoint' => 'https://api.paypal.com'
        ]
    ],

];
