<?php

use App\App;

Route::model( 'user', 'App\Models\User' );

Route::pattern( 'id', '[0-9]+' );
Route::pattern( 'playlist', '[0-9a-zA-Z-_]+' );
Route::pattern( 'video', '[0-9a-zA-Z-_]+' );
Route::pattern( 'gallery', '[0-9]+' );
Route::pattern( 'photo', '[0-9]+' );
Route::pattern( 'page', '[0-9]+' );
Route::pattern( 'name', '[0-9a-z-_]+' );

foreach( [ '', 'second', 'third' ] as $slug_prefix )
	Route::pattern( ( !empty( $slug_prefix ) ? $slug_prefix . '_' : '' ) . 'slug', '[0-9a-z-_]+' );

Route::get( '/', 'Site\PostTypes\PageController@set_default_lang' );

Route::group( [ 'prefix' => '{language}' ], function() {
	Route::get( '', 'Site\PostTypes\PageController@index' );

	Route::get( 'search', 'Site\SearchController@index' );
	Route::get( 'search/{page}', 'Site\SearchController@index' );

	Route::get( 'login', 'Auth\AuthController@get_login' );
	Route::post( 'login', 'Auth\AuthController@post_login' );
	Route::get( 'logout', 'Auth\AuthController@get_logout' );

	Route::get( 'youtube-playlist/{playlist}', 'Site\PostTypes\PageController@youtube_playlist' );
	Route::get( 'youtube-playlist/{playlist}/{page}', 'Site\PostTypes\PageController@youtube_playlist' );
	Route::get( 'youtube-video/{video}', 'Site\PostTypes\PageController@youtube_video' );

	Route::any( 'complete-payment', 'Site\PostTypes\PageController@complete_payment' );
	Route::any( 'cancel-payment', 'Site\PostTypes\PageController@cancel_payment' );

	Route::get( 'flickr-gallery/{gallery}', 'Site\PostTypes\PageController@flickr_gallery' );
	Route::get( 'flickr-gallery/{gallery}/{page}', 'Site\PostTypes\PageController@flickr_gallery' );
	Route::get( 'flickr-photo/{photo}', 'Site\PostTypes\PageController@flickr_photo' );

	Route::group( [ 'prefix' => 'admin', 'middleware' => 'admin' ], function() {
		Route::get( '', 'Admin\DashboardController@index' );
		Route::get( 'api', 'Admin\APIController@index' );

		if ( Schema::hasTable( 'options' ) ) :

			foreach( App::get_taxonomies() as $taxonomy ) :

				Route::get( $taxonomy->rewrite->admin->archive, $taxonomy->controller->admin->name . '@' . $taxonomy->controller->admin->archive );
				Route::get( $taxonomy->rewrite->admin->archive . '/{page}', $taxonomy->controller->admin->name . '@' . $taxonomy->controller->admin->archive );

				Route::get( $taxonomy->rewrite->admin->base . '/add', $taxonomy->controller->admin->name . '@add' );
				Route::post( $taxonomy->rewrite->admin->base . '/add', $taxonomy->controller->admin->name . '@add' );

				Route::get( $taxonomy->rewrite->admin->base . '/edit/{id}', $taxonomy->controller->admin->name . '@edit' );
				Route::post( $taxonomy->rewrite->admin->base . '/edit/{id}', $taxonomy->controller->admin->name . '@update' );

				Route::get( $taxonomy->rewrite->admin->base . '/delete/{id}', $taxonomy->controller->admin->name . '@delete' );

			endforeach;

			foreach( App::get_post_types() as $post_type ) :

				Route::get( $post_type->rewrite->admin->archive, $post_type->controller->admin->name . '@' . $post_type->controller->admin->archive );
				Route::get( $post_type->rewrite->admin->archive . '/{page}', $post_type->controller->admin->name . '@' . $post_type->controller->admin->archive );

				Route::get( $post_type->rewrite->admin->base . '/add', $post_type->controller->admin->name . '@add' );
				Route::post( $post_type->rewrite->admin->base . '/add', $post_type->controller->admin->name . '@add' );

				Route::get( $post_type->rewrite->admin->base . '/edit/{id}', $post_type->controller->admin->name . '@edit' );
				Route::post( $post_type->rewrite->admin->base . '/edit/{id}', $post_type->controller->admin->name . '@update' );

				Route::get( $post_type->rewrite->admin->base . '/delete/{id}', $post_type->controller->admin->name . '@delete' );

			endforeach;

		endif;
	});

	if ( Schema::hasTable( 'options' ) ) :

		foreach( App::get_taxonomies() as $taxonomy ) :

			if ( $taxonomy->has_archive && strlen( $taxonomy->rewrite->site->archive ) != 0 ) :

				Route::get( $taxonomy->rewrite->site->archive, $taxonomy->controller->site->name . '@' . $taxonomy->controller->site->archive );
				Route::get( $taxonomy->rewrite->site->archive . '/{page}', $taxonomy->controller->site->name . '@' . $taxonomy->controller->site->archive );

			endif;

			if ( strlen( $taxonomy->rewrite->site->base ) != 0 ) :

				Route::get( $taxonomy->rewrite->site->base . '/{name}', $taxonomy->controller->site->name . '@' . $taxonomy->controller->site->single );
				Route::get( $taxonomy->rewrite->site->base . '/{name}/{page}', $taxonomy->controller->site->name . '@' . $taxonomy->controller->site->single );

			endif;

		endforeach;

		foreach( App::get_post_types() as $post_type ) :

			if ( $post_type->has_archive ) :

				Route::get( $post_type->rewrite->site->archive, $post_type->controller->site->name . '@' . $post_type->controller->site->archive );
				Route::get( $post_type->rewrite->site->archive . '/{page}', $post_type->controller->site->name . '@' . $post_type->controller->site->archive );

				Route::get( ( !empty( $post_type->rewrite->site->base ) ? $post_type->rewrite->site->base . '/' : '' ) . '{slug}', $post_type->controller->site->name . '@' . $post_type->controller->site->single );

			elseif ( $post_type->name == 'page' ) :

				$page_post_type = $post_type;

			endif;

		endforeach;

		if ( isset( $page_post_type ) ) :

			Route::get( '{slug}', $page_post_type->controller->site->name . '@' . $page_post_type->controller->site->single );
			Route::get( '{slug}/{second_slug}', $page_post_type->controller->site->name . '@' . $page_post_type->controller->site->single );
			Route::get( '{slug}/{second_slug}/{third_slug}', $page_post_type->controller->site->name . '@' . $page_post_type->controller->site->single );

			Route::post( '{slug}', $page_post_type->controller->site->name . '@' . $page_post_type->controller->site->single );
			Route::post( '{slug}/{second_slug}', $page_post_type->controller->site->name . '@' . $page_post_type->controller->site->single );
			Route::post( '{slug}/{second_slug}/{third_slug}', $page_post_type->controller->site->name . '@' . $page_post_type->controller->site->single );

		endif;

	endif;
});
