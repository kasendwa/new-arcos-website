<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\App;

class Authenticate {

    protected $auth;

    public function __construct( Guard $auth ) {
        $this->auth = $auth;
    }

    public function handle( $request, Closure $next ) {
        if ( $this->auth->guest() )
            return $request->ajax() ? response( 'Unauthorized.', 401 ) : redirect()->guest( App::get_current_language() . '/login' );

        return $next( $request );
    }
    
}
