<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

use App\App;

class RedirectIfAuthenticated {
    
    protected $auth;

    public function __construct( Guard $auth ) {
        $this->auth = $auth;
    }

    public function handle( $request, Closure $next ) {
        return $this->auth->check() ? redirect( App::get_current_language() . '/admin' ) : $next( $request );
    }
    
}
