<?php

namespace App\Http\Controllers\Auth;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\App;
use App\Entities\Translation;

class AuthController extends Controller {

	use AuthenticatesUsers, ThrottlesLogins;

	protected $redirectTo = '/';

	public function __construct() {
		$this->middleware( 'guest', [ 'except' => 'get_logout' ] );
	}

	public function get_login() {
		View::share( 'site_info', App::get_site_info() );

		View::share( 'translations', ( object ) [
			'username' => Translation::get_translation( 'username', App::get_current_language() ),
			'password' => Translation::get_translation( 'password', App::get_current_language() ),
			'login' => Translation::get_translation( 'log-in', App::get_current_language() ),
		] );

		return view( 'auth.login' );
	}

	public function post_login() {
		$validator = Validator::make( Input::all(), [
			'username' => 'required|email',
			'password' => 'required|alphaNum|min:3'
		] );

		if ( $validator->fails() ) :

			return Redirect::to( 'login' )->withErrors( $validator )->withInput( Input::except( 'password' ) );

		else :

			$user_data = [
				'email' => Input::get( 'username' ),
				'password' => Input::get( 'password' )
			];

			return Redirect::to( Auth::attempt( $user_data ) ? App::get_current_language() . '/admin' : App::get_current_language() . '/login' );

		endif;
	}

	public function get_logout( $lang ) {
		Auth::logout();

		return redirect( $lang . '/login' );
	}

}