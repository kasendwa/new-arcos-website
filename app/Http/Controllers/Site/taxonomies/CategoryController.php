<?php

namespace App\Http\Controllers\Site\Taxonomies;

use App\Http\Controllers\Site\SiteController;

use App\App;
use App\Entities\Article;
use App\Entities\Translation;
use App\Entities\Category;

class CategoryController extends SiteController {

	public function archive( $lang ) {
		foreach( Category::get_all_categories() as $entry )
			$categories[] = Category::get_category( $entry, $lang );

		return view( 'site.categories.archive', [
			'title' => Translation::get_translation( 'categories', $lang ),
			'categories' => isset( $categories ) ? $categories : []
		] );
	}

	public function single( $lang, $slug, $page = 1 ) {
		$category = new Category( $slug );
		$per_page = 12;

		foreach( $category->get_articles( $per_page, $page ) as $entry )
			$articles[] = Article::get_article( $entry, $lang );

		return view( 'site.categories.single', [
			'category' => Category::get_category( $slug, $lang ),
			'articles' => $articles,
			'pagination' => ( object ) [
				'active' => $page,
				'total' => ceil( count( $category->get_all_articles() ) / $per_page )
			]
		] );
	}

}