<?php

namespace App\Http\Controllers\Site;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Netshell\Paypal\Facades\Paypal;

use App\Http\Controllers\BaseController;
use App\App;
use App\Models\Language;
use App\Entities\Translation;
use App\Entities\Menu;

class SiteController extends BaseController {

	public $_apiContext;

	public function __construct() {
		parent::__construct();

		$this->_apiContext = PayPal::ApiContext(
			config( 'services.paypal.sandbox.client_id' ),
			config( 'services.paypal.sandbox.secret' )
		);

		$this->_apiContext->setConfig( [
			'mode' => 'sandbox',
			'service.EndPoint' => config( 'services.paypal.sandbox.endpoint' ),
			'http.ConnectionTimeOut' => 30,
			'log.LogEnabled' => true,
			'log.FileName' => storage_path( 'logs/paypal.log' ),
			'log.LogLevel' => 'FINE'
		] );

		foreach( Language::all() as $language )
			$languages[] = ( object ) [
				'code' => $language->code,
				'name' => $language->name
			];

		foreach( Menu::get_menus() as $menu_id ) :

			$the_menu = new Menu( $menu_id );

			$items = [];

			foreach( $the_menu->get_items() as $item )
				$items[ $item->order ] = ( object ) [
					'title' => $item->title->{ App::get_current_language() },
					'anchor' => $item->anchor
				];

			$menus[ $the_menu->position ] = ( object ) [
				'title' => $the_menu->title->{ App::get_current_language() },
				'items' => $items
			];

		endforeach;

		View::share( [
			'languages' => isset( $languages ) ? $languages : [],
			'site_info' => App::get_site_info(),
			'translations' => ( object ) [
				'search' => Translation::get_translation( 'search', App::get_current_language() ),
				'read_more' => Translation::get_translation( 'read-more', App::get_current_language() ),
				'read_article' => Translation::get_translation( 'read-article', App::get_current_language() ),
				'at_a_glance' => Translation::get_translation( 'arcos-at-a-glance', App::get_current_language() ),
				'contact_us' => Translation::get_translation( 'contact-us', App::get_current_language() ),
				'get_involved' => Translation::get_translation( 'get-involved', App::get_current_language() ),
				'connect_with_us' => Translation::get_translation( 'connect-with-us', App::get_current_language() ),
				'menu' => Translation::get_translation( 'menu', App::get_current_language() ),
				'read_biography' => Translation::get_translation( 'read-biography', App::get_current_language() ),
				'related_articles' => Translation::get_translation( 'related-articles', App::get_current_language() ),
				'related_projects' => Translation::get_translation( 'related-projects', App::get_current_language() ),
				'position' => Translation::get_translation( 'position', App::get_current_language() ),
				'country' => Translation::get_translation( 'country', App::get_current_language() ),
				'visit_the_website' => Translation::get_translation( 'visit-the-website', App::get_current_language() ),
                'visit_website' => Translation::get_translation( 'visit-website', App::get_current_language() ),
                'download' => Translation::get_translation( 'download', App::get_current_language() ),
				'see_previous_editions' => Translation::get_translation( 'see-previous-editions', App::get_current_language() ),
				'search_results_title' => str_replace(
					'?', '"' . ( !is_null( Input::get( 's' ) ) ? Input::get( 's' ) : '' ) . '"',
					Translation::get_translation( 'search-results-for', App::get_current_language() )
				),
				'subscribe_to_our_newsletters' => Translation::get_translation( 'subscribe-to-our-newsletters', App::get_current_language() ),
				'checkboxes_of_newsletters_you_want_to_subscribe_to' => Translation::get_translation( 'checkboxes-of-newsletters-you-want-to-subscribe-to', App::get_current_language() ),
				'first_name' => Translation::get_translation( 'first-name', App::get_current_language() ),
				'last_name' => Translation::get_translation( 'last-name', App::get_current_language() ),
				'email_address' => Translation::get_translation( 'email-address', App::get_current_language() ),
				'phone_number' => Translation::get_translation( 'phone-number', App::get_current_language() ),
				'institution' => Translation::get_translation( 'institution', App::get_current_language() ),
				'job_position' => Translation::get_translation( 'job-position', App::get_current_language() ),
				'subscribe' => Translation::get_translation( 'subscribe', App::get_current_language() )
			],
			'top_menu_items' => App::get_top_menu_items( 0 ),
			'menus' => isset( $menus ) ? $menus : []
		]);
	}
	
}