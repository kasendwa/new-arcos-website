<?php

namespace App\Http\Controllers\Site;

use Illuminate\Support\Facades\Input;

use App\App;
use App\Models\Post;
use App\Entities\Page;
use App\Entities\Article;
use App\Entities\Project;

class SearchController extends SiteController {

	public function index( $lang, $page = 1 ) {
		$per_page = 20;

		$entries = App::search_results( Input::get( 's' ) );

		foreach( array_slice( $entries, ( $page - 1 ) * $per_page, $per_page ) as $id ) :

			$post = Post::find( $id );

			if ( $post->type == 'page' ) :

				$the_page = Page::get_page( $post->id, $lang );

				$results[] = ( object ) [
					'title' => $the_page->title,
					'thumbnail' => $the_page->thumbnail,
					'summary' => $the_page->summary,
					'permalink' => $the_page->permalink
				];

			elseif ( $post->type == 'article' ) :

				$article = Article::get_article( $post->id, $lang );

				$results[] = ( object ) [
					'title' => $article->title,
					'thumbnail' => $article->thumbnail,
					'summary' => $article->summary,
					'permalink' => $article->permalink
				];

			elseif ( $post->type == 'project' ) :

				$project = Project::get_project( $post->id, $lang );

				$results[] = ( object ) [
					'title' => $project->title,
					'thumbnail' => $project->thumbnail,
					'summary' => $project->summary,
					'permalink' => $project->permalink
				];

			endif;

		endforeach;

		return view( 'site.search', [
			'results' => isset( $results ) ? $results : [],
			'pagination' => ( object ) [
				'active' => $page,
				'total' => ceil( count( $entries ) / $per_page )
			],
			'permalink' => url( $lang . '/search' ),
			's' => Input::get( 's' )
		] );
	}

}