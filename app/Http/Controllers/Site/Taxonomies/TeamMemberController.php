<?php

namespace App\Http\Controllers\Site\Taxonomies;

use App\Http\Controllers\Site\SiteController;

use App\App;
use App\Entities\Article;
use App\Entities\Translation;
use App\Entities\TeamMember;

class TeamMemberController extends SiteController {

	public function single( $lang, $slug ) {
		return view( 'site.team.single', [
			'member' => TeamMember::get_team_member( $slug, $lang )
		] );
	}

}