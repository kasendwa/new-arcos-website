<?php

namespace App\Http\Controllers\Site\Taxonomies;

use App\Http\Controllers\Site\SiteController;

class EditionController extends SiteController {

	public function archive() {
		return view( 'site.editions.archive' );
	}

	public function single() {
		return view( 'site.editions.single' );
	}

}