<?php

namespace App\Http\Controllers\Site\PostTypes;

use App\Entities\PartnerCategory;
use App\Entities\ResourceCategory;
use Illuminate\Support\Facades\Input;
use Netshell\Paypal\Facades\Paypal;
use Illuminate\Support\Facades\Redirect;
use Request;

use App\App;
use App\Entities\Slider;
use App\Entities\Page;
use App\Entities\Article;
use App\Entities\Category;
use App\Entities\AboutSection;
use App\Entities\TeamCategory;
use App\Entities\Website;
use App\Entities\Newsletter;
use App\Entities\Project;
use App\Entities\Invoice;
use App\Entities\Translation;
use App\Entities\MailmanList;
use App\Entities\Payment;

use App\Http\Controllers\Site\SiteController;

class PageController extends SiteController {

	public function set_default_lang() {
		return redirect( '/' . App::get_default_option( 'language' ) );
	}

	public function index( $lang ) {
	    $featured_categories = json_decode( App::get_option( 'featured_categories', json_encode( [] ) ) );

		if ( is_array( $featured_categories ) )

			foreach( $featured_categories as $category_id )
				$categories[] = Category::get_category( $category_id, $lang );

		return view( 'site.pages.home', [
			'sliders' => Slider::get_sliders(),
			'categories' => isset( $categories ) ? $categories : [],
			'read_article' => App::get_option( 'read_article_' . $lang, '' )
		] );
	}

	public function complete_payment( Request $request ) {
		$id = $request->get( 'paymentId' );
		$token = $request->get( 'token' );
		$payer_id = $request->get( 'PayerID' );

		$payment = PayPal::getById($id, $this->_apiContext);

		$paymentExecution = PayPal::PaymentExecution();

		$paymentExecution->setPayerId($payer_id);
		$executePayment = $payment->execute( $paymentExecution, $this->_apiContext );
	}

	public function cancel_payment() {
		return 'Payment cancelled';
	}

	public static function payment_form( $lang, $referer ) {
		$controller = new PageController();

		$html = '<div class="row">

			<div class="payment_form col-lg-5 col-md-6 col-sm-6 col-xs-12">';

				if ( Input::has( 'payment-fname' ) ) :

					$invoice = Invoice::get_invoice( ( int ) Input::get( 'payment-invoice' ), $lang );

					$payment = ( object ) Payment::get_payment(
						Payment::add_payment( [
							'first_name' => Input::get( 'payment-fname' ),
							'last_name' => Input::get( 'payment-lname' ),
							'email' => Input::get( 'payment-email' ),
							'status' => 'Pending',
							'invoice' => $invoice->id
						] )
					);

					$payer = PayPal::Payer();
					$payer->setPaymentMethod( 'paypal' );

					$amount = PayPal::Amount();
					$amount->setCurrency( 'USD' );
					$amount->setTotal( ( float ) $invoice->amount  );

					$redirect_urls = PayPal::RedirectUrls();
					$redirect_urls->setReturnUrl( $referer . ( strpos( '?', $referer ) !== false ? '&payment=' . $payment->id : '?payment=' . $payment->id ) );
					$redirect_urls->setCancelUrl( $referer . ( strpos( '?', $referer ) !== false ? '&payment=' . $payment->id : '?payment=' . $payment->id ) );

					$transaction = PayPal::Transaction();
					$transaction->setAmount( $amount );
					$transaction->setDescription( $invoice->title );

					$payment = PayPal::Payment();
					$payment->setIntent( 'sale' );
					$payment->setPayer( $payer );
					$payment->setRedirectUrls( $redirect_urls );
					$payment->setTransactions( [
						$transaction
					] );

					$response = $payment->create( $controller->_apiContext );
					$redirect_url = $response->links[ 1 ]->href;

					return Redirect::to( $redirect_url );

				elseif ( Input::has( 'payment' ) && !Input::has( 'paymentId' ) ) :

					$payment = ( object ) Payment::get_payment( ( int ) Input::get( 'payment' ) );

					App::update_term_meta( $payment->id, 'status', 'Cancelled' );

					$html .= '<div class="alert alert-info">'
						. Translation::get_translation( 'payment-cancelled', $lang ) .
						'<a href="' . Request::url() . '">' . Translation::get_translation( 'try-again', $lang ) . '</a>
					</div>';

				elseif ( Input::has( 'paymentId' ) ) :

					$payment = ( object ) Payment::get_payment( ( int ) Input::get( 'payment' ) );

					App::update_term_meta( $payment->id, 'status', 'Successful' );
					App::update_term_meta( $payment->id, 'token', Input::get( 'token' ) );
					App::update_term_meta( $payment->id, 'payment_id', Input::get( 'paymentId' ) );

					$html .= '<div class="alert alert-success">' .
						Translation::get_translation( 'payment-successful', $lang ) . '
					</div>';

				else :

					$html .= '<form method="post">' .

						csrf_field() . '
						
						<div class="row">
						
							<div class="col-lg-6 col-md-6 col-sm-6">
						
								<div class="form-group">
								
									<label class="control-label" for="payment-fname">' . Translation::get_translation( 'first-name', $lang ) . '</label>
									
									<div>
										<input type="text" class="form-control" name="payment-fname" placeholder="' . Translation::get_translation( 'first-name', $lang ) . '" />
									</div>
								
								</div>
						
							</div>
							
							<div class="col-lg-6 col-md-6 col-sm-6">
						
								<div class="form-group">
								
									<label class="control-label" for="payment-lname">' . Translation::get_translation( 'last-name', $lang ) . '</label>
									
									<div>
										<input type="text" class="form-control" name="payment-lname" placeholder="' . Translation::get_translation( 'last-name', $lang ) . '" />
									</div>
								
								</div>
						
							</div>
						
						</div>
						
						<div class="form-group">
						
							<label class="control-label" for="payment-email">' . Translation::get_translation( 'email-address', $lang ) . '</label>
							
							<div>
								<input type="email" class="form-control" name="payment-email" placeholder="' . Translation::get_translation( 'email-address', $lang ) . '" />
							</div>
						
						</div>
						
						<div class="form-group">
						
							<label class="control-label" for="payment-invoice">' . Translation::get_translation( 'payment-invoice', $lang ) . '</label>
							
							<div>
								<select name="payment-invoice" class="form-control" >';

								foreach( Invoice::get_all_invoices() as $entry ) :

									$invoice = Invoice::get_invoice( $entry, $lang );

									$html .= '<option value="' . $invoice->id . '">' . $invoice->title . ' ($' . number_format( $invoice->amount ) . ')</option>';

								endforeach;

								$html .= '</select>
							</div>
							
						</div>
						
						<div class="form-group"> 
						
							<div>
							
								<button type="submit" class="btn btn-primary">
									' . Translation::get_translation( 'make-payment', $lang ) . '
								</button>
								
							</div>
							
						</div>
						
					</form>';

				endif;

			$html .= '</div>
		</div>';

		return $html;
	}

	public function do_shortcodes( $content, $lang, $referer = null ) {
		if ( strlen( $content ) > 0 && strpos( '[payment-form]', $content ) >= 0 ) :

			$return = call_user_func( [ 'App\Http\Controllers\Site\PostTypes\PageController', 'payment_form' ], $lang, $referer );

			$redirect_message = '<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="alert alert-info">' . Translation::get_translation( 'redirecting-to-secure-payment-gateway', $lang ) . '</div>
				</div>
			</div>';

			$content = str_replace( '[payment-form]', is_string( $return ) ? $return : '<span style="display: none;">' . $return . '</span>' . $redirect_message, $content );

		endif;

		return $content;
	}

	public function single( $lang, $slug, $second_slug = '', $third_slug = '' ) {
		empty( $third_slug ) && !empty( $second_slug ) ? ( $slug = $second_slug ) : null;
		!empty( $third_slug ) ? ( $slug = $third_slug ) : null;

		$page = Page::get_page( $slug, $lang );

		$params = [];

		if ( $page->template == 'template-1' ) :

			$the_page = new Page( $page->id );

			foreach( $the_page->get_related_articles( 6 ) as $entry )
				$related_articles[] = Article::get_article( $entry, $lang );

			$params[ 'related_articles' ] = isset( $related_articles ) ? $related_articles : [];

			foreach( $the_page->get_related_projects( 6 ) as $entry )
				$related_projects[] = Project::get_project( $entry, $lang );

			$params[ 'related_projects' ] = isset( $related_projects ) ? $related_projects : [];

		elseif ( $page->template == 'template-2' ) :

			foreach( Website::get_all_websites() as $entry )
				$websites[] = Website::get_website( $entry, $lang );

			$params[ 'websites' ] = isset( $websites ) ? $websites : [];

        elseif ( $page->template == 'template-3' ) :

            $the_page = new Page( $page->id );

            foreach( $the_page->get_related_projects( 6 ) as $entry )
                $related_projects[] = Project::get_project( $entry, $lang );

            $params[ 'related_projects' ] = isset( $related_projects ) ? $related_projects : [];

		elseif ( $page->template == 'template-4-1' ) :

			foreach( PartnerCategory::get_all_partner_categories() as $entry ) :

                $partner_category = new PartnerCategory( $entry );

                $partner_categories[ $partner_category->order ] = ( object ) [
                    'id' => $partner_category->id,
                    'title' => $partner_category->title->{ $lang },
                    'summary' => $partner_category->description->{ $lang },
                    'partners' => $partner_category->get_partners( $lang )
                ];

                ksort( $partner_categories );

            endforeach;

            $params[ 'partner_categories' ] = isset( $partner_categories ) ? $partner_categories : [];

        elseif ( $page->template == 'template-4-2' ) :

            foreach( ResourceCategory::get_all_resource_categories() as $entry ) :

                $resource_category = new ResourceCategory( $entry );

                $resource_categories[ $resource_category->order ] = ( object ) [
                    'id' => $resource_category->id,
                    'title' => $resource_category->title->{ $lang },
                    'summary' => $resource_category->description->{ $lang },
                    'resources' => $resource_category->get_resources( $lang )
                ];

                ksort( $resource_categories );

            endforeach;

            $params[ 'resource_categories' ] = isset( $resource_categories ) ? $resource_categories : [];

        elseif ( $page->template == 'template-5' ) :

			$the_page = new Page( $page->id );

			foreach( $the_page->get_related_articles( 6 ) as $entry )
				$related_articles[] = Article::get_article( $entry, $lang );

			$params[ 'related_articles' ] = isset( $related_articles ) ? $related_articles : [];

		elseif ( $page->template == 'template-6' ) :

			foreach( TeamCategory::get_team_categories() as $entry ) :

				$team_category = new TeamCategory( $entry );

				$team_categories[ $team_category->order ] = ( object ) [
					'id' => $team_category->id,
					'title' => $team_category->title->{ $lang },
					'members' => $team_category->get_members( $lang )
				];

				ksort( $team_categories );

			endforeach;

			$params[ 'team_categories' ] = isset( $team_categories ) ? $team_categories : [];

		elseif ( $page->template == 'template-7' ) :

			foreach( AboutSection::get_all_about_sections() as $entry )
				$about_sections[] = AboutSection::get_about_section( $entry, $lang );

			$params[ 'sections' ] = isset( $about_sections ) ? $about_sections : [];

		elseif ( $page->template == 'template-8' ) :

			$paged = Input::get( 'page' );
			
			$params[ 'galleries' ] = App::get_flickr_galleries( $paged != null ? $page : 1, 500 );

		elseif ( $page->template == 'template-9' ) :

			$params[ 'playlists' ] = App::get_youtube_playlists();

		elseif ( $page->template == 'template-10' ) :

			if ( Input::get( 'first_name', null ) != null ) :

				$result = MailmanList::subscribe( [
					'first_name' => Input::get( 'first_name', '' ),
					'last_name' => Input::get( 'last_name', '' ),
					'email' => Input::get( 'email', '' ),
					'phone_number' => Input::get( 'phone_number', '' ),
					'institution' => Input::get( 'institution', '' ),
					'job_position' => Input::get( 'job_position', '' ),
					'newsletters' => Input::get( 'newsletters', [] )
				] );

				$messages = [
					'success' => [
						'en' => 'Thank you for subscribing. Please check your email for further instructions',
						'fr' => 'Merci de vous être abonné. Vérifiez votre courrier électronique pour obtenir d\'autres instructions'
					],
					'failure' => [
						'en' => 'Sorry, something did not go right. Please try again!',
						'fr' => 'Désolé, quelque chose ne s\'est pas bien passé. Veuillez réessayer!'
					]
				];

				$params[ 'result_type' ] = $result;
				$params[ 'result_message' ] = $result == true ? $messages[ 'success' ][ $lang ] : $messages[ 'failure' ][ $lang ];

			endif;

            $lists = MailmanList::fetch_mailman_lists();

			foreach( Newsletter::get_all_newsletters() as $entry ) :

                $entry = Newsletter::get_newsletter( $entry, $lang );

				$newsletters[] = $entry;

			    $allowed_lists[ $entry->mailman_list ] = $lists[ $entry->mailman_list ];

			endforeach;

			$params[ 'newsletters' ] = isset( $newsletters ) ? $newsletters : [];
			$params[ 'lists' ] = isset( $allowed_lists ) ? $allowed_lists : [];

		endif;

		$page_data = Page::get_page( $slug, $lang );

		$page_data->content = $this->do_shortcodes( $page_data->content, $lang, $page_data->permalink );

		return view( 'site.pages.' . $page->template, array_merge(
			$params, [
				'page_data' => $page_data,
				'language' => $lang
			]
		) );
	}

	public function flickr_gallery( $lang, $gallery, $page = 1 ) {
		$per_page = 25;

		$info = App::get_flickr_gallery_info( $gallery );

		return view( 'site.pages.gallery', [
			'info' => $info,
			'photos' => App::get_flickr_gallery_photos( $gallery, $page, $per_page ),
			'pagination' => ( object ) [
				'active' => $page,
				'total' => ceil( $info->photos / $per_page )
			]
		] );
	}

	public function youtube_playlist( $lang, $playlist ) {
		$info = App::get_youtube_playlist_info( $playlist );

		return view( 'site.pages.playlist', [
			'info' => $info,
			'videos' => App::get_youtube_playlist_videos( $playlist )
		] );
	}

	public function youtube_video( $lang, $video ) {
		$info = App::get_youtube_video( $video );

		return view( 'site.pages.video', [
			'info' => $info
		] );
	}

}