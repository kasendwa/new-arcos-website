<?php

namespace App\Http\Controllers\Site\PostTypes;

use App\Http\Controllers\Site\SiteController;

use App\App;
use App\Entities\Project;
use App\Entities\Article;

class ProjectController extends SiteController {

	public function archive( $lang, $page = 1 ) {
		$per_page = 12;

		$post_type = App::get_post_type( 'project' );

		foreach( Project::get_projects( $per_page, $page ) as $entry )
			$projects[] = Project::get_project( $entry, $lang );

		return view( 'site.projects.archive', [
			'post_type' => ( object ) [
				'label' => $post_type->label->plural->{ $lang },
				'permalink' => $post_type->has_archive ? url( $lang . '/' . $post_type->rewrite->site->archive ) : url( $lang )
			],
			'projects' => $projects,
			'pagination' => ( object ) [
				'active' => $page,
				'total' => ceil( count( Project::get_all_projects() ) / $per_page )
			]
		] );
	}

	public function single( $lang, $slug ) {
		$project_data = Project::get_project( $slug, $lang );

		$project = new Project( $slug );

		foreach( $project->get_related_articles( 6 ) as $entry )
			$related_articles[] = Article::get_article( $entry, $lang );

		return view( 'site.projects.single', [
			'project_data' => $project_data,
			'related_articles' => isset( $related_articles ) ? $related_articles : []
		] );
	}

}