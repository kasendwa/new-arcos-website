<?php

namespace App\Http\Controllers\Site\PostTypes;

use Chencha\Share\ShareFacade as Share;

use App\Http\Controllers\Site\SiteController;

use App\App;
use App\Entities\Article;
use App\Entities\Translation;
use App\Entities\Category;

class ArticleController extends SiteController {

	public function archive( $lang, $page = 1 ) {
		$per_page = 12;

		$post_type = App::get_post_type( 'article' );

		foreach( Article::get_articles( $per_page, $page ) as $entry )
			$articles[] = Article::get_article( $entry, $lang );

		return view( 'site.articles.archive', [
			'post_type' => ( object ) [
				'label' => $post_type->label->plural->{ $lang },
				'permalink' => $post_type->has_archive ? url( $lang . '/' . $post_type->rewrite->site->archive ) : url( $lang )
			],
			'articles' => $articles,
			'pagination' => ( object ) [
				'active' => $page,
				'total' => ceil( count( Article::get_all_articles() ) / $per_page )
			]
		] );
	}

	public function single( $lang, $slug ) {
		$article = new Article( $slug );

		foreach( $article->get_categories() as $entry ) :

			$category = Category::get_category( $entry, $lang );

			$categories[] = '<a href="' . $category->permalink . '">' . $category->title . '</a>';

		endforeach;

		$article_data = Article::get_article( $slug, $lang );

		$article_data->meta = Translation::get_translation( 'posted-by', $lang );
		$article_data->meta .= ' <em>' . $article_data->author->name->first . ' ' . $article_data->author->name->middle . ' ' . $article_data->author->name->last;
		$article_data->meta .= ' </em>' . Translation::get_translation( 'on', $lang ) . ' <em>';
		$article_data->meta .= Translation::get_translation( strtolower( date( 'F', strtotime( $article_data->timestamp ) ) ), $lang ) . ' ';
		$article_data->meta .= date( 'j, Y', strtotime( $article_data->timestamp ) ) . '</em>';
		$article_data->meta .= ' ' . Translation::get_translation( 'under', $lang ) . ' ';
		$article_data->meta .= '<em>' . implode( '</em>, <em>', ( isset( $categories ) ? $categories : [] ) ) . '</em>';

		foreach( $article->get_related_articles( 6 ) as $entry )
			$related_articles[] = Article::get_article( $entry, $lang );

		return view( 'site.articles.single', [
			'share' => ( object ) [
				'facebook' => Share::load( $article_data->permalink, $article_data->title )->services( 'facebook' ),
				'twitter' => Share::load(
					App::shorten_url( $article_data->permalink ),
					( strlen( $article_data->title ) > 90 ? substr( $article_data->title, 0, 90 ) . '[...]' : $article_data->title ) . ' via @ARCOSNetwork'
				)->services( 'twitter' )
			],
			'article_data' => $article_data,
			'share_on' => Translation::get_translation( 'share-on', $lang ),
			'related_articles' => isset( $related_articles ) ? $related_articles : []
		] );
	}

}