<?php

namespace App\Http\Controllers\Site\PostTypes;

use App\Http\Controllers\Site\SiteController;

class NewsletterController extends SiteController {

	public function archive() {
		return view( 'site.newsletters.archive' );
	}

	public function single() {
		return view( 'site.newsletters.single' );
	}

}