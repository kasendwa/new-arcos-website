<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

use App\Http\Controllers\BaseController;

use App\App;
use App\Entities\Translation;
use App\Models\Language;
use App\Entities\User;

class AdminController  extends BaseController {

	public function __construct() {
		parent::__construct();

		$options = [
			'page_templates' => [
				'template-1' => [
					'name' => [
						'en' => 'How we do it, where we work',
						'fr' => 'Comment nous le faisons, où nous travaillons'
					],
					'properties' => [
						'has_related_articles' => true,
						'has_related_projects' => true
					]
				],
				'template-2' => [
					'name' => [
						'en' => 'Our websites',
						'fr' => 'Nos sites'
					]
				],
				'template-3' => [
					'name' => [
						'en' => 'What we do',
						'fr' => 'Ce que nous faisons'
					],
                    'properties' => [
                        'has_related_projects' => true
                    ]
				],
				'template-4-1' => [
					'name' => [
						'en' => 'Partners & resources (1)',
						'fr' => 'Partenaires et ressources (1)'
					]
				],
                'template-4-2' => [
                    'name' => [
                        'en' => 'Partners & resources (2)',
                        'fr' => 'Partenaires et ressources (2)'
                    ]
                ],
				'template-5' => [
					'name' => [
						'en' => 'Projects',
						'fr' => 'Projets'
					],
					'properties' => [
						'has_related_articles' => true,
					]
				],
				'template-6' => [
					'name' => [
						'en' => 'Our team',
						'fr' => 'Notre équipe'
					]
				],
				'template-7' => [
					'name' => [
						'en' => 'About us',
						'fr' => 'À propos de nous'
					]
				],
				'template-8' => [
					'name' => [
						'en' => 'Image gallery',
						'fr' => 'Galerie d\'images'
					]
				],
				'template-9' => [
					'name' => [
						'en' => 'Video gallery',
						'fr' => 'Galerie d\'images'
					]
				],
				'template-10' => [
					'name' => [
						'en' => 'Newsletters',
						'fr' => 'Newsletters'
					]
				],
                'template-11' => [
                    'name' => [
                        'en' => 'General template',
                        'fr' => 'General template'
                    ]
                ],
			],
            'taxonomies' => [
                'slider' => [
                    'label' => [
                        'singular' => [
                            'en' => 'Slider',
                            'fr' => 'Curseur'
                        ],
                        'plural' => [
                            'en' => 'Sliders',
                            'fr' => 'Sliders'
                        ]
                    ],
                    'has_archive' => false,
                    'rewrite' => [
                        'site' => [
                            'base' => false,
                            'archive' => false
                        ],
                        'admin' => [
                            'base' => 'slider',
                            'archive' => 'sliders'
                        ]
                    ],
                    'controller' => [
                        'admin' => 'Admin\Taxonomies\SliderController'
                    ]
                ],
                'about-section' => [
                    'label' => [
                        'singular' => [
                            'en' => 'About sections',
                            'fr' => 'À propos de la section'
                        ],
                        'plural' => [
                            'en' => 'About sections',
                            'fr' => 'À propos des sections'
                        ]
                    ],
                    'has_archive' => false,
                    'rewrite' => [
                        'admin' => [
                            'base' => 'about-section',
                            'archive' => 'about-sections'
                        ]
                    ],
                    'controller' => [
                        'admin' => 'Admin\Taxonomies\AboutSectionController'
                    ]
                ],
                'translation' => [
                    'label' => [
                        'singular' => [
                            'en' => 'Translation',
                            'fr' => 'Traduction'
                        ],
                        'plural' => [
                            'en' => 'Translations',
                            'fr' => 'Traductions'
                        ]
                    ],
                    'has_archive' => false,
                    'rewrite' => [
                        'admin' => [
                            'base' => 'translation',
                            'archive' => 'translations'
                        ]
                    ],
                    'controller' => [
                        'admin' => 'Admin\Taxonomies\TranslationController'
                    ]
                ],
                'category' => [
                    'label' => [
                        'singular' => [
                            'en' => 'Category',
                            'fr' => 'Catégorie'
                        ],
                        'plural' => [
                            'en' => 'Categories',
                            'fr' => 'Catégories'
                        ]
                    ],
                    'has_archive' => true,
                    'rewrite' => [
                        'site' => [
                            'base' => 'category',
                            'archive' => 'categories'
                        ],
                        'admin' => [
                            'base' => 'category',
                            'archive' => 'categories'
                        ]
                    ],
                    'controller' => [
                        'site' => 'Site\Taxonomies\CategoryController',
                        'admin' => 'Admin\Taxonomies\CategoryController'
                    ]
                ],
	            'invoice' => [
		            'label' => [
			            'singular' => [
				            'en' => 'Invoice',
				            'fr' => 'Facture d\'achat'
			            ],
			            'plural' => [
				            'en' => 'Invoices',
				            'fr' => 'Factures'
			            ]
		            ],
		            'has_archive' => true,
		            'rewrite' => [
			            'admin' => [
				            'base' => 'invoice',
				            'archive' => 'invoices'
			            ]
		            ],
		            'controller' => [
			            'admin' => 'Admin\Taxonomies\InvoiceController'
		            ]
	            ],
	            'payment' => [
		            'label' => [
			            'singular' => [
				            'en' => 'Payment',
				            'fr' => 'Paiement'
			            ],
			            'plural' => [
				            'en' => 'Payments',
				            'fr' => 'Paiements'
			            ]
		            ],
		            'has_archive' => true,
		            'rewrite' => [
			            'admin' => [
				            'archive' => 'payments'
			            ]
		            ],
		            'controller' => [
			            'admin' => 'Admin\Taxonomies\PaymentController'
		            ]
	            ],
                'newsletter' => [
                    'label' => [
                        'singular' => [
                            'en' => 'Newsletter',
                            'fr' => 'Bulletin'
                        ],
                        'plural' => [
                            'en' => 'Newsletters',
                            'fr' => 'Lettres d\'information'
                        ]
                    ],
                    'has_archive' => false,
                    'rewrite' => [
                        'site' => [
                            'base' => 'newsletter'
                        ],
                        'admin' => [
                            'base' => 'newsletter',
                            'archive' => 'newsletters'
                        ]
                    ],
                    'controller' => [
                        'site' => 'Site\Taxonomies\NewsletterController',
                        'admin' => 'Admin\Taxonomies\NewsletterController'
                    ]
                ],
                'edition' => [
                    'label' => [
                        'singular' => [
                            'en' => 'Edition',
                            'fr' => 'Édition'
                        ],
                        'plural' => [
                            'en' => 'Editions',
                            'fr' => 'Éditions'
                        ]
                    ],
                    'has_archive' => true,
                    'rewrite' => [
                        'site' => [
                            'base' => 'edition',
                        ],
                        'admin' => [
                            'base' => 'edition',
                            'archive' => 'editions'
                        ]
                    ],
                    'controller' => [
                        'site' => 'Site\Taxonomies\EditionController',
                        'admin' => 'Admin\Taxonomies\EditionController'
                    ]
                ],
                'team-category' => [
                    'label' => [
                        'singular' => [
                            'en' => 'Team category',
                            'fr' => 'Catégorie d\'équipe'
                        ],
                        'plural' => [
                            'en' => 'Team categories',
                            'fr' => 'Catégories d\'équipe'
                        ]
                    ],
                    'has_archive' => false,
                    'rewrite' => [
                        'admin' => [
                            'base' => 'team-category',
                            'archive' => 'team-categories'
                        ]
                    ],
                    'controller' => [
                        'site' => 'Site\Taxonomies\TeamCategoryController',
                        'admin' => 'Admin\Taxonomies\TeamCategoryController'
                    ]
                ],
                'team-member' => [
                    'label' => [
                        'singular' => [
                            'en' => 'Team member',
                            'fr' => 'Membre de l\'équipe'
                        ],
                        'plural' => [
                            'en' => 'Team members',
                            'fr' => 'Membres de l\'équipe'
                        ]
                    ],
                    'has_archive' => true,
                    'rewrite' => [
                        'site' => [
                            'base' => 'team-member',
                            'archive' => 'team-members'
                        ],
                        'admin' => [
                            'base' => 'team-member',
                            'archive' => 'team-members'
                        ]
                    ],
                    'controller' => [
                        'site' => 'Site\Taxonomies\TeamMemberController',
                        'admin' => 'Admin\Taxonomies\TeamMemberController'
                    ]
                ],
                'resource-category' => [
                    'label' => [
                        'singular' => [
                            'en' => 'Resource category',
                            'fr' => 'Catégorie de ressource'
                        ],
                        'plural' => [
                            'en' => 'Resource categories',
                            'fr' => 'Catégories de ressources'
                        ]
                    ],
                    'has_archive' => false,
                    'rewrite' => [
                        'admin' => [
                            'base' => 'resource-category',
                            'archive' => 'resource-categories'
                        ]
                    ],
                    'controller' => [
                        'site' => 'Site\Taxonomies\ResourceCategoryController',
                        'admin' => 'Admin\Taxonomies\ResourceCategoryController'
                    ]
                ],
                'resource' => [
                    'label' => [
                        'singular' => [
                            'en' => 'Resource',
                            'fr' => 'Ressource'
                        ],
                        'plural' => [
                            'en' => 'Resources',
                            'fr' => 'Ressources'
                        ]
                    ],
                    'has_archive' => false,
                    'rewrite' => [
                        'site' => [
                            'base' => false,
                            'archive' => false
                        ],
                        'admin' => [
                            'base' => 'resource',
                            'archive' => 'resources'
                        ]
                    ],
                    'controller' => [
                        'admin' => 'Admin\Taxonomies\ResourceController'
                    ]
                ],
                'partner-category' => [
                    'label' => [
                        'singular' => [
                            'en' => 'Partner category',
                            'fr' => 'Catégorie partenaire'
                        ],
                        'plural' => [
                            'en' => 'Partner categories',
                            'fr' => 'Catégories partenaires'
                        ]
                    ],
                    'has_archive' => false,
                    'rewrite' => [
                        'admin' => [
                            'base' => 'partner-category',
                            'archive' => 'partner-categories'
                        ]
                    ],
                    'controller' => [
                        'site' => 'Site\Taxonomies\PartnerCategoryController',
                        'admin' => 'Admin\Taxonomies\PartnerCategoryController'
                    ]
                ],
                'partner' => [
                    'label' => [
                        'singular' => [
                            'en' => 'Partner',
                            'fr' => 'Partenaire'
                        ],
                        'plural' => [
                            'en' => 'Partners',
                            'fr' => 'Les Partenaires'
                        ]
                    ],
                    'has_archive' => false,
                    'rewrite' => [
                        'site' => [
                            'base' => false,
                            'archive' => false
                        ],
                        'admin' => [
                            'base' => 'partner',
                            'archive' => 'partners'
                        ]
                    ],
                    'controller' => [
                        'admin' => 'Admin\Taxonomies\PartnerController'
                    ]
                ],
                'menu' => [
                    'label' => [
                        'singular' => [
                            'en' => 'Menu',
                            'fr' => 'Le menu'
                        ],
                        'plural' => [
                            'en' => 'Menus',
                            'fr' => 'Menus'
                        ]
                    ],
                    'has_archive' => false,
                    'rewrite' => [
                        'site' => [
                            'base' => false,
                            'archive' => false
                        ],
                        'admin' => [
                            'base' => 'menu',
                            'archive' => 'menus'
                        ]
                    ],
                    'controller' => [
                        'admin' => 'Admin\Taxonomies\MenuController'
                    ]
                ],
                'menu-item' => [
                    'label' => [
                        'singular' => [
                            'en' => 'Menu item',
                            'fr' => 'Élément du menu'
                        ],
                        'plural' => [
                            'en' => 'Menu items',
                            'fr' => 'Éléments du menu'
                        ]
                    ],
                    'has_archive' => false,
                    'rewrite' => [
                        'site' => [
                            'base' => false,
                            'archive' => false
                        ],
                        'admin' => [
                            'base' => 'menu-item',
                            'archive' => 'menu-items'
                        ]
                    ],
                    'controller' => [
                        'admin' => 'Admin\Taxonomies\MenuItemController'
                    ]
                ],
                'website' => [
                    'label' => [
                        'singular' => [
                            'en' => 'Website',
                            'fr' => 'Site Internet'
                        ],
                        'plural' => [
                            'en' => 'Websites',
                            'fr' => 'Sites Internet'
                        ]
                    ],
                    'has_archive' => false,
                    'rewrite' => [
                        'site' => [
                            'base' => false,
                            'archive' => false
                        ],
                        'admin' => [
                            'base' => 'website',
                            'archive' => 'websites'
                        ]
                    ],
                    'controller' => [
                        'admin' => 'Admin\Taxonomies\WebsiteController'
                    ]
                ],
                'file' => [
                    'label' => [
                        'singular' => [
                            'en' => 'File',
                            'fr' => 'Fichier'
                        ],
                        'plural' => [
                            'en' => 'Files',
                            'fr' => 'Des dossiers'
                        ]
                    ],
                    'has_archive' => false,
                    'rewrite' => [
                        'site' => [
                            'base' => false,
                            'archive' => false
                        ],
                        'admin' => [
                            'base' => 'file',
                            'archive' => 'files'
                        ]
                    ],
                    'controller' => [
                        'admin' => 'Admin\Taxonomies\FileController'
                    ]
                ]
            ],
        ];

		App::store_options( $options );

		$translations = [
			[ 'en' => 'Read more', 'fr' => 'Lire la suite' ],
			[ 'en' => 'Search', 'fr' => 'Chercher' ],
			[ 'en' => 'Username', 'fr' => 'Nom d\'utilisateur' ],
			[ 'en' => 'Password', 'fr' => 'Mot de passe' ],
			[ 'en' => 'Log In', 'fr' => 'S\'identifier' ],
			[ 'en' => 'Read article', 'fr' => 'Lire l\'article' ],
			[ 'en' => 'ARCOS at a glance', 'fr' => 'ARCOS en bref' ],
			[ 'en' => 'Contact us', 'fr' => 'Contactez nous' ],
			[ 'en' => 'Get involved', 'fr' => 'Être impliqué' ],
			[ 'en' => 'Connect with us', 'fr' => 'Connecte-toi avec nous' ],
			[ 'en' => 'January', 'fr' => 'Janvier' ],
			[ 'en' => 'February', 'fr' => 'Février' ],
			[ 'en' => 'March', 'fr' => 'Mars' ],
			[ 'en' => 'April', 'fr' => 'Avril' ],
			[ 'en' => 'May', 'fr' => 'Mai' ],
			[ 'en' => 'June', 'fr' => 'Juin' ],
			[ 'en' => 'July', 'fr' => 'Juillet' ],
			[ 'en' => 'August', 'fr' => 'Août' ],
			[ 'en' => 'September', 'fr' => 'Septembre' ],
			[ 'en' => 'October', 'fr' => 'Octobre' ],
			[ 'en' => 'November', 'fr' => 'Novembre' ],
			[ 'en' => 'December', 'fr' => 'Décembre' ],
			[ 'en' => 'Posted by', 'fr' => 'Posté par' ],
			[ 'en' => 'on', 'fr' => 'sur' ],
			[ 'en' => 'Share on', 'fr' => 'Partager sur' ],
			[ 'en' => 'under', 'fr' => 'en dessous de' ],
			[ 'en' => 'Categories', 'fr' => 'Catégories' ],
			[ 'en' => 'Related articles', 'fr' => 'Articles liés' ],
			[ 'en' => 'Menu', 'fr' => 'Menu' ],
			[ 'en' => 'Add page', 'fr' => 'Ajouter une page' ],
			[ 'en' => 'Edit page', 'fr' => 'Modifier la page' ],
			[ 'en' => 'Title', 'fr' => 'Titre' ],
			[ 'en' => 'Content', 'fr' => 'Contenu' ],
			[ 'en' => 'Choose an image', 'fr' => 'Choisissez une image' ],
			[ 'en' => 'Page parent', 'fr' => 'Parent de page' ],
			[ 'en' => 'Page template', 'fr' => 'Modèle de page' ],
			[ 'en' => 'Summary', 'fr' => 'Résumé' ],
			[ 'en' => 'Menu title', 'fr' => 'Titre du menu' ],
			[ 'en' => 'Show in menu', 'fr' => 'Afficher dans le menu' ],
			[ 'en' => 'Show this item', 'fr' => 'Afficher cet élément' ],
			[ 'en' => 'Menu order', 'fr' => 'Commande de menu' ],
			[ 'en' => 'Publish page', 'fr' => 'Publier la page' ],
			[ 'en' => 'Update page', 'fr' => 'Mise à jour de la page' ],
			[ 'en' => 'None', 'fr' => 'Aucun' ],
			[ 'en' => 'Thumbnail', 'fr' => 'La vignette' ],
			[ 'en' => 'Edit', 'fr' => 'Modifier' ],
			[ 'en' => 'Trash', 'fr' => 'Poubelle' ],
			[ 'en' => 'View', 'fr' => 'Vue' ],
			[ 'en' => 'Slug', 'fr' => 'Limace' ],
			[ 'en' => 'Read biography', 'fr' => 'Lire la biographie' ],
			[ 'en' => 'Country', 'fr' => 'Pays' ],
			[ 'en' => 'Position', 'fr' => 'Position' ],
			[ 'en' => 'Biography', 'fr' => 'La biographie' ],
			[ 'en' => 'Related projects', 'fr' => 'Projets liés' ],
			[ 'en' => 'Visit the website', 'fr' => 'Visitez le site Web' ],
			[ 'en' => 'Dashboard', 'fr' => 'Tableau de bord' ],
			[ 'en' => 'Search results for ?', 'fr' => 'Résultats de recherche pour ?' ],
			[ 'en' => 'Add translation', 'fr' => 'Ajouter une traduction' ],
			[ 'en' => 'Edit translation', 'fr' => 'Modifier la traduction' ],
			[ 'en' => 'Update translation', 'fr' => 'Mettre à jour la traduction' ],
			[ 'en' => 'Are you sure you want to delete this record?', 'fr' => 'Voulez-vous vraiment supprimer cet enregistrement?' ],
			[ 'en' => 'Page updated successfully', 'fr' => 'Page mise à jour avec succès' ],
			[ 'en' => 'Add category', 'fr' => 'Ajouter une catégorie' ],
			[ 'en' => 'Edit category', 'fr' => 'Modifier la catégorie' ],
			[ 'en' => 'Update category', 'fr' => 'Mettre à jour la catégorie' ],
			[ 'en' => 'Add article', 'fr' => 'Ajouter un article' ],
			[ 'en' => 'Edit article', 'fr' => 'Modifier l\'article' ],
			[ 'en' => 'Update article', 'fr' => 'Mise à jour de l\'article' ],
			[ 'en' => 'Add project', 'fr' => 'Ajouter un projet' ],
			[ 'en' => 'Edit project', 'fr' => 'Modifier le projet' ],
			[ 'en' => 'Update project', 'fr' => 'Mettre à jour le projet' ],
			[ 'en' => 'Add team category', 'fr' => 'Ajouter une catégorie d\'équipe' ],
			[ 'en' => 'Update team category', 'fr' => 'Mettre à jour la catégorie d\'équipe' ],
			[ 'en' => 'Edit team category', 'fr' => 'Modifier la catégorie d\'équipe' ],
			[ 'en' => 'Description', 'fr' => 'La description' ],
			[ 'en' => 'Order', 'fr' => 'Commande' ],
			[ 'en' => 'Add team member', 'fr' => 'Ajouter un membre de l\'équipe' ],
			[ 'en' => 'Edit team member', 'fr' => 'Éditer membre de l\'équipe' ],
			[ 'en' => 'Update team member', 'fr' => 'Mettre à jour les membres de l\'équipe' ],
			[ 'en' => 'Add slider', 'fr' => 'Ajouter un curseur' ],
			[ 'en' => 'Edit slider', 'fr' => 'Modifier le curseur' ],
			[ 'en' => 'Update slider', 'fr' => 'Curseur de mise à jour' ],
			[ 'en' => 'Taxonomy', 'fr' => 'Taxonomie' ],
			[ 'en' => 'Post type', 'fr' => 'Type de poste' ],
			[ 'en' => 'Entity type', 'fr' => 'Type d\'entité' ],
			[ 'en' => 'Entity name', 'fr' => 'Nom de l\'entité' ],
			[ 'en' => 'Entity', 'fr' => 'Entité' ],
			[ 'en' => 'Custom link', 'fr' => 'Lien personnalisé' ],
			[ 'en' => 'Edit menu', 'fr' => 'Modifier le menu' ],
			[ 'en' => 'Update menu', 'fr' => 'Mise à jour du menu' ],
			[ 'en' => 'Footer', 'fr' => 'Bas de page' ],
			[ 'en' => 'Edit about section', 'fr' => 'Modifier la section' ],
			[ 'en' => 'Update about section', 'fr' => 'Mise à jour de la section' ],
			[ 'en' => 'Anchor', 'fr' => 'Ancre' ],
			[ 'en' => 'Button text', 'fr' => 'Texte du bouton' ],
			[ 'en' => 'Add menu item', 'fr' => 'Ajouter un élément de menu' ],
			[ 'en' => 'Update menu item', 'fr' => 'Mettre à jour l\'élément de menu' ],
			[ 'en' => 'Edit menu item', 'fr' => 'Modifier l\'élément de menu' ],
			[ 'en' => 'URL', 'fr' => 'URL' ],
			[ 'en' => 'Add website', 'fr' => 'Ajouter un site web' ],
			[ 'en' => 'Edit website', 'fr' => 'Modifier le site' ],
			[ 'en' => 'Update website', 'fr' => 'Mise à jour du site' ],
			[ 'en' => 'Add newsletter', 'fr' => 'Ajouter un bulletin' ],
			[ 'en' => 'Edit newsletter', 'fr' => 'Modifier la newsletter' ],
			[ 'en' => 'Update newsletter', 'fr' => 'Mise à jour de la newsletter' ],
			[ 'en' => 'Mailman List', 'fr' => 'Mailman List' ],
			[ 'en' => 'See previous editions', 'fr' => 'Voir les éditions précédentes' ],
			[ 'en' => 'Subscribe to our newsletters', 'fr' => 'Abonnez-vous à nos newsletters' ],
			[ 'en' => 'Checkboxes of newsletters you want to subscribe to', 'fr' => 'Case à cocher des bulletins d\'information auxquels vous souhaitez vous abonner' ],
			[ 'en' => 'First name', 'fr' => 'Prénom' ],
			[ 'en' => 'Last name', 'fr' => 'Nom de famille' ],
			[ 'en' => 'Email address', 'fr' => 'Adresse e-mail' ],
			[ 'en' => 'Phone number', 'fr' => 'Numéro de téléphone' ],
			[ 'en' => 'Institution', 'fr' => 'Institution' ],
			[ 'en' => 'Job position', 'fr' => 'Poste' ],
			[ 'en' => 'Subscribe', 'fr' => 'Souscrire' ],
      [ 'en' => 'Add edition', 'fr' => 'Ajouter une édition' ],
      [ 'en' => 'Edit edition', 'fr' => 'Modifier l\'édition' ],
      [ 'en' => 'Update edition', 'fr' => 'Édition mise à jour' ],
      [ 'en' => 'Download', 'fr' => 'Télécharger' ],
      [ 'en' => 'Newsletter', 'fr' => 'Bulletin' ],
      [ 'en' => 'Status', 'fr' => 'Statut' ],
      [ 'en' => 'Draft', 'fr' => 'Brouillon' ],
      [ 'en' => 'Sent', 'fr' => 'Envoyé' ],
			[ 'en' => 'Add invoice', 'fr' => 'Ajouter une facture' ],
			[ 'en' => 'Edit invoice', 'fr' => 'Modifier la facture' ],
			[ 'en' => 'Update invoice', 'fr' => 'Mise à jour de la facture' ],
			[ 'en' => 'Add file', 'fr' => 'Ajouter le fichier' ],
			[ 'en' => 'Edit file', 'fr' => 'Modifier le fichier' ],
			[ 'en' => 'Update file', 'fr' => 'Fichier de mise à jour' ],
			[ 'en' => 'Amount', 'fr' => 'Montant' ],
			[ 'en' => 'Payment invoice', 'fr' => 'Facture de paiement' ],
			[ 'en' => 'Make payment', 'fr' => 'Effectuer le paiement' ],
			[ 'en' => 'Payment successful', 'fr' => 'Paiement réussi' ],
			[ 'en' => 'Payment cancelled', 'fr' => 'Paiement annulé' ],
			[ 'en' => 'Try again?', 'fr' => 'Réessayer?' ],
			[ 'en' => 'Redirecting to secure payment gateway', 'fr' => 'Redirection pour sécuriser la passerelle de paiement' ],
      [ 'en' => 'Add resource category', 'fr' => 'Ajouter une catégorie de ressource' ],
      [ 'en' => 'Edit resource category', 'fr' => 'Modifier la catégorie de ressource' ],
      [ 'en' => 'Update resource category', 'fr' => 'Mettre à jour la catégorie de ressource' ],
      [ 'en' => 'Add partner category', 'fr' => 'Ajouter la catégorie partenaire' ],
      [ 'en' => 'Edit partner category', 'fr' => 'Modifier la catégorie partenaire' ],
      [ 'en' => 'Update partner category', 'fr' => 'Mise à jour de la catégorie partenaire' ],
      [ 'en' => 'Add partner', 'fr' => 'Ajouter un partenaire' ],
      [ 'en' => 'Edit partner', 'fr' => 'Modifier le partenaire' ],
      [ 'en' => 'Update partner', 'fr' => 'Partenaire de mise à jour' ],
      [ 'en' => 'Add resource', 'fr' => 'Ajouter une ressource' ],
      [ 'en' => 'Edit resource', 'fr' => 'Modifier la ressource' ],
      [ 'en' => 'Update resource', 'fr' => 'Mettre à jour la ressource' ],
      [ 'en' => 'Visit website', 'fr' => 'Visiter le site' ]
		];

		foreach( $translations as $translation )
			Translation::add_translation( [
				'title' => $translation
			] );

		foreach( Language::all() as $language )
			$languages[] = ( object ) [
				'code' => $language->code,
				'name' => $language->name
			];

		View::share( [
			'languages' => $languages,
			'language' => App::get_current_language(),
			'site_info' => App::get_site_info(),
			'post_types' => App::get_post_types(),
			'taxonomies' => App::get_taxonomies(),
			'current_user' => User::get_user( Auth::id() ),
			'translations' => ( object ) [
				'edit' => Translation::get_translation( 'edit', App::get_current_language() ),
				'trash' => Translation::get_translation( 'trash', App::get_current_language() ),
				'view' => Translation::get_translation( 'view', App::get_current_language() ),
				'thumbnail' => Translation::get_translation( 'thumbnail', App::get_current_language() ),
				'title' => Translation::get_translation( 'title', App::get_current_language() ),
				'slug' => Translation::get_translation( 'slug', App::get_current_language() ),
				'description' => Translation::get_translation( 'description', App::get_current_language() ),
				'order' => Translation::get_translation( 'order', App::get_current_language() ),
				'position' => Translation::get_translation( 'position', App::get_current_language() ),

				'add_page' => Translation::get_translation( 'add-page', App::get_current_language() ),
				'edit_page' => Translation::get_translation( 'edit-page', App::get_current_language() ),
				'update_page' => Translation::get_translation( 'update-page', App::get_current_language() ),

				'add_article' => Translation::get_translation( 'add-article', App::get_current_language() ),
				'edit_article' => Translation::get_translation( 'edit-article', App::get_current_language() ),
				'update_article' => Translation::get_translation( 'update-article', App::get_current_language() ),

				'add_project' => Translation::get_translation( 'add-project', App::get_current_language() ),
				'edit_project' => Translation::get_translation( 'edit-project', App::get_current_language() ),
				'update_project' => Translation::get_translation( 'update-project', App::get_current_language() ),

				'form_translations' => ( object ) [
					'title' => ( object ) [
						'en' => Translation::get_translation( 'title', 'en' ),
						'fr' => Translation::get_translation( 'title', 'fr' )
					],
					'content' => ( object ) [
						'en' => Translation::get_translation( 'content', 'en' ),
						'fr' => Translation::get_translation( 'content', 'fr' )
					],
					'summary' => ( object ) [
						'en' => Translation::get_translation( 'summary', 'en' ),
						'fr' => Translation::get_translation( 'summary', 'fr' )
					],
					'menu' => ( object ) [
						'en' => Translation::get_translation( 'menu', 'en' ),
						'fr' => Translation::get_translation( 'menu', 'fr' )
					],
					'menu_title' => ( object ) [
						'en' => Translation::get_translation( 'menu-title', 'en' ),
						'fr' => Translation::get_translation( 'menu-title', 'fr' )
					],
					'description' => ( object ) [
						'en' => Translation::get_translation( 'description', 'en' ),
						'fr' => Translation::get_translation( 'description', 'fr' )
					],
					'country' => ( object ) [
						'en' => Translation::get_translation( 'country', 'en' ),
						'fr' => Translation::get_translation( 'country', 'fr' )
					],
					'position' => ( object ) [
						'en' => Translation::get_translation( 'position', 'en' ),
						'fr' => Translation::get_translation( 'position', 'fr' )
					],
					'biography' => ( object ) [
						'en' => Translation::get_translation( 'biography', 'en' ),
						'fr' => Translation::get_translation( 'biography', 'fr' )
					],
					'button_text' => ( object ) [
						'en' => Translation::get_translation( 'button-text', 'en' ),
						'fr' => Translation::get_translation( 'button-text', 'fr' )
					],
				],

				'show_in_menu' => Translation::get_translation( 'show-in-menu', App::get_current_language() ),
				'show_this_item' => Translation::get_translation( 'show-this-item', App::get_current_language() ),
				'menu_order' => Translation::get_translation( 'menu-order', App::get_current_language() ),

				'dashboard' => Translation::get_translation( 'dashboard', App::get_current_language() ),
				'footer' => Translation::get_translation( 'footer', App::get_current_language() ),

				'add_translation' => Translation::get_translation( 'add-translation', App::get_current_language() ),
				'edit_translation' => Translation::get_translation( 'edit-translation', App::get_current_language() ),
				'update_translation' => Translation::get_translation( 'update-translation', App::get_current_language() ),

				'add_category' => Translation::get_translation( 'add-category', App::get_current_language() ),
				'edit_category' => Translation::get_translation( 'edit-category', App::get_current_language() ),
				'update_category' => Translation::get_translation( 'update-category', App::get_current_language() ),

				'add_team_category' => Translation::get_translation( 'add-team-category', App::get_current_language() ),
				'edit_team_category' => Translation::get_translation( 'edit-team-category', App::get_current_language() ),
				'update_team_category' => Translation::get_translation( 'update-team-category', App::get_current_language() ),

				'add_team_member' => Translation::get_translation( 'add-team-member', App::get_current_language() ),
				'edit_team_member' => Translation::get_translation( 'edit-team-member', App::get_current_language() ),
				'update_team_member' => Translation::get_translation( 'update-team-member', App::get_current_language() ),

				'add_slider' => Translation::get_translation( 'add-slider', App::get_current_language() ),
				'edit_slider' => Translation::get_translation( 'edit-slider', App::get_current_language() ),
				'update_slider' => Translation::get_translation( 'update-slider', App::get_current_language() ),

				'entity_name' => Translation::get_translation( 'entity-name', App::get_current_language() ),
				'entity_type' => Translation::get_translation( 'entity-type', App::get_current_language() ),
				'entity' => Translation::get_translation( 'entity', App::get_current_language() ),
				'custom_link' => Translation::get_translation( 'custom-link', App::get_current_language() ),

				'edit_menu' => Translation::get_translation( 'edit-menu', App::get_current_language() ),
				'update_menu' => Translation::get_translation( 'update-menu', App::get_current_language() ),

				'menu' => Translation::get_translation( 'menu', App::get_current_language() ),
				'add_menu_item' => Translation::get_translation( 'add-menu-item', App::get_current_language() ),
				'edit_menu_item' => Translation::get_translation( 'edit-menu-item', App::get_current_language() ),
				'update_menu_item' => Translation::get_translation( 'update-menu-item', App::get_current_language() ),

				'edit_about_section' => Translation::get_translation( 'edit-about-section', App::get_current_language() ),
				'update_about_section' => Translation::get_translation( 'update-about-section', App::get_current_language() ),

				'anchor' => Translation::get_translation( 'anchor', App::get_current_language() ),
				'url' => Translation::get_translation( 'url', App::get_current_language() ),

				'add_website' => Translation::get_translation( 'add-website', App::get_current_language() ),
				'edit_website' => Translation::get_translation( 'edit-website', App::get_current_language() ),
				'update_website' => Translation::get_translation( 'update-website', App::get_current_language() ),

				'add_newsletter' => Translation::get_translation( 'add-newsletter', App::get_current_language() ),
				'edit_newsletter' => Translation::get_translation( 'edit-newsletter', App::get_current_language() ),
				'update_newsletter' => Translation::get_translation( 'update-newsletter', App::get_current_language() ),

				'add_invoice' => Translation::get_translation( 'add-invoice', App::get_current_language() ),
				'edit_invoice' => Translation::get_translation( 'edit-invoice', App::get_current_language() ),
				'update_invoice' => Translation::get_translation( 'update-invoice', App::get_current_language() ),

                'add_file' => Translation::get_translation( 'add-file', App::get_current_language() ),
                'edit_file' => Translation::get_translation( 'edit-file', App::get_current_language() ),
                'update_file' => Translation::get_translation( 'update-file', App::get_current_language() ),

                'add_resource_category' => Translation::get_translation( 'add-resource-category', App::get_current_language() ),
                'edit_resource_category' => Translation::get_translation( 'edit-resource-category', App::get_current_language() ),
                'update_resource_category' => Translation::get_translation( 'update-resource-category', App::get_current_language() ),

                'add_partner_category' => Translation::get_translation( 'add-partner-category', App::get_current_language() ),
                'edit_partner_category' => Translation::get_translation( 'edit-partner-category', App::get_current_language() ),
                'update_partner_category' => Translation::get_translation( 'update-partner-category', App::get_current_language() ),

                'add_partner' => Translation::get_translation( 'add-partner', App::get_current_language() ),
                'edit_partner' => Translation::get_translation( 'edit-partner', App::get_current_language() ),
                'update_partner' => Translation::get_translation( 'update-partner', App::get_current_language() ),

                'add_resource' => Translation::get_translation( 'add-resource', App::get_current_language() ),
                'edit_resource' => Translation::get_translation( 'edit-resource', App::get_current_language() ),
                'update_resource' => Translation::get_translation( 'update-resource', App::get_current_language() ),

				'mailman_list' => Translation::get_translation( 'mailman-list', App::get_current_language() ),

				'are_you_sure_you_want_to_delete_this_record' => Translation::get_translation( 'are-you-sure-you-want-to-delete-this-record', App::get_current_language() ),

                'add_edition' => Translation::get_translation( 'add-edition', App::get_current_language() ),
				'edit_edition' => Translation::get_translation( 'edit-edition', App::get_current_language() ),
				'update_edition' => Translation::get_translation( 'update-edition', App::get_current_language() ),

                'download' => Translation::get_translation( 'download', App::get_current_language() ),
                'newsletter' => Translation::get_translation( 'newsletter', App::get_current_language() ),
                'status' => Translation::get_translation( 'status', App::get_current_language() ),

                'draft' => Translation::get_translation( 'draft', App::get_current_language() ),
                'sent' => Translation::get_translation( 'sent', App::get_current_language() ),
				'amount' => Translation::get_translation( 'amount', App::get_current_language() ),

				'payment_invoice' => Translation::get_translation( 'payment-invoice', App::get_current_language() ),
				'make_payment' => Translation::get_translation( 'make-payment', App::get_current_language() ),

				'first_name' => Translation::get_translation( 'first-name', App::get_current_language() ),
				'last_name' => Translation::get_translation( 'last-name', App::get_current_language() ),
				'email_address' => Translation::get_translation( 'email-address', App::get_current_language() ),
			]
		] );
	}

}
