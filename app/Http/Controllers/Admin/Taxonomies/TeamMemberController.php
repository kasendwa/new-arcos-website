<?php

namespace App\Http\Controllers\Admin\Taxonomies;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

use App\App;
use App\Entities\Translation;
use App\Entities\TeamCategory;
use App\Entities\Attachment;
use App\Entities\TeamMember;
use App\Http\Controllers\Admin\AdminController;

class TeamMemberController extends AdminController {

	public function archive( $lang, $page = 1 ) {
		$taxonomy = App::get_taxonomy( 'team-member' );
		$per_page = 15;
		$page = ( int ) $page;

		foreach( TeamMember::get_team_members( $per_page, $page ) as $entry ) :

			$entry = TeamMember::get_team_member( $entry );

			$team_members[] = ( object ) [
				'id' => $entry->id,
				'avatar' => $entry->avatar,
				'title' => $entry->name,
				'permalink' => $entry->permalink
			];

		endforeach;

		return view( 'admin.team-members.archive', [
			'page_title' => $taxonomy->label->plural->{ $lang },
			'pagination' => ( object ) [
				'base_link' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->archive ),
				'active' => $page,
				'total' => ceil( count( TeamMember::get_all_team_members() ) / $per_page )
			],
			'items' => isset( $team_members ) ? $team_members : [],
			'items_actions_base' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->base ),
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function edit( $lang, $id ) {
		$taxonomy = App::get_taxonomy( 'team-member' );

		$team_member = TeamMember::get_team_member( ( int ) $id );

		foreach( TeamCategory::get_all_team_categories() as $entry ) :

			$entry = TeamCategory::get_team_category( $entry );

			$team_categories[ $entry->id ] = $entry->title->{ $lang };

		endforeach;

		return view( 'admin.team-members.edit', [
			'page_title' => Translation::get_translation( 'edit-team-member', $lang ),
			'item_data' => $team_member,
			'team_categories' => $team_categories,
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function update( $lang, $id ) {
		App::update_term_meta( $id, 'name', Input::get( 'name' ) );

		foreach( [ 'country', 'position', 'biography' ] as $element )

			foreach( App::get_languages() as $language )
				App::update_term_meta( $id, $element . '_' . $language->code, Input::get( $element . '_' . $language->code ) );

		App::update_term_meta( $id, 'team_category', Input::get( 'team_category' ) );
		App::update_term_meta( $id, 'order', Input::get( 'order' ) );

		if ( Input::hasFile( 'avatar' ) ) :

			$file = Input::file( 'avatar' );
			$file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

			$current = ( int ) App::get_term_meta( $id, 'avatar', 0 );

			if ( $current > 0 ) :

				App::delete_term_meta( $id, 'avatar' );

				$attachment = new Attachment( $current );

				$attachment->delete();

			endif;

			App::update_term_meta( $id, 'avatar', App::upload_media_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

			File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

		endif;

		$taxonomy = App::get_taxonomy( 'team-member' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?updated=true' );
	}

	public function delete( $lang, $id ) {
		$team_category = new TeamCategory( ( int ) $id );
		$team_category->delete();

		$taxonomy = App::get_taxonomy( 'team-category' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?deleted=true' );
	}

	public function add( $lang ) {
		$taxonomy = App::get_taxonomy( 'team-member' );

		if ( Input::get( 'name' ) != null ) :

			$term_id = TeamMember::add_team_member( [
				'name' => Input::get( 'name' ),
				'country' => [
					'en' => Input::get( 'country_en' ),
					'fr' => Input::get( 'country_fr' )
				],
				'position' => [
					'en' => Input::get( 'position_en' ),
					'fr' => Input::get( 'position_fr' )
				],
				'biography' => [
					'en' => Input::get( 'biography_en' ),
					'fr' => Input::get( 'biography_fr' )
				],
				'team_category' => Input::get( 'team_category' ),
				'order' => Input::get( 'order' )
			] );

			if ( Input::hasFile( 'avatar' ) ) :

				$file = Input::file( 'avatar' );
				$file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

				App::update_term_meta( $term_id, 'avatar', App::upload_media_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

				File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

			endif;

			TeamCategory::add_team_category( [
				'title' => [
					'en' => Input::get( 'title_en' ),
					'fr' => Input::get( 'title_fr' )
				],
				'order' => Input::get( 'order' )
			] );

			return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?added=true' );

		else :

			foreach( TeamCategory::get_all_team_categories() as $entry ) :

				$entry = TeamCategory::get_team_category( $entry );

				$team_categories[ $entry->id ] = $entry->title->{ $lang };

			endforeach;

			return view( 'admin.team-members.add', [
				'page_title' => Translation::get_translation( 'add-team-member', $lang ),
				'team_categories' => isset( $team_categories ) ? $team_categories : [],
				'active_menu' => $taxonomy->label->plural->{ $lang }
			] );

		endif;
	}

}