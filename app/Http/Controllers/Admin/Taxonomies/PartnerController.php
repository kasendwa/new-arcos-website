<?php

namespace App\Http\Controllers\Admin\Taxonomies;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

use App\App;
use App\Entities\Translation;
use App\Entities\Partner;
use App\Entities\PartnerCategory;
use App\Entities\Attachment;
use App\Http\Controllers\Admin\AdminController;

class PartnerController extends AdminController {

	public function archive( $lang, $page = 1 ) {
		$taxonomy = App::get_taxonomy( 'partner' );
		$per_page = 15;
		$page = ( int ) $page;

		foreach( Partner::get_partners( $per_page, $page ) as $entry ) :

			$entry = Partner::get_partner( $entry );

			$partners[] = ( object ) [
				'id' => $entry->id,
				'logo' => $entry->logo,
				'name' => $entry->name->{ $lang },
				'summary' => $entry->summary,
				'url' => $entry->url,
				'order' => $entry->order
			];

		endforeach;

		return view( 'admin.partners.archive', [
			'page_title' => $taxonomy->label->plural->{ $lang },
			'pagination' => ( object ) [
				'base_link' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->archive ),
				'active' => $page,
				'total' => ceil( count( Partner::get_all_partners() ) / $per_page )
			],
			'items' => isset( $partners ) ? $partners : [],
			'items_actions_base' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->base ),
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function edit( $lang, $id ) {
		$taxonomy = App::get_taxonomy( 'partner' );

        $partner = Partner::get_partner( ( int ) $id );

        foreach( PartnerCategory::get_all_partner_categories() as $entry ) :

            $entry = PartnerCategory::get_partner_category( $entry );

            $partner_categories[ $entry->id ] = $entry->title->{ $lang };

        endforeach;

		return view( 'admin.partners.edit', [
			'page_title' => Translation::get_translation( 'edit-partner', $lang ),
			'item_data' => $partner,
            'partner_categories' => $partner_categories,
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function update( $lang, $id ) {
		foreach( [ 'name', 'summary', 'motto' ] as $element )

			foreach( App::get_languages() as $language )
				App::update_term_meta( $id, $element . '_' . $language->code, Input::get( $element . '_' . $language->code ) );

		App::update_term_meta( $id, 'order', Input::get( 'order' ) );
        App::update_term_meta( $id, 'category', Input::get( 'partner_category' ) );
		App::update_term_meta( $id, 'url', Input::get( 'url' ) );

		if ( Input::hasFile( 'image' ) ) :

			$file = Input::file( 'image' );
			$file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

			$current = ( int ) App::get_term_meta( $id, 'logo', 0 );

			if ( $current > 0 ) :

				App::delete_term_meta( $id, 'logo' );

				$attachment = new Attachment( $current );

				$attachment->delete();

			endif;

			App::update_term_meta( $id, 'logo', App::upload_media_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

			File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

		endif;

		$taxonomy = App::get_taxonomy( 'partner' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?updated=true' );
	}

	public function add( $lang ) {
		$taxonomy = App::get_taxonomy( 'partner' );

		if ( Input::get( 'name_en' ) != null ) :

			$term_id = Partner::add_partner( [
				'name' => [
					'en' => Input::get( 'name_en' ),
					'fr' => Input::get( 'name_fr' )
				],
                'motto' => [
                    'en' => Input::get( 'motto_en' ),
                    'fr' => Input::get( 'motto_fr' )
                ],
				'summary' => [
					'en' => Input::get( 'summary_en' ),
					'fr' => Input::get( 'summary_fr' )
				],
				'url' => Input::get( 'url' ),
				'category' => Input::get( 'partner_category' ),
				'order' => Input::get( 'order' )
			] );

			if ( Input::hasFile( 'image' ) ) :

				$file = Input::file( 'image' );
				$file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

				App::update_term_meta( $term_id, 'logo', App::upload_media_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

				File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

			endif;

			return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?added=true' );

		else :

            foreach( PartnerCategory::get_all_partner_categories() as $entry ) :

                $entry = PartnerCategory::get_partner_category( $entry );

                $partner_categories[ $entry->id ] = $entry->title->{ $lang };

            endforeach;

			return view( 'admin.partners.add', [
				'page_title' => Translation::get_translation( 'add-partner', $lang ),
                'partner_categories' => isset( $partner_categories ) ? $partner_categories : [],
				'active_menu' => $taxonomy->label->plural->{ $lang }
			] );

		endif;
	}

	public function delete( $lang, $id ) {
		$partner = new Partner( ( int ) $id );
		$partner->delete();

		$taxonomy = App::get_taxonomy( 'partner' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?deleted=true' );
	}

}