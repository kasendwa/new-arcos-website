<?php

namespace App\Http\Controllers\Admin\Taxonomies;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;

use App\App;
use App\Entities\Translation;
use App\Entities\TeamCategory;
use App\Http\Controllers\Admin\AdminController;

class TeamCategoryController extends AdminController {

	public function archive( $lang, $page = 1 ) {
		$taxonomy = App::get_taxonomy( 'team-category' );
		$per_page = 15;
		$page = ( int ) $page;

		foreach( TeamCategory::get_team_categories( $per_page, $page ) as $entry ) :

			$entry = TeamCategory::get_team_category( $entry );

			$team_categories[] = ( object ) [
				'id' => $entry->id,
				'title' => $entry->title->{ $lang }
			];

		endforeach;

		return view( 'admin.team-categories.archive', [
			'page_title' => $taxonomy->label->plural->{ $lang },
			'pagination' => ( object ) [
				'base_link' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->archive ),
				'active' => $page,
				'total' => ceil( count( TeamCategory::get_all_team_categories() ) / $per_page )
			],
			'items' => isset( $team_categories ) ? $team_categories : [],
			'items_actions_base' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->base ),
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function edit( $lang, $id ) {
		$taxonomy = App::get_taxonomy( 'team-category' );

		$team_category = TeamCategory::get_team_category( ( int ) $id );

		return view( 'admin.team-categories.edit', [
			'page_title' => Translation::get_translation( 'edit-team-category', $lang ),
			'item_data' => $team_category,
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function update( $lang, $id ) {
		foreach( App::get_languages() as $language )
			App::update_term_meta( $id, 'title_' . $language->code, Input::get( 'title_' . $language->code ) );

		App::update_term_meta( $id, 'order', Input::get( 'order' ) );

		$taxonomy = App::get_taxonomy( 'team-category' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?updated=true' );
	}

	public function delete( $lang, $id ) {
		$team_category = new TeamCategory( ( int ) $id );
		$team_category->delete();

		$taxonomy = App::get_taxonomy( 'team-category' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?deleted=true' );
	}

	public function add( $lang ) {
		$taxonomy = App::get_taxonomy( 'team-category' );

		if ( Input::get( 'title_en' ) != null ) :

			TeamCategory::add_team_category( [
				'title' => [
					'en' => Input::get( 'title_en' ),
					'fr' => Input::get( 'title_fr' )
				],
				'order' => Input::get( 'order' )
			] );

			return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?added=true' );

		else :

			return view( 'admin.team-categories.add', [
				'page_title' => Translation::get_translation( 'add-team-category', $lang ),
				'active_menu' => $taxonomy->label->plural->{ $lang }
			] );

		endif;
	}

}