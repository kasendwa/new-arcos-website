<?php

namespace App\Http\Controllers\Admin\Taxonomies;

use Illuminate\Support\Facades\Input;

use App\App;
use App\Entities\Translation;
use App\Entities\Category;
use App\Http\Controllers\Admin\AdminController;

class CategoryController extends AdminController {

	public function archive( $lang, $page = 1 ) {
		$taxonomy = App::get_taxonomy( 'category' );
		$per_page = 15;
		$page = ( int ) $page;

		foreach( Category::get_categories( $per_page, $page ) as $entry ) :

			$entry = Category::get_category( $entry );

			$categories[] = ( object ) [
				'id' => $entry->id,
				'title' => $entry->title->{ $lang },
				'permalink' => $entry->permalink
			];

		endforeach;

		return view( 'admin.categories.archive', [
			'page_title' => $taxonomy->label->plural->{ $lang },
			'pagination' => ( object ) [
				'base_link' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->archive ),
				'active' => $page,
				'total' => ceil( count( Category::get_all_categories() ) / $per_page )
			],
			'items' => isset( $categories ) ? $categories : [],
			'items_actions_base' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->base ),
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function edit( $lang, $id ) {
		$taxonomy = App::get_taxonomy( 'category' );

		$category = Category::get_category( ( int ) $id );

		return view( 'admin.categories.edit', [
			'page_title' => Translation::get_translation( 'edit-category', $lang ),
			'item_data' => $category,
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function update( $lang, $id ) {
		foreach( App::get_languages() as $language ) :

			App::update_term_meta( $id, 'title_' . $language->code, Input::get( 'title_' . $language->code ) );
			App::update_term_meta( $id, 'description_' . $language->code, Input::get( 'description_' . $language->code ) );

		endforeach;

		$taxonomy = App::get_taxonomy( 'category' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?updated=true' );
	}

	public function delete( $lang, $id ) {
		$category = new Category( ( int ) $id );
		$category->delete();

		$taxonomy = App::get_taxonomy( 'category' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?deleted=true' );
	}

	public function add( $lang ) {
		$taxonomy = App::get_taxonomy( 'category' );

		if ( Input::get( 'title_en' ) != null ) :

			Category::add_category( [
				'title' => [
					'en' => Input::get( 'title_en' ),
					'fr' => Input::get( 'title_fr' )
				],
				'description' => [
					'en' => Input::get( 'description_en' ),
					'fr' => Input::get( 'description_fr' )
				]
			] );

			return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?added=true' );

		else :

			return view( 'admin.categories.add', [
				'page_title' => Translation::get_translation( 'add-category', $lang ),
				'active_menu' => $taxonomy->label->plural->{ $lang }
			] );

		endif;
	}

}