<?php

namespace App\Http\Controllers\Admin\Taxonomies;

use App\Http\Controllers\Admin\AdminController;

class LanguageController extends AdminController {

	public function archive() {
		return view( 'admin.languages.archive' );
	}

	public function single() {
		return view( 'admin.languages.single' );
	}

}