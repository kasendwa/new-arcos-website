<?php

namespace App\Http\Controllers\Admin\Taxonomies;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

use App\App;
use App\Entities\Translation;
use App\Entities\MailmanList;
use App\Entities\Newsletter;
use App\Http\Controllers\Admin\AdminController;

class NewsletterController extends AdminController {

	public function archive( $lang, $page = 1 ) {
		$taxonomy = App::get_taxonomy( 'newsletter' );
		$per_page = 15;
		$page = ( int ) $page;

		foreach( Newsletter::get_newsletters( $per_page, $page ) as $entry ) :

			$entry = Newsletter::get_newsletter( $entry );

			$newsletters[] = ( object ) [
				'id' => $entry->id,
				'title' => $entry->title->{ $lang },
				'thumbnail' => $entry->thumbnail,
				'mailman_list' => $entry->mailman_list,
				'permalink' => $entry->permalink
			];

		endforeach;

		return view( 'admin.newsletters.archive', [
			'page_title' => $taxonomy->label->plural->{ $lang },
			'pagination' => ( object ) [
				'base_link' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->archive ),
				'active' => $page,
				'total' => ceil( count( MailmanList::get_all_mailman_lists() ) / $per_page )
			],
			'items' => isset( $newsletters ) ? $newsletters : [],
			'lists' => ( array ) MailmanList::fetch_mailman_lists(),
			'items_actions_base' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->base ),
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function add( $lang ) {
		$taxonomy = App::get_taxonomy( 'newsletter' );

		if ( Input::get( 'title_en' ) != null ) :

			$term_id = Newsletter::add_newsletter( [
				'title' => [
					'en' => Input::get( 'title_en' ),
					'fr' => Input::get( 'title_fr' )
				],
				'description' => [
					'en' => Input::get( 'description_en' ),
					'fr' => Input::get( 'description_fr' )
				],
				'mailman_list' => Input::get( 'mailman_list' )
			] );

			if ( Input::hasFile( 'image' ) ) :

				$file = Input::file( 'image' );
				$file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

				App::update_term_meta( $term_id, 'image', App::upload_media_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

				File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

			endif;

			return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?added=true' );

		else :

			return view( 'admin.newsletters.add', [
				'page_title' => Translation::get_translation( 'add-newsletter', $lang ),
				'lists' => ( array ) MailmanList::fetch_mailman_lists(),
				'active_menu' => $taxonomy->label->plural->{ $lang }
			] );

		endif;
	}

	public function delete( $lang, $id ) {
		$newsletter = new Newsletter( ( int ) $id );
		$newsletter->delete();

		$taxonomy = App::get_taxonomy( 'newsletter' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?deleted=true' );
	}

	public function edit( $lang, $id ) {
		$taxonomy = App::get_taxonomy( 'newsletter' );

		$newsletter = Newsletter::get_newsletter( ( int ) $id );

		return view( 'admin.newsletters.edit', [
			'page_title' => Translation::get_translation( 'edit-newsletter', $lang ),
			'item_data' => $newsletter,
			'lists' => MailmanList::fetch_mailman_lists(),
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function update( $lang, $id ) {
		foreach( [ 'title', 'description' ] as $element )

			foreach( App::get_languages() as $language )
				App::update_term_meta( $id, $element . '_' . $language->code, Input::get( $element . '_' . $language->code ) );

		App::update_term_meta( $id, 'mailman_list', Input::get( 'mailman_list' ) );

		if ( Input::hasFile( 'image' ) ) :

			$file = Input::file( 'image' );
			$file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

			$current = ( int ) App::get_term_meta( $id, 'image', 0 );

			if ( $current > 0 ) :

				App::delete_term_meta( $id, 'image' );

				$attachment = new Attachment( $current );

				$attachment->delete();

			endif;

			App::update_term_meta( $id, 'image', App::upload_media_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

			File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

		endif;

		$taxonomy = App::get_taxonomy( 'newsletter' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?updated=true' );
	}

}