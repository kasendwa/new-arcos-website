<?php

namespace App\Http\Controllers\Admin\Taxonomies;

use Illuminate\Support\Facades\Input;

use App\App;
use App\Entities\Translation;
use App\Entities\PartnerCategory;
use App\Http\Controllers\Admin\AdminController;

class PartnerCategoryController extends AdminController {

    public function archive( $lang, $page = 1 ) {
        $taxonomy = App::get_taxonomy( 'partner-category' );
        $per_page = 15;
        $page = ( int ) $page;

        foreach( PartnerCategory::get_partner_categories( $per_page, $page ) as $entry ) :

            $entry = PartnerCategory::get_partner_category( $entry );

            $partner_categories[] = ( object ) [
                'id' => $entry->id,
                'title' => $entry->title->{ $lang }
            ];

        endforeach;

        return view( 'admin.partner-categories.archive', [
            'page_title' => $taxonomy->label->plural->{ $lang },
            'pagination' => ( object ) [
                'base_link' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->archive ),
                'active' => $page,
                'total' => ceil( count( PartnerCategory::get_all_partner_categories() ) / $per_page )
            ],
            'items' => isset( $partner_categories ) ? $partner_categories : [],
            'items_actions_base' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->base ),
            'active_menu' => $taxonomy->label->plural->{ $lang }
        ] );
    }

    public function add( $lang ) {
        $taxonomy = App::get_taxonomy( 'partner-category' );

        if ( Input::get( 'title_en' ) != null ) :

            $term_id = PartnerCategory::add_partner_category( [
                'title' => [
                    'en' => Input::get( 'title_en' ),
                    'fr' => Input::get( 'title_fr' )
                ],
                'description' => [
                    'en' => Input::get( 'description_en' ),
                    'fr' => Input::get( 'description_fr' )
                ],
                'order' => Input::get( 'order' )
            ] );

            return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?added=true' );

        else :

            return view( 'admin.partner-categories.add', [
                'page_title' => Translation::get_translation( 'add-partner-category', $lang ),
                'active_menu' => $taxonomy->label->plural->{ $lang }
            ] );

        endif;
    }

    public function delete( $lang, $id ) {
        $partner_category = new PartnerCategory( ( int ) $id );
        $partner_category->delete();

        $taxonomy = App::get_taxonomy( 'partner-category' );

        return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?deleted=true' );
    }

    public function edit( $lang, $id ) {
        $taxonomy = App::get_taxonomy( 'partner-category' );

        $partner_category = PartnerCategory::get_partner_category( ( int ) $id );

        return view( 'admin.partner-categories.edit', [
            'page_title' => Translation::get_translation( 'edit-partner-category', $lang ),
            'item_data' => $partner_category,
            'active_menu' => $taxonomy->label->plural->{ $lang }
        ] );
    }

    public function update( $lang, $id ) {
        foreach( [ 'title', 'description' ] as $element )

            foreach( App::get_languages() as $language )
                App::update_term_meta( $id, $element . '_' . $language->code, Input::get( $element . '_' . $language->code ) );

        App::update_term_meta( $id, 'order', Input::get( 'order' ) );

        $taxonomy = App::get_taxonomy( 'partner-category' );

        return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?updated=true' );
    }

}