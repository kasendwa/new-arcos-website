<?php

namespace App\Http\Controllers\Admin\Taxonomies;

use Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

use App\App;
use App\Entities\Translation;
use App\Entities\Edition;
use App\Entities\Newsletter;
use App\Http\Controllers\Admin\AdminController;

class EditionController extends AdminController {

    public function archive( $lang, $page = 1 ) {
        $taxonomy = App::get_taxonomy( 'edition' );
        $per_page = 15;
        $page = ( int ) $page;

        foreach( Edition::get_editions( $per_page, $page ) as $entry ) :

            $entry = Edition::get_edition( $entry );

            $newsletter = Newsletter::get_newsletter( $entry->newsletter, $lang );

            $editions[] = ( object ) [
                'id' => $entry->id,
                'title' => $entry->title->{ $lang },
                'newsletter' => $newsletter->title,
                'status' => $entry->status,
                'permalink' => $entry->file
            ];

        endforeach;

        return view( 'admin.editions.archive', [
            'page_title' => $taxonomy->label->plural->{ $lang },
            'pagination' => ( object ) [
                'base_link' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->archive ),
                'active' => $page,
                'total' => ceil( count( Edition::get_all_editions() ) / $per_page )
            ],
            'items' => isset( $editions ) ? $editions : [],
            'items_actions_base' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->base ),
            'active_menu' => $taxonomy->label->plural->{ $lang }
        ] );
    }

    public function add( $lang ) {
        $taxonomy = App::get_taxonomy( 'edition' );

        if ( Input::get( 'title_en' ) != null ) :

            $term_id = Edition::add_edition( [
                'title' => [
                    'en' => Input::get( 'title_en' ),
                    'fr' => Input::get( 'title_fr' )
                ],
                'description' => [
                    'en' => Input::get( 'description_en' ),
                    'fr' => Input::get( 'description_fr' )
                ],
                'newsletter' => Input::get( 'newsletter' ),
                'status' => Input::get( 'status' )
            ] );

            if ( Input::hasFile( 'file' ) ) :

                $file = Input::file( 'file' );
                $file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

                App::update_term_meta( $term_id, 'file', App::upload_file_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

                File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

            endif;

			if ( Input::get( 'status' ) == 'sent' ) :

				$edition =  new Edition( $term_id );

				$edition->send();

			endif;

			return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?added=true' );

        else :

            foreach( Newsletter::get_all_newsletters() as $entry ) :

                $newsletter = Newsletter::get_newsletter( $entry, $lang );

                $newsletters[ $newsletter->id ] = $newsletter->title;

            endforeach;

            return view( 'admin.editions.add', [
                'page_title' => Translation::get_translation( 'add-edition', $lang ),
                'newsletters' => isset( $newsletters ) ? $newsletters : [],
                'active_menu' => $taxonomy->label->plural->{ $lang }
            ] );

        endif;
    }

    public function delete( $lang, $id ) {
        $newsletter = new Newsletter( ( int ) $id );
        $newsletter->delete();

        $taxonomy = App::get_taxonomy( 'edition' );

        return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?deleted=true' );
    }

    public function edit( $lang, $id ) {
        $taxonomy = App::get_taxonomy( 'edition' );

        $edition = Edition::get_edition( ( int ) $id );

        foreach( Newsletter::get_all_newsletters() as $entry ) :

            $newsletter = Newsletter::get_newsletter( $entry, $lang );

            $newsletters[ $newsletter->id ] = $newsletter->title;

        endforeach;

        return view( 'admin.editions.edit', [
            'page_title' => Translation::get_translation( 'edit-edition', $lang ),
            'item_data' => $edition,
            'newsletters' => isset( $newsletters ) ? $newsletters : [],
            'active_menu' => $taxonomy->label->plural->{ $lang }
        ] );
    }

    public function update( $lang, $id ) {
        foreach( [ 'title', 'description' ] as $element )

            foreach( App::get_languages() as $language )
                App::update_term_meta( $id, $element . '_' . $language->code, Input::get( $element . '_' . $language->code ) );

		if ( Input::get( 'status' ) !== null ) :

        	App::update_term_meta( $id, 'status', Input::get( 'status' ) );

		endif;

        App::update_term_meta( $id, 'newsletter', Input::get( 'newsletter' ) );

        if ( Input::hasFile( 'file' ) ) :

            $file = Input::file( 'file' );
            $file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

            $current = ( int ) App::get_term_meta( $id, 'file', 0 );

            if ( $current > 0 ) :

                App::delete_term_meta( $id, 'file' );

                $attachment = new Attachment( $current );

                $attachment->delete();

            endif;

            App::update_term_meta( $id, 'file', App::upload_file_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

            File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

        endif;

        $taxonomy = App::get_taxonomy( 'edition' );

        if ( Input::get( 'status' ) == 'sent' ) :

			$edition =  new Edition( $id );

        	$edition->send();

        endif;

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?updated=true' );
    }

}