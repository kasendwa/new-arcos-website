<?php

namespace App\Http\Controllers\Admin\Taxonomies;

use Illuminate\Support\Facades\Input;

use App\App;
use App\Entities\Translation;
use App\Entities\Attachment;
use App\Entities\File;
use App\Http\Controllers\Admin\AdminController;

class FileController extends AdminController {

	public function archive( $lang, $page = 1 ) {
		$taxonomy = App::get_taxonomy( 'file' );
		$per_page = 15;
		$page = ( int ) $page;

		foreach( File::get_files( $per_page, $page ) as $entry ) :

			$entry = File::get_file( $entry );

			$files[] = ( object ) [
				'id' => $entry->id,
				'name' => $entry->name->{ $lang },
				'url' => $entry->url
			];

		endforeach;

		return view( 'admin.files.archive', [
			'page_title' => $taxonomy->label->plural->{ $lang },
			'pagination' => ( object ) [
				'base_link' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->archive ),
				'active' => $page,
				'total' => ceil( count( FIle::get_all_files() ) / $per_page )
			],
			'items' => isset( $files ) ? $files : [],
			'items_actions_base' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->base ),
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function edit( $lang, $id ) {
		$taxonomy = App::get_taxonomy( 'file' );

		$file = File::get_file( ( int ) $id );

		return view( 'admin.files.edit', [
			'page_title' => Translation::get_translation( 'edit-file', $lang ),
			'item_data' => $file,
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function update( $lang, $id ) {
		foreach( [ 'name' ] as $element )

			foreach( App::get_languages() as $language )
				App::update_term_meta( $id, $element . '_' . $language->code, Input::get( $element . '_' . $language->code ) );

		if ( Input::hasFile( 'file' ) ) :

			$file = Input::file( 'file' );
			$file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

			$current = ( int ) App::get_term_meta( $id, 'file', 0 );

			if ( $current > 0 ) :

				App::delete_term_meta( $id, 'file' );

				$attachment = new Attachment( $current );

				$attachment->delete();

			endif;

			App::update_term_meta( $id, 'file', App::upload_file_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

            \Illuminate\Support\Facades\File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

		endif;

		$taxonomy = App::get_taxonomy( 'file' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?updated=true' );
	}

	public function add( $lang ) {
		$taxonomy = App::get_taxonomy( 'file' );

		if ( Input::get( 'name_en' ) != null ) :

			$term_id = File::add_file( [
				'name' => [
					'en' => Input::get( 'name_en' ),
					'fr' => Input::get( 'name_fr' )
				]
			] );

			if ( Input::hasFile( 'file' ) ) :

				$file = Input::file( 'file' );
				$file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

				App::update_term_meta( $term_id, 'file', App::upload_file_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

                \Illuminate\Support\Facades\File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

			endif;

			return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?added=true' );

		else :

			return view( 'admin.files.add', [
				'page_title' => Translation::get_translation( 'add-file', $lang ),
				'active_menu' => $taxonomy->label->plural->{ $lang }
			] );

		endif;
	}

	public function delete( $lang, $id ) {
		$file = new File( ( int ) $id );
		$file->delete();

		$taxonomy = App::get_taxonomy( 'file' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?deleted=true' );
	}

}