<?php

namespace App\Http\Controllers\Admin\Taxonomies;


use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;

use App\App;
use App\Entities\Translation;
use App\Entities\Menu;
use App\Http\Controllers\Admin\AdminController;

class MenuController extends AdminController {

	public function archive( $lang, $page = 1 ) {
		$taxonomy = App::get_taxonomy( 'menu' );
		$per_page = 15;
		$page = ( int ) $page;

		foreach( Menu::get_menus( $per_page, $page ) as $entry ) :

			$entry = Menu::get_menu( $entry );

			$menus[] = ( object ) [
				'id' => $entry->id,
				'title' => $entry->title->{ $lang },
				'position' => $entry->position
			];

		endforeach;

		return view( 'admin.menus.archive', [
			'page_title' => $taxonomy->label->plural->{ $lang },
			'pagination' => ( object ) [
				'base_link' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->archive ),
				'active' => $page,
				'total' => ceil( count( Menu::get_all_menus() ) / $per_page )
			],
			'items' => isset( $menus ) ? $menus : [],
			'items_actions_base' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->base ),
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function edit( $lang, $id ) {
		$taxonomy = App::get_taxonomy( 'menu' );

		$menu = Menu::get_menu( ( int ) $id );

		return view( 'admin.menus.edit', [
			'page_title' => Translation::get_translation( 'edit-menu', $lang ),
			'item_data' => $menu,
			'active_menu' => $taxonomy->label->plural->{ $lang },
			'positions' => [
				'footer-1' => Translation::get_translation( 'footer', $lang ) . ' 1',
				'footer-2' => Translation::get_translation( 'footer', $lang ) . ' 2',
				'footer-3' => Translation::get_translation( 'footer', $lang ) . ' 3',
				'footer-4' => Translation::get_translation( 'footer', $lang ) . ' 4'
			]
		] );
	}

	public function update( $lang, $id ) {
		foreach( App::get_languages() as $language )
			App::update_term_meta( $id, 'title_' . $language->code, Input::get( 'title_' . $language->code ) );

		App::update_term_meta( $id, 'position', Input::get( 'position' ) );

		$taxonomy = App::get_taxonomy( 'menu' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?updated=true' );
	}

}