<?php

namespace App\Http\Controllers\Admin\Taxonomies;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

use App\App;
use App\Entities\Translation;
use App\Entities\Resource;
use App\Entities\ResourceCategory;
use App\Entities\Attachment;
use App\Http\Controllers\Admin\AdminController;

class ResourceController extends AdminController {

	public function archive( $lang, $page = 1 ) {
		$taxonomy = App::get_taxonomy( 'resource' );
		$per_page = 15;
		$page = ( int ) $page;

		foreach( Resource::get_resources( $per_page, $page ) as $entry ) :

			$entry = Resource::get_resource( $entry );

			$resources[] = ( object ) [
				'id' => $entry->id,
				'screenshot' => $entry->screenshot,
				'name' => $entry->name->{ $lang },
				'summary' => $entry->summary,
				'url' => $entry->url,
				'order' => $entry->order
			];

		endforeach;

		return view( 'admin.resources.archive', [
			'page_title' => $taxonomy->label->plural->{ $lang },
			'pagination' => ( object ) [
				'base_link' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->archive ),
				'active' => $page,
				'total' => ceil( count( Resource::get_all_resources() ) / $per_page )
			],
			'items' => isset( $resources ) ? $resources : [],
			'items_actions_base' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->base ),
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function edit( $lang, $id ) {
		$taxonomy = App::get_taxonomy( 'resource' );

        $resource = Resource::get_resource( ( int ) $id );

        foreach( ResourceCategory::get_all_resource_categories() as $entry ) :

            $entry = ResourceCategory::get_resource_category( $entry );

            $resource_categories[ $entry->id ] = $entry->title->{ $lang };

        endforeach;

		return view( 'admin.resources.edit', [
			'page_title' => Translation::get_translation( 'edit-resource', $lang ),
			'item_data' => $resource,
            'resource_categories' => $resource_categories,
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function update( $lang, $id ) {
		foreach( [ 'name', 'summary' ] as $element )

			foreach( App::get_languages() as $language )
				App::update_term_meta( $id, $element . '_' . $language->code, Input::get( $element . '_' . $language->code ) );

		App::update_term_meta( $id, 'order', Input::get( 'order' ) );
        App::update_term_meta( $id, 'category', Input::get( 'resource_category' ) );

		if ( Input::hasFile( 'image' ) ) :

			$file = Input::file( 'image' );
			$file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

			$current = ( int ) App::get_term_meta( $id, 'screenshot', 0 );

			if ( $current > 0 ) :

				App::delete_term_meta( $id, 'screenshot' );

				$attachment = new Attachment( $current );

				$attachment->delete();

			endif;

			App::update_term_meta( $id, 'screenshot', App::upload_media_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

			File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

		endif;

        if ( Input::hasFile( 'file' ) ) :

            $file = Input::file( 'file' );
            $file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

            $current = ( int ) App::get_term_meta( $id, 'file', 0 );

            if ( $current > 0 ) :

                App::delete_term_meta( $id, 'file' );

                $attachment = new Attachment( $current );

                $attachment->delete();

            endif;

            App::update_term_meta( $id, 'file', App::upload_file_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

            File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

        endif;

		$taxonomy = App::get_taxonomy( 'resource' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?updated=true' );
	}

	public function add( $lang ) {
		$taxonomy = App::get_taxonomy( 'resource' );

		if ( Input::get( 'name_en' ) != null ) :

			$term_id = Resource::add_resource( [
				'name' => [
					'en' => Input::get( 'name_en' ),
					'fr' => Input::get( 'name_fr' )
				],
				'summary' => [
					'en' => Input::get( 'summary_en' ),
					'fr' => Input::get( 'summary_fr' )
				],
				'category' => Input::get( 'resource_category' ),
				'order' => Input::get( 'order' )
			] );

			if ( Input::hasFile( 'image' ) ) :

				$file = Input::file( 'image' );
				$file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

				App::update_term_meta( $term_id, 'screenshot', App::upload_media_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

				File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

			endif;

            if ( Input::hasFile( 'file' ) ) :

                $file = Input::file( 'file' );
                $file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

                App::update_term_meta( $term_id, 'file', App::upload_file_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

                File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

            endif;

			return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?added=true' );

		else :

            foreach( ResourceCategory::get_all_resource_categories() as $entry ) :

                $entry = ResourceCategory::get_resource_category( $entry );

                $resource_categories[ $entry->id ] = $entry->title->{ $lang };

            endforeach;

			return view( 'admin.resources.add', [
				'page_title' => Translation::get_translation( 'add-resource', $lang ),
                'resource_categories' => isset( $resource_categories ) ? $resource_categories : [],
				'active_menu' => $taxonomy->label->plural->{ $lang }
			] );

		endif;
	}

	public function delete( $lang, $id ) {
		$resource = new Resource( ( int ) $id );
		$resource->delete();

		$taxonomy = App::get_taxonomy( 'resource' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?deleted=true' );
	}

}