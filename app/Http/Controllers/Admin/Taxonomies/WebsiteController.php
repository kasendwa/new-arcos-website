<?php

namespace App\Http\Controllers\Admin\Taxonomies;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

use App\App;
use App\Entities\Translation;
use App\Entities\Website;
use App\Entities\Attachment;
use App\Http\Controllers\Admin\AdminController;

class WebsiteController extends AdminController {

	public function archive( $lang, $page = 1 ) {
		$taxonomy = App::get_taxonomy( 'website' );
		$per_page = 15;
		$page = ( int ) $page;

		foreach( Website::get_websites( $per_page, $page ) as $entry ) :

			$entry = Website::get_website( $entry );

			$websites[] = ( object ) [
				'id' => $entry->id,
				'screenshot' => $entry->screenshot,
				'name' => $entry->name->{ $lang },
				'summary' => $entry->summary,
				'url' => $entry->url,
				'order' => $entry->order
			];

		endforeach;

		return view( 'admin.websites.archive', [
			'page_title' => $taxonomy->label->plural->{ $lang },
			'pagination' => ( object ) [
				'base_link' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->archive ),
				'active' => $page,
				'total' => ceil( count( Website::get_all_websites() ) / $per_page )
			],
			'items' => isset( $websites ) ? $websites : [],
			'items_actions_base' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->base ),
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function edit( $lang, $id ) {
		$taxonomy = App::get_taxonomy( 'website' );

		$website = Website::get_website( ( int ) $id );

		return view( 'admin.websites.edit', [
			'page_title' => Translation::get_translation( 'edit-website', $lang ),
			'item_data' => $website,
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function update( $lang, $id ) {
		foreach( [ 'name', 'summary' ] as $element )

			foreach( App::get_languages() as $language )
				App::update_term_meta( $id, $element . '_' . $language->code, Input::get( $element . '_' . $language->code ) );

		App::update_term_meta( $id, 'order', Input::get( 'order' ) );
		App::update_term_meta( $id, 'url', Input::get( 'url' ) );

		if ( Input::hasFile( 'image' ) ) :

			$file = Input::file( 'image' );
			$file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

			$current = ( int ) App::get_term_meta( $id, 'image', 0 );

			if ( $current > 0 ) :

				App::delete_term_meta( $id, 'image' );

				$attachment = new Attachment( $current );

				$attachment->delete();

			endif;

			App::update_term_meta( $id, 'screenshot', App::upload_media_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

			File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

		endif;

		$taxonomy = App::get_taxonomy( 'website' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?updated=true' );
	}

	public function add( $lang ) {
		$taxonomy = App::get_taxonomy( 'website' );

		if ( Input::get( 'name_en' ) != null ) :

			$term_id = Website::add_website( [
				'name' => [
					'en' => Input::get( 'name_en' ),
					'fr' => Input::get( 'name_fr' )
				],
				'summary' => [
					'en' => Input::get( 'summary_en' ),
					'fr' => Input::get( 'summary_fr' )
				],
				'url' => Input::get( 'url' ),
				'order' => Input::get( 'order' )
			] );

			if ( Input::hasFile( 'image' ) ) :

				$file = Input::file( 'image' );
				$file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

				App::update_term_meta( $term_id, 'screenshot', App::upload_media_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

				File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

			endif;

			return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?added=true' );

		else :

			return view( 'admin.websites.add', [
				'page_title' => Translation::get_translation( 'add-website', $lang ),
				'active_menu' => $taxonomy->label->plural->{ $lang }
			] );

		endif;
	}

	public function delete( $lang, $id ) {
		$website = new Website( ( int ) $id );
		$website->delete();

		$taxonomy = App::get_taxonomy( 'website' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?deleted=true' );
	}

}