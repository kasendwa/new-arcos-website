<?php

namespace App\Http\Controllers\Admin\Taxonomies;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

use App\App;
use App\Entities\Translation;
use App\Entities\TeamCategory;
use App\Entities\Attachment;
use App\Entities\Slider;
use App\Http\Controllers\Admin\AdminController;

class SliderController extends AdminController {

	public function archive( $lang, $page = 1 ) {
		$taxonomy = App::get_taxonomy( 'slider' );
		$per_page = 15;
		$page = ( int ) $page;

		foreach( Slider::get_admin_sliders( $per_page, $page ) as $entry ) :

			$entry = Slider::get_slider( $entry );

			$sliders[] = ( object ) [
				'id' => $entry->id,
				'thumbnail' => $entry->thumbnail,
				'title' => $entry->content->title->{ $lang },
				'order' => $entry->order
			];

		endforeach;

		return view( 'admin.sliders.archive', [
			'page_title' => $taxonomy->label->plural->{ $lang },
			'pagination' => ( object ) [
				'base_link' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->archive ),
				'active' => $page,
				'total' => ceil( count( Slider::get_sliders() ) / $per_page )
			],
			'items' => isset( $sliders ) ? $sliders : [],
			'items_actions_base' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->base ),
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function edit( $lang, $id ) {
		$taxonomy = App::get_taxonomy( 'slider' );

		$slider = Slider::get_slider( ( int ) $id );

		$slider->entity = ( object ) [
			'type' => App::get_term_meta( $slider->id, 'entity_type' ),
			'name' => App::get_term_meta( $slider->id, 'entity_name' ),
			'id' => App::get_term_meta( $slider->id, 'entity_id' )
		];

		return view( 'admin.sliders.edit', [
			'page_title' => Translation::get_translation( 'edit-slider', $lang ),
			'item_data' => $slider,
			'entity_types' => ( object ) [
				'' => '',
				'post_type' => Translation::get_translation( 'post-type', App::get_current_language() ),
				'taxonomy' => Translation::get_translation( 'taxonomy', App::get_current_language() )
			],
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function update( $lang, $id ) {
		foreach( [ 'title', 'text', 'button_text' ] as $element )

			foreach( App::get_languages() as $language )
				App::update_term_meta( $id, $element . '_' . $language->code, Input::get( $element . '_' . $language->code ) );

		App::update_term_meta( $id, 'order', Input::get( 'order' ) );

		if ( empty( Input::get( 'entity_type' ) ) ) :

			App::update_term_meta( $id, 'custom_link', Input::get( 'custom_link' ) );

			App::update_term_meta( $id, 'entity_type', '' );
			App::update_term_meta( $id, 'entity_name', '' );
			App::update_term_meta( $id, 'entity_id', '' );

		else :

			App::update_term_meta( $id, 'entity_type', Input::get( 'entity_type' ) );
			App::update_term_meta( $id, 'entity_name', Input::get( 'entity_name' ) );
			App::update_term_meta( $id, 'entity_id', Input::get( 'entity_id' ) );

			App::update_term_meta( $id, 'custom_link', '' );

		endif;

		if ( Input::hasFile( 'image' ) ) :

			$file = Input::file( 'image' );
			$file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

			$current = ( int ) App::get_term_meta( $id, 'image', 0 );

			if ( $current > 0 ) :

				App::delete_term_meta( $id, 'image' );

				$attachment = new Attachment( $current );

				$attachment->delete();

			endif;

			App::update_term_meta( $id, 'image', App::upload_media_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

			File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

		endif;

		$taxonomy = App::get_taxonomy( 'slider' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?updated=true' );
	}

	public function delete( $lang, $id ) {
		$slider = new Slider( ( int ) $id );
		$slider->delete();

		$taxonomy = App::get_taxonomy( 'slider' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?deleted=true' );
	}

	public function add( $lang ) {
		$taxonomy = App::get_taxonomy( 'slider' );

		if ( Input::get( 'title_en' ) != null ) :

			$data = [];

			$data[ 'order' ] = Input::get( 'order', 0 );
			$data[ 'custom_link' ] = Input::get( 'custom_link', '' );

			if ( Input::hasFile( 'image' ) ) :

				$file = Input::file( 'image' );
				$file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

				$data[ 'image' ] = App::upload_media_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

				File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

			endif;

			$data[ 'content' ] = [
				'title' => [
					'en' => Input::get( 'title_en' ),
					'fr' => Input::get( 'title_fr' )
				],
				'text' => [
					'en' => Input::get( 'text_en' ),
					'fr' => Input::get( 'text_fr' )
				]
			];

			$data[ 'anchor' ] = [
				'entity' => [
					'type' => Input::get( 'entity_type', '' ),
					'name' => Input::get( 'entity_name','' ),
					'id' => Input::get( 'entity_id', '' )
				],
				'text' => [
					'en' => Input::get( 'button_text_en' ),
					'fr' => Input::get( 'button_text_fr' )
				]
			];

			Slider::add_slider( $data );

			return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?added=true' );

		else :

			return view( 'admin.sliders.add', [
				'page_title' => Translation::get_translation( 'add-slider', $lang ),
				'entity_types' => ( object ) [
					'' => '',
					'post_type' => Translation::get_translation( 'post-type', App::get_current_language() ),
					'taxonomy' => Translation::get_translation( 'taxonomy', App::get_current_language() )
				],
				'active_menu' => $taxonomy->label->plural->{ $lang }
			] );

		endif;
	}

}