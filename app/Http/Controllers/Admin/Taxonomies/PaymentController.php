<?php

namespace App\Http\Controllers\Admin\Taxonomies;

use App\App;
use App\Entities\Invoice;
use App\Entities\Payment;
use App\Http\Controllers\Admin\AdminController;

class PaymentController extends AdminController {

	public function archive( $lang, $page = 1 ) {
		$taxonomy = App::get_taxonomy( 'payment' );
		$per_page = 15;
		$page = ( int ) $page;

		foreach( Payment::get_payments( $per_page, $page ) as $entry ) :

			$entry = ( object ) Payment::get_payment( $entry );

			$invoice = Invoice::get_invoice( $entry->invoice );

			$payments[] = ( object ) [
				'id' => $entry->id,
				'token' => $entry->token,
				'first_name' => $entry->first_name,
				'last_name' => $entry->last_name,
				'email' => $entry->email,
				'invoice' => $entry->invoice,
				'status' => $entry->status,
				'amount' => number_format( $invoice->amount ),
				'timestamp' => $entry->timestamp
			];

		endforeach;

		return view( 'admin.payments.archive', [
			'page_title' => $taxonomy->label->plural->{ $lang },
			'pagination' => ( object ) [
				'base_link' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->archive ),
				'active' => $page,
				'total' => ceil( count( Payment::get_all_payments() ) / $per_page )
			],
			'items' => isset( $payments ) ? $payments : [],
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

}