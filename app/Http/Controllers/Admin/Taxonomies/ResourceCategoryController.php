<?php

namespace App\Http\Controllers\Admin\Taxonomies;

use Illuminate\Support\Facades\Input;

use App\App;
use App\Entities\Translation;
use App\Entities\ResourceCategory;
use App\Http\Controllers\Admin\AdminController;

class ResourceCategoryController extends AdminController {

    public function archive( $lang, $page = 1 ) {
        $taxonomy = App::get_taxonomy( 'resource-category' );
        $per_page = 15;
        $page = ( int ) $page;

        foreach( ResourceCategory::get_resource_categories( $per_page, $page ) as $entry ) :

            $entry = ResourceCategory::get_resource_category( $entry );

            $resource_categories[] = ( object ) [
                'id' => $entry->id,
                'title' => $entry->title->{ $lang }
            ];

        endforeach;

        return view( 'admin.resource-categories.archive', [
            'page_title' => $taxonomy->label->plural->{ $lang },
            'pagination' => ( object ) [
                'base_link' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->archive ),
                'active' => $page,
                'total' => ceil( count( ResourceCategory::get_all_resource_categories() ) / $per_page )
            ],
            'items' => isset( $resource_categories ) ? $resource_categories : [],
            'items_actions_base' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->base ),
            'active_menu' => $taxonomy->label->plural->{ $lang }
        ] );
    }

    public function add( $lang ) {
        $taxonomy = App::get_taxonomy( 'resource-category' );

        if ( Input::get( 'title_en' ) != null ) :

            $term_id = ResourceCategory::add_resource_category( [
                'title' => [
                    'en' => Input::get( 'title_en' ),
                    'fr' => Input::get( 'title_fr' )
                ],
                'description' => [
                    'en' => Input::get( 'description_en' ),
                    'fr' => Input::get( 'description_fr' )
                ],
                'order' => Input::get( 'order' )
            ] );

            return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?added=true' );

        else :

            return view( 'admin.resource-categories.add', [
                'page_title' => Translation::get_translation( 'add-resource-category', $lang ),
                'active_menu' => $taxonomy->label->plural->{ $lang }
            ] );

        endif;
    }

    public function delete( $lang, $id ) {
        $resource_category = new ResourceCategory( ( int ) $id );
        $resource_category->delete();

        $taxonomy = App::get_taxonomy( 'resource-category' );

        return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?deleted=true' );
    }

    public function edit( $lang, $id ) {
        $taxonomy = App::get_taxonomy( 'resource-category' );

        $resource_category = ResourceCategory::get_resource_category( ( int ) $id );

        return view( 'admin.resource-categories.edit', [
            'page_title' => Translation::get_translation( 'edit-resource-category', $lang ),
            'item_data' => $resource_category,
            'active_menu' => $taxonomy->label->plural->{ $lang }
        ] );
    }

    public function update( $lang, $id ) {
        foreach( [ 'title', 'description' ] as $element )

            foreach( App::get_languages() as $language )
                App::update_term_meta( $id, $element . '_' . $language->code, Input::get( $element . '_' . $language->code ) );

        App::update_term_meta( $id, 'order', Input::get( 'order' ) );

        $taxonomy = App::get_taxonomy( 'resource-category' );

        return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?updated=true' );
    }

}