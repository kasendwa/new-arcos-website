<?php

namespace App\Http\Controllers\Admin\Taxonomies;

use Illuminate\Support\Facades\Input;

use App\App;
use App\Entities\Translation;
use App\Entities\Menu;
use App\Entities\MenuItem;
use App\Http\Controllers\Admin\AdminController;

class MenuItemController extends AdminController {

	public function archive( $lang, $page = 1 ) {
		$taxonomy = App::get_taxonomy( 'menu-item' );
		$per_page = 15;
		$page = ( int ) $page;

		foreach( MenuItem::get_menu_items( $per_page, $page ) as $entry ) :

			$entry = MenuItem::get_menu_item( $entry, $lang );

			$menu = Menu::get_menu( $entry->menu, $lang );

			$menu_items[] = ( object ) [
				'id' => $entry->id,
				'title' => $entry->title,
				'order' => $entry->order,
				'menu' => $menu->title
			];

		endforeach;

		return view( 'admin.menu-items.archive', [
			'page_title' => $taxonomy->label->plural->{ $lang },
			'pagination' => ( object ) [
				'base_link' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->archive ),
				'active' => $page,
				'total' => ceil( count( MenuItem::get_all_menu_items() ) / $per_page )
			],
			'items' => isset( $menu_items ) ? $menu_items : [],
			'items_actions_base' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->base ),
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function edit( $lang, $id ) {
		$taxonomy = App::get_taxonomy( 'menu-item' );

		$menu_item = MenuItem::get_menu_item( ( int ) $id );

		$menu_item->entity = ( object ) [
			'type' => App::get_term_meta( $menu_item->id, 'entity_type' ),
			'name' => App::get_term_meta( $menu_item->id, 'entity_name' ),
			'id' => App::get_term_meta( $menu_item->id, 'entity_id' )
		];

		foreach( Menu::get_all_menus() as $entry ) :

			$menu = Menu::get_menu( $entry, $lang );

			$menus[ $menu->id ] = $menu->title;

		endforeach;

		return view( 'admin.menu-items.edit', [
			'page_title' => Translation::get_translation( 'edit-slider', $lang ),
			'item_data' => $menu_item,
			'entity_types' => ( object ) [
				'' => '',
				'post_type' => Translation::get_translation( 'post-type', App::get_current_language() ),
				'taxonomy' => Translation::get_translation( 'taxonomy', App::get_current_language() )
			],
			'menus' => isset( $menus ) ? $menus : [],
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function update( $lang, $id ) {
		foreach( [ 'title' ] as $element )

			foreach( App::get_languages() as $language )
				App::update_term_meta( $id, $element . '_' . $language->code, Input::get( $element . '_' . $language->code ) );

		App::update_term_meta( $id, 'order', Input::get( 'order' ) );
		App::update_term_meta( $id, 'menu', Input::get( 'menu' ) );

		if ( empty( Input::get( 'entity_type' ) ) ) :

			App::update_term_meta( $id, 'custom_link', Input::get( 'custom_link' ) );

			App::update_term_meta( $id, 'entity_type', '' );
			App::update_term_meta( $id, 'entity_name', '' );
			App::update_term_meta( $id, 'entity_id', '' );

		else :

			App::update_term_meta( $id, 'entity_type', Input::get( 'entity_type' ) );
			App::update_term_meta( $id, 'entity_name', Input::get( 'entity_name' ) );
			App::update_term_meta( $id, 'entity_id', Input::get( 'entity_id' ) );

			App::update_term_meta( $id, 'custom_link', '' );

		endif;

		$taxonomy = App::get_taxonomy( 'menu-item' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?updated=true' );
	}

	public function delete( $lang, $id ) {
		$menu_item = new MenuItem( ( int ) $id );
		$menu_item->delete();

		$taxonomy = App::get_taxonomy( 'menu-item' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?deleted=true' );
	}

	public function add( $lang ) {
		$taxonomy = App::get_taxonomy( 'menu-item' );

		if ( Input::get( 'title_en' ) != null ) :

			$data = [];

			$data[ 'order' ] = Input::get( 'order', 0 );
			$data[ 'menu' ] = Input::get( 'menu', '' );
			$data[ 'custom_link' ] = Input::get( 'custom_link', '' );

			$data[ 'title' ] = [
				'en' => Input::get( 'title_en' ),
				'fr' => Input::get( 'title_fr' )
			];

			$data[ 'anchor' ] = [
				'entity' => [
					'type' => Input::get( 'entity_type', '' ),
					'name' => Input::get( 'entity_name','' ),
					'id' => Input::get( 'entity_id', '' )
				],
				'text' => [
					'en' => Input::get( 'button_text_en' ),
					'fr' => Input::get( 'button_text_fr' )
				]
			];

			MenuItem::add_menu_item( $data );

			return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?added=true' );

		else :
			
			foreach( Menu::get_all_menus() as $entry ) :
				
				$menu = Menu::get_menu( $entry, $lang );
			
				$menus[ $menu->id ] = $menu->title;
				
			endforeach;

			return view( 'admin.menu-items.add', [
				'page_title' => Translation::get_translation( 'add-menu-item', $lang ),
				'entity_types' => ( object ) [
					'' => '',
					'post_type' => Translation::get_translation( 'post-type', App::get_current_language() ),
					'taxonomy' => Translation::get_translation( 'taxonomy', App::get_current_language() )
				],
				'menus' => isset( $menus ) ? $menus : [],
				'active_menu' => $taxonomy->label->plural->{ $lang }
			] );

		endif;
	}

}