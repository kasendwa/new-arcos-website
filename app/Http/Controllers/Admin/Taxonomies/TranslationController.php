<?php

namespace App\Http\Controllers\Admin\Taxonomies;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;

use App\App;
use App\Entities\Translation;
use App\Http\Controllers\Admin\AdminController;

class TranslationController extends AdminController {

	public function archive( $lang, $page = 1 ) {
		$taxonomy = App::get_taxonomy( 'translation' );
		$per_page = 15;
		$page = ( int ) $page;

		foreach( Translation::get_translations( $per_page, $page ) as $entry ) :

			$entry = Translation::get_translation( $entry );

			$translations[] = ( object ) [
				'id' => $entry->id,
				'title' => $entry->title->{ $lang }
			];

		endforeach;

		return view( 'admin.translations.archive', [
			'page_title' => $taxonomy->label->plural->{ $lang },
			'pagination' => ( object ) [
				'base_link' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->archive ),
				'active' => $page,
				'total' => ceil( count( Translation::get_all_translations() ) / $per_page )
			],
			'items' => isset( $translations ) ? $translations : [],
			'items_actions_base' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->base ),
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function edit( $lang, $id ) {
		$taxonomy = App::get_taxonomy( 'translation' );

		$translation = Translation::get_translation( ( int ) $id );

		return view( 'admin.translations.edit', [
			'page_title' => Translation::get_translation( 'edit-translation', $lang ),
			'item_data' => $translation,
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function update( $lang, $id ) {
		foreach( App::get_languages() as $language )
			App::update_term_meta( ( int ) $id, 'title_' . $language->code, Input::get( 'title_' . $language->code ) );

		$taxonomy = App::get_taxonomy( 'translation' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?updated=true' );
	}

	public function delete( $lang, $id ) {
		$translation = new Translation( ( int ) $id );
		$translation->delete();

		$taxonomy = App::get_taxonomy( 'translation' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?deleted=true' );
	}

	public function add( $lang ) {
		$taxonomy = App::get_taxonomy( 'translation' );

		if ( Input::get( 'title_en' ) != null ) :

			Translation::add_translation( [
				'title' => [
					'en' => Input::get( 'title_en' ),
					'fr' => Input::get( 'title_fr' )
				]
			] );

			return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?added=true' );

		else :

			return view( 'admin.translations.add', [
				'page_title' => Translation::get_translation( 'add-translation', $lang ),
				'active_menu' => $taxonomy->label->plural->{ $lang }
			] );

		endif;
	}

}