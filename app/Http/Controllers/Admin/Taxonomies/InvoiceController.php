<?php

namespace App\Http\Controllers\Admin\Taxonomies;

use Illuminate\Support\Facades\Input;

use App\App;
use App\Entities\Translation;
use App\Entities\Invoice;
use App\Http\Controllers\Admin\AdminController;

class InvoiceController extends AdminController {

	public function archive( $lang, $page = 1 ) {
		$taxonomy = App::get_taxonomy( 'invoice' );
		$per_page = 15;
		$page = ( int ) $page;

		foreach( Invoice::get_invoices( $per_page, $page ) as $entry ) :

			$entry = Invoice::get_invoice( $entry, $lang );

			$invoices[] = ( object ) [
				'id' => $entry->id,
				'title' => $entry->title,
				'amount' => $entry->amount
			];

		endforeach;

		return view( 'admin.invoices.archive', [
			'page_title' => $taxonomy->label->plural->{ $lang },
			'pagination' => ( object ) [
				'base_link' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->archive ),
				'active' => $page,
				'total' => ceil( count( Invoice::get_all_invoices() ) / $per_page )
			],
			'items' => isset( $invoices ) ? $invoices : [],
			'items_actions_base' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->base ),
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function edit( $lang, $id ) {
		$taxonomy = App::get_taxonomy( 'invoice' );

		$invoice = Invoice::get_invoice( ( int ) $id );

		return view( 'admin.invoices.edit', [
			'page_title' => Translation::get_translation( 'edit-invoice', $lang ),
			'item_data' => $invoice,
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function update( $lang, $id ) {
		foreach( [ 'title' ] as $element )

			foreach( App::get_languages() as $language )
				App::update_term_meta( $id, $element . '_' . $language->code, Input::get( $element . '_' . $language->code ) );

		App::update_term_meta( $id, 'amount', Input::get( 'amount' ) );

		$taxonomy = App::get_taxonomy( 'invoice' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?updated=true' );
	}

	public function delete( $lang, $id ) {
		$invoice = new Invoice( ( int ) $id );
		$invoice->delete();

		$taxonomy = App::get_taxonomy( 'invoice' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?deleted=true' );
	}

	public function add( $lang ) {
		$taxonomy = App::get_taxonomy( 'invoice' );

		if ( Input::get( 'title_en' ) != null ) :

			$data = [];

			$data[ 'amount' ] = Input::get( 'amount', 0 );

			$data[ 'title' ] = [
				'en' => Input::get( 'title_en' ),
				'fr' => Input::get( 'title_fr' )
			];

			Invoice::add_invoice( $data );

			return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?added=true' );

		else :

			return view( 'admin.invoices.add', [
				'page_title' => Translation::get_translation( 'add-invoice', $lang ),
				'active_menu' => $taxonomy->label->plural->{ $lang }
			] );

		endif;
	}

}