<?php

namespace App\Http\Controllers\Admin\Taxonomies;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

use App\App;
use App\Entities\Translation;
use App\Entities\AboutSection;
use App\Entities\Attachment;
use App\Http\Controllers\Admin\AdminController;

class AboutSectionController extends AdminController {

	public function archive( $lang, $page = 1 ) {
		$taxonomy = App::get_taxonomy( 'about-section' );
		$per_page = 15;
		$page = ( int ) $page;

		foreach( AboutSection::get_about_sections( $per_page, $page ) as $entry ) :

			$entry = AboutSection::get_about_section( $entry );

			$about_sections[] = ( object ) [
				'id' => $entry->id,
				'thumbnail' => $entry->thumbnail,
				'title' => $entry->title->{ $lang },
				'content' => $entry->content,
				'order' => $entry->order
			];

		endforeach;

		return view( 'admin.about-sections.archive', [
			'page_title' => $taxonomy->label->plural->{ $lang },
			'pagination' => ( object ) [
				'base_link' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->archive ),
				'active' => $page,
				'total' => ceil( count( AboutSection::get_all_about_sections() ) / $per_page )
			],
			'items' => isset( $about_sections ) ? $about_sections : [],
			'items_actions_base' => url( $lang . '/admin/' . $taxonomy->rewrite->admin->base ),
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function edit( $lang, $id ) {
		$taxonomy = App::get_taxonomy( 'about-section' );

		$about_section = AboutSection::get_about_section( ( int ) $id );

		return view( 'admin.about-sections.edit', [
			'page_title' => Translation::get_translation( 'edit-about-section', $lang ),
			'item_data' => $about_section,
			'active_menu' => $taxonomy->label->plural->{ $lang }
		] );
	}

	public function update( $lang, $id ) {
		foreach( [ 'title', 'content' ] as $element )

			foreach( App::get_languages() as $language )
				App::update_term_meta( $id, $element . '_' . $language->code, Input::get( $element . '_' . $language->code ) );

		App::update_term_meta( $id, 'order', Input::get( 'order' ) );

		if ( Input::hasFile( 'image' ) ) :

			$file = Input::file( 'image' );
			$file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

			$current = ( int ) App::get_term_meta( $id, 'image', 0 );

			if ( $current > 0 ) :

				App::delete_term_meta( $id, 'image' );

				$attachment = new Attachment( $current );

				$attachment->delete();

			endif;

			App::update_term_meta( $id, 'image', App::upload_media_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

			File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

		endif;

		$taxonomy = App::get_taxonomy( 'about-section' );

		return redirect( $lang . '/admin/' . $taxonomy->rewrite->admin->archive . '?updated=true' );
	}

}