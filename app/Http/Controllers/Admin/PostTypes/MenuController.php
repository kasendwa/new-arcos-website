<?php
/**
 * Created by PhpStorm.
 * User: Kasendwa
 * Date: 1/18/2017
 * Time: 4:45 PM
 */

namespace App\Http\Controllers\Admin\PostTypes;

use App\Http\Controllers\Admin\AdminController;

class MenuController extends AdminController {

	public function archive() {
		return view( 'admin.menus.archive' );
	}

	public function single() {
		return view( 'admin.menus.single' );
	}

}