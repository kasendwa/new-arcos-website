<?php

namespace App\Http\Controllers\Admin\PostTypes;

use App\Entities\Attachment;
use App\Entities\Project;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

use App\App;
use App\Models\Post;
use App\Entities\Page;
use App\Entities\Article;
use App\Entities\Translation;

use App\Http\Controllers\Admin\AdminController;

class PageController extends AdminController {

	public function archive( $lang, $page = 1 ) {
		$post_type = App::get_post_type( 'page' );
		$per_page = 15;
		$page = ( int ) $page;

		foreach( Page::get_pages( $per_page, $page ) as $entry ) :

			$entry = Page::get_page( $entry, $lang );

			$pages[] = ( object ) [
				'id' => $entry->id,
				'title' => $entry->title,
				'name' => $entry->name,
				'show_in_menu' => $entry->show_in_menu,
				'menu_order' => $entry->menu_order,
				'permalink' => $entry->permalink,
				'thumbnail' => $entry->thumbnail
			];

		endforeach;

		return view( 'admin.pages.archive', [
			'page_title' => $post_type->label->plural->{ $lang },
			'pagination' => ( object ) [
				'base_link' => url( $lang . '/admin/' . $post_type->rewrite->admin->archive ),
				'active' => $page,
				'total' => ceil( count( Page::get_all_pages() ) / $per_page )
			],
			'items' => isset( $pages ) ? $pages : [],
			'items_actions_base' => url( $lang . '/admin/' . $post_type->rewrite->admin->base ),
			'active_menu' => $post_type->label->plural->{ $lang }
		] );
	}

	public function edit( $lang, $id ) {
		$page = Page::get_page( ( int ) $id );

		$parents = [ 0 => Translation::get_translation( 'none', $lang ) ];

		foreach( Page::get_all_pages() as $entry ) :

			$entry = Page::get_page( $entry );

			$parents[ $entry->id ] = $entry->title->{ $lang };

		endforeach;

		$templates = [];

		foreach( Page::get_templates() as $key => $entry ) :

			$templates[ $key ] = ( object ) [
				'name' => $entry->name->{ $lang },
				'has_related_articles' => $entry->properties->has_related_articles,
				'has_related_projects' => $entry->properties->has_related_projects,
			];

		endforeach;

		$articles = ( object ) [
			'selected' => $page->related_articles,
			'list' => []
		];

		foreach( Article::get_all_articles() as $entry ) :

			$entry = Article::get_article( $entry );

			$articles->list[ $entry->id ] = $entry->title->{ $lang };

		endforeach;

		$projects = ( object ) [
			'selected' => $page->related_projects,
			'list' => []
		];

		foreach( Project::get_all_projects() as $entry ) :

			$entry = Project::get_project( $entry );

			$projects->list[ $entry->id ] = $entry->title->{ $lang };

		endforeach;

		$post_type = App::get_post_type( 'page' );

		return view( 'admin.pages.edit', [
			'page_title' => Translation::get_translation( 'edit-page', $lang ),
			'item_data' => $page,
			'parents' => $parents,
			'templates' => $templates,
			'articles' => $articles,
			'projects' => $projects,
			'active_menu' => $post_type->label->plural->{ $lang }
		] );
	}

	public function update( $lang, $id ) {
		$post = Post::find( ( int ) $id );

		$name = Input::get( 'name' );

		if ( $post->name != $name ) :

			$counter = 1;

			while( App::get_post( $name ) != null ) :

				$counter++;

				$segments = explode( '-', $name );
				$suffix = array_pop( $segments );

				$name = $counter > 2 ? str_replace( '-' . $suffix, '', $name ) : $name;
				$name = $name . '-' . $counter;

			endwhile;

			$post->name = $name;

			$post->save();

		endif;

		if ( Input::hasFile( 'featured_image' ) ) :

			$file = Input::file( 'featured_image' );
			$file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

			$current = ( int ) App::get_post_meta( $id, 'thumbnail', 0 );

			if ( $current > 0 ) :

				App::delete_post_meta( $id, 'thumbnail' );

				$attachment = new Attachment( $current );

				$attachment->delete();

			endif;

			App::update_post_meta( $id, 'thumbnail', App::upload_media_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

			File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

		endif;

		$meta_data = [];

		foreach( [ 'title', 'content', 'menu_title' ] as $item ) :

            if ( $item == 'content' ) :

                $meta_data[ $item . '_en' ] = App::process_content_for_saving( Input::get( $item . '_en' ) );
                $meta_data[ $item . '_fr' ] = App::process_content_for_saving( Input::get( $item . '_fr' ) );

            else :

                $meta_data[ $item . '_en' ] = Input::get( $item . '_en' );
                $meta_data[ $item . '_fr' ] = Input::get( $item . '_fr' );

			endif;

		endforeach;

		foreach( $meta_data as $key => $value )
			App::update_post_meta( $id, $key, $value );

		App::update_post_meta( $id, 'summary_en', trim( strip_tags( Input::get( 'summary_en' ) ) ) );
		App::update_post_meta( $id, 'summary_fr', trim( strip_tags( Input::get( 'summary_fr' ) ) ) );

		App::update_post_meta( $id, 'parent_id', ( int ) Input::get( 'parent' ) );
		App::update_post_meta( $id, 'template', Input::get( 'template' ) );
		App::update_post_meta( $id, 'menu_order', ( int ) Input::get( 'menu_order' ) );
		App::update_post_meta( $id, 'show_in_menu', ( boolean ) Input::get( 'show_in_menu' ) );

		foreach( ( array ) Input::get( 'related_articles' ) as $entry )
			$related_articles[] = ( int ) $entry;

		App::update_post_meta( $id, 'related_articles', json_encode( isset( $related_articles ) ? $related_articles : [] ) );

		foreach( ( array ) Input::get( 'related_projects' ) as $entry )
			$related_projects[] = ( int ) $entry;

		App::update_post_meta( $id, 'related_projects', json_encode( isset( $related_projects ) ? $related_projects : [] ) );

		$post_type = App::get_post_type( 'page' );

		return redirect( $lang . '/admin/' . $post_type->rewrite->admin->archive . '?updated=true' );
	}

	public function delete( $lang, $id ) {
		$page = new Page( ( int ) $id );
		$page->delete();

		$post_type = App::get_post_type( 'page' );

		return redirect( $lang . '/admin/' . $post_type->rewrite->admin->archive . '?deleted=true' );
	}

	public function add( $lang ) {
		if ( Input::get( 'title_en' ) != null ) :

			$params = [
				'title' => [
					'en' => Input::get( 'title_en' ),
					'fr' => Input::get( 'title_fr' )
				],
				'menu_title' => [
					'en' => !empty( Input::get( 'menu_title_en' ) ) ? Input::get( 'menu_title_en' ) : Input::get( 'title_en' ),
					'fr' => !empty( Input::get( 'menu_title_fr' ) ) ? Input::get( 'menu_title_fr' ) : Input::get( 'title_fr' )
				],
				'author' => App::get_user( 'admin' )->id,
				'status' => 'publish',
				'show_in_menu' => ( boolean ) Input::get( 'show_in_menu' ),
				'menu_order' => ( int ) Input::get( 'menu_order' ),
				'content' => [
					'en' => App::process_content_for_saving( Input::get( 'content_en' ) ),
					'fr' => App::process_content_for_saving( Input::get( 'content_fr' ) )
				]
			];

			$post_id = Page::add_page( $params );

			if ( Input::hasFile( 'featured_image' ) ) :

				$file = Input::file( 'featured_image' );
				$file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

				App::update_post_meta( $post_id, 'thumbnail', App::upload_media_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

				File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

			endif;

			App::add_post_meta( $post_id, 'parent', Input::get( 'parent' ) );
			App::add_post_meta( $post_id, 'template', Input::get( 'template' ) );

			foreach( ( array ) Input::get( 'related_articles' ) as $entry )
				$related_articles[] = ( int ) $entry;

			App::add_post_meta( $post_id, 'related_articles', json_encode( isset( $related_articles ) ? $related_articles : [] ) );

			foreach( ( array ) Input::get( 'related_projects' ) as $entry )
				$related_projects[] = ( int ) $entry;

			App::add_post_meta( $post_id, 'related_projects', json_encode( isset( $related_projects ) ? $related_projects : [] ) );

			!empty( Input::get( 'summary_en' ) ) ? App::update_post_meta( $post_id, 'summary_en', trim( strip_tags( Input::get( 'summary_en' ) ) ) ) : null;
			!empty( Input::get( 'summary_fr' ) ) ? App::update_post_meta( $post_id, 'summary_fr', trim( strip_tags( Input::get( 'summary_fr' ) ) ) ) : null;

			$post_type = App::get_post_type( 'page' );

			return redirect( $lang . '/admin/' . $post_type->rewrite->admin->archive . '?added=true' );

		else :

			$parents = [ 0 => Translation::get_translation( 'none', $lang ) ];

			foreach( Page::get_all_pages() as $entry ) :

				$entry = Page::get_page( $entry );

				$parents[ $entry->id ] = $entry->title->{ $lang };

			endforeach;

			$templates = [];

			foreach( Page::get_templates() as $key => $entry ) :

				$templates[ $key ] = ( object ) [
					'name' => $entry->name->{ $lang },
					'has_related_articles' => $entry->properties->has_related_articles,
					'has_related_projects' => $entry->properties->has_related_projects,
				];

			endforeach;

			$articles = ( object ) [
				'selected' => [],
				'list' => []
			];

			foreach( Article::get_all_articles() as $entry ) :

				$entry = Article::get_article( $entry );

				$articles->list[ $entry->id ] = $entry->title->{ $lang };

			endforeach;

			$projects = ( object ) [
				'selected' => [],
				'list' => []
			];

			foreach( Project::get_all_projects() as $entry ) :

				$entry = Project::get_project( $entry );

				$projects->list[ $entry->id ] = $entry->title->{ $lang };

			endforeach;

			$post_type = App::get_post_type( 'page' );

			return view( 'admin.pages.add', [
				'page_title' => Translation::get_translation( 'add-page', $lang ),
				'parents' => $parents,
				'templates' => $templates,
				'articles' => $articles,
				'projects' => $projects,
				'active_menu' => $post_type->label->plural->{ $lang }
			] );

		endif;
	}

}