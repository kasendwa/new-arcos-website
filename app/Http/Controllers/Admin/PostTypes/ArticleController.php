<?php

namespace App\Http\Controllers\Admin\PostTypes;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

use App\App;
use App\Models\Post;
use App\Entities\Article;
use App\Entities\Attachment;
use App\Entities\Category;
use App\Entities\Translation;

use App\Http\Controllers\Admin\AdminController;

class ArticleController extends AdminController {

	public function archive( $lang, $page = 1 ) {
		$post_type = App::get_post_type( 'article' );
		$per_page = 15;
		$page = ( int ) $page;

		foreach( Article::get_articles( $per_page, $page ) as $entry ) :

			$entry = Article::get_article( $entry, $lang );

			$articles[] = ( object ) [
				'id' => $entry->id,
				'title' => $entry->title,
				'name' => $entry->name,
				'permalink' => $entry->permalink,
				'thumbnail' => $entry->thumbnail
			];

		endforeach;

		return view( 'admin.articles.archive', [
			'page_title' => $post_type->label->plural->{ $lang },
			'pagination' => ( object ) [
				'base_link' => url( $lang . '/admin/' . $post_type->rewrite->admin->archive ),
				'active' => $page,
				'total' => ceil( count( Article::get_all_articles() ) / $per_page )
			],
			'items' => isset( $articles ) ? $articles : [],
			'items_actions_base' => url( $lang . '/admin/' . $post_type->rewrite->admin->base ),
			'active_menu' => $post_type->label->plural->{ $lang }
		] );
	}

	public function edit( $lang, $id ) {
		$article = Article::get_article( ( int ) $id );

		$categories = ( object ) [
			'selected' => $article->categories,
			'list' => []
		];

		foreach( Category::get_all_categories() as $entry ) :

			$entry = Category::get_category( $entry );

			$categories->list[ $entry->id ] = $entry->title->{ $lang };

		endforeach;

		$post_type = App::get_post_type( 'article' );

		return view( 'admin.articles.edit', [
			'page_title' => Translation::get_translation( 'edit-article', $lang ),
			'item_data' => $article,
			'categories' => $categories,
			'active_menu' => $post_type->label->plural->{ $lang }
		] );
	}

	public function update( $lang, $id ) {
		$post = Post::find( ( int ) $id );

		$name = Input::get( 'name' );

		if ( $post->name != $name ) :

			$counter = 1;

			while( App::get_post( $name ) != null ) :

				$counter++;

				$segments = explode( '-', $name );
				$suffix = array_pop( $segments );

				$name = $counter > 2 ? str_replace( '-' . $suffix, '', $name ) : $name;
				$name = $name . '-' . $counter;

			endwhile;

			$post->name = $name;

			$post->save();

		endif;

		if ( Input::hasFile( 'featured_image' ) ) :

			$file = Input::file( 'featured_image' );
			$file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

			$current = ( int ) App::get_post_meta( $id, 'thumbnail', 0 );

			if ( $current > 0 ) :

				App::delete_post_meta( $id, 'thumbnail' );

				$attachment = new Attachment( $current );

				$attachment->delete();

			endif;

			App::update_post_meta( $id, 'thumbnail', App::upload_media_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

			File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

		endif;

		$meta_data = [];

		foreach( [ 'title', 'content' ] as $item ) :

			if ( $item == 'content' ) :

                $meta_data[ $item . '_en' ] = App::process_content_for_saving( Input::get( $item . '_en' ) );
                $meta_data[ $item . '_fr' ] = App::process_content_for_saving( Input::get( $item . '_fr' ) );

            else :

                $meta_data[ $item . '_en' ] = Input::get( $item . '_en' );
                $meta_data[ $item . '_fr' ] = Input::get( $item . '_fr' );

            endif;

		endforeach;

		foreach( $meta_data as $key => $value )
			App::update_post_meta( $id, $key, $value );

		App::update_post_meta( $id, 'summary_en', trim( strip_tags( Input::get( 'summary_en' ) ) ) );
		App::update_post_meta( $id, 'summary_fr', trim( strip_tags( Input::get( 'summary_fr' ) ) ) );

		foreach( ( array ) Input::get( 'categories' ) as $entry )
			$categories[] = ( int ) $entry;

		App::update_post_meta( $id, 'categories', json_encode( $categories ) );

		$post_type = App::get_post_type( 'article' );

		return redirect( $lang . '/admin/' . $post_type->rewrite->admin->archive . '?updated=true' );
	}

	public function delete( $lang, $id ) {
		$article = new Article( ( int ) $id );
		$article->delete();

		$post_type = App::get_post_type( 'article' );

		return redirect( $lang . '/admin/' . $post_type->rewrite->admin->archive . '?deleted=true' );
	}

	public function add( $lang ) {
		if ( Input::get( 'title_en' ) != null ) :

			$params = [
				'title' => [
					'en' => Input::get( 'title_en' ),
					'fr' => Input::get( 'title_fr' )
				],
				'author' => App::get_user( 'admin' )->id,
				'status' => 'publish',
				'content' => [
                    'en' => App::process_content_for_saving( Input::get( 'content_en' ) ),
                    'fr' => App::process_content_for_saving( Input::get( 'content_fr' ) )
				]
			];

			$post_id = Article::add_article( $params );

			if ( Input::hasFile( 'featured_image' ) ) :

				$file = Input::file( 'featured_image' );
				$file->move( public_path( 'uploads/temp' ), $file->getClientOriginalName() );

				App::update_post_meta( $post_id, 'thumbnail', App::upload_media_from_path( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) ) );

				File::delete( public_path( 'uploads/temp/' . $file->getClientOriginalName() ) );

			endif;

			foreach( ( array ) Input::get( 'categories' ) as $entry )
				$categories[] = ( int ) $entry;

			App::add_post_meta( $post_id, 'categories', json_encode( isset( $categories ) ? $categories : [] ) );

			!empty( Input::get( 'summary_en' ) ) ? App::update_post_meta( $post_id, 'summary_en', trim( strip_tags( Input::get( 'summary_en' ) ) ) ) : null;
			!empty( Input::get( 'summary_fr' ) ) ? App::update_post_meta( $post_id, 'summary_fr', trim( strip_tags( Input::get( 'summary_fr' ) ) ) ) : null;

			$post_type = App::get_post_type( 'article' );

			return redirect( $lang . '/admin/' . $post_type->rewrite->admin->archive . '?added=true' );

		else :

			$post_type = App::get_post_type( 'article' );

			$categories = ( object ) [
				'selected' => [],
				'list' => []
			];

			foreach( Category::get_all_categories() as $entry ) :

				$entry = Category::get_category( $entry );

				$categories->list[ $entry->id ] = $entry->title->{ $lang };

			endforeach;

			return view( 'admin.articles.add', [
				'page_title' => Translation::get_translation( 'add-article', $lang ),
				'categories' => $categories,
				'active_menu' => $post_type->label->plural->{ $lang }
			] );

		endif;
	}

}