<?php

namespace App\Http\Controllers\Admin;

use App\App;

class DashboardController extends AdminController {

	public function index() {
		return redirect( App::get_current_language() . '/admin/pages' );
	}

}