<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Article;
use App\Entities\Category;
use App\Entities\Page;
use App\Entities\Project;
use App\Entities\TeamMember;
use Illuminate\Support\Facades\Input;

use App\App;

class APIController extends AdminController {

	public function index( $lang ) {
		$method = Input::get( 'method', null );

		if ( $method == 'get_post_types' ) :

			foreach( App::get_post_types() as $entry )
				$post_types[] = [
					'name' => $entry->name,
					'label' => $entry->label->singular->{ App::get_current_language() }
				];

			return response()->json( [
				'result' => 'successful',
				'entries' => isset( $post_types ) ? $post_types : []
			] );

		elseif ( $method == 'get_taxonomies' ) :

			foreach( App::get_taxonomies() as $entry )

				if ( !empty( $entry->rewrite->site->base ) )
					$taxonomies[] = [
						'name' => $entry->name,
						'label' => $entry->label->singular->{ App::get_current_language() }
					];

			return response()->json( [
				'result' => 'successful',
				'entries' => isset( $taxonomies ) ? $taxonomies : []
			] );

		elseif ( $method == 'get_entity_entries' ) :

			$type = Input::get( 'entity_type' );
			$name = Input::get( 'entity_name' );

			if ( $type == 'post_type' ) :

				if ( in_array( $name, [ 'page', 'article', 'project' ] ) ) :

					if ( $name == 'page' ) :

						foreach( Page::get_all_pages() as $entry ) :

							$page = Page::get_page( $entry, $lang );

							$entries[] = [
								'name' => $page->id,
								'label' => $page->title
							];

						endforeach;

					elseif ( $name == 'article' ) :

						foreach( Article::get_all_articles() as $entry ) :

							$article = Article::get_article( $entry, $lang );

							$entries[] = [
								'name' => $article->id,
								'label' => $article->title
							];

						endforeach;

					elseif ( $name == 'project' ) :

						foreach( Project::get_all_projects() as $entry ) :

							$project = Project::get_project( $entry, $lang );

							$entries[] = [
								'name' => $project->id,
								'label' => $project->title
							];

						endforeach;

					endif;

				endif;

			elseif ( $type == 'taxonomy' ) :

				if ( in_array( $name, [ 'category', 'team-member' ] ) ) :

					if ( $name == 'category' ) :

						foreach( Category::get_all_categories() as $entry ) :

							$category = Category::get_category( $entry, $lang );

							$entries[] = [
								'value' => $category->id,
								'label' => $category->title
							];

						endforeach;

					elseif ( $name == 'team-member' ) :

						foreach( TeamMember::get_all_team_members() as $entry ) :

							$team_member = TeamMember::get_team_member( $entry, $lang );

							$entries[] = [
								'value' => $team_member->id,
								'label' => $team_member->name
							];

						endforeach;

					endif;

				endif;

			endif;

			return response()->json( [
				'result' => 'successful',
				'entries' => isset( $entries ) ? $entries : []
			] );

		endif;
	}

}