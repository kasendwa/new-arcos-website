<?php

namespace App;

use App\Entities\Attachment;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

use JeroenG\Flickr\Api;
use JeroenG\Flickr\Flickr;
use Alaouy\Youtube\Facades\Youtube;
use Intervention\Image\Facades\Image;
use Mbarwick83\Shorty\Facades\Shorty;
use GuzzleHttp;

use App\Models\Language;
use App\Models\Option;

use App\Models\UserRole;
use App\Models\UserRoleMeta;
use App\Models\User;
use App\Models\UserMeta;

use App\Models\Post;
use App\Models\PostMeta;
use App\Entities\Page;

use App\Models\Term;
use App\Models\TermMeta;

class App {

	// Languages.

	public static function add_language( $args ) {
		return Language::create( $args );
	}

	public static function get_languages() {
		$records = Language::orderBy( 'name', 'asc' )->get();

		return !$records->isEmpty() ? $records : [];
	}

	public static function get_language( $code ) {
		return App::get_language_by( 'code', $code );
	}

	public static function get_language_by( $field, $value ) {
		$record = Language::where( $field, '=', $value )->get();

		return !$record->isEmpty() ? $record->first() : null;
	}

	// Options.

	public static function store_options( $options, $prefix = '' ) {
		foreach( $options as $key => $value ) :

			if ( is_array( $value ) ) :

				$option_name = !empty( $prefix ) ? $prefix . '_' . $key : $key;
				$option_value = json_encode( array_keys( $value ) );

				$record = App::get_option( $option_name, null );

				$record != null ? App::update_option( $option_name, $option_value ) : App::add_option( [
					'name' => $option_name,
					'value' => $option_value
				] );

				App::store_options( $value, ( !empty( $prefix ) ? $prefix . '_' : '' ) . $key );

			else :

				$option_name = !empty( $prefix ) ? $prefix . '_' . $key : $key;
				$option_value = $value;

				$record = App::get_option( $option_name, null );

				$record != null ? App::update_option( $option_name, $option_value ) : App::add_option( [
					'name' => $option_name,
					'value' => $option_value
				] );

			endif;

		endforeach;
	}

	public static function add_option( $args ) {
		return Option::create( $args );
	}

	public static function get_option( $name, $default = '' ) {
		return App::get_option_by( 'name', $name, $default );
	}

	public static function get_option_by( $field, $value, $default = '' ) {
		$record = Option::where( $field, '=', $value )->get();

		return !$record->isEmpty() ? $record->first()->value : $default;
	}

	public static function update_option( $name, $value ) {
		$record = Option::where( 'name', '=', $name )->get()->first();

		if ( !is_null( $record ) ) :

			$record->value = $value;

			$record->save();

		endif;
	}

	public static function delete_option( $name ) {
		$record = App::get_option( 'name', $name, null );

		!is_null( $record ) ? $record->delete() : null;
	}

	// User Roles

	public static function get_permissions_template() {
		return [
			'manage_options' => true,
			'post_types' => [
				'default' => [
					'crud' => [
						'create' => true,
						'read' => [
							'own' => true,
							'others' => true
						],
						'update' => [
							'own' => true,
							'others' => true,
						],
						'delete' => [
							'own' => true,
							'others' => false
						]
					],
					'status' => [
						'draft' => true,
						'publish' => true,
						'unpublish' => true
					]
				]
			],
			'taxonomies' => [
				'default' => [
					'crud' => [
						'create' => true,
						'read' => [
							'own' => true,
							'others' => true
						],
						'update' => [
							'own' => true,
							'others' => true,
						],
						'delete' => [
							'own' => true,
							'others' => false
						]
					]
				]
			]
		];
	}

	public static function add_user_role( $args ) {
		return UserRole::create( $args );
	}

	public static function get_user_roles() {
		$records = UserRole::orderBy( 'name', 'asc' )->get();

		return !$records->isEmpty() ? $records : [];
	}

	public static function get_user_role( $identifier ) {
		return is_numeric( $identifier ) ? App::get_user_role_by( 'id', $identifier ) : App::get_user_role_by( 'name', $identifier );
	}

	public static function get_user_role_by( $field, $value ) {
		$record = UserRole::where( $field, '=', $value )->get();

		return !$record->isEmpty() ? $record->first() : null;
	}

	public static function add_user_role_meta( $user_role_id, $key, $value = '' ) {
		return UserRoleMeta::create( [
			'user_role_id' => $user_role_id,
			'key' => $key,
			'value' => $value
		] );
	}

	public static function get_user_role_meta( $user_role_id, $key, $default = '' ) {
		$where = [
			[ 'user_role_id', '=', $user_role_id ],
			[ 'key', '=', $key ]
		];

		$record = UserRoleMeta::where( $where )->get();

		return !$record->isEmpty() ? $record->first() : $default;
	}

	public static function update_user_role_meta( $user_role_id, $key, $value ) {
		$record = App::get_user_role_meta( $user_role_id, $key, null );

		if ( is_null( $record ) )
			$record = App::add_user_role_meta( $user_role_id, $key, $value );

		$record->value = $value;

		$record->save();
	}

	public static function delete_user_role_meta( $user_role_id, $key ) {
		$record = App::get_user_role_meta( $user_role_id, $key, null );

		!is_null( $record ) ? $record->delete() : null;
	}

	// Users

	public static function add_user( $args ) {
		return User::create( $args );
	}

	public static function get_users() {
		$records = User::orderBy( 'username', 'asc' )->get();

		return !$records->isEmpty() ? $records : [];
	}

	public static function get_user( $identifier ) {
		return is_numeric( $identifier ) ? App::get_user_by( 'id', $identifier ) : App::get_user_by( 'username', $identifier );
	}

	public static function get_user_by( $field, $value ) {
		$record = User::where( $field, '=', $value )->get();

		return !$record->isEmpty() ? $record->first() : null;
	}

	public static function add_user_meta( $user_id, $key, $value = '' ) {
		return UserMeta::create( [
			'user_id' => $user_id,
			'key' => $key,
			'value' => $value
		] );
	}

	public static function get_user_meta( $user_id, $key, $default = '' ) {
		$record = UserMeta::where( 'user_id', '=', $user_id )->where( 'key', '=', $key )->get();

		return !$record->isEmpty() ? $record->first()->value : $default;
	}

	public static function update_user_meta( $user_id, $key, $value ) {
		$record = App::get_user_meta( $user_id, $key, null );

		if ( is_null( $record ) )
			$record = App::add_user_meta( $user_id, $key, $value );

		$record->value = $value;

		$record->save();
	}

	public static function delete_user_meta( $user_id, $key ) {
		$record = App::get_user_meta( $user_id, $key, null );

		!is_null( $record ) ? $record->delete() : null;
	}

	// Post types

	public static function get_default_post_type_controller() {
		return 'Site\PostTypes\PostController';
	}

	public static function get_default_post_type_controller_archive_method() {
		return 'archive';
	}

	public static function get_default_post_type_controller_single_method() {
		return 'single';
	}

	public static function get_post_types() {
		$option = App::get_option( 'post_types' );

		foreach( is_array( json_decode( $option ) ) ? json_decode( $option ) : [] as $entry ) :

			$post_types[] = ( object ) [
				'name' => $entry,
				'label' => ( object ) [
					'singular' => ( object ) [
						'en' => App::get_option( 'post_types_' . $entry . '_label_singular_en' ),
						'fr' => App::get_option( 'post_types_' . $entry . '_label_singular_fr' )
					],
					'plural' => ( object ) [
						'en' => App::get_option( 'post_types_' . $entry . '_label_plural_en' ),
						'fr' => App::get_option( 'post_types_' . $entry . '_label_plural_fr' )
					]
				],
				'has_archive' => App::get_option( 'post_types_' . $entry . '_has_archive' ) == 1 ? true : false,
				'rewrite' => ( object ) [
					'site' => ( object ) [
						'base' => App::get_option( 'post_types_' . $entry . '_rewrite_site_base' ),
						'archive' => App::get_option( 'post_types_' . $entry . '_rewrite_site_archive' )
					],
					'admin' => ( object ) [
						'base' => App::get_option( 'post_types_' . $entry . '_rewrite_admin_base' ),
						'archive' => App::get_option( 'post_types_' . $entry . '_rewrite_admin_archive' )
					]
				],
				'controller' => ( object ) [
					'site' => ( object ) [
						'name' => App::get_option( 'post_types_' . $entry . '_controller_site', App::get_default_post_type_controller() ),
						'archive' => App::get_option( 'post_types_' . $entry . '_controller_site_archive', App::get_default_post_type_controller_archive_method() ),
						'single' => App::get_option( 'post_types_' . $entry . '_controller_site_single', App::get_default_post_type_controller_single_method() )
					],
					'admin' => ( object ) [
						'name' => App::get_option( 'post_types_' . $entry . '_controller_admin', App::get_default_post_type_controller() ),
						'archive' => App::get_option( 'post_types_' . $entry . '_controller_admin_archive', App::get_default_post_type_controller_archive_method() ),
						'single' => App::get_option( 'post_types_' . $entry . '_controller_admin_single', App::get_default_post_type_controller_single_method() )
					]
				]
			];

		endforeach;

		return isset( $post_types ) ? $post_types : [];
	}

	public static function get_post_type( $name ) {
		foreach( App::get_post_types() as $entry ) :

			if ( $entry->name == $name ) :

				$post_type = $entry;

				break;

			endif;

		endforeach;

		return isset( $post_type ) ? $post_type : null;
	}

	public static function add_post( $args ) {
		!isset( $args[ 'user_id' ] ) ? $args[ 'user_id' ] = App::get_user( 'admin' )->id : null;

		return Post::create( $args );
	}

	public static function get_posts() {
		$records = User::orderBy( 'created_at', 'asc' )->get();

		return !$records->isEmpty() ? $records : [];
	}

	public static function get_post( $identifier ) {
		return is_numeric( $identifier ) ? App::get_post_by( 'id', $identifier ) : App::get_post_by( 'name', $identifier );
	}

	public static function get_post_by( $field, $value ) {
		$record = Post::where( $field, '=', $value )->get();

		return !$record->isEmpty() ? $record->first() : null;
	}

	public static function add_post_meta( $post_id, $key, $value = '' ) {
		return PostMeta::create( [
			'post_id' => $post_id,
			'key' => $key,
			'value' => $value
		] );
	}

	public static function get_post_meta( $post_id, $key, $default = null ) {
		$record = PostMeta::where( 'post_id', '=', $post_id )->where( 'key', '=', $key )->get();

		return !$record->isEmpty() ? $record->first()->value : $default;
	}

	public static function update_post_meta( $post_id, $key, $value ) {
		$record = PostMeta::where( 'post_id', '=', $post_id )->where( 'key', '=', $key )->get()->first();

		if ( is_null( $record ) )
			$record = App::add_post_meta( $post_id, $key, $value );

		$record->value = $value;

		$record->save();
	}

	public static function delete_post_meta( $post_id, $key ) {
		$record = PostMeta::where( 'post_id', '=', $post_id )->where( 'key', '=', $key )->get()->first();

		!is_null( $record ) ? $record->forceDelete() : null;
	}

	// Taxonomies

	public static function get_default_taxonomy_controller() {
		return 'Site\Taxonomies\TaxonomyController';
	}

	public static function get_default_taxonomy_controller_archive_method() {
		return 'archive';
	}

	public static function get_default_taxonomy_controller_single_method() {
		return 'single';
	}

	public static function get_taxonomies() {
		$option = App::get_option( 'taxonomies' );

		foreach( is_array( json_decode( $option ) ) ? json_decode( $option ) : [] as $entry ) :

			$taxonomies[] = ( object ) [
				'name' => $entry,
				'label' => ( object ) [
					'singular' => ( object ) [
						'en' => App::get_option( 'taxonomies_' . $entry . '_label_singular_en' ),
						'fr' => App::get_option( 'taxonomies_' . $entry . '_label_singular_fr' )
					],
					'plural' => ( object ) [
						'en' => App::get_option( 'taxonomies_' . $entry . '_label_plural_en' ),
						'fr' => App::get_option( 'taxonomies_' . $entry . '_label_plural_fr' )
					]
				],
				'has_archive' => App::get_option( 'taxonomies_' . $entry . '_has_archive' ) == 1 ? true : false,
				'rewrite' => ( object ) [
					'site' => ( object ) [
						'base' => App::get_option( 'taxonomies_' . $entry . '_rewrite_site_base' ),
						'archive' => App::get_option( 'taxonomies_' . $entry . '_rewrite_site_archive' )
					],
					'admin' => ( object ) [
						'base' => App::get_option( 'taxonomies_' . $entry . '_rewrite_admin_base' ),
						'archive' => App::get_option( 'taxonomies_' . $entry . '_rewrite_admin_archive' )
					]
				],
				'controller' => ( object ) [
					'site' => ( object ) [
						'name' => App::get_option( 'taxonomies_' . $entry . '_controller_site', App::get_default_post_type_controller() ),
						'archive' => App::get_option( 'taxonomies_' . $entry . '_controller_site_archive', App::get_default_post_type_controller_archive_method() ),
						'single' => App::get_option( 'taxonomies_' . $entry . '_controller_site_single', App::get_default_post_type_controller_single_method() )
					],
					'admin' => ( object ) [
						'name' => App::get_option( 'taxonomies_' . $entry . '_controller_admin', App::get_default_post_type_controller() ),
						'archive' => App::get_option( 'taxonomies_' . $entry . '_controller_admin_archive', App::get_default_post_type_controller_archive_method() ),
						'single' => App::get_option( 'taxonomies_' . $entry . '_controller_admin_single', App::get_default_post_type_controller_single_method() )
					]
				]
			];

		endforeach;

		return isset( $taxonomies ) ? $taxonomies : [];
	}

	public static function get_taxonomy( $name ) {
		foreach( App::get_taxonomies() as $entry ) :

			if ( $entry->name == $name ) :

				$taxonomy = $entry;

				break;

			endif;

		endforeach;

		return isset( $taxonomy ) ? $taxonomy : null;
	}

	public static function add_term( $args ) {
		return Term::create( $args );
	}

	public static function get_terms() {
		$records = Term::orderBy( 'created_at', 'asc' )->get();

		return !$records->isEmpty() ? $records : [];
	}

	public static function get_term( $identifier ) {
		return is_numeric( $identifier ) ? App::get_term_by( 'id', $identifier ) : App::get_term_by( 'slug', $identifier );
	}

	public static function get_term_by( $field, $value ) {
		$record = Term::where( $field, '=', $value )->get();

		return !$record->isEmpty() ? $record->first() : null;
	}

	public static function add_term_meta( $term_id, $key, $value = '' ) {
		return TermMeta::create( [
			'term_id' => $term_id,
			'key' => $key,
			'value' => $value
		] );
	}

	public static function get_term_meta( $term_id, $key, $default = null ) {
		$record = TermMeta::where( 'term_id', '=', $term_id )
					->where( 'key', '=', $key )
					->get();

		return !$record->isEmpty() ? $record->first()->value : $default;
	}

	public static function update_term_meta( $term_id, $key, $value ) {
		$record = TermMeta::where( 'term_id', '=', $term_id )
					->where( 'key', '=', $key )
					->get();

		if ( $record->isEmpty() )
			$record = App::add_term_meta( $term_id, $key, $value );

		else
			$record = $record->first();

		$record->value = $value;

		$record->save();
	}

	public static function delete_term_meta( $term_id, $key ) {
		$record = TermMeta::where( 'term_id', '=', $term_id )->where( 'key', '=', $key )->get()->first();

		!is_null( $record ) ? $record->forceDelete() : null;
	}


	// Utils.

	public static function get_current_language() {
		return isset( Request::segments()[ 0 ] ) ? Request::segments()[ 0 ] : App::get_default_option( 'language' );
	}

	public static function get_default_option( $option ) {
		return App::get_option( 'defaults_' . $option, null );
	}

	public static function get_site_info() {
		$lang_code = App::get_current_language();

		$info[ 'lang' ] = $lang_code;
		$info[ 'name' ] = App::get_option( 'name_' . $lang_code );
		$info[ 'defaults' ] = ( object ) [
			'page_title' => App::get_default_option( 'page_title_' . $lang_code ),
			'meta' =>  ( object ) [
				'author' => App::get_default_option( 'meta_author' ),
				'keywords' => App::get_default_option( 'meta_keywords' ),
				'description' => App::get_default_option( 'meta_description_' . $lang_code )
			]
		];

		return ( object ) $info;
	}

	public static function get_top_menu_items( $parent_id = 0, $slug_prefix = '' ) {
		$posts = DB::table( 'posts' )
					->select( 'posts.id' )
					->leftJoin( 'post_meta', 'posts.id', '=', 'post_meta.post_id' )
					->where( 'posts.type', '=', 'page' )
					->where( 'post_meta.key', '=', 'parent_id' )
					->where( 'post_meta.value', '=', $parent_id )
					->groupBy( 'posts.id' )
					->get();

		foreach( $posts as $post ) :

			$page = Page::get_page( $post->id );

			if ( $page->show_in_menu ) :

				$pages[ $page->menu_order ] = ( object ) [
					'id' => $page->id,
					'name' => $page->name,
					'permalink' => $page->permalink,
					'title' => $page->title->{ App::get_current_language() },
					'thumbnail' => $page->thumbnail,
					'menu_title' => $page->menu_title->{ App::get_current_language() },
					'children' => App::get_top_menu_items( $post->id, $page->name )
				];

			endif;

		endforeach;

		return isset( $pages ) ? $pages : [];
	}

	public static function get_image_sizes() {
		$sizes = ( array ) json_decode( App::get_option( 'image_sizes', json_encode( [] ) ) );

		foreach( $sizes as $size ) :

			$image_size = [];

			foreach( ( array ) json_decode( App::get_option( 'image_sizes_' . $size, json_encode( [] ) ) ) as $key )
				$image_size[ $key ] = App::get_option( 'image_sizes_' . $size . '_' . $key, '' );

			$image_sizes[ $size ] = ( object ) $image_size;

		endforeach;

		return isset( $image_sizes ) ? $image_sizes : [];
	}

	public static function create_image_thumbnails( $image, $dir ) {
		foreach( App::get_image_sizes() as $size ) :

			$thumb = Image::make( $dir . '/' . $image->basename );

			$filename = str_replace( '.' . File::extension( $image->basename ), '', $image->basename ) . '-' . $size->width . 'x' . $size->height . '.' . File::extension( $image->basename );

			$thumb->fit( $size->width, $size->height, function( $constraint ) {
				global $size;

				isset( $size->crop ) && $size->crop == false ? $constraint->aspectRatio() : null;
				isset( $size->upsize ) && $size->upsize == false ? $constraint->upsize() : null;
			} )->save( $dir . '/' . $filename );

		endforeach;
	}

	public static function upload_media_from_path( $url = '' ) {
		$dir = public_path( 'uploads/' . date( 'Y' ) . '/' . date( 'm' ) );

		!File::exists( $dir ) ? File::makeDirectory( $dir, 0775, true ) : null;

		$filename = basename( $url );

		while( File::exists( $dir . '/' . $filename ) ) :

			$counter = isset( $counter ) ? $counter + 1 : 1;

			$filename = str_replace( '.' . File::extension( $url ), '', basename( $url ) ) . '-' . $counter . '.' . File::extension( $url );

		endwhile;

		$image = Image::make( $url );

		$image->save( $dir . '/' . $filename );

		App::create_image_thumbnails( $image, $dir );

		return Attachment::add_attachment([
			'title' => str_replace( '.' . File::extension( $url ), '', basename( $url ) ),
			'url' => url( 'uploads/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . $filename )
		]);
	}

    public static function upload_file_from_path( $url = '' ) {
        $dir = public_path( 'uploads/' . date( 'Y' ) . '/' . date( 'm' ) );

        !File::exists( $dir ) ? File::makeDirectory( $dir, 0775, true ) : null;

        $filename = basename( $url );

        while( File::exists( $dir . '/' . $filename ) ) :

            $counter = isset( $counter ) ? $counter + 1 : 1;

            $filename = str_replace( '.' . File::extension( $url ), '', basename( $url ) ) . '-' . $counter . '.' . File::extension( $url );

        endwhile;

        File::move( $url, $dir . '/' . $filename );

        return Attachment::add_attachment([
            'title' => str_replace( '.' . File::extension( $url ), '', basename( $url ) ),
            'url' => url( 'uploads/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . $filename )
        ] );
    }


	public static function get_flickr_galleries( $page = 1, $per_page= 500 ) {
		$flickr = new Flickr( new Api( env( 'FLICKR_KEY' ), 'php_serial' ) );

		$response = $flickr->listSets([
			'user_id' => env( 'FLICKR_USER_ID' ),
			'page' => $page,
			'per_page' => $per_page
		]);

		foreach( $response->photosets as $set )

			foreach( ( array ) $set as $info )

				if ( is_array( $info ) ) :

					$info = ( object ) $info;

					$cover = App::get_flickr_gallery_photos( $info->id, 1, 1 );

					$albums[] = ( object ) [
						'id' => $info->id,
						'title' => str_replace( '_', ' ', $info->title[ '_content' ] ),
						'description' => $info->description[ '_content' ],
						'photos' => $info->photos,
						'cover' => ( object ) $cover[ 0 ]->thumbnails,
						'permalink' => url( App::get_current_language() . '/flickr-gallery/' . $info->id )
					];

				endif;

		return isset( $albums ) ? $albums : [];
	}

	public static function get_flickr_gallery_info( $gallery ) {
		$flickr = new Flickr( new Api( env( 'FLICKR_KEY' ), 'php_serial' ) );

		$response = $flickr->request( 'flickr.photosets.getInfo', [ 'photoset_id' => $gallery, 'user_id' => env( 'FLICKR_USER_ID' ) ] );

		$set = ( object ) $response->photoset;

		$info = ( object ) [
			'id' => $set->id,
			'title' => str_replace( '_', ' ', $set->title[ '_content' ] ),
			'description' => App::clickable_links( App::nl2p( $set->description[ '_content' ] ) ),
			'photos' => ( int ) $set->photos,
			'permalink' => url( App::get_current_language() . '/flickr-gallery/' . $set->id )
		];

		return $info;
	}

	public static function get_flickr_gallery_photos( $album_id, $page = 1, $per_page = 20 ) {
		$flickr = new Flickr( new Api( env( 'FLICKR_KEY' ), 'php_serial' ) );

		$response = $flickr->photosForSet( $album_id, env( 'FLICKR_USER_ID' ), [ 'page' => $page, 'per_page' => $per_page ] );

		foreach( $response->photoset as $set )

			if ( is_array( $set ) ) :

				foreach( ( array ) $set as $info ) :

					$info = ( object ) $info;

					$photos[] = App::get_flickr_gallery_photo( $info->id );

				endforeach;

			endif;

		return isset( $photos ) ? $photos : [];
	}

	public static function get_flickr_gallery_photo( $photo_id ) {
		$flickr = new Flickr( new Api( env( 'FLICKR_KEY' ), 'php_serial' ) );

		$response = $flickr->photoInfo( $photo_id );

		$info = ( object ) [
			'title' => $response->photo[ 'title' ][ '_content' ],
			'description' => $response->photo[ 'description' ][ '_content' ],
			'url' => $response->photo[ 'urls' ][ 'url' ][ 0 ][ '_content' ],
			'thumbnails' => []
		];

		$response = $flickr->request( 'flickr.photos.getSizes', [ 'photo_id' => $photo_id ] );

		foreach( $response->sizes[ 'size' ] as $entry ) :

			$entry[ 'label' ] == 'Large Square' ? $info->thumbnails[ 'thumbnail' ] = $entry[ 'source' ] : null;
			$entry[ 'label' ] == 'Medium' ? $info->thumbnails[ 'medium' ] = $entry[ 'source' ] : null;
			$entry[ 'label' ] == 'Medium 640' ? $info->thumbnails[ 'medium' ] = $entry[ 'source' ] : null;
			$entry[ 'label' ] == 'Large' ? $info->thumbnails[ 'large' ] = $entry[ 'source' ] : null;

		endforeach;

		return isset( $info ) ? $info : [];
	}


	public static function get_youtube_playlists() {
		$response = Youtube::getPlaylistsByChannelId( env( 'YOUTUBE_CHANNEL_ID' ) );

		foreach( $response[ 'results' ] as $item ) :

			$thumbnails = [];

			foreach( $item->snippet->thumbnails as $key => $entry )

				if ( in_array( $key, [ 'medium', 'high', 'standard' ] ) )
					$thumbnails[ $key ] = $entry->url;

			$playlists[] = ( object ) [
				'id' => $item->id,
				'title' => $item->snippet->title,
				'description' => $item->snippet->description,
				'cover' => ( object ) $thumbnails,
				'permalink' => url( App::get_current_language() . '/youtube-playlist/' . $item->id ),
				'count' => count( App::get_youtube_playlist_videos( $item->id ) )
			];

		endforeach;

		return isset( $playlists ) ? $playlists : [];
	}

	public static function get_youtube_playlist_info( $playlist ) {
		$response = Youtube::getPlaylistById( $playlist );

		$thumbnails = [];

		foreach( $response->snippet->thumbnails as $key => $entry )

			if ( in_array( $key, [ 'medium', 'high', 'standard' ] ) )
				$thumbnails[ $key ] = $entry->url;

		$info = ( object ) [
			'id' => $response->id,
			'title' => $response->snippet->title,
			'description' => $response->snippet->description,
			'cover' => ( object ) $thumbnails,
			'permalink' => url( App::get_current_language() . '/youtube-playlist/' . $response->id )
		];

		return $info;
	}

	public static function get_youtube_playlist_videos( $playlist ) {
		$response = Youtube::getPlaylistItemsByPlaylistId( $playlist );

		foreach( $response[ 'results' ] as $item ) :

			$thumbnails = [];

			foreach( $item->snippet->thumbnails as $key => $entry )

				if ( in_array( $key, [ 'medium', 'high', 'standard' ] ) )
					$thumbnails[ $key ] = $entry->url;

			$videos[] = ( object ) [
				'id' => $item->id,
				'title' => $item->snippet->title,
				'description' => $item->snippet->description,
				'thumbnails' => ( object ) $thumbnails,
				'permalink' => url( App::get_current_language() . '/youtube-video/' . $item->snippet->resourceId->videoId )
			];

		endforeach;

		return isset( $videos ) ? $videos : [];
	}

	public static function get_youtube_video( $video ) {
		$video = Youtube::getVideoInfo( $video );

		$info = ( object ) [
			'id' => $video->id,
			'title' => $video->snippet->title,
			'description' => App::clickable_links( App::nl2p( $video->snippet->description ) ),
			'iframe' => $video->player->embedHtml
		];

		return isset( $info ) ? $info : null;
	}


	public static function nl2p( $string ) {
	    $text = '';

	    foreach( explode( "\n", $string ) as $line )

	        if ( trim( $line ) )
	            $text .= '<p>' . $line . '</p>';

		return $text;
	}

	public static function clickable_links( $text ) {
		return preg_replace( '!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1">$1</a>', $text );
	}

	public static function shorten_url( $url ) {
		return $url; //Shorty::shorten( $url );
	}

	public static function search_results( $search_string ) {
		$posts = DB::table( 'posts' )
			->select( DB::raw( 'posts.id, COUNT( posts.id ) AS occurences' ) )
			->leftJoin( 'post_meta', 'posts.id', '=', 'post_meta.post_id' )
			->where( 'posts.status', '=', 'publish' )
			->where( function( $query ) use ( $search_string ) {
				foreach ( explode( ' ', $search_string ) as $term )
					$query->orWhere( function ( $query ) use ( $term ) {
						$query->whereIn( 'post_meta.key', [ 'title_en', 'title_fr', 'summary_en', 'summary_fr', 'content_en', 'content_fr' ] )
							->where( 'post_meta.value', 'LIKE', '%' . $term . '%' );
					});
			})
			->groupBy( 'posts.id' )
			->orderBy( 'occurences', 'DESC' )
			->get();

		foreach( $posts as $post )
			$results[] = $post->id;

		return isset( $results ) ? $results : [];
	}


	public static function get_mailman_url() {
		return 'http://lists.arcosnetwork.org/cgi-bin/mailman';
	}

	public static function fetch_mailman_lists() {
		$client = new GuzzleHttp\Client();

		$response = $client->request( 'GET', App::get_mailman_url() . '/admin' );
		$response = $response->getBody()->getContents();

		$start = strpos( $response, '>', strpos( $response, '<table' ) );
		$end = strpos( $response, '</table>' );

		$table_rows = substr( $response, $start, ( $end - $start ) );

		foreach( explode( '</tr>', $table_rows ) as $row_index => $row ) :

			if ( $row_index >= 4 ) :

				$cell_details = explode( '</td>', $row );

				if ( isset( $cell_details[ 1 ] ) ) :

					$description = trim( strip_tags( $cell_details[ 1 ] ) );

					if ( $description != '[no description available]' ) :

						$titles = explode( '/', $description );

						$entries[] = [
							'name' => trim( strip_tags( $cell_details[ 0 ] ) ),
							'title' => [
								'en' => isset( $titles[ 0 ] ) ? trim( $titles[ 0 ] ) : '',
								'fr' => isset( $titles[ 1 ] ) ? trim( $titles[ 1 ] ) : ''
							]
						];

					endif;

				endif;

			endif;

		endforeach;

		return isset( $entries ) ? $entries : [];
	}

	public static function process_content_for_saving( $content = '' ) {
	    if ( !empty( $content ) ) :

            $dom = new \DomDocument();

            $internalErrors = libxml_use_internal_errors( true );

            $dom->loadHtml( '<?xml encoding="UTF-8">' . $content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD );

            libxml_use_internal_errors( $internalErrors );

            $images = $dom->getElementsByTagName( 'img' );

            foreach( $images as $img ) :

                $src = $img->getAttribute( 'src' );

                if ( preg_match( '/data:image/', $src ) ) :

                    preg_match( '/data:image\/(?<mime>.*?)\;/', $src, $groups );

                    $mimetype = $groups[ 'mime' ];

                    $filepath = 'uploads/temp/' . uniqid() . '.' . $mimetype;

                    $image = Image::make( $src )
                        ->encode( $mimetype, 100 )
                        ->save( public_path( $filepath ) );

                    $attachment = new Attachment( App::upload_file_from_path( $filepath ) );

                    File::delete( public_path( $filepath ) );

                    $img->removeAttribute( 'src' );
                    $img->setAttribute( 'src', $attachment->url );

                endif;

            endforeach;

            return $dom->saveHTML();

        endif;

        return $content;
    }

}