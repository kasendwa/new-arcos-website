<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostMeta extends Model {

	protected $table = 'post_meta';
	protected $guarded  = [ 'id' ];
	protected $fillable = [ 'post_id', 'key', 'value' ];
	protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];
	protected $hidden = [ 'created_at', 'updated_at', 'deleted_at' ];

}