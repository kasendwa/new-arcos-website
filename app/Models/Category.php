<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Category extends Model implements SluggableInterface {

	use SluggableTrait;

	protected $sluggable = [
		'build_from' => 'name',
		'save_to' => 'slug',
	];

	protected $dates = [ 'deleted_at' ];
	protected $guarded  = [ 'id' ];

	public function permalink() {
		return url( '/articles/' . $this->slug );
	}

	public function get_description() {
		return nl2br( $this->description );
	}

	public function get_author() {
		return $this->belongsTo( User::class, 'user_id' );
	}

	public function get_post() {
		return $this->hasMany( Post::class, 'category_id' );
	}

	public function language() {
		return $this->belongsTo( Language::class, 'language_id' );
	}
    
}
