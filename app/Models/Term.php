<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Term extends Model {

	protected $table = 'terms';
	protected $guarded  = [ 'id' ];
	protected $fillable = [ 'slug', 'taxonomy' ];
	protected $dates = [ 'created_at', 'updated_at' ];
	protected $hidden = [ 'updated_at' ];

}
