<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model {

	protected $table = 'user_meta';
	protected $guarded  = [ 'id' ];
	protected $fillable = [ 'user_id', 'key', 'value' ];
	protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];
	protected $hidden = [ 'created_at', 'updated_at', 'deleted_at' ];

}