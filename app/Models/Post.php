<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

	protected $table = 'posts';
	protected $guarded  = [ 'id' ];
	protected $fillable = [ 'name', 'type', 'status', 'user_id', 'user_id_edited' ];
	protected $dates = [ 'created_at', 'updated_at' ];
	protected $hidden = [ 'updated_at' ];

}
