<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TermMeta extends Model {

	protected $table = 'term_meta';
	protected $guarded  = [ 'id' ];
	protected $fillable = [ 'term_id', 'key', 'value' ];
	protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];
	protected $hidden = [ 'created_at', 'updated_at', 'deleted_at' ];

}