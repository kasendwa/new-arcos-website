<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRoleMeta extends Model {

	protected $table = 'user_role_meta';
	protected $guarded  = [ 'id' ];
	protected $fillable = [ 'user_role_id', 'key', 'value' ];
	protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];
	protected $hidden = [ 'created_at', 'updated_at', 'deleted_at' ];
	
	private $rules = [
		'user_role_id' => 'required',
		'key' => 'required|min:1'
	];

}