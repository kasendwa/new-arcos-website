<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model {

	protected $table = 'options';
	protected $guarded  = [ 'id' ];
	protected $fillable = [ 'name', 'value' ];
	protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];
	protected $hidden = [ 'created_at', 'updated_at', 'deleted_at' ];

}