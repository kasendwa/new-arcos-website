<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Language extends Model {

	protected $table = 'languages';
	protected $guarded  = [ 'id' ];
	protected $fillable = [ 'name', 'code' ];
	protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];
	protected $hidden = [ 'created_at', 'updated_at', 'deleted_at' ];

	private $rules = [
        'name' => 'required|min:2',
        'lang_code' => 'required|min:2'
	];
    
}