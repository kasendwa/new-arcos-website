<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider {
    
    public function boot() {
        Blade::setEchoFormat( 'e(utf8_encode(%s))' );
    }

    public function register() {

    }
    
}
