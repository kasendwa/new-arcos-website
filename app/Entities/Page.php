<?php

namespace App\Entities;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

use App\App;
use App\Models\Post;

class Page {

	public $id;
	public $name;
	public $title;
	public $menu_title;
	public $show_in_menu;
	public $menu_order;
	public $thumbnail;
	public $summary;
	public $content;
	public $permalink;
	public $template;
	public $parent;

	function __construct( $identifier = 0 ) {
		$post = App::get_post( $identifier );

		if ( $post !== null ) {
			$this->id = $post->id;
			$this->name = $post->name;
			$this->title = ( object ) [
				'en' => App::get_post_meta( $this->id, 'title_en', '' ),
				'fr' => App::get_post_meta( $this->id, 'title_fr', '' )
			];
			$this->menu_title = ( object ) [
				'en' => App::get_post_meta( $this->id, 'menu_title_en', '' ),
				'fr' => App::get_post_meta( $this->id, 'menu_title_fr', '' )
			];
			$this->show_in_menu = ( boolean ) App::get_post_meta( $this->id, 'show_in_menu', 0 );
			$this->menu_order = ( int ) App::get_post_meta( $this->id, 'menu_order', 0 );
			$this->thumbnail = $this->get_thumbnails();
			$this->summary = ( object ) [
				'en' => trim( strip_tags( App::get_post_meta( $this->id, 'summary_en', '' ) ) ),
				'fr' => trim( strip_tags( App::get_post_meta( $this->id, 'summary_fr', '' ) ) )
			];
			$this->content = ( object ) [
				'en' => App::get_post_meta( $this->id, 'content_en', '' ),
				'fr' => App::get_post_meta( $this->id, 'content_fr', '' )
			];
			$this->permalink = $this->get_permalink();
			$this->template = App::get_post_meta( $this->id, 'template', App::get_option( 'defaults_page_template' ) );
			$this->parent = ( int ) App::get_post_meta( $this->id, 'parent_id', 0 );
		}
	}

	public function delete() {
		$attachment_id = App::get_post_meta( $this->id, 'thumbnail', 0 );

		if ( $attachment_id > 0 ) :

			App::delete_post_meta( $this->id, 'thumbnail' );

			$attachment = new Attachment( $attachment_id );

			$attachment->id != null ? $attachment->delete() : null;

		endif;

		Post::find( $this->id )->delete();
	}

	private function get_thumbnails() {
		$thumbnails = [];

		$thumbnail_id = ( int ) App::get_post_meta( $this->id, 'thumbnail', 0 );

		if ( $thumbnail_id != 0 ) :
			
			$attachment = Attachment::get_attachment( $thumbnail_id );

			$path_segments = explode( 'uploads', $attachment->url );

			$path = 'uploads' . end( $path_segments );

			foreach( App::get_image_sizes() as $size => $params ) :

				$filename = str_replace( '.' . File::extension( $path ), '', $path ) . '-' . $params->width . 'x' . $params->height . '.' . File::extension( $path );

				File::exists( $filename ) ? $thumbnails[ str_replace( '-', '_', $size ) ] = url( $filename ) : null;

			endforeach;

		endif;

		return ( object ) $thumbnails;
	}
	
	private function get_permalink() {
		$parent_meta = App::get_post_meta( $this->id, 'parent_id' );

		while ( $parent_meta != null ) :

			$parent_post = App::get_post( ( int ) $parent_meta );

			$parent_post != null ? ( $slug = $parent_post->name . ( isset( $slug ) ? '/' . $slug : '' ) ) : null;

			$parent_meta = $parent_post != null ? App::get_post_meta( $parent_post->id, 'parent_id' ) : null;

		endwhile;

		return url( '/' . App::get_current_language() . ( isset( $slug ) ? '/' . $slug : '' ) . '/' . $this->name  );
	}

	public function get_related_articles( $number = 6 ) {
		foreach( ( array ) json_decode( App::get_post_meta( $this->id, 'related_articles', json_encode( [] ) ) ) as $entry )

            if ( App::get_post( ( int ) $entry ) != null )
			    $articles[] = ( int ) $entry;

		$articles = !isset( $articles ) ? Article::get_all_articles() : $articles;

		return array_slice( $articles, 0, $number );
	}

	public function get_related_projects( $number = 6 ) {
		foreach( ( array ) json_decode( App::get_post_meta( $this->id, 'related_projects', json_encode( [] ) ) ) as $entry )

            if ( App::get_post( ( int ) $entry ) != null )
                $projects[] = ( int ) $entry;

		$projects = !isset( $projects ) ? Project::get_all_projects() : $projects;

		return array_slice( $projects, 0, $number );
	}

	public static function get_page( $identifier, $language = null ) {
		$page = new Page( App::get_post( $identifier )->id );

		return $language == null ?
			( object ) [
				'id' => $page->id,
				'name' => $page->name,
				'title' => $page->title,
				'menu_title' => $page->menu_title,
				'show_in_menu' => $page->show_in_menu,
				'menu_order' => $page->menu_order,
				'thumbnail' => $page->thumbnail,
				'summary' => $page->summary,
				'content' => $page->content,
				'permalink' => $page->permalink,
				'template' => $page->template,
				'parent' => $page->parent,
				'related_articles' => $page->get_related_articles( 6 ),
				'related_projects' => $page->get_related_projects( 6 )
			]
			:
			( object ) [
				'id' => $page->id,
				'name' => $page->name,
				'title' => $page->title->{ $language },
				'menu_title' => $page->menu_title->{ $language },
				'show_in_menu' => $page->show_in_menu,
				'menu_order' => $page->menu_order,
				'thumbnail' => $page->thumbnail,
				'summary' => $page->summary->{ $language },
				'content' => $page->content->{ $language },
				'permalink' => $page->permalink,
				'template' => $page->template,
				'parent' => $page->parent,
				'related_articles' => $page->get_related_articles( 6 ),
				'related_projects' => $page->get_related_projects( 6 )
			];
	}

	public static function add_page( $params ) {
		$args = [];

		$args[ 'type' ] = 'page';
		$args[ 'status' ] = isset( $params[ 'status' ] ) ? $params[ 'status' ] : 'draft';

		isset( $params[ 'name' ] ) ? ( $args[ 'name' ] = str_slug( $params[ 'name' ], '-' ) ) : null;
		isset( $params[ 'title' ][ 'en' ] ) && !isset( $params[ 'name' ] ) ? ( $args[ 'name' ] = str_slug( $params[ 'title' ][ 'en' ], '-' ) ) : null;
		isset( $params[ 'author' ] ) ? ( $args[ 'user_id' ] = $params[ 'author' ] ) : null;

		!isset( $params[ 'menu_order' ] ) ? ( $params[ 'menu_order' ] = 0 ) : null;
		!isset( $params[ 'show_in_menu' ] ) ? ( $params[ 'show_in_menu' ] = false ) : null;

		$counter = 1;

		while( App::get_post( $args[ 'name' ] ) != null ) :

			$counter++;

			$segments = explode( '-', $args[ 'name' ] );
			$suffix = array_pop( $segments );

			$args[ 'name' ] = $counter > 2 ? str_replace( '-' . $suffix, '', $args[ 'name' ] ) : $args[ 'name' ];
			$args[ 'name' ] = $args[ 'name' ] . '-' . $counter;

		endwhile;

		$post = App::add_post( $args );

		if ( !isset( $params[ 'summary' ] ) || ( isset( $params[ 'summary' ] ) && isset( $params[ 'summary' ][ 'en' ] ) && ( empty( $params[ 'summary' ][ 'en' ] ) && empty( $params[ 'summary' ][ 'fr' ] ) ) ) )
			$params[ 'summary' ] = [
				'en' => str_replace( '...', '', Str::words( strip_tags( $params[ 'content' ][ 'en' ] ), 55 ) ),
				'fr' => str_replace( '...', '', Str::words( strip_tags( $params[ 'content' ][ 'fr' ] ), 55 ) ),
			];

		foreach( [ 'title', 'menu_title', 'content', 'summary' ] as $entry )

			foreach( $params[ $entry ] as $language => $value )
				App::add_post_meta( $post->id, $entry . '_' . $language, $value );

		App::add_post_meta( $post->id, 'show_in_menu', ( int ) $params[ 'show_in_menu' ] );
		App::add_post_meta( $post->id, 'menu_order', ( int ) $params[ 'menu_order' ] );

		return $post->id;
	}

	public static function get_pages( $per_page = 15, $page = 1 ) {
		$posts = DB::table( 'posts' )
			->select( 'posts.id' )
			->leftJoin( 'post_meta', 'posts.id', '=', 'post_meta.post_id' )
			->where( 'posts.type', '=', 'page' )
			->groupBy( 'posts.id' )
			->orderBy( 'posts.name', 'asc' )
			->offset( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $posts as $post )
			$pages[] = $post->id;

		return isset( $pages ) ? $pages : [];
	}

	public static function get_all_pages() {
		$posts = DB::table( 'posts' )
			->select( 'id' )
			->where( 'type', '=', 'page' )
			->groupBy( 'id' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $posts as $post )
			$pages[] = $post->id;

		return isset( $pages ) ? $pages : [];
	}

	public static function get_templates() {
		$templates = [];

		foreach( json_decode( App::get_option( 'page_templates', json_encode( [] ) ) ) as $entry ) :

			$templates[ $entry ] = ( object ) [
				'name' => ( object ) [
					'en' => App::get_option( 'page_templates_' . $entry . '_name_en', '' ),
					'fr' => App::get_option( 'page_templates_' . $entry . '_name_fr', '' )
				],
				'properties' => ( object ) [
					'has_related_articles' => ( boolean ) App::get_option( 'page_templates_' . $entry . '_properties_has_related_articles', false ),
					'has_related_projects' => ( boolean ) App::get_option( 'page_templates_' . $entry . '_properties_has_related_projects', false )
				]
			];

		endforeach;

		return $templates;
	}

}