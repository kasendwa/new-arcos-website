<?php

namespace App\Entities;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

use App\App;
use App\Models\Post;

class Article {

	public $id;
	public $name;
	public $title;
	public $thumbnail;
	public $summary;
	public $content;
	public $permalink;
	public $author;
	public $timestamp;

	function __construct( $identifier = 0 ) {
		$post = App::get_post( $identifier );

		if ( $post !== null ) {
			$this->id = $post->id;
			$this->name = $post->name;
			$this->title = ( object ) [
				'en' => App::get_post_meta( $this->id, 'title_en', '' ),
				'fr' => App::get_post_meta( $this->id, 'title_fr', '' )
			];
			$this->thumbnail = $this->get_thumbnails();
			$this->summary = ( object ) [
				'en' => trim( strip_tags( App::get_post_meta( $this->id, 'summary_en', '' ) ) ),
				'fr' => trim( strip_tags( App::get_post_meta( $this->id, 'summary_fr', '' ) ) )
			];
			$this->content = ( object ) [
				'en' => App::get_post_meta( $this->id, 'content_en', '' ),
				'fr' => App::get_post_meta( $this->id, 'content_fr', '' )
			];
			$this->permalink = $this->get_permalink();
			$this->author = User::get_user( $post->user_id );
			$this->categories = $this->get_categories();
			$this->timestamp = $post->created_at;
		}
	}

	public function delete() {
		$attachment_id = App::get_post_meta( $this->id, 'thumbnail', 0 );

		if ( $attachment_id > 0 ) :

			App::delete_post_meta( $this->id, 'thumbnail' );

			$attachment = new Attachment( $attachment_id );

			$attachment->id != null ? $attachment->delete() : null;

		endif;

		Post::find( $this->id )->delete();
	}

	private function get_thumbnails() {
		$thumbnails = [];

		$thumbnail_id = ( int ) App::get_post_meta( $this->id, 'thumbnail', 0 );

		if ( $thumbnail_id != 0 ) :

			$attachment = Attachment::get_attachment( $thumbnail_id );

			$path_segments = explode( 'uploads', $attachment->url );

			$path = 'uploads' . end( $path_segments );

			foreach( App::get_image_sizes() as $size => $params ) :

				$filename = str_replace( '.' . File::extension( $path ), '', $path ) . '-' . $params->width . 'x' . $params->height . '.' . File::extension( $path );

				File::exists( $filename ) ? $thumbnails[ str_replace( '-', '_', $size ) ] = url( $filename ) : null;

			endforeach;

		endif;

		return ( object ) $thumbnails;
	}

	private function get_permalink() {
		return url( '/' . App::get_current_language() . '/' . App::get_option( 'post_types_article_rewrite_site_base', 'article' ) . '/' . $this->name );
	}

	public function get_categories() {
		$entries = ( array ) json_decode( App::get_post_meta( $this->id, 'categories', json_encode( [] ) ) );

		foreach( $entries as $entry )
			$categories[] = ( int ) $entry;

		return isset( $categories ) && is_array( $categories ) ? $categories : [];
	}

	public function get_related_articles( $number = 6 ) {
		$related_articles = json_decode( App::get_post_meta( $this->id, 'related_articles', json_encode( [] ) ) );

		if ( count( $related_articles ) > 0 )
			return $related_articles;

		$entries = [];

		foreach( $this->get_categories() as $entry ) :

			$category = new category( $entry );

			$entries = array_unique( array_merge( $entries, $category->get_all_articles() ) );

			shuffle( $entries );

		endforeach;

		return array_slice( $entries, 0, $number );
	}

	public static function get_article( $identifier, $language = null ) {
		$article = new Article( App::get_post( $identifier )->id );

		return $language == null ?
			( object ) [
				'id' => $article->id,
				'name' => $article->name,
				'title' => $article->title,
				'thumbnail' => $article->thumbnail,
				'summary' => $article->summary,
				'content' => $article->content,
				'permalink' => $article->permalink,
				'author' => $article->author,
				'categories' => $article->categories,
				'timestamp' => $article->timestamp
			]
			:
			( object ) [
				'id' => $article->id,
				'name' => $article->name,
				'title' => $article->title->{ $language },
				'thumbnail' => $article->thumbnail,
				'summary' => $article->summary->{ $language },
				'content' => $article->content->{ $language },
				'permalink' => $article->permalink,
				'author' => $article->author,
				'categories' => $article->categories,
				'timestamp' => $article->timestamp
			];
	}

	public static function add_article( $params ) {
		$args = [];

		$args[ 'type' ] = 'article';
		$args[ 'status' ] = isset( $params[ 'status' ] ) ? $params[ 'status' ] : 'draft';

		isset( $params[ 'name' ] ) ? ( $args[ 'name' ] = str_slug( $params[ 'name' ], '-' ) ) : null;
		isset( $params[ 'title' ][ 'en' ] ) && !isset( $params[ 'name' ] ) ? ( $args[ 'name' ] = str_slug( $params[ 'title' ][ 'en' ], '-' ) ) : null;
		isset( $params[ 'author' ] ) ? ( $args[ 'user_id' ] = $params[ 'author' ] ) : null;

		if ( !isset( $params[ 'summary' ] ) || ( isset( $params[ 'summary' ] ) && isset( $params[ 'summary' ][ 'en' ] ) && ( empty( $params[ 'summary' ][ 'en' ] ) && empty( $params[ 'summary' ][ 'fr' ] ) ) ) )
			$params[ 'summary' ] = [
				'en' => str_replace( '...', '', Str::words( strip_tags( $params[ 'content' ][ 'en' ] ), 55 ) ),
				'fr' => str_replace( '...', '', Str::words( strip_tags( $params[ 'content' ][ 'fr' ] ), 55 ) ),
			];

		$counter = 1;

		while( App::get_post( $args[ 'name' ] ) != null ) :

			$counter++;

			$segments = explode( '-', $args[ 'name' ] );
			$suffix = array_pop( $segments );

			$args[ 'name' ] = $counter > 2 ? str_replace( '-' . $suffix, '', $args[ 'name' ] ) : $args[ 'name' ];
			$args[ 'name' ] = $args[ 'name' ] . '-' . $counter;

		endwhile;

		$post = App::add_post( $args );

		foreach( [ 'title', 'content', 'summary' ] as $entry )

			foreach( $params[ $entry ] as $language => $value )
				App::add_post_meta( $post->id, $entry . '_' . $language, $value );

		return $post->id;
	}

	public static function get_all_articles() {
		$posts = DB::table( 'posts' )
			->select( 'id' )
			->where( 'type', '=', 'article' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $posts as $post )
			$articles[] = $post->id;

		return isset( $articles ) ? $articles : [];
	}

	public static function get_articles( $per_page = 12, $page = 1 ) {
		$posts = DB::table( 'posts' )
			->select( 'id' )
			->where( 'type', '=', 'article' )
			->orderBy( 'id', 'desc' )
			->skip( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $posts as $post )
			$articles[] = $post->id;

		return isset( $articles ) ? $articles : [];
	}
}