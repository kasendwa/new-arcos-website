<?php

namespace App\Entities;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

use App\App;
use App\Models\Post;

class Project {

	public $id;
	public $name;
	public $title;
	public $thumbnail;
	public $summary;
	public $content;
	public $related_articles;
	public $permalink;

	function __construct( $identifier = 0 ) {
		$post = App::get_post( $identifier );

		if ( $post !== null ) {
			$this->id = $post->id;
			$this->name = $post->name;
			$this->title = ( object ) [
				'en' => App::get_post_meta( $this->id, 'title_en', '' ),
				'fr' => App::get_post_meta( $this->id, 'title_fr', '' )
			];
			$this->thumbnail = $this->get_thumbnails();
			$this->summary = ( object ) [
				'en' => App::get_post_meta( $this->id, 'summary_en', '' ),
				'fr' => App::get_post_meta( $this->id, 'summary_fr', '' )
			];
			$this->content = ( object ) [
				'en' => App::get_post_meta( $this->id, 'content_en', '' ),
				'fr' => App::get_post_meta( $this->id, 'content_fr', '' )
			];
			$this->related_articles = json_decode( App::get_post_meta( $this->id, 'related_articles', json_encode( [] ) ) );
			$this->permalink = $this->get_permalink();
		}
	}

	public function delete() {
		$attachment_id = App::get_post_meta( $this->id, 'thumbnail', 0 );

		if ( $attachment_id > 0 ) :

			App::delete_post_meta( $this->id, 'thumbnail' );

			$attachment = new Attachment( $attachment_id );

			$attachment->id != null ? $attachment->delete() : null;

		endif;

		Post::find( $this->id )->delete();
	}

	private function get_thumbnails() {
		$thumbnails = [];

		$thumbnail_id = ( int ) App::get_post_meta( $this->id, 'thumbnail', 0 );

		if ( $thumbnail_id != 0 ) :

			$attachment = Attachment::get_attachment( $thumbnail_id );

			$path_segments = explode( 'uploads', $attachment->url );

			$path = 'uploads' . end( $path_segments );

			foreach( App::get_image_sizes() as $size => $params ) :

				$filename = str_replace( '.' . File::extension( $path ), '', $path ) . '-' . $params->width . 'x' . $params->height . '.' . File::extension( $path );

				File::exists( $filename ) ? $thumbnails[ str_replace( '-', '_', $size ) ] = url( $filename ) : null;

			endforeach;

		endif;

		return ( object ) $thumbnails;
	}

	private function get_permalink() {
		return url( '/' . App::get_current_language() . '/' . App::get_option( 'post_types_project_rewrite_site_base', 'project' ) . '/' . $this->name );
	}

	public function get_related_articles( $number = 6 ) {
		$related_articles = json_decode( App::get_post_meta( $this->id, 'related_articles', json_encode( [] ) ) );

		if ( count( $related_articles ) > 0 )
			return $related_articles;

		$entries = [];

		foreach( $this->get_categories() as $entry ) :

			$category = new category( $entry );

			$entries = array_unique( array_merge( $entries, $category->get_all_articles() ) );

			shuffle( $entries );

		endforeach;

		return array_slice( $entries, 0, $number );
	}

	public static function get_project( $identifier, $language = null ) {
		$project = new Project( App::get_post( $identifier )->id );

		return $language == null ?
			( object ) [
				'id' => $project->id,
				'name' => $project->name,
				'title' => $project->title,
				'thumbnail' => $project->thumbnail,
				'summary' => $project->summary,
				'content' => $project->content,
				'related_articles' => $project->related_articles,
				'permalink' => $project->permalink,
			]
			:
			( object ) [
				'id' => $project->id,
				'name' => $project->name,
				'title' => $project->title->{ $language },
				'thumbnail' => $project->thumbnail,
				'summary' => strip_tags( $project->summary->{ $language } ),
				'content' => $project->content->{ $language },
				'related_articles' => $project->related_articles,
				'permalink' => $project->permalink,
			];
	}

	public static function add_project( $params ) {
		$args = [];

		$args[ 'type' ] = 'project';
		$args[ 'status' ] = isset( $params[ 'status' ] ) ? $params[ 'status' ] : 'draft';

		isset( $params[ 'name' ] ) ? ( $args[ 'name' ] = str_slug( $params[ 'name' ], '-' ) ) : null;
		isset( $params[ 'title' ][ 'en' ] ) && !isset( $params[ 'name' ] ) ? ( $args[ 'name' ] = str_slug( $params[ 'title' ][ 'en' ], '-' ) ) : null;
		isset( $params[ 'author' ] ) ? ( $args[ 'user_id' ] = $params[ 'author' ] ) : null;

		if ( !isset( $params[ 'summary' ] ) )
			$params[ 'summary' ] = [
				'en' => str_replace( '...', '', Str::words( strip_tags( $params[ 'content' ][ 'en' ] ), 55 ) ),
				'fr' => str_replace( '...', '', Str::words( strip_tags( $params[ 'content' ][ 'fr' ] ), 55 ) ),
			];

		$counter = 1;

		while( App::get_post( $args[ 'name' ] ) != null ) :

			$counter++;

			$segments = explode( '-', $args[ 'name' ] );
			$suffix = array_pop( $segments );

			$args[ 'name' ] = $counter > 2 ? str_replace( '-' . $suffix, '', $args[ 'name' ] ) : $args[ 'name' ];
			$args[ 'name' ] = $args[ 'name' ] . '-' . $counter;

		endwhile;

		$post = App::add_post( $args );

		foreach( [ 'title', 'content', 'summary' ] as $entry )

			foreach( $params[ $entry ] as $language => $value )
				App::add_post_meta( $post->id, $entry . '_' . $language, $value );

		return $post->id;
	}

	public static function get_projects( $per_page = 12, $page = 1 ) {
		$posts = DB::table( 'posts' )
			->select( 'id' )
			->where( 'type', '=', 'project' )
			->orderBy( 'id', 'desc' )
			->skip( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $posts as $post )
			$projects[] = $post->id;

		return isset( $projects ) ? $projects : [];
	}

	public static function get_all_projects() {
		$posts = DB::table( 'posts' )
			->select( 'id' )
			->where( 'type', '=', 'project' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $posts as $post )
			$projects[] = $post->id;

		return isset( $projects ) ? $projects : [];
	}

}