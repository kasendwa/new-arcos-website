<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;

use App\App;
use App\Models\Term;

class PartnerCategory {

	public $id;
	public $title;
	public $description;
	public $order;

	function __construct( $identifier = 0 ) {
		$term = App::get_term( $identifier );

		if ( $term !== null ) {
			$this->id = $term->id;
			$this->title = ( object ) [
				'en' => App::get_term_meta( $this->id, 'title_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'title_fr', '' )
			];
			$this->description = ( object ) [
				'en' => App::get_term_meta( $this->id, 'description_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'description_fr', '' )
			];
            $this->order = ( int ) App::get_term_meta( $this->id, 'order', 0 );
		}
	}

	public function delete() {
		Term::find( $this->id )->delete();
	}

	public static function get_partner_category( $identifier, $lang = null ) {
		$partner_category = new PartnerCategory( App::get_term( $identifier )->id );

		return $lang == null ?
			( object ) [
				'id' => $partner_category->id,
				'title' => $partner_category->title,
				'description' => $partner_category->description,
                'order' => $partner_category->order
			] :
			( object ) [
                'id' => $partner_category->id,
                'title' => $partner_category->title->{ $lang },
                'description' => $partner_category->description->{ $lang },
                'order' => $partner_category->order
			];
	}

    public function get_partners() {
        $terms = DB::table( 'terms' )
            ->select( 'terms.id' )
            ->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
            ->where( 'terms.taxonomy', '=', 'partner' )
            ->where( 'term_meta.key', '=', 'category' )
            ->where( 'term_meta.value', '=', $this->id )
            ->groupBy( 'terms.id' )
            ->orderBy( 'terms.created_at', 'desc' )
            ->get();

        foreach( $terms as $term )
            $partners[] = Partner::get_partner( $term->id );

        return isset( $partners ) ? $partners : [];
    }

	public static function add_partner_category( $params ) {
		$args = [];

		$args[ 'taxonomy' ] = 'partner-category';

		isset( $params[ 'title' ] ) && isset( $params[ 'title' ][ 'en' ] ) ? ( $args[ 'slug' ] = str_slug( $params[ 'title' ][ 'en' ], '-' ) ) : null;
		!isset( $args[ 'slug' ] ) ? ( $args[ 'slug' ] = time() . rand( 100, 999 ) ) : null;

		$counter = 1;

		while( App::get_term( $args[ 'slug' ] ) != null ) :

			$counter++;

			$segments = explode( '-', $args[ 'slug' ] );
			$suffix = array_pop( $segments );

			$args[ 'slug' ] = $counter > 2 ? str_replace( '-' . $suffix, '', $args[ 'slug' ] ) : $args[ 'slug' ];
			$args[ 'slug' ] = $args[ 'slug' ] . '-' . $counter;

		endwhile;

		$term = App::add_term( $args );

		foreach( [ 'title', 'description' ] as $entry )

			if ( isset( $params[ $entry ] ) && is_array( $params[ $entry ] ) )

				foreach( $params[ $entry ] as $language => $value )
					App::add_term_meta( $term->id, $entry . '_' . $language, $value );

        isset( $params[ 'order' ] ) ? App::add_term_meta( $term->id, 'order', $params[ 'order' ] ) : null;

		return $term->id;
	}

	public static function get_partner_categories( $per_page = 15, $page = 1 ) {
		$terms = DB::table( 'terms' )
			->select( 'terms.id' )
			->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
			->where( 'terms.taxonomy', '=', 'partner-category' )
			->groupBy( 'terms.id' )
			->orderBy( 'terms.slug', 'asc' )
			->offset( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $terms as $term )
			$partner_categories[] = $term->id;

		return isset( $partner_categories ) ? $partner_categories : [];
	}

	public static function get_all_partner_categories() {
		$terms = DB::table( 'terms' )
			->select( 'id' )
			->where( 'taxonomy', '=', 'partner-category' )
			->groupBy( 'id' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $terms as $term )
            $partner_categories[] = $term->id;

		return isset( $partner_categories ) ? $partner_categories : [];
	}

}