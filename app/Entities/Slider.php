<?php

namespace App\Entities;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

use App\App;
use App\Models\Term;

class Slider {

	public $id;
	public $thumbnail;
	public $content;
	public $anchor;
	public $entity;
	public $order;

	function __construct( $identifier = 0 ) {
		$term = App::get_term( $identifier );

		if ( $term !== null ) {
			$this->id = $term->id;
			$this->slug = $term->slug;
			$this->thumbnail = $this->get_thumbnails();
			$this->content = ( object ) [
				'title' => ( object ) [
					'en' => App::get_term_meta( $this->id, 'title_en', '' ),
					'fr' => App::get_term_meta( $this->id, 'title_fr', '' )
				],
				'text' => ( object ) [
					'en' => strip_tags( App::get_term_meta( $this->id, 'text_en', '' ) ),
					'fr' => strip_tags( App::get_term_meta( $this->id, 'text_fr', '' ) )
				]
			];
			$this->anchor = ( object ) [
				'link' => $this->generate_slider_link(),
				'text' => ( object ) [
					'en' => App::get_term_meta( $this->id, 'button_text_en', '' ),
					'fr' => App::get_term_meta( $this->id, 'button_text_fr', '' )
				]
			];

			$this->order = App::get_term_meta( $this->id, 'order', 0 );
		}
	}

	public function delete() {
		Term::find( $this->id )->delete();
	}

	private function generate_slider_link() {
		$entity_type = App::get_term_meta( $this->id, 'entity_type' );
		$entity_name = App::get_term_meta( $this->id, 'entity_name' );
		$entity_id = App::get_term_meta( $this->id, 'entity_id' );
		$custom_link = App::get_term_meta( $this->id, 'custom_link' );

		$this->entity = ( object ) [
		    'type' => App::get_term_meta( $this->id, 'entity_type' ),
            'name' => App::get_term_meta( $this->id, 'entity_name' ),
            'id' => App::get_term_meta( $this->id, 'entity_id' ),
            'custom_link' => App::get_term_meta( $this->id, 'custom_link' )
        ];

		if ( $custom_link != null && !empty( $custom_link ) ) :

			return $custom_link;

		elseif ( ( $entity_type != null && !empty( $entity_type ) ) && ( $entity_name != null && !empty( $entity_name ) ) ) :

			if ( $entity_type == 'post_type' ) :

				$post_type = App::get_post_type( $entity_name );

				if ( ( $entity_id == null || empty( $entity_id ) ) && !empty( $post_type->rewrite->site->base ) ) :

					$slug = $post_type->rewrite->site->archive;

				elseif ( $entity_id != null && !empty( $entity_id ) && !empty( $post_type->rewrite->site->base ) ) :

					$post = App::get_post( $entity_id );

				    $post != null ? ( $slug = $post_type->rewrite->site->base . '/' . $post->name ) : null;

				elseif ( $entity_id != null && !empty( $entity_id ) && !$post_type->has_archive && $entity_name == 'page' ) :

					$page = new Page( ( int ) $entity_id );

					return $page->permalink;

				endif;

			endif;

			return url( '/' . App::get_current_language() . ( isset( $slug ) ? '/' . $slug : '' ) );

		else :

			return url( '/' . App::get_current_language() );

		endif;
	}

	private function get_thumbnails() {
		$thumbnails = [];

		$thumbnail_id = ( int ) App::get_term_meta( $this->id, 'image', null );

		if ( $thumbnail_id != 0 ) :

			$attachment = Attachment::get_attachment( $thumbnail_id );

			$path_segments = explode( 'uploads', $attachment->url );

			$path = 'uploads' . end( $path_segments );

			foreach( App::get_image_sizes() as $size => $params ) :

				$filename = str_replace( '.' . File::extension( $path ), '', $path ) . '-' . $params->width . 'x' . $params->height . '.' . File::extension( $path );

				File::exists( $filename ) ? $thumbnails[ str_replace( '-', '_', $size ) ] = url( $filename ) : null;

			endforeach;

		endif;

		return ( object ) $thumbnails;
	}

	public static function get_sliders() {
		foreach( Term::where( 'taxonomy', '=', 'slider' )->get() as $term )
			$sliders[ ( int ) App::get_term_meta( $term->id, 'order', 0 ) ] = Slider::get_slider( $term->id, App::get_current_language() );

		return array_values( isset( $sliders ) ? $sliders : [] );
	}

	public static function get_slider( $identifier, $language = null ) {
		$slider = new Slider( App::get_term( $identifier )->id );

		return $language == null ?
			( object ) [
				'id' => $slider->id,
				'thumbnail' => $slider->thumbnail,
				'content' => $slider->content,
				'anchor' => $slider->anchor,
				'order' => $slider->order,
                'entity' => $slider->entity
			] :
			( object ) [
				'id' => $slider->id,
				'thumbnail' => $slider->thumbnail,
				'content' => ( object ) [
					'title' => $slider->content->title->{ $language },
					'text' => $slider->content->text->{ $language }
				],
				'anchor' => ( object ) [
					'link' => $slider->anchor->link,
					'text' => $slider->anchor->text->{ $language }
				],
				'order' => $slider->order,
                'entity' => $slider->entity
			];
	}

	public static function add_slider( $params ) {
		$args = [];

		$args[ 'taxonomy' ] = 'slider';

		isset( $params[ 'content' ] ) && isset( $params[ 'content' ][ 'title' ] ) && isset( $params[ 'content' ][ 'title' ][ 'en' ] ) ? ( $args[ 'slug' ] = str_slug( $params[ 'content' ][ 'title' ][ 'en' ], '-' ) ) : null;
		!isset( $args[ 'slug' ] ) ? ( $args[ 'slug' ] = time() . rand( 100, 999 ) ) : null;

		$counter = 1;

		while( App::get_term( $args[ 'slug' ] ) != null ) :

			$counter++;

			$segments = explode( '-', $args[ 'slug' ] );
			$suffix = array_pop( $segments );

			$args[ 'slug' ] = $counter > 2 ? str_replace( '-' . $suffix, '', $args[ 'slug' ] ) : $args[ 'slug' ];
			$args[ 'slug' ] = $args[ 'slug' ] . '-' . $counter;

		endwhile;

		$term = App::add_term( $args );

		if ( isset( $params[ 'image' ] ) && is_string( $params[ 'image' ] ) )
			$params[ 'image' ] = App::upload_media_from_path( $params[ 'image' ] );

		isset( $params[ 'image' ] ) ? App::add_term_meta( $term->id, 'image', $params[ 'image' ] ) : null;

		isset( $params[ 'custom_link' ] ) ? App::add_term_meta( $term->id, 'custom_link', $params[ 'custom_link' ] ) : null;

		foreach( [ 'title', 'text' ] as $entry )

			if ( isset( $params[ 'content' ] ) && isset( $params[ 'content' ][ $entry ] ) && is_array( $params[ 'content' ][ $entry ] ) )

				foreach( $params[ 'content' ][ $entry ] as $language => $value )
					App::add_term_meta( $term->id, $entry . '_' . $language, $value );

		foreach( [ 'type', 'name', 'id' ] as $entry )

			if ( isset( $params[ 'anchor' ] ) && isset( $params[ 'anchor' ][ 'entity' ] ) && isset( $params[ 'anchor' ][ 'entity' ][ $entry ] ) && !empty( $params[ 'anchor' ][ 'entity' ][ $entry ] ) )
				App::add_term_meta( $term->id, 'entity_' . $entry, $params[ 'anchor' ][ 'entity' ][ $entry ] );

		if ( isset( $params[ 'anchor' ] ) && isset( $params[ 'anchor' ][ 'text' ] ) && is_array( $params[ 'anchor' ][ 'text' ] ) )

			foreach( $params[ 'anchor' ][ 'text' ] as $language => $value )
				App::add_term_meta( $term->id, 'button_text_' . $language, $value );

		isset( $params[ 'order' ] ) ? App::add_term_meta( $term->id, 'order', $params[ 'order' ] ) : null;

		return $term->id;
	}

	public static function get_admin_sliders( $per_page = 15, $page = 1 ) {
		$terms = DB::table( 'terms' )
			->select( 'terms.id' )
			->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
			->where( 'terms.taxonomy', '=', 'slider' )
			->groupBy( 'terms.id' )
			->orderBy( 'terms.slug', 'asc' )
			->offset( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $terms as $term )
			$sliders[] = $term->id;

		return isset( $sliders ) ? $sliders : [];
	}

}