<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;

use App\App;

class Menu {

	public $id;
	public $title;
	public $position;

	function __construct( $identifier = 0 ) {
		$term = App::get_term( $identifier );

		if ( $term !== null ) {
			$this->id = $term->id;
			$this->title = ( object ) [
				'en' => App::get_term_meta( $this->id, 'title_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'title_fr', '' )
			];
			$this->position = App::get_term_meta( $this->id, 'position', 0 );
		}
	}

	public function get_items( $lang = null ) {
		$terms = DB::table( 'terms' )
			->select( 'terms.id' )
			->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
			->where( 'terms.taxonomy', '=', 'menu-item' )
			->where( 'term_meta.key', '=', 'menu' )
			->where( 'term_meta.value', '=', $this->id )
			->groupBy( 'terms.id' )
			->get();

		foreach( $terms as $term ) :

			$menu_item = MenuItem::get_menu_item( $term->id, $lang );

			$menu_items[ $menu_item->order ] = $menu_item;

		endforeach;

		return isset( $menu_items ) ? $menu_items : [];
	}

	public static function get_menu( $identifier, $lang = null ) {
		$menu = new Menu( App::get_term( $identifier )->id );

		return $lang == null ?
			( object ) [
				'id' => $menu->id,
				'title' => $menu->title,
				'position' => $menu->position
			] :
			( object ) [
				'id' => $menu->id,
				'title' => $menu->title->{ $lang },
				'position' => $menu->position
			];
	}

	public static function get_menus( $per_page = 15, $page = 1 ) {
		$terms = DB::table( 'terms' )
			->select( 'terms.id' )
			->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
			->where( 'terms.taxonomy', '=', 'menu' )
			->groupBy( 'terms.id' )
			->orderBy( 'terms.slug', 'asc' )
			->offset( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $terms as $term )
			$menus[] = $term->id;

		return isset( $menus ) ? $menus : [];
	}

	public static function add_menu( $params ) {
		$args = [];

		$args[ 'taxonomy' ] = 'menu';

		isset( $params[ 'title' ] ) && isset( $params[ 'title' ][ 'en' ] ) ? ( $args[ 'slug' ] = str_slug( $params[ 'title' ][ 'en' ], '-' ) ) : null;
		!isset( $args[ 'slug' ] ) ? ( $args[ 'slug' ] = time() . rand( 100, 999 ) ) : null;

		$counter = 1;

		while( App::get_term( $args[ 'slug' ] ) != null ) :

			$counter++;

			$segments = explode( '-', $args[ 'slug' ] );
			$suffix = array_pop( $segments );

			$args[ 'slug' ] = $counter > 2 ? str_replace( '-' . $suffix, '', $args[ 'slug' ] ) : $args[ 'slug' ];
			$args[ 'slug' ] = $args[ 'slug' ] . '-' . $counter;

		endwhile;

		$term = App::add_term( $args );

		if ( isset( $params[ 'title' ] ) && is_array( $params[ 'title' ] ) )

			foreach( $params[ 'title' ] as $language => $value )
				App::add_term_meta( $term->id, 'title_' . $language, $value );

		isset( $params[ 'position' ] ) ? App::add_term_meta( $term->id, 'position', $params[ 'position' ] ) : null;

		return $term->id;
	}

	public static function get_all_menus() {
		$terms = DB::table( 'terms' )
			->select( 'id' )
			->where( 'taxonomy', '=', 'menu' )
			->groupBy( 'id' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $terms as $term )
			$menus[] = $term->id;

		return isset( $menus ) ? $menus : [];
	}

}