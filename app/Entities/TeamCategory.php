<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;

use App\App;
use App\Models\Term;
use App\Entities\TeamMember;

class TeamCategory {

	public $id;
	public $title;
	public $order;

	function __construct( $identifier = 0 ) {
		$term = App::get_term( $identifier );

		if ( $term !== null ) {
			$this->id = $term->id;
			$this->title = ( object ) [
				'en' => App::get_term_meta( $this->id, 'title_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'title_fr', '' )
			];
			$this->order = ( int ) App::get_term_meta( $this->id, 'order', 0 );
		}
	}

	public function delete() {
		Term::find( $this->id )->delete();
	}

	public function get_members( $lang = null ) {
		$terms = DB::table( 'terms' )
			->select( 'terms.id' )
			->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
			->where( 'terms.taxonomy', '=', 'team-member' )
			->where( 'term_meta.key', '=', 'team_category' )
			->where( 'term_meta.value', '=', $this->id )
			->groupBy( 'terms.id' )
			->get();

		foreach( $terms as $term ) :

			$team_member = TeamMember::get_team_member( $term->id, $lang );

			$team_members[ $team_member->order ] = $team_member;

			ksort( $team_members );

		endforeach;

		return isset( $team_members ) ? $team_members : [];
	}

	public static function get_team_category( $identifier, $lang = null ) {
		$team_category = new TeamCategory( App::get_term( $identifier )->id );

		return $lang == null ?
			( object ) [
				'id' => $team_category->id,
				'title' => $team_category->title,
				'order' => $team_category->order
			] :
			( object ) [
				'id' => $team_category->id,
				'title' => $team_category->title->{ $lang },
				'order' => $team_category->order
			];
	}

	public static function get_team_categories( $per_page = 15, $page = 1 ) {
		$terms = DB::table( 'terms' )
			->select( 'terms.id' )
			->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
			->where( 'terms.taxonomy', '=', 'team-category' )
			->groupBy( 'terms.id' )
			->orderBy( 'terms.slug', 'asc' )
			->offset( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $terms as $term )
			$team_categories[] = $term->id;

		return isset( $team_categories ) ? $team_categories : [];
	}

	public static function add_team_category( $params ) {
		$args = [];

		$args[ 'taxonomy' ] = 'team-category';

		isset( $params[ 'title' ] ) && isset( $params[ 'title' ][ 'en' ] ) ? ( $args[ 'slug' ] = str_slug( $params[ 'title' ][ 'en' ], '-' ) ) : null;
		!isset( $args[ 'slug' ] ) ? ( $args[ 'slug' ] = time() . rand( 100, 999 ) ) : null;

		$counter = 1;

		while( App::get_term( $args[ 'slug' ] ) != null ) :

			$counter++;

			$segments = explode( '-', $args[ 'slug' ] );
			$suffix = array_pop( $segments );

			$args[ 'slug' ] = $counter > 2 ? str_replace( '-' . $suffix, '', $args[ 'slug' ] ) : $args[ 'slug' ];
			$args[ 'slug' ] = $args[ 'slug' ] . '-' . $counter;

		endwhile;

		$term = App::add_term( $args );

		if ( isset( $params[ 'title' ] ) && is_array( $params[ 'title' ] ) )

			foreach( $params[ 'title' ] as $language => $value )
				App::add_term_meta( $term->id, 'title_' . $language, $value );

		isset( $params[ 'order' ] ) ? App::add_term_meta( $term->id, 'order', $params[ 'order' ] ) : null;

		return $term->id;
	}

	public static function get_all_team_categories() {
		$terms = DB::table( 'terms' )
			->select( 'id' )
			->where( 'taxonomy', '=', 'team-category' )
			->groupBy( 'id' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $terms as $term )
			$team_categories[] = $term->id;

		return isset( $team_categories ) ? $team_categories : [];
	}

}