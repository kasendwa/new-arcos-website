<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

use App\App;
use App\Models\Term;

class Newsletter {

	public $id;
	public $slug;
	public $title;
	public $description;
	public $thumbnail;
	public $mailman_list;
	public $permalink;

	function __construct( $identifier = 0 ) {
		$term = App::get_term( $identifier );

		if ( $term !== null ) {
			$this->id = $term->id;
			$this->slug = $term->slug;
			$this->title = ( object ) [
				'en' => App::get_term_meta( $this->id, 'title_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'title_fr', '' )
			];
			$this->description = ( object ) [
				'en' => App::get_term_meta( $this->id, 'description_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'description_fr', '' )
			];
			$this->thumbnail = $this->get_thumbnails();
			$this->mailman_list = App::get_term_meta( $this->id, 'mailman_list', '' );
			$this->permalink = $this->get_permalink();
		}
	}

	private function get_thumbnails() {
		$thumbnails = [];

		$thumbnail_id = ( int ) App::get_term_meta( $this->id, 'image', null );

		if ( $thumbnail_id != 0 ) :

			$attachment = Attachment::get_attachment( $thumbnail_id );

			$path_segments = explode( 'uploads', $attachment->url );

			$path = 'uploads' . end( $path_segments );

			foreach( App::get_image_sizes() as $size => $params ) :

				$filename = str_replace( '.' . File::extension( $path ), '', $path ) . '-' . $params->width . 'x' . $params->height . '.' . File::extension( $path );

				File::exists( $filename ) ? $thumbnails[ str_replace( '-', '_', $size ) ] = url( $filename ) : null;

			endforeach;

		endif;

		return ( object ) $thumbnails;
	}

	private function get_permalink() {
		return url( '/' . App::get_current_language() . '/' . App::get_option( 'taxonomies_newsletter_rewrite_site_base', 'newsletter' ) . '/' . $this->slug );
	}

	public function delete() {
		Term::find( $this->id )->delete();
	}

	public static function get_newsletter( $identifier, $lang = null ) {
		$newsletter = new Newsletter( App::get_term( $identifier )->id );

		return $lang == null ?
			( object ) [
				'id' => $newsletter->id,
				'slug' => $newsletter->slug,
				'title' => $newsletter->title,
				'description' => $newsletter->description,
				'thumbnail' => $newsletter->thumbnail,
				'mailman_list' => $newsletter->mailman_list,
				'permalink' => $newsletter->permalink,
			] :
			( object ) [
				'id' => $newsletter->id,
				'slug' => $newsletter->slug,
				'title' => $newsletter->title->{ $lang },
				'description' => $newsletter->description->{ $lang },
				'thumbnail' => $newsletter->thumbnail,
				'mailman_list' => $newsletter->mailman_list,
				'permalink' => $newsletter->permalink,
			];
	}

	public static function add_newsletter( $params ) {
		$args = [];

		$args[ 'taxonomy' ] = 'newsletter';

		isset( $params[ 'title' ] ) && isset( $params[ 'title' ][ 'en' ] ) ? ( $args[ 'slug' ] = str_slug( $params[ 'title' ][ 'en' ], '-' ) ) : null;
		!isset( $args[ 'slug' ] ) ? ( $args[ 'slug' ] = time() . rand( 100, 999 ) ) : null;

		$counter = 1;

		while( App::get_term( $args[ 'slug' ] ) != null ) :

			$counter++;

			$segments = explode( '-', $args[ 'slug' ] );
			$suffix = array_pop( $segments );

			$args[ 'slug' ] = $counter > 2 ? str_replace( '-' . $suffix, '', $args[ 'slug' ] ) : $args[ 'slug' ];
			$args[ 'slug' ] = $args[ 'slug' ] . '-' . $counter;

		endwhile;

		$term = App::add_term( $args );

		if ( isset( $params[ 'image' ] ) && is_string( $params[ 'image' ] ) )
			$params[ 'image' ] = App::upload_media_from_path( $params[ 'image' ] );

		isset( $params[ 'image' ] ) ? App::add_term_meta( $term->id, 'image', $params[ 'image' ] ) : null;

		isset( $params[ 'mailman_list' ] ) ? App::add_term_meta( $term->id, 'mailman_list', $params[ 'mailman_list' ] ) : null;

		foreach( [ 'title', 'description' ] as $entry )

			if ( isset( $params[ $entry ] ) && is_array( $params[ $entry ] ) )

				foreach( $params[ $entry ] as $language => $value )
					App::add_term_meta( $term->id, $entry . '_' . $language, $value );

		return $term->id;
	}

	public static function get_newsletters( $per_page = 15, $page = 1 ) {
		$terms = DB::table( 'terms' )
			->select( 'terms.id' )
			->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
			->where( 'terms.taxonomy', '=', 'newsletter' )
			->groupBy( 'terms.id' )
			->orderBy( 'terms.slug', 'asc' )
			->offset( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $terms as $term )
			$newsletters[] = $term->id;

		return isset( $newsletters ) ? $newsletters : [];
	}

	public static function get_all_newsletters() {
		$terms = DB::table( 'terms' )
			->select( 'id' )
			->where( 'taxonomy', '=', 'newsletter' )
			->groupBy( 'id' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $terms as $term )
			$newsletters[] = $term->id;

		return isset( $newsletters ) ? $newsletters : [];
	}

}