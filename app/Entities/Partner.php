<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

use App\App;
use App\Models\Term;

class Partner {

	public $id;
	public $name;
	public $logo;
	public $summary;
	public $url;
    public $motto;
    public $partner_category;
	public $order;

	function __construct( $identifier = 0 ) {
		$term = App::get_term( $identifier );

		if ( $term !== null ) {
			$this->id = $term->id;
			$this->name = ( object ) [
				'en' => App::get_term_meta( $this->id, 'name_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'name_fr', '' )
			];
            $this->motto = ( object ) [
                'en' => App::get_term_meta( $this->id, 'motto_en', '' ),
                'fr' => App::get_term_meta( $this->id, 'motto_fr', '' )
            ];
			$this->logo = $this->get_thumbnails();
			$this->summary = ( object ) [
				'en' => App::get_term_meta( $this->id, 'summary_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'summary_fr', '' )
			];
			$this->url = App::get_term_meta( $this->id, 'url', '#' );
            $this->partner_category = App::get_term_meta( $this->id, 'category', '' );
			$this->order = App::get_term_meta( $this->id, 'order', 0 );
		}
	}

	private function get_thumbnails() {
		$thumbnails = [];

		$thumbnail_id = ( int ) App::get_term_meta( $this->id, 'logo', null );

		if ( $thumbnail_id != 0 ) :

			$attachment = Attachment::get_attachment( $thumbnail_id );

			$path_segments = explode( 'uploads', $attachment->url );

			$path = 'uploads' . end( $path_segments );

			foreach( App::get_image_sizes() as $size => $params ) :

				$filename = str_replace( '.' . File::extension( $path ), '', $path ) . '-' . $params->width . 'x' . $params->height . '.' . File::extension( $path );

				File::exists( $filename ) ? $thumbnails[ str_replace( '-', '_', $size ) ] = url( $filename ) : null;

			endforeach;

			$thumbnails[ 'full' ] = url( $path );

		endif;

		return ( object ) $thumbnails;
	}

	public static function get_partner( $identifier, $lang = null ) {
		$partner = new Partner( App::get_term( $identifier )->id );

		return $lang == null ?
			( object ) [
				'id' => $partner->id,
				'name' => $partner->name,
				'logo' => $partner->logo,
				'summary' => $partner->summary,
				'url' => $partner->url,
                'motto' => $partner->motto,
                'partner_category' => $partner->partner_category,
				'order' => $partner->order
			] :
			( object ) [
				'id' => $partner->id,
				'name' => $partner->name->{ $lang },
				'logo' => $partner->logo,
				'summary' => $partner->summary->{ $lang },
				'url' => $partner->url,
                'motto' => $partner->motto->{ $lang },
                'partner_category' => $partner->partner_category,
				'order' => $partner->order
			];
	}
	
	public function delete() {
		Term::find( $this->id )->delete();
	}

	public static function add_partner( $params ) {
		$args = [];

		$args[ 'taxonomy' ] = 'partner';

		isset( $params[ 'name' ] ) && isset( $params[ 'name' ][ 'en' ] ) ? ( $args[ 'slug' ] = str_slug( $params[ 'name' ][ 'en' ], '-' ) ) : null;
		!isset( $args[ 'slug' ] ) ? ( $args[ 'slug' ] = time() . rand( 100, 999 ) ) : null;

		$counter = 1;

		while( App::get_term( $args[ 'slug' ] ) != null ) :

			$counter++;

			$segments = explode( '-', $args[ 'slug' ] );
			$suffix = array_pop( $segments );

			$args[ 'slug' ] = $counter > 2 ? str_replace( '-' . $suffix, '', $args[ 'slug' ] ) : $args[ 'slug' ];
			$args[ 'slug' ] = $args[ 'slug' ] . '-' . $counter;

		endwhile;

		$term = App::add_term( $args );

		if ( isset( $params[ 'logo' ] ) && is_string( $params[ 'logo' ] ) )
			$params[ 'logo' ] = App::upload_media_from_path( $params[ 'logo' ] );

		isset( $params[ 'logo' ] ) ? App::add_term_meta( $term->id, 'logo', $params[ 'logo' ] ) : null;

		isset( $params[ 'url' ] ) ? App::add_term_meta( $term->id, 'url', $params[ 'url' ] ) : null;
        isset( $params[ 'category' ] ) ? App::add_term_meta( $term->id, 'category', $params[ 'category' ] ) : null;

		foreach( [ 'name', 'summary', 'motto' ] as $entry )

			if ( isset( $params[ $entry ] ) && is_array( $params[ $entry ] ) )

				foreach( $params[ $entry ] as $language => $value )
					App::add_term_meta( $term->id, $entry . '_' . $language, $value );

		isset( $params[ 'order' ] ) ? App::add_term_meta( $term->id, 'order', $params[ 'order' ] ) : null;

		return $term->id;
	}

	public static function get_partners( $per_page = 15, $page = 1 ) {
		$terms = DB::table( 'terms' )
			->select( 'terms.id' )
			->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
			->where( 'terms.taxonomy', '=', 'partner' )
			->groupBy( 'terms.id' )
			->orderBy( 'terms.slug', 'asc' )
			->offset( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $terms as $term )
			$partners[] = $term->id;

		return isset( $partners ) ? $partners : [];
	}

	public static function get_all_partners() {
		$terms = DB::table( 'terms' )
			->select( 'id' )
			->where( 'taxonomy', '=', 'partner' )
			->groupBy( 'id' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $terms as $term )
            $partners[] = $term->id;

		return isset( $partners ) ? $partners : [];
	}
}