<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

use App\App;
use App\Models\Term;

class Resource {

	public $id;
	public $name;
	public $screenshot;
	public $summary;
	public $url;
    public $resource_category;
	public $order;

	function __construct( $identifier = 0 ) {
		$term = App::get_term( $identifier );

		if ( $term !== null ) {
			$this->id = $term->id;
			$this->name = ( object ) [
				'en' => App::get_term_meta( $this->id, 'name_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'name_fr', '' )
			];
			$this->screenshot = $this->get_thumbnails();
			$this->summary = ( object ) [
				'en' => App::get_term_meta( $this->id, 'summary_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'summary_fr', '' )
			];

            $attachment = new Attachment( App::get_term_meta( $this->id, 'file', 0 ) );

            if ( $attachment->id != null )
                $this->url = $attachment->url;

            $this->resource_category = App::get_term_meta( $this->id, 'category', '' );
			$this->order = App::get_term_meta( $this->id, 'order', 0 );
		}
	}

	private function get_thumbnails() {
		$thumbnails = [];

		$thumbnail_id = ( int ) App::get_term_meta( $this->id, 'screenshot', null );

		if ( $thumbnail_id != 0 ) :

			$attachment = Attachment::get_attachment( $thumbnail_id );

			$path_segments = explode( 'uploads', $attachment->url );

			$path = 'uploads' . end( $path_segments );

			foreach( App::get_image_sizes() as $size => $params ) :

				$filename = str_replace( '.' . File::extension( $path ), '', $path ) . '-' . $params->width . 'x' . $params->height . '.' . File::extension( $path );

				File::exists( $filename ) ? $thumbnails[ str_replace( '-', '_', $size ) ] = url( $filename ) : null;

			endforeach;

		endif;

		return ( object ) $thumbnails;
	}

	public static function get_resource( $identifier, $lang = null ) {
		$resource = new Resource( App::get_term( $identifier )->id );

		return $lang == null ?
			( object ) [
				'id' => $resource->id,
				'name' => $resource->name,
				'screenshot' => $resource->screenshot,
				'summary' => $resource->summary,
				'url' => $resource->url,
                'resource_category' => $resource->resource_category,
				'order' => $resource->order
			] :
			( object ) [
				'id' => $resource->id,
				'name' => $resource->name->{ $lang },
				'screenshot' => $resource->screenshot,
				'summary' => $resource->summary->{ $lang },
				'url' => $resource->url,
                'resource_category' => $resource->resource_category,
				'order' => $resource->order
			];
	}
	
	public function delete() {
		Term::find( $this->id )->delete();
	}

	public static function add_resource( $params ) {
		$args = [];

		$args[ 'taxonomy' ] = 'resource';

		isset( $params[ 'name' ] ) && isset( $params[ 'name' ][ 'en' ] ) ? ( $args[ 'slug' ] = str_slug( $params[ 'name' ][ 'en' ], '-' ) ) : null;
		!isset( $args[ 'slug' ] ) ? ( $args[ 'slug' ] = time() . rand( 100, 999 ) ) : null;

		$counter = 1;

		while( App::get_term( $args[ 'slug' ] ) != null ) :

			$counter++;

			$segments = explode( '-', $args[ 'slug' ] );
			$suffix = array_pop( $segments );

			$args[ 'slug' ] = $counter > 2 ? str_replace( '-' . $suffix, '', $args[ 'slug' ] ) : $args[ 'slug' ];
			$args[ 'slug' ] = $args[ 'slug' ] . '-' . $counter;

		endwhile;

		$term = App::add_term( $args );

		if ( isset( $params[ 'screenshot' ] ) && is_string( $params[ 'screenshot' ] ) )
			$params[ 'screenshot' ] = App::upload_media_from_path( $params[ 'screenshot' ] );

		isset( $params[ 'screenshot' ] ) ? App::add_term_meta( $term->id, 'screenshot', $params[ 'screenshot' ] ) : null;

        if ( isset( $params[ 'file' ] ) && is_string( $params[ 'file' ] ) )
            $params[ 'file' ] = App::upload_file_from_path( $params[ 'file' ] );

        isset( $params[ 'file' ] ) ? App::add_term_meta( $term->id, 'file', $params[ 'file' ] ) : null;

        isset( $params[ 'category' ] ) ? App::add_term_meta( $term->id, 'category', $params[ 'category' ] ) : null;

		foreach( [ 'name', 'summary' ] as $entry )

			if ( isset( $params[ $entry ] ) && is_array( $params[ $entry ] ) )

				foreach( $params[ $entry ] as $language => $value )
					App::add_term_meta( $term->id, $entry . '_' . $language, $value );

		isset( $params[ 'order' ] ) ? App::add_term_meta( $term->id, 'order', $params[ 'order' ] ) : null;

		return $term->id;
	}

	public static function get_resources( $per_page = 15, $page = 1 ) {
		$terms = DB::table( 'terms' )
			->select( 'terms.id' )
			->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
			->where( 'terms.taxonomy', '=', 'resource' )
			->groupBy( 'terms.id' )
			->orderBy( 'terms.slug', 'asc' )
			->offset( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $terms as $term )
			$resources[] = $term->id;

		return isset( $resources ) ? $resources : [];
	}

	public static function get_all_resources() {
		$terms = DB::table( 'terms' )
			->select( 'id' )
			->where( 'taxonomy', '=', 'resource' )
			->groupBy( 'id' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $terms as $term )
            $resources[] = $term->id;

		return isset( $resources ) ? $resources : [];
	}
}