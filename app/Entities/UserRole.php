<?php
/**
 * Created by PhpStorm.
 * User: Kasendwa
 * Date: 1/8/2017
 * Time: 1:33 PM
 */

namespace App\Entities;

use App\App;

class UserRole {

	public $id;
	public $name;

	function __construct( $identifier = 0 ) {
		$role = App::get_user_role( $identifier );

		if ( $role !== null ) {
			$this->id = $role->id;
			$this->name = $role->name;
		}
	}

	public static function get_user_role( $identifier ) {
		$role = new UserRole( App::get_user( $identifier )->id );

		return ( object ) [
			'id' => $role->id,
			'name' => $role->name
		];
	}

}