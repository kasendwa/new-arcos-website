<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;

use App\App;
use App\Models\Term;

class Invoice {

	public $id;
	public $title;
	public $amount;

	function __construct( $identifier = 0 ) {
		$term = App::get_term( $identifier );

		if ( $term !== null ) {
			$this->id = $term->id;
			$this->title = ( object ) [
				'en' => App::get_term_meta( $this->id, 'title_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'title_fr', '' )
			];
			$this->amount = App::get_term_meta( $this->id, 'amount', 0 );
		}
	}

	public function delete() {
		Term::find( $this->id )->delete();
	}

	public static function get_invoice( $identifier, $language = null ) {
		$invoice = new Invoice( App::get_term( $identifier )->id );

		return $language == null ?
			( object ) [
				'id' => $invoice->id,
				'title' => $invoice->title,
				'amount' => $invoice->amount
			] :
			( object ) [
				'id' => $invoice->id,
				'title' => $invoice->title->{ $language },
				'amount' => $invoice->amount
			];
	}

	public static function add_invoice( $params ) {
		$args = [];

		$args[ 'taxonomy' ] = 'invoice';

		isset( $params[ 'title' ] ) && isset( $params[ 'title' ][ 'en' ] ) ? ( $args[ 'slug' ] = str_slug( $params[ 'title' ][ 'en' ], '-' ) ) : null;
		!isset( $args[ 'slug' ] ) ? ( $args[ 'slug' ] = time() . rand( 100, 999 ) ) : null;

		$counter = 1;

		while( App::get_term( $args[ 'slug' ] ) != null ) :

			$counter++;

			$segments = explode( '-', $args[ 'slug' ] );
			$suffix = array_pop( $segments );

			$args[ 'slug' ] = $counter > 2 ? str_replace( '-' . $suffix, '', $args[ 'slug' ] ) : $args[ 'slug' ];
			$args[ 'slug' ] = $args[ 'slug' ] . '-' . $counter;

		endwhile;

		$term = App::add_term( $args );

		if ( isset( $params[ 'title' ] ) )

			foreach( $params[ 'title' ] as $language => $value )
				App::add_term_meta( $term->id, 'title_' . $language, $value );

		isset( $params[ 'amount' ] ) ? App::add_term_meta( $term->id, 'amount', $params[ 'amount' ] ) : null;

		return $term->id;
	}

	public static function get_invoices( $per_page = 15, $page = 1 ) {
		$terms = DB::table( 'terms' )
			->select( 'terms.id' )
			->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
			->where( 'terms.taxonomy', '=', 'invoice' )
			->groupBy( 'terms.id' )
			->orderBy( 'terms.slug', 'asc' )
			->offset( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $terms as $term )
			$invoices[] = $term->id;

		return isset( $invoices ) ? $invoices : [];
	}

	public static function get_all_invoices() {
		$terms = DB::table( 'terms' )
			->select( 'id' )
			->where( 'taxonomy', '=', 'invoice' )
			->groupBy( 'id' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $terms as $term )
			$invoices[] = $term->id;

		return isset( $invoices ) ? $invoices : [];
	}

}