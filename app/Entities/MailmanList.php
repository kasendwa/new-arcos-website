<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;

use GuzzleHttp;
use Medoo\Medoo;

use App\App;

class MailmanList {

	public static function subscribe( $user_data ) {
		$db = new Medoo( [
			'database_type' => 'mysql',
			'database_name' => 'ARCOS_Contacts',
			'server' => 'localhost',
			'username' => 'contacts_user',
			'password' => 'contacts@user123!',
			'charset' => 'utf8',
		] );

		$institution = null;

		if ( isset( $user_data[ 'institution' ] ) && !empty( $user_data[ 'institution' ] ) ) :

			$rows = $db->select( 'institutions', [ 'inst_id' ], [
				'OR' => [
					'inst_acronym' => $user_data[ 'institution' ],
					'inst_name' => $user_data[ 'institution' ]
				],
				'LIMIT' => 1
			] );

			$institution = count( $rows ) > 0 ? $rows[ 0 ][ 'inst_id' ] : null;

			if ( is_null( $institution ) ) :

				$db->insert( 'institutions', [
					'inst_name' => $user_data[ 'institution' ]
				] );

				$institution = $db->id();

			endif;

		endif;

		$params = [];

		isset( $user_data[ 'first_name' ] ) && !empty( $user_data[ 'first_name' ] ) ? $params[ 'c_first_name' ] = $user_data[ 'first_name' ] : null;
		isset( $user_data[ 'last_name' ] ) && !empty( $user_data[ 'last_name' ] ) ? $params[ 'c_last_name' ] = $user_data[ 'last_name' ] : null;
		isset( $user_data[ 'email' ] ) && !empty( $user_data[ 'email' ] ) ? $params[ 'c_email' ] = $user_data[ 'email' ] : null;
		isset( $user_data[ 'phone_number' ] ) && !empty( $user_data[ 'phone_number' ] ) ? $params[ 'c_phone' ] = $user_data[ 'phone_number' ] : null;
		!is_null( $institution ) ? $params[ 'inst_id' ] = $institution : null;
		isset( $user_data[ 'job_position' ] ) && !empty( $user_data[ 'job_position' ] ) ? $params[ 'c_position' ] = $user_data[ 'job_position' ] : null;
		$params[ 'regist_date' ] = date( 'Y-m-d' );

		$db->insert( 'contacts', $params );

		if ( isset( $user_data[ 'newsletters' ] ) && is_array( $user_data[ 'newsletters' ] ) ) :

			foreach( $user_data[ 'newsletters' ] as $newsletter ) :

				$client = new GuzzleHttp\Client();

				$response = $client->request( 'POST', App::get_mailman_url() . '/subscribe/' . $newsletter, [
					'form_params' => [
						'email' => $user_data[ 'email' ],
						'name' => $user_data[ 'first_name' ] . ' ' . $user_data[ 'last_name' ],
						'digest' => 0
					]
				] );

				$result = strpos( $response->getBody()->getContents(), 'subscription request has been received' ) > 0 ? true : false;

			endforeach;

		endif;

		return isset( $result ) ? $result : false;
	}

	public static function get_mailman_list( $identifier, $lang = null ) {
		$mailman_list = new MailmanList( App::get_term( $identifier )->id );

		return $lang == null ?
			( object ) [
				'id' => $mailman_list->id,
				'slug' => $mailman_list->slug,
				'name' => $mailman_list->name,
				'title' => $mailman_list->title
			] :
			( object ) [
				'id' => $mailman_list->id,
				'slug' => $mailman_list->slug,
				'name' => $mailman_list->name,
				'title' => $mailman_list->title->{ $lang }
			];
	}

	public static function add_mailman_list( $params ) {
		$args = [];

		$args[ 'taxonomy' ] = 'mailman-list';
		$args[ 'slug' ] = 'mailmanlist-' . ( isset( $params[ 'name' ] ) ? $params[ 'name' ] : '' );

		$term = App::add_term( $args );

		if ( isset( $params[ 'title' ] ) && is_array( $params[ 'title' ] ) )

			foreach( $params[ 'title' ] as $language => $value )
				App::add_term_meta( $term->id, 'title_' . $language, $value );

		return $term->id;
	}

	public static function get_all_mailman_lists() {
		$terms = DB::table( 'terms' )
			->select( 'id' )
			->where( 'taxonomy', '=', 'mailman-list' )
			->groupBy( 'id' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $terms as $term )
			$categories[] = $term->id;

		return isset( $categories ) ? $categories : [];
	}

	public static function fetch_mailman_lists() {
		$client = new GuzzleHttp\Client();

		$response = $client->request( 'GET', 'http://lists.arcosnetwork.org/cgi-bin/mailman/admin' )->getBody()->getContents();

		$first_string = '<td><strong><font size="+2">Description</font></strong></td>';

		$table = substr( $response, ( strpos( $response, $first_string ) +  strlen( $first_string ) + 8 ) );
		$table = substr( $table, 0, strpos( $table, '</table>' ) );

		$mailing_lists = [];

		foreach( explode( '</tr>', $table ) as $tr ) :

			$tds = explode( '<td>', $tr );

			if ( isset( $tds[ 2 ] ) ) :

				$label = str_replace( '[no description available]', '', strip_tags( $tds[ 2 ] ) );

				$label_chunks = explode( '/', $label );

				if ( strlen( trim( $label_chunks[ 0 ] ) ) > 0 ) :

					$labels = ( object ) [
						'en' => isset( $label_chunks[ 0 ] ) ? trim( $label_chunks[ 0 ] ) : '',
						'fr' => isset( $label_chunks[ 1 ] ) ? trim( $label_chunks[ 1 ] ) : ''
					];

					$mailing_lists[ trim( strtolower( strip_tags( $tds[ 1 ] ) ) ) ] = $labels;

					unset( $labels );

				endif;

			endif;

		endforeach;

		return $mailing_lists;
	}

}