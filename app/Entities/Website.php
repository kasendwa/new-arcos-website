<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

use App\App;
use App\Models\Term;

class Website {

	public $id;
	public $name;
	public $screenshot;
	public $summary;
	public $url;
	public $order;

	function __construct( $identifier = 0 ) {
		$term = App::get_term( $identifier );

		if ( $term !== null ) {
			$this->id = $term->id;
			$this->name = ( object ) [
				'en' => App::get_term_meta( $this->id, 'name_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'name_fr', '' )
			];
			$this->screenshot = $this->get_thumbnails();
			$this->summary = ( object ) [
				'en' => App::get_term_meta( $this->id, 'summary_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'summary_fr', '' )
			];
			$this->url = App::get_term_meta( $this->id, 'url', '#' );
			$this->order = App::get_term_meta( $this->id, 'order', 0 );
		}
	}

	private function get_thumbnails() {
		$thumbnails = [];

		$thumbnail_id = ( int ) App::get_term_meta( $this->id, 'screenshot', null );

		if ( $thumbnail_id != 0 ) :

			$attachment = Attachment::get_attachment( $thumbnail_id );

			$path_segments = explode( 'uploads', $attachment->url );

			$path = 'uploads' . end( $path_segments );

			foreach( App::get_image_sizes() as $size => $params ) :

				$filename = str_replace( '.' . File::extension( $path ), '', $path ) . '-' . $params->width . 'x' . $params->height . '.' . File::extension( $path );

				File::exists( $filename ) ? $thumbnails[ str_replace( '-', '_', $size ) ] = url( $filename ) : null;

			endforeach;

		endif;

		return ( object ) $thumbnails;
	}

	public static function get_website( $identifier, $lang = null ) {
		$website = new Website( App::get_term( $identifier )->id );

		return $lang == null ?
			( object ) [
				'id' => $website->id,
				'name' => $website->name,
				'screenshot' => $website->screenshot,
				'summary' => $website->summary,
				'url' => $website->url,
				'order' => $website->order
			] :
			( object ) [
				'id' => $website->id,
				'name' => $website->name->{ $lang },
				'screenshot' => $website->screenshot,
				'summary' => $website->summary->{ $lang },
				'url' => $website->url,
				'order' => $website->order
			];
	}
	
	public function delete() {
		Term::find( $this->id )->delete();
	}

	public static function add_website( $params ) {
		$args = [];

		$args[ 'taxonomy' ] = 'website';

		isset( $params[ 'name' ] ) && isset( $params[ 'name' ][ 'en' ] ) ? ( $args[ 'slug' ] = str_slug( $params[ 'name' ][ 'en' ], '-' ) ) : null;
		!isset( $args[ 'slug' ] ) ? ( $args[ 'slug' ] = time() . rand( 100, 999 ) ) : null;

		$counter = 1;

		while( App::get_term( $args[ 'slug' ] ) != null ) :

			$counter++;

			$segments = explode( '-', $args[ 'slug' ] );
			$suffix = array_pop( $segments );

			$args[ 'slug' ] = $counter > 2 ? str_replace( '-' . $suffix, '', $args[ 'slug' ] ) : $args[ 'slug' ];
			$args[ 'slug' ] = $args[ 'slug' ] . '-' . $counter;

		endwhile;

		$term = App::add_term( $args );

		if ( isset( $params[ 'screenshot' ] ) && is_string( $params[ 'screenshot' ] ) )
			$params[ 'screenshot' ] = App::upload_media_from_path( $params[ 'screenshot' ] );

		isset( $params[ 'screenshot' ] ) ? App::add_term_meta( $term->id, 'screenshot', $params[ 'screenshot' ] ) : null;

		isset( $params[ 'url' ] ) ? App::add_term_meta( $term->id, 'url', $params[ 'url' ] ) : null;

		foreach( [ 'name', 'summary' ] as $entry )

			if ( isset( $params[ $entry ] ) && is_array( $params[ $entry ] ) )

				foreach( $params[ $entry ] as $language => $value )
					App::add_term_meta( $term->id, $entry . '_' . $language, $value );

		isset( $params[ 'order' ] ) ? App::add_term_meta( $term->id, 'order', $params[ 'order' ] ) : null;

		return $term->id;
	}

	public static function get_websites( $per_page = 15, $page = 1 ) {
		$terms = DB::table( 'terms' )
			->select( 'terms.id' )
			->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
			->where( 'terms.taxonomy', '=', 'website' )
			->groupBy( 'terms.id' )
			->orderBy( 'terms.slug', 'asc' )
			->offset( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $terms as $term )
			$websites[] = $term->id;

		return isset( $websites ) ? $websites : [];
	}

	public static function get_all_websites() {
		$terms = DB::table( 'terms' )
			->select( 'id' )
			->where( 'taxonomy', '=', 'website' )
			->groupBy( 'id' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $terms as $term )
			$websites[] = $term->id;

		return isset( $websites ) ? $websites : [];
	}
}