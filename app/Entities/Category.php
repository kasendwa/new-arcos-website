<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;

use App\App;
use App\Models\Term;

class Category {

	public $id;
	public $slug;
	public $title;
	public $description;
	public $permalink;

	function __construct( $identifier = 0 ) {
		$term = App::get_term( $identifier );

		if ( $term !== null ) {
			$this->id = $term->id;
			$this->slug = $term->slug;
			$this->title = ( object ) [
				'en' => App::get_term_meta( $this->id, 'title_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'title_fr', '' )
			];
			$this->description = ( object ) [
				'en' => App::get_term_meta( $this->id, 'description_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'description_fr', '' )
			];
			$this->permalink = $this->get_permalink();
		}
	}

	private function get_permalink() {
		return url( '/' . App::get_current_language() . '/' . App::get_option( 'taxonomies_category_rewrite_site_base', 'category' ) . '/' . $this->slug );
	}

	public function delete() {
		Term::find( $this->id )->delete();
	}

	public function get_featured_articles( $number = 6, $lang = null ) {
		$posts = DB::table( 'posts' )
			->select( 'posts.id' )
			->leftJoin( 'post_meta', 'posts.id', '=', 'post_meta.post_id' )
			->where( 'posts.type', '=', 'article' )
			->where( 'post_meta.key', '=', 'categories' )
			->where( function( $query ) {
				$query->where( 'post_meta.value', 'LIKE', '%[' . $this->id . ',%' )
					->orWhere( 'post_meta.value', 'LIKE', '%,' . $this->id . ',%' )
					->orWhere( 'post_meta.value', 'LIKE', '%,' . $this->id . ']%' )
					->orWhere( 'post_meta.value', 'LIKE', '%[' . $this->id . ']%' );
			} )
			->groupBy( 'posts.id' )
			->orderBy( 'posts.created_at', 'desc' )
			->limit( $number )
			->get();

		foreach( $posts as $post )
			$articles[] = Article::get_article( $post->id, $lang );

		return isset( $articles ) ? $articles : [];
	}

	public function get_articles( $per_page = 15, $page = 1 ) {
		$posts = DB::table( 'posts' )
			->select( 'posts.id' )
			->leftJoin( 'post_meta', 'posts.id', '=', 'post_meta.post_id' )
			->where( 'posts.type', '=', 'article' )
			->where( 'post_meta.key', '=', 'categories' )
			->where( function( $query ) {
				$query->where( 'post_meta.value', 'LIKE', '%[' . $this->id . ',%' )
					->orWhere( 'post_meta.value', 'LIKE', '%,' . $this->id . ',%' )
					->orWhere( 'post_meta.value', 'LIKE', '%,' . $this->id . ']%' )
					->orWhere( 'post_meta.value', 'LIKE', '%[' . $this->id . ']%' );
			} )
			->groupBy( 'posts.id' )
			->orderBy( 'posts.created_at', 'desc' )
			->offset( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $posts as $post )
			$articles[] = $post->id;

		return isset( $articles ) ? $articles : [];
	}

	public function get_all_articles() {
		$posts = DB::table( 'posts' )
			->select( 'posts.id' )
			->leftJoin( 'post_meta', 'posts.id', '=', 'post_meta.post_id' )
			->where( 'posts.type', '=', 'article' )
			->where( 'post_meta.key', '=', 'categories' )
			->where( function( $query ) {
				$query->where( 'post_meta.value', 'LIKE', '%[' . $this->id . ',%' )
					->orWhere( 'post_meta.value', 'LIKE', '%,' . $this->id . ',%' )
					->orWhere( 'post_meta.value', 'LIKE', '%,' . $this->id . ']%' )
					->orWhere( 'post_meta.value', 'LIKE', '%[' . $this->id . ']%' );
			} )
			->groupBy( 'posts.id' )
			->orderBy( 'posts.created_at', 'desc' )
			->get();

		foreach( $posts as $post )
			$articles[] = $post->id;

		return is_array( $articles ) ? $articles : [];
	}

	public static function get_category( $identifier, $lang = null ) {
		$category = new Category( App::get_term( $identifier )->id );

		return $lang == null ?
			( object ) [
				'id' => $category->id,
				'slug' => $category->slug,
				'title' => $category->title,
				'description' => $category->description,
				'permalink' => $category->permalink,
				'featured_articles' => $category->get_featured_articles( 6 ),
			] :
			( object ) [
				'id' => $category->id,
				'slug' => $category->slug,
				'title' => $category->title->{ $lang },
				'description' => $category->description->{ $lang },
				'permalink' => $category->permalink,
				'featured_articles' => $category->get_featured_articles( 6, $lang )
			];
	}

	public static function add_category( $params ) {
		$args = [];

		$args[ 'taxonomy' ] = 'category';

		isset( $params[ 'title' ] ) && isset( $params[ 'title' ][ 'en' ] ) ? ( $args[ 'slug' ] = str_slug( $params[ 'title' ][ 'en' ], '-' ) ) : null;
		!isset( $args[ 'slug' ] ) ? ( $args[ 'slug' ] = time() . rand( 100, 999 ) ) : null;

		$counter = 1;

		while( App::get_term( $args[ 'slug' ] ) != null ) :

			$counter++;

			$segments = explode( '-', $args[ 'slug' ] );
			$suffix = array_pop( $segments );

			$args[ 'slug' ] = $counter > 2 ? str_replace( '-' . $suffix, '', $args[ 'slug' ] ) : $args[ 'slug' ];
			$args[ 'slug' ] = $args[ 'slug' ] . '-' . $counter;

		endwhile;

		$term = App::add_term( $args );

		foreach( [ 'title', 'description' ] as $entry )

			if ( isset( $params[ $entry ] ) && is_array( $params[ $entry ] ) )

				foreach( $params[ $entry ] as $language => $value )
					App::add_term_meta( $term->id, $entry . '_' . $language, $value );

		return $term->id;
	}

	public static function get_categories( $per_page = 15, $page = 1 ) {
		$terms = DB::table( 'terms' )
			->select( 'terms.id' )
			->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
			->where( 'terms.taxonomy', '=', 'category' )
			->groupBy( 'terms.id' )
			->orderBy( 'terms.slug', 'asc' )
			->offset( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $terms as $term )
			$categories[] = $term->id;

		return isset( $categories ) ? $categories : [];
	}

	public static function get_all_categories() {
		$terms = DB::table( 'terms' )
			->select( 'id' )
			->where( 'taxonomy', '=', 'category' )
			->groupBy( 'id' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $terms as $term )
			$categories[] = $term->id;

		return isset( $categories ) ? $categories : [];
	}

}