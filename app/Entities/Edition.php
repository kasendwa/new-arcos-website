<?php

namespace App\Entities;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Illuminate\Support\Facades\DB;

use App\App;
use App\Models\Term;

class Edition {

	public $id;
	public $title;
	public $description;
	public $newsletter;
	public $file;
	public $status;

	function __construct( $identifier = 0 ) {
		$term = App::get_term( $identifier );

		if ( $term !== null ) {
			$this->id = $term->id;
			$this->title = ( object ) [
				'en' => App::get_term_meta( $this->id, 'title_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'title_fr', '' )
			];
			$this->description = ( object ) [
				'en' => App::get_term_meta( $this->id, 'description_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'description_fr', '' )
			];
			$this->newsletter = App::get_term_meta( $this->id, 'newsletter', '' );
			$this->file = $this->get_file();
			$this->status = App::get_term_meta( $this->id, 'status', '' );
		}
	}

	private function get_file() {
		$file_id = ( int ) App::get_term_meta( $this->id, 'file', null );

		if ( $file_id != 0 ) :

			$attachment = Attachment::get_attachment( $file_id );

			return $attachment->url;

		endif;

		return '';
	}

	public function send() {
	    $newsletter = Newsletter::get_newsletter( $this->newsletter );
		$edition = $this;
		$file = $edition->file;

		$mail = new PHPMailer(true);

		try {
			$mail->isSMTP();
			$mail->Host = 'smtp.gmail.com';
			$mail->SMTPAuth = true;
			$mail->Username = 'arcosnetwork@gmail.com';
			$mail->Password = 'arcos4nature';
			$mail->SMTPSecure = 'tls';
			$mail->Port = 587;

			$mail->setFrom('arcosnetwork@gmail.com', 'ARCOS Network');
			$mail->addAddress($newsletter->mailman_list . '@lists.arcosnetwork.org');

			$mail->addAttachment('uploads/' . date( 'Y/m/' ) . basename($file));

			$mail->isHTML(true);
			$mail->Subject = $edition->title->en;
			$mail->Body = $edition->description->en;
			$mail->AltBody = $edition->description->en;

			if ( !$mail->send() ) {
				var_dump( $mail->ErrorInfo );
			};
		} catch (Exception $e) {
			var_dump( $e->message );
		}
    }

	public function delete() {
		Term::find( $this->id )->delete();
	}

	public static function get_edition( $identifier, $lang = null ) {
		$edition = new Edition( App::get_term( $identifier )->id );

		return $lang == null ?
			( object ) [
				'id' => $edition->id,
				'title' => $edition->title,
				'description' => $edition->description,
                'newsletter' => $edition->newsletter,
				'file' => $edition->file,
				'status' => $edition->status
			] :
			( object ) [
                'id' => $edition->id,
                'title' => $edition->title->{ $lang },
                'description' => $edition->description->{ $lang },
                'newsletter' => $edition->newsletter,
                'file' => $edition->file,
                'status' => $edition->status
			];
	}

	public static function add_edition( $params ) {
		$args = [];

		$args[ 'taxonomy' ] = 'edition';

		isset( $params[ 'title' ] ) && isset( $params[ 'title' ][ 'en' ] ) ? ( $args[ 'slug' ] = str_slug( $params[ 'title' ][ 'en' ], '-' ) ) : null;
		!isset( $args[ 'slug' ] ) ? ( $args[ 'slug' ] = time() . rand( 100, 999 ) ) : null;

		$counter = 1;

		while( App::get_term( $args[ 'slug' ] ) != null ) :

			$counter++;

			$segments = explode( '-', $args[ 'slug' ] );
			$suffix = array_pop( $segments );

			$args[ 'slug' ] = $counter > 2 ? str_replace( '-' . $suffix, '', $args[ 'slug' ] ) : $args[ 'slug' ];
			$args[ 'slug' ] = $args[ 'slug' ] . '-' . $counter;

		endwhile;

		$term = App::add_term( $args );

		if ( isset( $params[ 'file' ] ) && is_string( $params[ 'file' ] ) )
			$params[ 'file' ] = App::upload_file_from_path( $params[ 'file' ] );

		isset( $params[ 'file' ] ) ? App::add_term_meta( $term->id, 'file', $params[ 'file' ] ) : null;

		isset( $params[ 'newsletter' ] ) ? App::add_term_meta( $term->id, 'newsletter', $params[ 'newsletter' ] ) : null;

        isset( $params[ 'status' ] ) ? App::add_term_meta( $term->id, 'status', $params[ 'status' ] ) : null;

		foreach( [ 'title', 'description' ] as $entry )

			if ( isset( $params[ $entry ] ) && is_array( $params[ $entry ] ) )

				foreach( $params[ $entry ] as $language => $value )
					App::add_term_meta( $term->id, $entry . '_' . $language, $value );

		return $term->id;
	}

	public static function get_editions( $per_page = 15, $page = 1 ) {
		$terms = DB::table( 'terms' )
			->select( 'terms.id' )
			->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
			->where( 'terms.taxonomy', '=', 'edition' )
			->groupBy( 'terms.id' )
			->orderBy( 'terms.slug', 'asc' )
			->offset( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $terms as $term )
			$editions[] = $term->id;

		return isset( $editions ) ? $editions : [];
	}

	public static function get_all_editions() {
		$terms = DB::table( 'terms' )
			->select( 'id' )
			->where( 'taxonomy', '=', 'edition' )
			->groupBy( 'id' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $terms as $term )
            $editions[] = $term->id;

		return isset( $editions ) ? $editions : [];
	}

}