<?php

namespace App\Entities;

use App\App;
use App\Models\Post;

class Attachment {

	public $id;
	public $title;
	public $url;

	function __construct( $identifier = 0 ) {
		$post = App::get_post( $identifier );

		if ( $post !== null ) {
			$this->id = $post->id;
			$this->title = ( object ) [
				'en' => App::get_post_meta( $this->id, 'title_en', '' ),
				'fr' => App::get_post_meta( $this->id, 'title_fr', '' )
			];
			$this->url = App::get_post_meta( $this->id, 'url', '' );
		}
	}

	public static function get_attachment( $identifier, $language = null ) {
		$attachment = new Attachment( App::get_post( $identifier )->id );

		return $language == null ?
			( object ) [
				'id' => $attachment->id,
				'title' => $attachment->title,
				'url' => $attachment->url
			]
			:
			( object ) [
				'id' => $attachment->id,
				'title' => $attachment->title->{ $language },
				'url' => $attachment->url
			];
	}

	public static function add_attachment( $params ) {
		$args = [];

		$args[ 'type' ] = 'attachment';
		$args[ 'status' ] = 'publish';

		isset( $params[ 'title' ] ) && isset( $params[ 'title' ][ 'en' ] ) ? ( $args[ 'name' ] = str_slug( $params[ 'title' ][ 'en' ], '-' ) ) : null;
		!isset( $args[ 'name' ] ) ? ( $args[ 'name' ] = time() . rand( 100, 999 ) ) : null;

		$post = App::add_post( $args );

		foreach( [ 'title', 'description' ] as $entry )

			if ( isset( $params[ $entry ] ) && is_array( $params[ $entry ] ) )

				foreach( $params[ $entry ] as $language => $value )
					App::add_post_meta( $post->id, $entry . '_' . $language, $value );

		isset( $params[ 'url' ] ) ? App::add_post_meta( $post->id, 'url', $params[ 'url' ] ) : null;

		return $post->id;
	}

	public function delete() {
		$base_path = public_path( str_replace( url(), '', $this->url ) );

		$paths = [ $base_path ];

		foreach( App::get_image_sizes() as $size ) :

			$sections = explode( '.', $base_path );

			$paths[] = str_replace( '.' . end( $sections ), '', $base_path ) . '-' . $size->width . 'x' . $size->height . '.' . end( $sections );

		endforeach;

		foreach( $paths as $path )

			if ( \Illuminate\Support\Facades\File::exists( $path ) )
				\Illuminate\Support\Facades\File::delete( $path );

		Post::find( $this->id )->delete();
	}


}