<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;

use App\App;
use App\Models\Term;

class File {

	public $id;
	public $name;
	public $url;

	function __construct( $identifier = 0 ) {
		$term = App::get_term( $identifier );

		if ( $term !== null ) {
			$this->id = $term->id;
			$this->name = ( object ) [
				'en' => App::get_term_meta( $this->id, 'name_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'name_fr', '' )
			];

			$attachment = new Attachment( App::get_term_meta( $this->id, 'file', 0 ) );

			if ( $attachment->id != null )
			    $this->url = $attachment->url;
		}
	}

	public static function get_file( $identifier, $lang = null ) {
		$file = new File( App::get_term( $identifier )->id );

		return $lang == null ?
			( object ) [
				'id' => $file->id,
				'name' => $file->name,
				'url' => $file->url
			] :
			( object ) [
				'id' => $file->id,
				'name' => $file->name->{ $lang },
				'url' => $file->url
			];
	}
	
	public function delete() {
		Term::find( $this->id )->delete();
	}

	public static function add_file( $params ) {
		$args = [];

		$args[ 'taxonomy' ] = 'file';

		isset( $params[ 'name' ] ) && isset( $params[ 'name' ][ 'en' ] ) ? ( $args[ 'slug' ] = str_slug( $params[ 'name' ][ 'en' ], '-' ) ) : null;
		!isset( $args[ 'slug' ] ) ? ( $args[ 'slug' ] = time() . rand( 100, 999 ) ) : null;

		$counter = 1;

		while( App::get_term( $args[ 'slug' ] ) != null ) :

			$counter++;

			$segments = explode( '-', $args[ 'slug' ] );
			$suffix = array_pop( $segments );

			$args[ 'slug' ] = $counter > 2 ? str_replace( '-' . $suffix, '', $args[ 'slug' ] ) : $args[ 'slug' ];
			$args[ 'slug' ] = $args[ 'slug' ] . '-' . $counter;

		endwhile;

		$term = App::add_term( $args );

		if ( isset( $params[ 'file' ] ) && is_string( $params[ 'file' ] ) )
			$params[ 'file' ] = App::upload_file_from_path( $params[ 'file' ] );

		isset( $params[ 'file' ] ) ? App::add_term_meta( $term->id, 'file', $params[ 'file' ] ) : null;

		foreach( [ 'name' ] as $entry )

			if ( isset( $params[ $entry ] ) && is_array( $params[ $entry ] ) )

				foreach( $params[ $entry ] as $language => $value )
					App::add_term_meta( $term->id, $entry . '_' . $language, $value );

		return $term->id;
	}

	public static function get_files( $per_page = 15, $page = 1 ) {
		$terms = DB::table( 'terms' )
			->select( 'terms.id' )
			->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
			->where( 'terms.taxonomy', '=', 'file' )
			->groupBy( 'terms.id' )
			->orderBy( 'terms.slug', 'asc' )
			->offset( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $terms as $term )
			$files[] = $term->id;

		return isset( $files ) ? $files : [];
	}

	public static function get_all_files() {
		$terms = DB::table( 'terms' )
			->select( 'id' )
			->where( 'taxonomy', '=', 'file' )
			->groupBy( 'id' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $terms as $term )
			$files[] = $term->id;

		return isset( $files ) ? $files : [];
	}
}