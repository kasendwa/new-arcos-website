<?php

namespace App\Entities;

use App\Models\Term;
use Illuminate\Support\Facades\DB;

use App\App;

class Translation {

	public $id;
	public $slug;
	public $title;

	function __construct( $identifier = 0 ) {
		$term = App::get_term( $identifier );

		if ( $term !== null ) {
			$this->id = $term->id;
			$this->slug = $term->slug;
			$this->title = ( object ) [
				'en' => App::get_term_meta( $this->id, 'title_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'title_fr', '' )
			];
		}
	}

	public function delete() {
		Term::find( $this->id )->delete();
	}

	public static function get_translation( $identifier, $language = null ) {
		$translation = new Translation( App::get_term( $identifier )->id );

		return $language == null ?
			( object ) [
				'id' => $translation->id,
				'slug' => $translation->slug,
				'title' => $translation->title
			] : $translation->title->{ $language };
	}

	public static function add_translation( $params ) {
		$args = [];

		$args[ 'taxonomy' ] = 'translation';

		isset( $params[ 'title' ] ) && isset( $params[ 'title' ][ 'en' ] ) ? ( $args[ 'slug' ] = str_slug( $params[ 'title' ][ 'en' ], '-' ) ) : null;
		!isset( $args[ 'slug' ] ) ? ( $args[ 'slug' ] = time() . rand( 100, 999 ) ) : null;

		$term_exists = App::get_term_by( 'slug', $args[ 'slug' ] );

		if ( $term_exists == null ) :

			$term = App::add_term( $args );

			if ( isset( $params[ 'title' ] ) && is_array( $params[ 'title' ] ) ) :

				foreach( $params[ 'title' ] as $language => $value )
					App::add_term_meta( $term->id, 'title_' . $language, $value );

			endif;

			return $term->id;

		else :

			foreach( $params[ 'title' ] as $language => $value )
				App::update_term_meta( $term_exists->id, 'title_' . $language, $value );

			return $term_exists->id;

		endif;
	}

	public static function get_translations( $per_page = 15, $page = 1 ) {
		$terms = DB::table( 'terms' )
			->select( 'terms.id' )
			->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
			->where( 'terms.taxonomy', '=', 'translation' )
			->groupBy( 'terms.id' )
			->orderBy( 'terms.slug', 'asc' )
			->offset( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $terms as $term )
			$translations[] = $term->id;

		return isset( $translations ) ? $translations : [];
	}

	public static function get_all_translations() {
		$terms = DB::table( 'terms' )
			->select( 'id' )
			->where( 'taxonomy', '=', 'translation' )
			->groupBy( 'id' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $terms as $term )
			$translations[] = $term->id;

		return isset( $translations ) ? $translations : [];
	}

}