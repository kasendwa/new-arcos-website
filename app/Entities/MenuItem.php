<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;

use App\App;
use App\Models\Term;

class MenuItem {

	public $id;
	public $title;
	public $menu;
	public $anchor;
	public $order;

	function __construct( $identifier = 0 ) {
		$term = App::get_term( $identifier );

		if ( $term !== null ) {
			$this->id = $term->id;
			$this->slug = $term->slug;
			$this->title = ( object ) [
				'en' => App::get_term_meta( $this->id, 'title_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'title_fr', '' )
			];
			$this->menu = ( int ) App::get_term_meta( $this->id, 'menu', '' );
			$this->anchor = $this->generate_link();
			$this->order = App::get_term_meta( $this->id, 'order', 0 );
		}
	}

	private function generate_link() {
		$entity_type = App::get_term_meta( $this->id, 'entity_type', '' );
		$entity_name = App::get_term_meta( $this->id, 'entity_name', '' );
		$entity_id = App::get_term_meta( $this->id, 'entity_id', '' );
		$custom_link = App::get_term_meta( $this->id, 'custom_link', '' );

		if ( $custom_link != null && !empty( $custom_link ) ) :

			return $custom_link;

		elseif ( ( $entity_type != null && !empty( $entity_type ) ) && ( $entity_name != null && !empty( $entity_name ) ) ) :

			if ( $entity_type == 'post_type' ) :

				$post_type = App::get_post_type( $entity_name );

				if ( ( $entity_id == null || empty( $entity_id ) ) && $post_type->has_archive ) :

					$slug = $post_type->rewrite->site->archive;

				elseif ( $entity_id != null && !empty( $entity_id ) && $post_type->has_archive ) :

					$post = App::get_post( $entity_id );

					$post != null ? ( $slug = $entity_id . '/' . $post_type->rewrite->site->base . '/' . $post->name ) : null;

				elseif ( $entity_id != null && !empty( $entity_id ) && !$post_type->has_archive && $entity_name == 'page' ) :

					$page = new Page( ( int ) $entity_id );

					return $page->permalink;

				endif;

			endif;

			return url( '/' . App::get_current_language() . ( isset( $slug ) ? '/' . $slug : '' ) );

		else :

			return url( '/' . App::get_current_language() );

		endif;
	}

	public function delete() {
		Term::find( $this->id )->delete();
	}

	public static function get_menu_item( $identifier, $language = null ) {
		$menu_item = new MenuItem( App::get_term( $identifier )->id );

		return $language == null ?
			( object ) [
				'id' => $menu_item->id,
				'title' => $menu_item->title,
				'menu' => $menu_item->menu,
				'anchor' => $menu_item->anchor,
				'order' => $menu_item->order
			] :
			( object ) [
				'id' => $menu_item->id,
				'title' => $menu_item->title->{ $language },
				'menu' => $menu_item->menu,
				'anchor' => $menu_item->anchor,
				'order' => $menu_item->order
			];
	}

	public static function add_menu_item( $params ) {
		$args = [];

		$args[ 'taxonomy' ] = 'menu-item';

		isset( $params[ 'title' ] ) && isset( $params[ 'title' ][ 'en' ] ) ? ( $args[ 'slug' ] = str_slug( $params[ 'title' ][ 'en' ], '-' ) ) : null;
		!isset( $args[ 'slug' ] ) ? ( $args[ 'slug' ] = time() . rand( 100, 999 ) ) : null;

		$counter = 1;

		while( App::get_term( $args[ 'slug' ] ) != null ) :

			$counter++;

			$segments = explode( '-', $args[ 'slug' ] );
			$suffix = array_pop( $segments );

			$args[ 'slug' ] = $counter > 2 ? str_replace( '-' . $suffix, '', $args[ 'slug' ] ) : $args[ 'slug' ];
			$args[ 'slug' ] = $args[ 'slug' ] . '-' . $counter;

		endwhile;

		$term = App::add_term( $args );

		isset( $params[ 'custom_link' ] ) ? App::add_term_meta( $term->id, 'custom_link', $params[ 'custom_link' ] ) : null;
		isset( $params[ 'menu' ] ) ? App::add_term_meta( $term->id, 'menu', $params[ 'menu' ] ) : null;

		if ( isset( $params[ 'title' ] ) )

			foreach( $params[ 'title' ] as $language => $value )
				App::add_term_meta( $term->id, 'title_' . $language, $value );

		foreach( [ 'type', 'name', 'id' ] as $entry )

			if ( isset( $params[ 'anchor' ] ) && isset( $params[ 'anchor' ][ 'entity' ] ) && isset( $params[ 'anchor' ][ 'entity' ][ $entry ] ) && !empty( $params[ 'anchor' ][ 'entity' ][ $entry ] ) )
				App::add_term_meta( $term->id, 'entity_' . $entry, $params[ 'anchor' ][ 'entity' ][ $entry ] );

		isset( $params[ 'order' ] ) ? App::add_term_meta( $term->id, 'order', $params[ 'order' ] ) : null;

		return $term->id;
	}

	public static function get_menu_items( $per_page = 15, $page = 1 ) {
		$terms = DB::table( 'terms' )
			->select( 'terms.id' )
			->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
			->where( 'terms.taxonomy', '=', 'menu-item' )
			->groupBy( 'terms.id' )
			->orderBy( 'terms.slug', 'asc' )
			->offset( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $terms as $term )
			$menu_items[] = $term->id;

		return isset( $menu_items ) ? $menu_items : [];
	}

	public static function get_all_menu_items() {
		$terms = DB::table( 'terms' )
			->select( 'id' )
			->where( 'taxonomy', '=', 'menu-item' )
			->groupBy( 'id' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $terms as $term )
			$menu_items[] = $term->id;

		return isset( $menu_items ) ? $menu_items : [];
	}

}