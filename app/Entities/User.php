<?php
/**
 * Created by PhpStorm.
 * User: Kasendwa
 * Date: 1/8/2017
 * Time: 1:33 PM
 */

namespace App\Entities;

use App\App;

class User {

	public $id;
	public $username;
	public $email;
	public $name;
	public $role;

	function __construct( $identifier = 0 ) {
		$user = App::get_user( $identifier );

		if ( $user !== null ) {
			$this->id = $user->id;
			$this->username = $user->username;
			$this->email = $user->email;
			$this->name = ( object ) [
				'first' => App::get_user_meta( $this->id, 'first_name', '' ),
				'middle' => App::get_user_meta( $this->id, 'middle_name', '' ),
				'last' => App::get_user_meta( $this->id, 'last_name', '' )
			];
			$this->role = UserRole::get_user_role( $user->user_role );
		}
	}

	public static function get_user( $identifier ) {
		if ( App::get_user( $identifier ) != null ) :

			$user = new User( App::get_user( $identifier )->id );

			return ( object ) [
				'id' => $user->id,
				'username' => $user->username,
				'email' => $user->email,
				'name' => $user->name,
				'role' => $user->role
			];

		else :

			return null;

		endif;
	}

}