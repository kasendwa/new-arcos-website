<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;

use App\App;
use App\Models\Term;

class Payment {

	public $id;
	public $token;
	public $first_name;
	public $last_name;
	public $email;
	public $invoice;
	public $status;
	public $timestamp;

	function __construct( $identifier = 0 ) {
		$term = App::get_term( $identifier );

		if ( $term !== null ) {
			$this->id = $term->id;
			$this->token = App::get_term_meta( $this->id, 'token', 0 );
			$this->first_name = App::get_term_meta( $this->id, 'first_name', 0 );
			$this->last_name = App::get_term_meta( $this->id, 'last_name', 0 );
			$this->email = App::get_term_meta( $this->id, 'email', 0 );
			$this->invoice = App::get_term_meta( $this->id, 'invoice', 0 );
			$this->status = App::get_term_meta( $this->id, 'status', 0 );
			$this->timestamp = App::get_term_meta( $this->id, 'timestamp', date( 'Y-m-d H:i:s' ) );
		}
	}

	public function delete() {
		Term::find( $this->id )->delete();
	}

	public static function get_payment( $identifier ) {
		$payment = new Payment( App::get_term( $identifier )->id );

		return [
			'id' => $payment->id,
			'token' => $payment->token,
			'first_name' => $payment->first_name,
			'last_name' => $payment->last_name,
			'email' => $payment->email,
			'invoice' => $payment->invoice,
			'status' => $payment->status,
			'timestamp' => $payment->timestamp
		];
	}

	public static function add_payment( $params ) {
		$args = [];

		$args[ 'taxonomy' ] = 'payment';

		!isset( $args[ 'slug' ] ) ? ( $args[ 'slug' ] = time() . rand( 100, 999 ) ) : null;

		$counter = 1;

		while( App::get_term( $args[ 'slug' ] ) != null ) :

			$counter++;

			$segments = explode( '-', $args[ 'slug' ] );
			$suffix = array_pop( $segments );

			$args[ 'slug' ] = $counter > 2 ? str_replace( '-' . $suffix, '', $args[ 'slug' ] ) : $args[ 'slug' ];
			$args[ 'slug' ] = $args[ 'slug' ] . '-' . $counter;

		endwhile;

		$term = App::add_term( $args );

		isset( $params[ 'token' ] ) ? App::add_term_meta( $term->id, 'token', $params[ 'amount' ] ) : null;
		isset( $params[ 'first_name' ] ) ? App::add_term_meta( $term->id, 'first_name', $params[ 'first_name' ] ) : null;
		isset( $params[ 'last_name' ] ) ? App::add_term_meta( $term->id, 'last_name', $params[ 'last_name' ] ) : null;
		isset( $params[ 'email' ] ) ? App::add_term_meta( $term->id, 'email', $params[ 'email' ] ) : null;
		isset( $params[ 'invoice' ] ) ? App::add_term_meta( $term->id, 'invoice', $params[ 'invoice' ] ) : null;
		isset( $params[ 'status' ] ) ? App::add_term_meta( $term->id, 'status', $params[ 'status' ] ) : null;

		App::add_term_meta( $term->id, 'timestamp', date( 'Y-m-d H:i:s' ) );

		return $term->id;
	}

	public static function get_payments( $per_page = 15, $page = 1 ) {
		$terms = DB::table( 'terms' )
			->select( 'terms.id' )
			->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
			->where( 'terms.taxonomy', '=', 'payment' )
			->groupBy( 'terms.id' )
			->orderBy( 'terms.slug', 'asc' )
			->offset( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $terms as $term )
			$payments[] = $term->id;

		return isset( $payments ) ? $payments : [];
	}

	public static function get_all_payments() {
		$terms = DB::table( 'terms' )
			->select( 'id' )
			->where( 'taxonomy', '=', 'payment' )
			->groupBy( 'id' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $terms as $term )
			$payments[] = $term->id;

		return isset( $payments ) ? $payments : [];
	}

}