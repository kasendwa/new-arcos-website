<?php

namespace App\Entities;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

use App\App;

class AboutSection {

	public $id;
	public $title;
	public $content;
	public $thumbnail;
	public $order;

	function __construct( $identifier = 0 ) {
		$term = App::get_term( $identifier );

		if ( $term !== null ) {
			$this->id = $term->id;
			$this->title = ( object ) [
				'en' => App::get_term_meta( $this->id, 'title_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'title_fr', '' )
			];
			$this->content = ( object ) [
				'en' => App::get_term_meta( $this->id, 'content_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'content_fr', '' )
			];
			$this->thumbnail = $this->get_thumbnails();
			$this->order = App::get_term_meta( $this->id, 'order', 0 );
		}
	}

	private function get_thumbnails() {
		$thumbnails = [];

		$thumbnail_id = ( int ) App::get_term_meta( $this->id, 'image', null );

		if ( $thumbnail_id != 0 ) :

			$attachment = Attachment::get_attachment( $thumbnail_id );

			$path_segments = explode( 'uploads', $attachment->url );

			$path = 'uploads' . end( $path_segments );

			foreach( App::get_image_sizes() as $size => $params ) :

				$filename = str_replace( '.' . File::extension( $path ), '', $path ) . '-' . $params->width . 'x' . $params->height . '.' . File::extension( $path );

				File::exists( $filename ) ? $thumbnails[ str_replace( '-', '_', $size ) ] = url( $filename ) : null;

			endforeach;

		endif;

		return ( object ) $thumbnails;
	}

	public static function get_about_sections( $per_page = 15, $page = 1 ) {
		$terms = DB::table( 'terms' )
			->select( 'terms.id' )
			->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
			->where( 'terms.taxonomy', '=', 'about-section' )
			->groupBy( 'terms.id' )
			->orderBy( 'terms.slug', 'asc' )
			->offset( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $terms as $term )
			$menus[] = $term->id;

		return isset( $menus ) ? $menus : [];
	}

	public static function get_about_section( $identifier, $language = null ) {
		$section = new AboutSection( App::get_term( $identifier )->id );

		return $language == null ?
			( object ) [
				'id' => $section->id,
				'title' => $section->title,
				'content' => $section->content,
				'thumbnail' => $section->thumbnail,
				'order' => $section->order
			] :
			( object ) [
				'id' => $section->id,
				'title' => $section->title->{ $language },
				'content' => $section->content->{ $language },
				'thumbnail' => $section->thumbnail,
				'order' => $section->order
			];
	}

	public static function add_section( $params ) {
		$args = [];

		$args[ 'taxonomy' ] = 'about-section';

		isset( $params[ 'title' ] ) && isset( $params[ 'title' ][ 'en' ] ) ? ( $args[ 'slug' ] = str_slug( $params[ 'title' ][ 'en' ], '-' ) ) : null;
		!isset( $args[ 'slug' ] ) ? ( $args[ 'slug' ] = time() . rand( 100, 999 ) ) : null;

		$term = App::add_term( $args );

		if ( isset( $params[ 'image' ] ) && is_string( $params[ 'image' ] ) )
			$params[ 'image' ] = App::upload_media_from_path( $params[ 'image' ] );

		isset( $params[ 'image' ] ) ? App::add_term_meta( $term->id, 'image', $params[ 'image' ] ) : null;

		foreach( [ 'title', 'content' ] as $entry )

			if ( isset( $params[ $entry ] ) && is_array( $params[ $entry ] ) )

				foreach( $params[ $entry ] as $language => $value )
					App::add_term_meta( $term->id, $entry . '_' . $language, $value );

		isset( $params[ 'order' ] ) ? App::add_term_meta( $term->id, 'order', $params[ 'order' ] ) : null;

		return $term->id;
	}

	public static function get_all_about_sections() {
		$terms = DB::table( 'terms' )
			->select( 'id' )
			->where( 'taxonomy', '=', 'about-section' )
			->groupBy( 'id' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $terms as $term )
			$about_sections[] = $term->id;

		return isset( $about_sections ) ? $about_sections : [];
	}

}