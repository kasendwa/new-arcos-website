<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;

use App\App;
use App\Models\Term;

class ResourceCategory {

	public $id;
	public $title;
	public $description;
	public $order;

	function __construct( $identifier = 0 ) {
		$term = App::get_term( $identifier );

		if ( $term !== null ) {
			$this->id = $term->id;
			$this->title = ( object ) [
				'en' => App::get_term_meta( $this->id, 'title_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'title_fr', '' )
			];
			$this->description = ( object ) [
				'en' => App::get_term_meta( $this->id, 'description_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'description_fr', '' )
			];
            $this->order = ( int ) App::get_term_meta( $this->id, 'order', 0 );
		}
	}

	public function delete() {
		Term::find( $this->id )->delete();
	}

	public static function get_resource_category( $identifier, $lang = null ) {
		$resource_category = new ResourceCategory( App::get_term( $identifier )->id );

		return $lang == null ?
			( object ) [
				'id' => $resource_category->id,
				'title' => $resource_category->title,
				'description' => $resource_category->description,
                'order' => $resource_category->order
			] :
			( object ) [
                'id' => $resource_category->id,
                'title' => $resource_category->title->{ $lang },
                'description' => $resource_category->description->{ $lang },
                'order' => $resource_category->order
			];
	}

	public static function add_resource_category( $params ) {
		$args = [];

		$args[ 'taxonomy' ] = 'resource-category';

		isset( $params[ 'title' ] ) && isset( $params[ 'title' ][ 'en' ] ) ? ( $args[ 'slug' ] = str_slug( $params[ 'title' ][ 'en' ], '-' ) ) : null;
		!isset( $args[ 'slug' ] ) ? ( $args[ 'slug' ] = time() . rand( 100, 999 ) ) : null;

		$counter = 1;

		while( App::get_term( $args[ 'slug' ] ) != null ) :

			$counter++;

			$segments = explode( '-', $args[ 'slug' ] );
			$suffix = array_pop( $segments );

			$args[ 'slug' ] = $counter > 2 ? str_replace( '-' . $suffix, '', $args[ 'slug' ] ) : $args[ 'slug' ];
			$args[ 'slug' ] = $args[ 'slug' ] . '-' . $counter;

		endwhile;

		$term = App::add_term( $args );

		foreach( [ 'title', 'description' ] as $entry )

			if ( isset( $params[ $entry ] ) && is_array( $params[ $entry ] ) )

				foreach( $params[ $entry ] as $language => $value )
					App::add_term_meta( $term->id, $entry . '_' . $language, $value );

        isset( $params[ 'order' ] ) ? App::add_term_meta( $term->id, 'order', $params[ 'order' ] ) : null;

		return $term->id;
	}

    public function get_resources() {
        $terms = DB::table( 'terms' )
            ->select( 'terms.id' )
            ->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
            ->where( 'terms.taxonomy', '=', 'resource' )
            ->where( 'term_meta.key', '=', 'category' )
            ->where( 'term_meta.value', '=', $this->id )
            ->groupBy( 'terms.id' )
            ->orderBy( 'terms.created_at', 'desc' )
            ->get();

        foreach( $terms as $term )
            $resources[] = Resource::get_resource( $term->id );

        return isset( $resources ) ? $resources : [];
    }

	public static function get_resource_categories( $per_page = 15, $page = 1 ) {
		$terms = DB::table( 'terms' )
			->select( 'terms.id' )
			->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
			->where( 'terms.taxonomy', '=', 'resource-category' )
			->groupBy( 'terms.id' )
			->orderBy( 'terms.slug', 'asc' )
			->offset( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $terms as $term )
			$resource_categories[] = $term->id;

		return isset( $resource_categories ) ? $resource_categories : [];
	}

	public static function get_all_resource_categories() {
		$terms = DB::table( 'terms' )
			->select( 'id' )
			->where( 'taxonomy', '=', 'resource-category' )
			->groupBy( 'id' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $terms as $term )
            $resource_categories[] = $term->id;

		return isset( $resource_categories ) ? $resource_categories : [];
	}

}