<?php

namespace App\Entities;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

use App\App;
use App\Models\Term;

class TeamMember {

	public $id;
	public $slug;
	public $name;
	public $country;
	public $avatar;
	public $position;
	public $biography;
	public $order;
	public $team_category;
	public $permalink;

	function __construct( $identifier = 0 ) {
		$term = App::get_term( $identifier );

		if ( $term !== null ) {
			$this->id = $term->id;
			$this->slug = $term->slug;
			$this->name = App::get_term_meta( $this->id, 'name', '' );
			$this->country = ( object ) [
				'en' => App::get_term_meta( $this->id, 'country_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'country_fr', '' )
			];
			$this->avatar = $this->get_avatars();
			$this->position = ( object ) [
				'en' => App::get_term_meta( $this->id, 'position_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'position_fr', '' )
			];
			$this->biography = ( object ) [
				'en' => App::get_term_meta( $this->id, 'biography_en', '' ),
				'fr' => App::get_term_meta( $this->id, 'biography_fr', '' )
			];
			$this->order = App::get_term_meta( $this->id, 'order', 0 );
			$this->team_category = App::get_term_meta( $this->id, 'team_category', 0 );
			$this->permalink = $this->get_permalink();
		}
	}

	private function get_permalink() {
		return url( '/' . App::get_current_language() . '/' . App::get_option( 'taxonomies_team-member_rewrite_site_base', 'category' ) . '/' . $this->slug );
	}

	private function get_avatars() {
		$thumbnails = [];

		$thumbnail_id = ( int ) App::get_term_meta( $this->id, 'avatar', null );

		if ( $thumbnail_id != 0 ) :

			$attachment = Attachment::get_attachment( $thumbnail_id );

			$path_segments = explode( 'uploads', $attachment->url );

			$path = 'uploads' . end( $path_segments );

			foreach( App::get_image_sizes() as $size => $params ) :

				$filename = str_replace( '.' . File::extension( $path ), '', $path ) . '-' . $params->width . 'x' . $params->height . '.' . File::extension( $path );

				File::exists( $filename ) ? $thumbnails[ str_replace( '-', '_', $size ) ] = url( $filename ) : null;

			endforeach;

		endif;

		return ( object ) $thumbnails;
	}

	public function delete() {
		$avatar_id = App::get_term_meta( $this->id, 'avatar', 0 );

		if ( $avatar_id > 0 ) :

			App::delete_term_meta( $this->id, 'avatar' );

			$avatar = new Attachment( $avatar_id );

			$avatar->id != null ? $avatar->delete() : null;

		endif;

		Term::find( $this->id )->delete();
	}

	public static function get_team_member( $identifier, $language = null ) {
		$team_member = new TeamMember( App::get_term( $identifier )->id );

		return $language == null ?
			( object ) [
				'id' => $team_member->id,
				'name' => $team_member->name,
				'country' => $team_member->country,
				'avatar' => $team_member->avatar,
				'position' => $team_member->position,
				'biography' => $team_member->biography,
				'order' => $team_member->order,
				'team_category' => $team_member->team_category,
				'permalink' => $team_member->permalink
			] :
			( object ) [
				'id' => $team_member->id,
				'name' => $team_member->name,
				'country' => $team_member->country->{ $language },
				'avatar' => $team_member->avatar,
				'position' => $team_member->position->{ $language },
				'biography' => $team_member->biography->{ $language },
				'order' => $team_member->order,
				'team_category' => $team_member->team_category,
				'permalink' => $team_member->permalink
			];
	}

	public static function get_team_members( $per_page = 15, $page = 1 ) {
		$terms = DB::table( 'terms' )
			->select( 'terms.id' )
			->leftJoin( 'term_meta', 'terms.id', '=', 'term_meta.term_id' )
			->where( 'terms.taxonomy', '=', 'team-member' )
			->groupBy( 'terms.id' )
			->orderBy( 'terms.slug', 'asc' )
			->offset( ( $page - 1 ) * $per_page )
			->limit( $per_page )
			->get();

		foreach( $terms as $term )
			$team_members[] = $term->id;

		return isset( $team_members ) ? $team_members : [];
	}

	public static function add_team_member( $params ) {
		$args = [];

		$args[ 'taxonomy' ] = 'team-member';

		isset( $params[ 'name' ] ) ? ( $args[ 'slug' ] = str_slug( $params[ 'name' ], '-' ) ) : null;
		!isset( $args[ 'slug' ] ) ? ( $args[ 'slug' ] = time() . rand( 100, 999 ) ) : null;

		$term = App::add_term( $args );

		if ( isset( $params[ 'avatar' ] ) && is_string( $params[ 'avatar' ] ) )
			$params[ 'avatar' ] = App::upload_media_from_path( $params[ 'avatar' ] );

		isset( $params[ 'avatar' ] ) ? App::add_term_meta( $term->id, 'avatar', $params[ 'avatar' ] ) : null;
		isset( $params[ 'name' ] ) ? App::add_term_meta( $term->id, 'name', $params[ 'name' ] ) : null;

		foreach( [ 'country', 'position', 'biography' ] as $entry )

			if ( isset( $params[ $entry ] ) && is_array( $params[ $entry ] ) )

				foreach( $params[ $entry ] as $language => $value )
					App::add_term_meta( $term->id, $entry . '_' . $language, $value );

		isset( $params[ 'team_category' ] ) ? App::add_term_meta( $term->id, 'team_category', $params[ 'team_category' ] ) : null;

		isset( $params[ 'order' ] ) ? App::add_term_meta( $term->id, 'order', $params[ 'order' ] ) : null;

		return $term->id;
	}

	public static function get_all_team_members() {
		$terms = DB::table( 'terms' )
			->select( 'id' )
			->where( 'taxonomy', '=', 'team-member' )
			->groupBy( 'id' )
			->orderBy( 'id', 'desc' )
			->get();

		foreach( $terms as $term )
			$team_members[] = $term->id;

		return isset( $team_members ) ? $team_members : [];
	}

}